﻿using Nop.Core.Domain.Customers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Nop.Core.Domain.PushNotifications
{    
    [XmlRoot("PushNotification")]
    public class PushNotification : BaseEntity
    {

        #region Public properties        

        /// <summary>
        /// Gets or sets customer id
        /// </summary>
        [XmlElementAttribute("user")]
        public int CustomerId { get; set; }

        /// <summary>
        /// Gets or sets badge
        /// </summary>
        [XmlElementAttribute("badge")]
        public int Badge { get; set; }

        /// <summary>
        /// Gets or sets short title
        /// </summary>
        [XmlElementAttribute("shorttitle")]
        public string ShortTitle { get; set; }

        /// <summary>
        /// Gets or sets long title
        /// </summary>
        [XmlElementAttribute("longtitle")]
        public string LongTitle { get; set; }

        /// <summary>
        /// Gets or sets contents
        /// </summary>
        [XmlElementAttribute("contents")]
        public string Contents { get; set; }

        /// <summary>
        /// Gets or sets language
        /// </summary>
        [XmlElementAttribute("language")]
        public string Language { get; set; }

        /// <summary>
        /// Gets or sets transaction ID
        /// </summary>
        [XmlElementAttribute("transactionid")]
        public string TransactionId { get; set; }

        /// <summary>
        /// Gets or sets created datetime
        /// </summary>        
        [XmlIgnore]
        public DateTime CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets updated datetime
        /// </summary>
        [XmlIgnore]
        public DateTime? UpdatedOnUtc { get; set; }

        #endregion

        #region Navigation properties

        /// <summary>
        /// Gets or sets order
        /// </summary>
        //public virtual Customer Customer { get; set; }

        #endregion

    }

    public class PushAPIResponse
    {
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public List<TransactionID> TransactionIDs { get; set; }
    }
    public class TransactionID
    {
        public string UserId { get; set; }
        public string TransactionId { get; set; }
    }
    public enum PushLanguage
    {
        En = 2,
        Ar = 1
    }
}
