﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Core.Domain.PushNotifications
{
    public enum OrderPushNotificationType
    {
        /// <summary>
        /// Order completion push notification
        /// </summary>
        ORDER_COMPLETION = 1,

        /// <summary>
        /// Order cancellation push notification
        /// </summary>
        ORDER_CANCELLATION = 2
    }
}
