using Nop.Core.Domain.Localization;

namespace Nop.Core.Domain.Shipping
{
    /// <summary>
    /// Represents a shipment
    /// </summary>
    public partial class Warehouse : BaseEntity, ILocalizedEntity
    {
        /// <summary>
        /// Gets or sets the warehouse name
        /// </summary>
        public string Name { get; set; }

        // For Almailem: Adding 6 new columns:- StoreCode, StoreTimings, Features, Latitude, Longitude, Governorate.

        public string StoreCode { get; set; }
        public string StoreTimings { get; set; }
        public string Features { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Governorate { get; set; }

        public bool IsWarehouse { get; set; }
        

        /// <summary>
        /// Gets or sets the admin comment
        /// </summary>
        public string AdminComment { get; set; }

        /// <summary>
        /// Gets or sets the address identifier of the warehouse
        /// </summary>
        public int AddressId { get; set; }
    }
}