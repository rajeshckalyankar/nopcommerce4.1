﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Core.Domain
{
    public static class ShippingMethodName
    {
        public static string MOBILE_VAN = "Mobile Van";
        public static string VISIT_OUR_STORE = "Visit Our Store";
    }
}
