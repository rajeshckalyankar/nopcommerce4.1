﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Core.Domain.Payments
{
    public class MasterCardPaymentTransaction : BaseEntity
    {
        #region Public properties

        public int PaymentTransactionId { get; set; }

        public string Receipt { get; set; }

        public string PaymentId { get; set; }     

        public string TransactionId { get; set; }        

        public string OrderId { get; set; }

        public Double? OrderAmount { get; set; }       

        public string OrderCurrency { get; set; }

        public string OrderDescription { get; set; }

        public string Result { get; set; } // SUCCESS

        public string Status { get; set; } // CAPTURED

        public string AcquirerCode { get; set; } // 00

        public string AcquirerMessage { get; set; } //Approved 

        public string GatewayCode { get; set; } // APPROVED

        public string AuthorizationCode { get; set; }

        public string ErrorCode { get; set; }

        public string ErrorText { get; set; }

        public DateTime TransactionCreationTime { get; set; }

        public DateTime? CreatedOnUtc { get; set; }
        



        #endregion

        #region Navigation properties

        /// <summary>
        /// To do the mapping with PaymentTransaction Table
        /// </summary>
        public virtual PaymentTransaction PaymentTransaction { get; set; }

        #endregion
    }
}
