namespace Nop.Core.Domain.Payments
{
    /// <summary>
    /// Represents a payment status enumeration
    /// </summary>
    public enum PaymentStatus
    {
        /// <summary>
        /// Pending
        /// </summary>
        Pending = 10,

        /// <summary>
        /// Authorized
        /// </summary>
        Authorized = 20,

        /// <summary>
        /// Paid
        /// </summary>
        Paid = 30,

        /// <summary>
        /// Partially Refunded
        /// </summary>
        PartiallyRefunded = 35,

        /// <summary>
        /// Refunded
        /// </summary>
        Refunded = 40,

        /// <summary>
        /// Voided
        /// </summary>
        Voided = 50,

        Failed = 60
    }

    /// <summary>
    /// Represents a payment transaction status enumeration
    /// </summary>
    public enum PaymentTransactionStatus
    {
        /// <summary>
        /// Payment transaction is initiated
        /// </summary>
        INITIATED = 10,

        /// <summary>
        /// Payment transaction was successful
        /// </summary>
        SUCCESS = 20,

        /// <summary>
        /// Payment transaction got failed
        /// </summary>
        FAILED = 30
    }

    public static class KNETTransactionResult
    {
        /// <summary>
        /// KNET payment is successful
        /// </summary>
        public const string CAPTURED = "CAPTURED";

        /// <summary>
        /// KNET payment is not successful because of incorrect information
        /// </summary>
        public const string NOT_CAPTURED = "NOT CAPTURED";

        /// <summary>
        /// KNET has denied the transaction
        /// </summary>
        public const string DENIED_BY_RISK = "DENIED BY RISK";

        /// <summary>
        /// Request timeout by KNET
        /// </summary>
        public const string HOST_TIMEOUT = "HOST TIMEOUT";

        /// <summary>
        /// User has cancelled the transaction
        /// </summary>
        public const string CANCELED = "CANCELED";
    }

    public enum KNETTransactionResultEnum
    {
        /// <summary>
        /// KNET payment is successful
        /// </summary>
        CAPTURED = 0,
        /// <summary>
        /// KNET payment is not successful because of incorrect information
        /// </summary>
        NOT_CAPTURED = 1,
        /// <summary>
        /// KNET has denied the transaction
        /// </summary>
        DENIED_BY_RISK = 2,
        /// <summary>
        /// Request timeout by KNET
        /// </summary>
        HOST_TIMEOUT = 3,
        /// <summary>
        /// User has cancelled the transaction
        /// </summary>
        CANCELED = 4,
        /// <summary>
        /// Default value for enum
        /// </summary>
        DEFAULT = 5
    }

    public static class MasterCardTransactionResult
    {
        /// <summary>
        /// MasterCard payment is successful
        /// </summary>
        public const string CAPTURED = "CAPTURED";

        /// <summary>
        /// MasterCard payment is not successful because of incorrect information
        /// </summary>
        public const string NOT_CAPTURED = "NOT CAPTURED";

        /// <summary>
        /// MasterCard has denied the transaction
        /// </summary>
        public const string DENIED_BY_RISK = "DENIED BY RISK";

        /// <summary>
        /// Request timeout by MasterCard
        /// </summary>
        public const string HOST_TIMEOUT = "TIME OUT";

        /// <summary>
        /// User has cancelled the transaction
        /// </summary>
        public const string CANCELED = "CANCELED";
    }

    public enum MasterCardTransactionResultEnum
    {
        /// <summary>
        /// MasterCard payment is successful
        /// </summary>
        CAPTURED = 0,
        /// <summary>
        /// MasterCard payment is not successful because of incorrect information
        /// </summary>
        NOT_CAPTURED = 1,
        /// <summary>
        /// MasterCard has denied the transaction
        /// </summary>
        DENIED_BY_RISK = 2,
        /// <summary>
        /// Request timeout by MasterCard
        /// </summary>
        TIME_OUT = 3,
        /// <summary>
        /// User has cancelled the transaction
        /// </summary>
        CANCELED = 4,
        /// <summary>
        /// Default value for enum
        /// </summary>
        DEFAULT = 5
    }
}
