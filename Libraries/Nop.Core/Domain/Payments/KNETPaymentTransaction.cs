﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Core.Domain.Payments
{
    public class KNETPaymentTransaction : BaseEntity
    {
        #region Public properties

        public string PaymentId { get; set; }

        public int PaymentTransactionId { get; set; }

        public string TranId { get; set; }

        public string Auth { get; set; }

        public string Ref { get; set; }

        public string Result { get; set; }

        public string UDF1 { get; set; }

        public string UDF2 { get; set; }

        public string UDF3 { get; set; }

        public string UDF4 { get; set; }

        public string UDF5 { get; set; }
                
        public string PostDate { get; set; }

        public string Avr { get; set; }

        public string AuthRespCode { get; set; }

        public string ErrorCode { get; set; }

        public string ErrorText { get; set; }

        public DateTime? CreatedOnUtc { get; set; }

        public Double? Amt { get; set; }

        #endregion

        #region Navigation properties

        public virtual PaymentTransaction PaymentTransaction { get; set; }

        #endregion

    }
}
