﻿using Nop.Core.Domain.Orders;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Nop.Core.Domain.Payments
{
    public partial class PaymentTransaction : BaseEntity
    {
        #region Public properties

        /// <summary>
        /// TrackID of payment transaction
        /// </summary>
        public string TrackId { get; set; }

        /// <summary>
        /// OrderID with which payment is associated
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// Status of payment transaction
        /// </summary>
        public int TransactionStatus { get; set; }

        /// <summary>
        /// Date and time of creation of payment transaction
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }

        #endregion

        #region Navigation properties

        /// <summary>
        /// Gets or sets order
        /// </summary>
        public virtual Order Order { get; set; }

        #endregion

        #region Custom properties

        public PaymentTransactionStatus PaymentTransactionStatus
        {
            get => (PaymentTransactionStatus)TransactionStatus;
            set=> TransactionStatus = (int)value;
        }
        #endregion
    }
}
