﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Core.Domain.Customers
{
    public enum CustomerRoleID
    {
        /// <summary>
        /// Administrator
        /// </summary>
        ADMINISTRATOR = 1,

        /// <summary>
        /// Forum moderator
        /// </summary>
        FORUM_MODERATOR = 2,

        /// <summary>
        /// Registered user
        /// </summary>
        REGISTERED = 3,

        /// <summary>
        /// Guest user
        /// </summary>
        GUEST = 4,

        /// <summary>
        /// Vendor
        /// </summary>
        VENDOR = 5
    }
}
