﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.PushNotifications;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Data.Mapping.PushNotifications
{
    public partial class PushNotificationMap : NopEntityTypeConfiguration<PushNotification>
    {
        public override void Configure(EntityTypeBuilder<PushNotification> builder)
        {
            builder.ToTable(nameof(PushNotification));
            builder.HasKey(pushNotification => pushNotification.Id);

            builder.Property(pushNotification => pushNotification.Badge).HasColumnType("int");
            builder.Property(pushNotification => pushNotification.Contents).HasColumnType("varchar(max)").IsRequired();
            builder.Property(pushNotification => pushNotification.Language).HasColumnType("varchar(10)").IsRequired();
            builder.Property(pushNotification => pushNotification.LongTitle).HasColumnType("varchar(max)");
            builder.Property(pushNotification => pushNotification.ShortTitle).HasColumnType("varchar(100)");
            builder.Property(pushNotification => pushNotification.TransactionId).HasColumnType("varchar(200)");
            builder.Property(pushNotification => pushNotification.CreatedOnUtc).HasColumnType("datetime2(7)").IsRequired();
            builder.Property(pushNotification => pushNotification.UpdatedOnUtc).HasColumnType("datetime2(7)");

            //builder.HasOne(pushNotification => pushNotification.Customer)
            //    .WithMany()
            //    .HasForeignKey(pushNotification => pushNotification.CustomerId)
            //    .IsRequired();

            base.Configure(builder);
        }
    }
}
