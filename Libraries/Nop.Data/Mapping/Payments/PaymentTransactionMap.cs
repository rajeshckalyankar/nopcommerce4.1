﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Data.Mapping.Payments
{
    public partial class PaymentTransactionMap : NopEntityTypeConfiguration<PaymentTransaction>
    {
        public override void ApplyConfiguration(ModelBuilder builder)
        {
            builder.Entity<PaymentTransaction>()
                .HasIndex(paymentTransaction => paymentTransaction.TrackId)
                .IsUnique();

            base.ApplyConfiguration(builder);
        }

        public override void Configure(EntityTypeBuilder<PaymentTransaction> builder)
        {
            builder.ToTable(nameof(PaymentTransaction));
            builder.HasKey(paymentTransaction => paymentTransaction.Id);

            builder.Property(paymentTransaction => paymentTransaction.TrackId).HasColumnType("varchar(100)").IsRequired();
            builder.Property(paymentTransaction => paymentTransaction.TransactionStatus).HasColumnType("int").IsRequired();
            builder.Property(paymentTransaction => paymentTransaction.CreatedOnUtc).HasColumnType("datetime2(7)").IsRequired();

            builder.HasOne(paymentTransaction => paymentTransaction.Order)
                .WithMany()
                .HasForeignKey(paymentTransaction => paymentTransaction.OrderId)
                .IsRequired();

            builder.Ignore(paymentTransaction => paymentTransaction.PaymentTransactionStatus);
                       
            base.Configure(builder);
        }
    }
}
