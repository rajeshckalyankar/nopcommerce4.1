﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Payments;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Data.Mapping.Payments
{
    public partial class KNETPaymentTransactionMap : NopEntityTypeConfiguration<KNETPaymentTransaction>
    {
        
        public override void Configure(EntityTypeBuilder<KNETPaymentTransaction> builder)
        {
            builder.ToTable(nameof(KNETPaymentTransaction));
            builder.HasKey(oKNETPaymentTransaction => oKNETPaymentTransaction.Id);

            builder.Property(oKNETPaymentTransaction => oKNETPaymentTransaction.PaymentId).HasColumnType("varchar(100)");
            builder.Property(oKNETPaymentTransaction => oKNETPaymentTransaction.TranId).HasColumnType("varchar(100)");
            builder.Property(oKNETPaymentTransaction => oKNETPaymentTransaction.Auth).HasColumnType("varchar(100)");
            builder.Property(oKNETPaymentTransaction => oKNETPaymentTransaction.Ref).HasColumnType("varchar(100)");
            builder.Property(oKNETPaymentTransaction => oKNETPaymentTransaction.Result).HasColumnType("varchar(100)");
            builder.Property(oKNETPaymentTransaction => oKNETPaymentTransaction.UDF1).HasColumnType("varchar(100)");
            builder.Property(oKNETPaymentTransaction => oKNETPaymentTransaction.UDF2).HasColumnType("varchar(100)");
            builder.Property(oKNETPaymentTransaction => oKNETPaymentTransaction.UDF3).HasColumnType("varchar(100)");
            builder.Property(oKNETPaymentTransaction => oKNETPaymentTransaction.UDF4).HasColumnType("varchar(100)");
            builder.Property(oKNETPaymentTransaction => oKNETPaymentTransaction.UDF5).HasColumnType("varchar(100)");
            builder.Property(oKNETPaymentTransaction => oKNETPaymentTransaction.PostDate).HasColumnType("varchar(30)");
            builder.Property(oKNETPaymentTransaction => oKNETPaymentTransaction.Avr).HasColumnType("varchar(100)");
            builder.Property(oKNETPaymentTransaction => oKNETPaymentTransaction.AuthRespCode).HasColumnType("varchar(100)");
            builder.Property(oKNETPaymentTransaction => oKNETPaymentTransaction.AuthRespCode).HasColumnType("varchar(100)");
            builder.Property(oKNETPaymentTransaction => oKNETPaymentTransaction.ErrorText).HasColumnType("varchar(100)");
            builder.Property(oKNETPaymentTransaction => oKNETPaymentTransaction.CreatedOnUtc).HasColumnType("datetime2(7)").IsRequired();
            builder.Property(oKNETPaymentTransaction => oKNETPaymentTransaction.Amt).HasColumnType("decimal(18,4)");
            
            builder.HasOne(oKNETPaymentTransaction => oKNETPaymentTransaction.PaymentTransaction)
                .WithMany()
                .HasForeignKey(oKNETPaymentTransaction => oKNETPaymentTransaction.PaymentTransactionId)
                .IsRequired();

            base.Configure(builder);
        }
    }
}
