﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Payments;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Data.Mapping.Payments
{
    public class MasterCardPaymentTransactionMap : NopEntityTypeConfiguration<MasterCardPaymentTransaction>
    {
        public override void Configure(EntityTypeBuilder<MasterCardPaymentTransaction> builder)
        {
            builder.ToTable(nameof(MasterCardPaymentTransaction));
            builder.HasKey(oMasterCardPaymentTransaction => oMasterCardPaymentTransaction.Id);

            builder.Property(oMasterCardPaymentTransaction => oMasterCardPaymentTransaction.Receipt).HasColumnType("varchar(100)");
            builder.Property(oMasterCardPaymentTransaction => oMasterCardPaymentTransaction.PaymentId).HasColumnType("varchar(100)");
            builder.Property(oMasterCardPaymentTransaction => oMasterCardPaymentTransaction.TransactionId).HasColumnType("varchar(100)");
            builder.Property(oMasterCardPaymentTransaction => oMasterCardPaymentTransaction.OrderId).HasColumnType("varchar(100)");
            builder.Property(oMasterCardPaymentTransaction => oMasterCardPaymentTransaction.OrderAmount).HasColumnType("decimal(18,4)");
            builder.Property(oMasterCardPaymentTransaction => oMasterCardPaymentTransaction.OrderCurrency).HasColumnType("varchar(10)");
            builder.Property(oMasterCardPaymentTransaction => oMasterCardPaymentTransaction.OrderDescription).HasColumnType("varchar(100)");
            builder.Property(oMasterCardPaymentTransaction => oMasterCardPaymentTransaction.Result).HasColumnType("varchar(20)");
            builder.Property(oMasterCardPaymentTransaction => oMasterCardPaymentTransaction.Status).HasColumnType("varchar(20)");
            builder.Property(oMasterCardPaymentTransaction => oMasterCardPaymentTransaction.AcquirerCode).HasColumnType("varchar(10)");
            builder.Property(oMasterCardPaymentTransaction => oMasterCardPaymentTransaction.AcquirerMessage).HasColumnType("varchar(100)");
            builder.Property(oMasterCardPaymentTransaction => oMasterCardPaymentTransaction.GatewayCode).HasColumnType("varchar(30)");
            builder.Property(oMasterCardPaymentTransaction => oMasterCardPaymentTransaction.AuthorizationCode).HasColumnType("varchar(100)");
            builder.Property(oMasterCardPaymentTransaction => oMasterCardPaymentTransaction.ErrorCode).HasColumnType("varchar(10)");
            builder.Property(oMasterCardPaymentTransaction => oMasterCardPaymentTransaction.ErrorText).HasColumnType("varchar(1000)");
            builder.Property(oMasterCardPaymentTransaction => oMasterCardPaymentTransaction.TransactionCreationTime).HasColumnType("datetime2(7)");
            builder.Property(oMasterCardPaymentTransaction => oMasterCardPaymentTransaction.CreatedOnUtc).HasColumnType("datetime2(7)").IsRequired();

            builder.HasOne(oMasterCardPaymentTransaction => oMasterCardPaymentTransaction.PaymentTransaction)
                .WithMany()
                .HasForeignKey(oMasterCardPaymentTransaction => oMasterCardPaymentTransaction.PaymentTransactionId)
                .IsRequired();

            base.Configure(builder);
        }
    }
}
