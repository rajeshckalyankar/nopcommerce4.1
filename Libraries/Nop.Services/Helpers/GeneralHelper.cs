﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Services.Helpers
{
    public static class GeneralHelper
    {
        public static string GenerateTrackId(string Prefix = null, string Suffix = null, bool UseGuid = false)
        {
            return (string.IsNullOrEmpty(Prefix) ? "" : (Prefix + "_"))
                + DateTime.UtcNow.ToString("yyyyMMddHHmmssfff")
                + (UseGuid ? ("_" + Guid.NewGuid().ToString()) : "")
                + (string.IsNullOrEmpty(Suffix) ? "" : ("_" + Suffix));
        }

        public static string CreateTransactionId()
        {
            string transactionId = string.Empty;

            transactionId = DateTime.Now.ToString("ddMMyyyyHmmss") + "_" + Guid.NewGuid().ToString();

            return transactionId;
        }
    }
}
