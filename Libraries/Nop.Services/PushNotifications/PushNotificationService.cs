﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using Nop.Core.Data;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.PushNotifications;
using Nop.Services.Events;
using Nop.Services.Logging;

namespace Nop.Services.PushNotifications
{
    public class PushNotificationService : IPushNotificationService
    {
        #region Fields

        private readonly IEventPublisher _eventPublisher;
        private readonly IRepository<PushNotification> _pushNotificationRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly ILogger _logger;

        #endregion

        #region Ctor

        public PushNotificationService
            (
                IEventPublisher eventPublisher, 
                IRepository<PushNotification> pushNotificationRepository,
                IHostingEnvironment hostingEnvironment,
                ILogger logger
            )
        {
            _eventPublisher = eventPublisher;
            _pushNotificationRepository = pushNotificationRepository;
            _hostingEnvironment = hostingEnvironment;
            _logger = logger;
        }

        #endregion

        public virtual void InsertPushNotification(PushNotification pushNotification)
        {
            if (pushNotification == null)
                throw new ArgumentNullException(nameof(pushNotification));

            pushNotification.CreatedOnUtc = DateTime.UtcNow;

            _pushNotificationRepository.Insert(pushNotification);
            _logger.Information("Inserted Push Notification data in DB.");
            //event notification
            _eventPublisher.EntityInserted(pushNotification);
        }

        public virtual void UpdatePushNotification(List<PushNotification> lstPushNotifications, List<TransactionID> TransactionIDs)
        {
            _logger.Information("Updating TransactionId for Push Notification data in DB.");

            if (lstPushNotifications == null)
                throw new ArgumentNullException(nameof(lstPushNotifications));

            if (TransactionIDs == null)
                throw new ArgumentNullException(nameof(TransactionIDs));

            foreach (TransactionID _transactionID in TransactionIDs)
            {
                foreach (PushNotification _pushNotification in lstPushNotifications)
                {
                    if (_pushNotification.Id == Convert.ToInt32(_transactionID.UserId) && _pushNotification.TransactionId == null)
                    {
                        _logger.Information("In if condition: _pushNotification.Id = " + Convert.ToString(_pushNotification.Id) + " & _transactionID.UserId = " + _transactionID.UserId);

                        _pushNotification.TransactionId = _transactionID.TransactionId;
                        _pushNotification.UpdatedOnUtc = DateTime.UtcNow;

                        //update table - PushNotification.
                        _pushNotificationRepository.Update(_pushNotification);
                        //event notification
                        _eventPublisher.EntityUpdated(_pushNotification);
                        
                        _logger.Information("Id = " + Convert.ToString(_pushNotification.Id) + " & TransactionId = " + _pushNotification.TransactionId);

                    }
                    
                }
            }           
            _logger.Information("Updated TransactionId for Push Notification data in DB.");            
        }

        public virtual void CreateXMLRowDataForPush(PushNotification pushNotification, ref StringBuilder sb)
        {
            var myStringWriter = new System.IO.StringWriter();
            XmlSerializer serializer = new XmlSerializer(typeof(PushNotification));
            serializer.Serialize(myStringWriter, pushNotification);
            string rowData = Convert.ToString(myStringWriter);
            rowData = rowData.Replace("<Id>", "<content_id>");
            rowData = rowData.Replace("</Id>", "</content_id>");
            string rowData1 = rowData.Remove(0, rowData.IndexOf("<content_id>")); string rowData2 = rowData1.Replace("</PushNotification>", "");
            sb.Append("<row>");
            sb.Append(rowData2);
            sb.Append("</row>");
        }

        public virtual PushAPIResponse CallPushNotificationService(string[] xmlData)
        {
            XmlDocument xmlDocument = new XmlDocument();
            string contentRootPath = _hostingEnvironment.ContentRootPath;
            string xmlPNConfigurationFilePath = contentRootPath + "/App_Data/Push.InstantFetchService.xml";

            _logger.Information("Read PN config xml file.");

            xmlDocument.Load(xmlPNConfigurationFilePath);
            XmlElement root = xmlDocument.DocumentElement;
            XmlNodeList nodes = root.ChildNodes[0].ChildNodes;

            string uri = nodes.Cast<XmlNode>().FirstOrDefault(x => x.Name == "URI").InnerText;
            string apiUser = nodes.Cast<XmlNode>().FirstOrDefault(x => x.Name == "ApiUser").InnerText;
            string apiPassword = nodes.Cast<XmlNode>().FirstOrDefault(x => x.Name == "ApiPassword").InnerText;
            string serviceId = nodes.Cast<XmlNode>().FirstOrDefault(x => x.Name == "ServiceId").InnerText;
            string scheduleTime = nodes.Cast<XmlNode>().FirstOrDefault(x => x.Name == "ScheduleTime").InnerText;
            string isInstant = nodes.Cast<XmlNode>().FirstOrDefault(x => x.Name == "IsInstant").InnerText;

            string output = string.Empty;
            PushAPIResponse pushResponse = null;
            HttpWebRequest request = WebRequest.Create(uri) as HttpWebRequest;

            request.Credentials = CredentialCache.DefaultCredentials;
            request.Accept = "application/json";
            request.ContentType = "application/json";
            request.Method = "POST";

            _logger.Information("Create input object for PN service.");
            object input = new
            {
                ApiUser = apiUser,
                ApiPassword = apiPassword,
                ServiceId = serviceId,
                ScheduleTime = scheduleTime,
                IsInstant = isInstant,
                XmlDataList = xmlData
            };

            string inputJson = JsonConvert.SerializeObject(input);

            byte[] bytes = Encoding.UTF8.GetBytes(inputJson);

            try
            {
                using (Stream stream = request.GetRequestStream())
                {
                    //obj.WriteObject(stream, input);
                    stream.Write(bytes, 0, bytes.Length);
                    stream.Close();
                }

                _logger.Information("Start: Call PN WCF service..");
                using (HttpWebResponse httpResponse = (HttpWebResponse)request.GetResponse())
                {
                    using (Stream stream = httpResponse.GetResponseStream())
                    {
                        output = (new StreamReader(stream)).ReadToEnd();
                        pushResponse = JsonConvert.DeserializeObject<PushAPIResponse>(output);
                    }
                }
                _logger.Information("End: Call PN WCF service..");
            }
            catch (Exception ex)
            {
                _logger.Error("An error occured in Push Notification Service processing", ex);
            }
            return pushResponse;
        }
    }
}
