﻿using System;
using System.Collections.Generic;
using System.Text;
using Nop.Core.Domain.PushNotifications;

namespace Nop.Services.PushNotifications
{
    /// <summary>
    /// Push notification sender
    /// </summary>
    public partial class PushNotificationSender : IPushNotificationSender
    {
        #region Properties

        private readonly IPushNotificationService _pushNotificationService;
        
        #endregion

        #region Ctor

        public PushNotificationSender(IPushNotificationService pushNotificationService)
        {
            _pushNotificationService = pushNotificationService;
        }

        #endregion

        public virtual void SendPushNotification(PushNotification pushNotification)
        {
            try
            {
                //Implement push notification API call and update pushNotification object

                //_pushNotificationService.UpdatePushNotification(pushNotification);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
