﻿using Nop.Core.Domain.PushNotifications;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Services.PushNotifications
{
    /// <summary>
    /// Push notification sender
    /// </summary>
    public partial interface IPushNotificationSender
    {
        /// <summary>
        /// Sends a push notification
        /// </summary>
        /// <param name="pushNotification"></param>
        void SendPushNotification(PushNotification pushNotification);
    }
}
