﻿using Nop.Core.Domain.Orders;
using Nop.Core.Domain.PushNotifications;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Services.PushNotifications
{
    public partial interface IPushNotificationService
    {
        /// <summary>
        /// Inserts a push notification
        /// </summary>
        /// <param name="pushNotification"></param>
        void InsertPushNotification(PushNotification pushNotification);

        /// <summary>
        /// Updates a push notification
        /// </summary>
        /// <param name="queuedEmail">Queued email</param>
        void UpdatePushNotification(List<PushNotification> lstPushNotifications, List<TransactionID> TransactionIDs);
        void CreateXMLRowDataForPush(PushNotification pushNotification, ref StringBuilder sb);
        PushAPIResponse CallPushNotificationService(string[] xmlData);
    }
}
