﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Vendors;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.DataStructures;
using Nop.Services.Stores;

namespace Nop.Plugin.Api.Services
{
    public class ProductApiService : IProductApiService
    {
        private readonly IStoreMappingService _storeMappingService;
        private readonly IRepository<Product> _productRepository;
        private readonly IRepository<ProductCategory> _productCategoryMappingRepository;
        private readonly IRepository<Vendor> _vendorRepository; 
        private readonly IRepository<SpecificationAttributeOption> _specificationAttributeOptionRepository;

        public ProductApiService(IRepository<Product> productRepository,
            IRepository<ProductCategory> productCategoryMappingRepository,
            IRepository<Vendor> vendorRepository,
            IStoreMappingService storeMappingService,
            IRepository<SpecificationAttributeOption> specificationAttributeOptionRepository
            )
        {
            _productRepository = productRepository;
            _productCategoryMappingRepository = productCategoryMappingRepository;
            _vendorRepository = vendorRepository;
            _storeMappingService = storeMappingService;
            _specificationAttributeOptionRepository = specificationAttributeOptionRepository;
        }

        enum ProductAttribute
        {        
            Color = 0,
            Width,
            AspectRatio,
            RimSize,
            LoadIndex,
            SpeedIndex,
            RunFlat,
            VehicleType,
            Temperature
        }
        
        public IList<Product> GetTiresByWAR(string widthValue, string aspectRatioValue, string rimSizeValue)
        {
            var query = GetProductsQuery(publishedStatus:true);
            query = query.OrderBy(product => product.Id);

            IList<int> idList = _specificationAttributeOptionRepository.Table
                                .Where(rec => (rec.Name == widthValue && rec.SpecificationAttributeId == (int)ProductAttribute.Width)
                                || (rec.Name == aspectRatioValue && rec.SpecificationAttributeId == (int)ProductAttribute.AspectRatio)
                                || (rec.Name == rimSizeValue && rec.SpecificationAttributeId == (int)ProductAttribute.RimSize))
                                .Select(filteredRec => filteredRec.Id)
                                .ToList();

            query = query.Where(c => idList.All(oID => c.ProductSpecificationAttributes.Select(sa => sa.SpecificationAttributeOptionId).Contains(oID))); //sa.SpecificationAttributeOptionId == 20));
            
            return new ApiList<Product>(query, 0, 10000);
        }

        public IList<Product> GetTiresByBrand(int BrandID)
        {
            var query = GetProductsQuery();
            query = query.OrderBy(product => product.Id);

            query = query.Where(oProduct => oProduct.ProductManufacturers.Where(oProductManufacturer => oProductManufacturer.ManufacturerId == BrandID).Any());

            //query = query.Where(c => (c.ProductManufacturers.Select(sa => sa.ManufacturerId = BrandID)); //sa.SpecificationAttributeOptionId == 20));
            //query = query.Where(c => BrandID.(oID => c.ProductManufacturers.Select(sa => sa.ManufacturerId = BrandID))); //sa.SpecificationAttributeOptionId == 20));

            return new ApiList<Product>(query, 0, 10000);
        }

        public IList<Product> GetProducts(IList<int> ids = null,
            DateTime? createdAtMin = null, DateTime? createdAtMax = null, DateTime? updatedAtMin = null, DateTime? updatedAtMax = null,
           int limit = Configurations.DefaultLimit, int page = Configurations.DefaultPageValue, int sinceId = Configurations.DefaultSinceId,
           int? categoryId = null, string vendorName = null, bool? publishedStatus = null)
        {
            var query = GetProductsQuery(createdAtMin, createdAtMax, updatedAtMin, updatedAtMax, vendorName, publishedStatus, ids, categoryId);

            return new ApiList<Product>(query, page - 1, limit);
        }
        
        public int GetProductsCount(DateTime? createdAtMin = null, DateTime? createdAtMax = null, 
            DateTime? updatedAtMin = null, DateTime? updatedAtMax = null, bool? publishedStatus = null, string vendorName = null, 
            int? categoryId = null)
        {
            var query = GetProductsQuery(createdAtMin, createdAtMax, updatedAtMin, updatedAtMax, vendorName,
                                         publishedStatus, categoryId: categoryId);

            return query.ToList().Count(p => _storeMappingService.Authorize(p));
        }

        public Product GetProductById(int productId)
        {
            if (productId == 0)
                return null;

            return _productRepository.Table.FirstOrDefault(product => product.Id == productId && !product.Deleted);
        }

        public Product GetProductByIdNoTracking(int productId)
        {
            if (productId == 0)
                return null;

            return _productRepository.Table.FirstOrDefault(product => product.Id == productId && !product.Deleted);
        }

        private IQueryable<Product> GetProductsQuery(DateTime? createdAtMin = null, DateTime? createdAtMax = null, 
            DateTime? updatedAtMin = null, DateTime? updatedAtMax = null, string vendorName = null, 
            bool? publishedStatus = null, IList<int> ids = null, int? categoryId = null)
            
        {
            var query = _productRepository.Table;

            if (ids != null && ids.Count > 0)
            {
                query = query.Where(c => ids.Contains(c.Id));
            }

            if (publishedStatus != null)
            {
                query = query.Where(c => c.Published == publishedStatus.Value);
            }

            // always return products that are not deleted!!!
            query = query.Where(c => !c.Deleted);

            if (createdAtMin != null)
            {
                query = query.Where(c => c.CreatedOnUtc > createdAtMin.Value);
            }

            if (createdAtMax != null)
            {
                query = query.Where(c => c.CreatedOnUtc < createdAtMax.Value);
            }

            if (updatedAtMin != null)
            {
               query = query.Where(c => c.UpdatedOnUtc > updatedAtMin.Value);
            }

            if (updatedAtMax != null)
            {
                query = query.Where(c => c.UpdatedOnUtc < updatedAtMax.Value);
            }

            if (!string.IsNullOrEmpty(vendorName))
            {
                query = from vendor in _vendorRepository.Table
                        join product in _productRepository.Table on vendor.Id equals product.VendorId
                        where vendor.Name == vendorName && !vendor.Deleted && vendor.Active
                        select product;
            }

            if (categoryId != null)
            {
                var categoryMappingsForProduct = from productCategoryMapping in _productCategoryMappingRepository.Table
                                                 where productCategoryMapping.CategoryId == categoryId
                                                 select productCategoryMapping;

                query = from product in query
                        join productCategoryMapping in categoryMappingsForProduct on product.Id equals productCategoryMapping.ProductId
                        select product;
            }
                        
            query = query.OrderBy(product => product.Id);

            return query;
        }
    }
}