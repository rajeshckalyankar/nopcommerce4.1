﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Core.Data;
using Nop.Core.Domain.Payments;
using Nop.Plugin.Api.Factories;
using Nop.Services.Events;

namespace Nop.Plugin.Api.Services
{
    public class PaymentApiService : IPaymentApiService
    {
        private readonly IRepository<PaymentTransaction> _paymentTransactionRepository;
        private readonly IRepository<KNETPaymentTransaction> _KNETPaymentTransactionRepository;
        private readonly IRepository<MasterCardPaymentTransaction> _MasterCardPaymentTransactionRepository;
        private readonly IEventPublisher _eventPublisher;

        public PaymentApiService(IRepository<PaymentTransaction> paymentTransactionRepository,
            IRepository<KNETPaymentTransaction> oKNETPaymentTransactionRepository,
            IRepository<MasterCardPaymentTransaction> oMasterCardPaymentTransactionRepository,
            IEventPublisher eventPublisher)
        {
            _paymentTransactionRepository = paymentTransactionRepository;
            _KNETPaymentTransactionRepository = oKNETPaymentTransactionRepository;
            _MasterCardPaymentTransactionRepository = oMasterCardPaymentTransactionRepository;
            _eventPublisher = eventPublisher;
        }

        /// <summary>
        /// Inserts a payment transaction
        /// </summary>
        /// <param name="oPaymentTransaction"></param>
        public virtual void InsertPaymentTransaction(PaymentTransaction oPaymentTransaction)
        {
            if (oPaymentTransaction == null)
                throw new ArgumentNullException(nameof(oPaymentTransaction));

            _paymentTransactionRepository.Insert(oPaymentTransaction);

            //event notification
            _eventPublisher.EntityInserted(oPaymentTransaction);
        }

        /// <summary>
        /// Updates a payment transaction
        /// </summary>
        /// <param name="oPaymentTransaction">Queued email</param>
        public virtual void UpdatePaymentTransaction(PaymentTransaction oPaymentTransaction)
        {
            if (oPaymentTransaction == null)
                throw new ArgumentNullException(nameof(oPaymentTransaction));

            _paymentTransactionRepository.Update(oPaymentTransaction);

            //event notification
            _eventPublisher.EntityUpdated(oPaymentTransaction);
        }

        /// <summary>
        /// Updates a KNET payment transaction
        /// </summary>
        /// <param name="oKNETPaymentTransaction"></param>
        public void UpdateKNETPaymentTransaction(KNETPaymentTransaction oKNETPaymentTransaction)
        {
            if (oKNETPaymentTransaction == null)
                throw new ArgumentNullException(nameof(oKNETPaymentTransaction));

            _KNETPaymentTransactionRepository.Update(oKNETPaymentTransaction);

            //event notification
            _eventPublisher.EntityUpdated(oKNETPaymentTransaction);
        }

        /// <summary>
        /// Get payment transaction by TrackId
        /// </summary>
        /// <param name="TrackId"></param>
        /// <returns></returns>
        public PaymentTransaction GetPaymentTransactionByTrackId(string TrackId)
        {
            if (string.IsNullOrEmpty(TrackId))
                return null;

            return _paymentTransactionRepository.Table.FirstOrDefault(oPaymentTransaction => oPaymentTransaction.TrackId == TrackId);
        }

        public void InsertMasterCardPaymentTransaction(MasterCardPaymentTransaction oMasterCardPaymentTransaction)
        {
            if (oMasterCardPaymentTransaction == null)
                throw new ArgumentNullException(nameof(oMasterCardPaymentTransaction));

            _MasterCardPaymentTransactionRepository.Insert(oMasterCardPaymentTransaction);

            //event notification
            _eventPublisher.EntityInserted(oMasterCardPaymentTransaction);
        }
    }
}
