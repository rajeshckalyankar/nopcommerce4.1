﻿using Nop.Core.Domain.Payments;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Api.Services
{
    public interface IPaymentApiService
    {
        /// <summary>
        /// Inserts a payment transaction
        /// </summary>
        /// <param name="oPaymentTransaction"></param>
        void InsertPaymentTransaction(PaymentTransaction oPaymentTransaction);

        /// <summary>
        /// Updates a payment transaction
        /// </summary>
        /// <param name="oPaymentTransaction"></param>
        void UpdatePaymentTransaction(PaymentTransaction oPaymentTransaction);

        /// <summary>
        /// Updates a KNET payment transaction
        /// </summary>
        /// <param name="oKNETPaymentTransaction"></param>
        void UpdateKNETPaymentTransaction(KNETPaymentTransaction oKNETPaymentTransaction);

        /// <summary>
        /// Inserts a MasterCard payment transaction
        /// </summary>
        /// <param name="oMasterCardPaymentTransaction"></param>
        void InsertMasterCardPaymentTransaction(MasterCardPaymentTransaction oMasterCardPaymentTransaction);

        /// <summary>
        /// Get payment transaction by TrackId
        /// </summary>
        /// <param name="TrackId"></param>
        /// <returns></returns>
        PaymentTransaction GetPaymentTransactionByTrackId(string TrackId);
    }
}
