﻿using System;
using System.Collections.Generic;
using System.Text;
using Nop.Plugin.Api.DTOs.ShoppingCarts;
using Nop.Web.Models.Catalog;

namespace Nop.Plugin.Api.Models.ShoppingCartsParameters
{
    public class ViewCartResponse
    {
        public ShoppingCartItemsRootObject shoppingCartItemsRootObject;
        public CatalogPagingFilteringModel catalogPagingFilteringModel;
    }
}
