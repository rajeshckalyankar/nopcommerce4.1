using Nop.Core.Domain.Common;

namespace Nop.Plugin.Api.Models.ShoppingCartsParameters
{
    /// <summary>
    /// Represents a Store Address.
    /// </summary>
    public class StoreDetails 
    {
        public int Id { get; set; }        
        public string Name { get; set; } 
        public string StoreCode { get; set; }
        public string StoreTimings { get; set; }
        public string Features { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Governorate { get; set; }
        public string AdminComment { get; set; }
        public int AddressId { get; set; }
        public Address StoreAddress { get; set; }

        public bool IsWarehouse { get; set; }
    }
}