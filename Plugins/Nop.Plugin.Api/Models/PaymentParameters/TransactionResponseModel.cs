using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Nop.Plugin.Api.Models
{
    public class TransactionResponseModel
    {
        public string OrderId { get; set; }
        public string OrderAmount { get; set; }
        public string OrderCurrency { get; set; }
        public string OrderDescription { get; set; }
        public string ApiResult { get; set; }        
       
        public string CreationTime { get; set; }
        public string Result { get; set; } // SUCCESS
        public string Status { get; set; } // CAPTURED
        public string AcquirerCode { get; set; } // 00
        public string AcquirerMessage { get; set; } //Approved 
        public string GatewayCode { get; set; } // APPROVED
        public string TransactionId { get; set; }
        public string AuthorizationCode { get; set; }
        public string Receipt { get; set; } 


        /// <summary>
        /// Parses JSON response from Hosted/Browser Checkout transaction into TransactionResponse object
        /// </summary>
        /// <param name="response">response from API
        /// <returns>TransactionResponseModel</returns>
        public static TransactionResponseModel toTransactionResponseModel(string response)
        {
            TransactionResponseModel model = new TransactionResponseModel();

            JObject jObject = JObject.Parse(response);
            var transactionList = jObject["transaction"];
            model.GatewayCode = transactionList[0]["response"]["gatewayCode"].ToObject<String>();
            model.ApiResult = transactionList[0]["result"].ToObject<String>();
            model.OrderAmount = transactionList[0]["order"]["amount"].ToObject<String>();
            model.OrderCurrency = transactionList[0]["order"]["currency"].ToObject<String>();
            model.OrderId = transactionList[0]["order"]["id"].ToObject<String>();

            model.CreationTime = transactionList[0]["order"]["creationTime"].ToObject<String>();
            model.Status = transactionList[0]["order"]["status"].ToObject<String>(); // CAPTURED/NOT CAPTURED
            model.Result = transactionList[0]["result"].ToObject<String>(); // SUCCESS/FAILURE
            model.AcquirerCode = transactionList[0]["response"]["acquirerCode"].ToObject<String>();
            model.AcquirerMessage = transactionList[0]["response"]["acquirerMessage"].ToObject<String>();
            model.TransactionId = transactionList[0]["transaction"]["acquirer"]["transactionId"].ToObject<String>();
            model.AuthorizationCode = model.Result == "SUCCESS"? transactionList[0]["transaction"]["authorizationCode"].ToObject<String>(): "";
            model.Receipt = transactionList[0]["transaction"]["receipt"].ToObject<String>();

            return model;
        }


        public static TransactionResponseModel fromMasterpassResponseToTransactionResponseModel(string response)
        {
            TransactionResponseModel model = new TransactionResponseModel();

            JObject jObject = JObject.Parse(response);
            model.GatewayCode = jObject["response"]["gatewayCode"].ToObject<String>();
            model.ApiResult =   jObject["result"].ToObject<String>();
            model.OrderAmount = jObject["order"]["amount"].ToObject<String>();
            model.OrderCurrency = jObject["order"]["currency"].ToObject<String>();
            model.OrderId = jObject["order"]["id"].ToObject<String>();

            return model;
        }


    }

  

}
