﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Api.Models.PaymentParameters
{
    public class KNETPaymentTransaction
    {
        #region Private properties

        private string _payment_id;
        private string _track_id;
        private string _tran_id;
        private string _auth;
        private string _ref;
        private string _result;
        private string _udf1;
        private string _udf2;
        private string _udf3;
        private string _udf4;
        private string _udf5;
        private Double? _amt;
        private string _post_date;
        private string _avr;
        private string _auth_resp_code;
        private string _error_code;
        private string _error_text;

        #endregion

        #region Public properties

        public string PaymentID
        {
            get
            {
                return _payment_id;
            }
            set
            {
                _payment_id = value;
            }
        }
        public string TrackID
        {
            get
            {
                return _track_id;
            }
            set
            {
                _track_id = value;

            }
        }
        public string TranID
        {
            get
            {
                return _tran_id;
            }
            set
            {
                _tran_id = value;

            }
        }
        public string Auth
        {
            get
            {
                return _auth;
            }
            set
            {
                _auth = value;

            }
        }
        public string Ref
        {
            get
            {
                return _ref;
            }
            set
            {
                _ref = value;

            }
        }
        public string Result
        {
            get
            {
                return _result;
            }
            set
            {
                _result = value;

            }
        }
        public string UDF1
        {
            get
            {
                return _udf1;
            }
            set
            {
                _udf1 = value;

            }
        }
        public string UDF2
        {
            get
            {
                return _udf2;
            }
            set
            {
                _udf2 = value;

            }
        }
        public string UDF3
        {
            get
            {
                return _udf3;
            }
            set
            {
                _udf3 = value;

            }
        }
        public string UDF4
        {
            get
            {
                return _udf4;
            }
            set
            {
                _udf4 = value;

            }
        }
        public string UDF5
        {
            get
            {
                return _udf5;
            }
            set
            {
                _udf5 = value;

            }
        }
        public Double? Amt
        {
            get
            {
                return _amt;
            }
            set
            {
                _amt = value;
            }
        }
        public string PostDate
        {
            get
            {
                return _post_date;
            }
            set
            {
                _post_date = value;

            }
        }
        public string Avr
        {
            get
            {
                return _avr;
            }
            set
            {
                _avr = value;

            }
        }
        public string AuthRespCode
        {
            get
            {
                return _auth_resp_code;
            }
            set
            {
                _auth_resp_code = value;

            }
        }
        public string ErrorCode
        {
            get
            {
                return _error_code;
            }
            set
            {
                _error_code = value;

            }
        }
        public string ErrorText
        {
            get
            {
                return _error_text;
            }
            set
            {
                _error_text = value;

            }
        }

        #endregion

        public KNETPaymentTransaction(string payment_id, string track_id, string tran_id, string auth, string @ref, string result, string udf1, string udf2, string udf3, string udf4, string udf5, Double? amt, string post_date, string avr, string auth_resp_code, string error_code, string error_text)
        {
            _payment_id = payment_id;
            _track_id = track_id;
            _tran_id = tran_id;
            _auth = auth;
            _ref = @ref;
            _result = result;
            _udf1 = udf1;
            _udf2 = udf2;
            _udf3 = udf3;
            _udf4 = udf4;
            _udf5 = udf5;
            _amt = amt;
            _post_date = post_date;
            _avr = avr;
            _auth_resp_code = auth_resp_code;
            _error_code = error_code;
            _error_text = error_text;
        }
    }
}
