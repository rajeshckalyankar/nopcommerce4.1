﻿using Nop.Web.Areas.Admin.Models.Common;
using Nop.Web.Framework.Models;

namespace Nop.Plugin.Api.Models.Customer
{
    /// <summary>
    /// Represents a customer address model
    /// </summary>
    public partial class CustomerAddressModel : BaseNopModel
    {
        #region Ctor

        public CustomerAddressModel()
        {
            this.Address = new Web.Areas.Admin.Models.Common.AddressModel();
        }

        #endregion

        #region Properties

        public int CustomerId { get; set; }

        public Web.Areas.Admin.Models.Common.AddressModel Address { get; set; }

        #endregion
    }
}