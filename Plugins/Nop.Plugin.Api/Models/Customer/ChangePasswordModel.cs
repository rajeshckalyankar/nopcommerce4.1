﻿using System.ComponentModel.DataAnnotations; 
using Nop.Web.Framework.Mvc.ModelBinding; 

namespace Nop.Plugin.Api.Models.Customer
{    
    public partial class ChangePasswordModel : BaseNopModel
    {
        public string Email { get; set; }
        public string Phone { get; set; }

        [NoTrim]
        [DataType(DataType.Password)]
        [NopResourceDisplayName("Account.ChangePassword.Fields.OldPassword")]
        public string OldPassword { get; set; }

        [NoTrim]
        [DataType(DataType.Password)]
        [NopResourceDisplayName("Account.ChangePassword.Fields.NewPassword")]
        public string NewPassword { get; set; }

        [NoTrim]
        [DataType(DataType.Password)]
        [NopResourceDisplayName("Account.ChangePassword.Fields.ConfirmNewPassword")]
        public string ConfirmNewPassword { get; set; }

        public string Result { get; set; }
    }
}