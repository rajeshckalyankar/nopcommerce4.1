﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;
using Nop.Web.Models.Common;

namespace Nop.Plugin.Api.Models.Customer
{
    /// <summary>
    /// Represents a customer address list model
    /// </summary>
    public partial class CustomerAddressListModel : BasePagedListModel<Nop.Web.Areas.Admin.Models.Common.AddressModel>
    {
    }
}