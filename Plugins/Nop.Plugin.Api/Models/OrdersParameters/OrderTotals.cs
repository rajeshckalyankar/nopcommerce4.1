﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;
using Nop.Web.Models.Common;
using static Nop.Plugin.Api.Controllers.BaseApiController;

namespace Nop.Plugin.Api.Models.OrdersParameters
{
    /// <summary>
    /// Represents a customer address list model
    /// </summary>
    public  class OrderTotals //: BasePagedListModel<Nop.Web.Models.ShoppingCart.OrderTotalsModel>
    {
        public Nop.Web.Models.ShoppingCart.OrderTotalsModel Data;

        public ErrorResponse Error;
    }
}