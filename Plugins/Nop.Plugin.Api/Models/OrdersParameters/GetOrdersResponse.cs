﻿using System;
using System.Collections.Generic;
using System.Text;
using Nop.Plugin.Api.DTOs.Orders;
using Nop.Web.Models.Catalog;

namespace Nop.Plugin.Api.Models.OrdersParameters
{
    public class GetOrdersResponse
    {
        public OrdersRootObject ordersRootObject;
        public CatalogPagingFilteringModel catalogPagingFilteringModel;
    }
}
