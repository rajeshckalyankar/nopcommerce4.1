﻿using System;
using System.Collections.Generic;
using System.Text;
using static Nop.Plugin.Api.Controllers.BaseApiController;

namespace Nop.Plugin.Api.Models.OrdersParameters
{
    public class RevertOrderResponse
    {
        public bool Data;

        public ErrorResponse Error;
    }
}
