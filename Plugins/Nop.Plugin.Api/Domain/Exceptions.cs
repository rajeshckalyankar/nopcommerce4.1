﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Plugin.Api.Domain
{
    public class InvalidPageRequestException : Exception
    {
        public InvalidPageRequestException(string Message) : base(Message)
        {

        }
    }

    public class RequestTimeoutException : Exception
    {
        public RequestTimeoutException(string Message) : base(Message)
        {

        }
    }

    public class InvalidParametersException : Exception
    {
        public InvalidParametersException(string Message) : base(Message)
        {

        }
    }
}