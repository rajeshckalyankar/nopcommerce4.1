﻿using static Nop.Plugin.Api.Controllers.BaseApiController;

namespace Nop.Plugin.Api.JSON.ActionResults
{
    public class GenericJsonResponse
    {
        public object Data;

        public ErrorResponse Error; 
    }
}
