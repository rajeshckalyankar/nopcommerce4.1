﻿namespace Nop.Plugin.Api.JSON.Serializers
{
    using System.Collections.Generic;
    using Nop.Plugin.Api.DTOs;
    using Nop.Web.Models.Catalog;

    public interface IJsonFieldsSerializer
    {
        string Serialize(ISerializableObject objectToSerialize, string fields);
    }
}
