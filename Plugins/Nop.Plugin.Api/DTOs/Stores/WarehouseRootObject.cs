﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Nop.Plugin.Api.Models.ShoppingCartsParameters;

namespace Nop.Plugin.Api.DTOs.Stores
{
    public class WarehouseRootObject : ISerializableObject
    {
        public WarehouseRootObject()
        {
            ShowroomIDs = new List<int>();
        }

        [JsonProperty("showrooms")]
        public IList<int> ShowroomIDs { get; set; }
        
        public string ErrorCode { get; set; }
        public string ErrorDesc { get; set; }
        public string ErrorTitle { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "showrooms";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(int);
        }
    }

    public class StockwiseStoreList
    {
        public List<StoreDetails> fullyAvailableStoreDetails;
        public List<StoreDetails> partiallyAvailableStoreDetails;
    }
}
