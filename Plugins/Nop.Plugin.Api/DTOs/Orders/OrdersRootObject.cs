﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using static Nop.Plugin.Api.Controllers.BaseApiController;

namespace Nop.Plugin.Api.DTOs.Orders
{
    public class OrdersRootObject : ISerializableObject
    {
        public OrdersRootObject()
        {
            Orders = new List<OrderDto>();
        }

        [JsonProperty("orders")]
        public IList<OrderDto> Orders { get; set; }

        public ErrorResponse Error;

        public string GetPrimaryPropertyName()
        {
            return "orders";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(OrderDto);
        }
    }

    public class NewOrdersRootObject : ISerializableObject
    {
        public NewOrdersRootObject()
        {
            Orders = new OrderDto();
        }

        [JsonProperty("orders")]
        public OrderDto Orders { get; set; }

        public ErrorResponse Error;

        public string GetPrimaryPropertyName()
        {
            return "orders";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(OrderDto);
        }

    }
}