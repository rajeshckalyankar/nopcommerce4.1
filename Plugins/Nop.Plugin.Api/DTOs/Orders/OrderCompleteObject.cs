﻿using Newtonsoft.Json;
using Nop.Plugin.Api.Controllers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Api.DTOs.Orders
{
    /// <summary>
    /// Class to be used as a response for CompleteOrder Api on Alimailem integration details.
    /// </summary>
    class OrderCompleteObject : ISerializableObject
    {
        public OrderCompleteObject()
        {
            ResultStatus = new Dictionary<string, StatusResult>();
        }

        /// <summary>
        /// Represents the Request status, if the request is a succes, partial success or a failure
        /// </summary>
        [JsonProperty("status")]
        public StatusCode Status { get; set; }

        /// <summary>
        /// Represents the OrderId and if they are successfully given the status or not.
        /// if not then their reason for not being successful is given.
        /// </summary>
        [JsonProperty("result_status")]
        public Dictionary<string, StatusResult> ResultStatus { get; set; }

        [JsonProperty("order_status_sync_result")]
        public IList<OrderStatusSync> OrderStatusSyncResult;

        public ErrorResponseData Errors;

        public string GetPrimaryPropertyName()
        {
            return "OrdersComplete";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(OrderDto);
        }
    }

    public class ErrorResponseData
    {
        public string ErrorCode;
        public string ErrorDesc;
        public string ErrorTitle;
    }

    /// <summary>
    /// Enum to get the different request status
    /// </summary>
    public enum StatusCode
    {
        Success = 1,
        PartialSuccess = 2,
        Failed = 3
    }

    /// <summary>
    /// Enum to get the different failure reason of an order.
    /// </summary>
    public enum StatusResult
    {
        Successful = 1,
        OrderNotFound = 2,
        NotPaidYet = 3,
        AlreadyComplete = 4,
        PaymentStatusNotValid = 5,
        OrderStatusNotValid = 6,
        AlreadyCancelled =7
    }
}
