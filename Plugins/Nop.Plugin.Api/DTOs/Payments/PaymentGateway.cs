﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Api.DTOs.Payments
{
    public class PaymentGateway
    {
        public string Id;
        public string Code;
        public string DisplayName;
        //public string IconURL;
        public string IsActive;
    }
}
