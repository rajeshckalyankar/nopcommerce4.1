﻿using FluentValidation.Attributes;
using Newtonsoft.Json;
using Nop.Plugin.Api.Validators;
using Nop.Core.Domain.Catalog;

namespace Nop.Plugin.Api.DTOs.SpecificationAttributes
{
    [JsonObject(Title = "specification_attribute_option")]
    [Validator(typeof(SpecificationAttributeOptionDtoValidator))]
    public class SpecificationAttributeOptionDto
    {
        /// <summary>
        /// Gets or sets the id
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the specification attribute identifier
        /// </summary>
        [JsonProperty("specification_attribute_id")]
        public int SpecificationAttributeId { get; set; }

        /// <summary>
        /// Gets or sets the specification attribute
        /// </summary>
        [JsonProperty("specification_attribute")]
        public string SpecificationAttributeName {
            get
            {
                return ((SpecificationAttribute)SpecificationAttributeId).ToString();
            }            
        }
        /// <summary>
        /// Gets or sets the name
        /// </summary>
        [JsonProperty("value")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the display order
        /// </summary>
        [JsonProperty("display_order")]
        public int DisplayOrder { get; set; }

        /// <summary>
        /// Gets or sets the color RGB value (used when you want to display "Color squares" instead of text)
        /// </summary>
        [JsonProperty("color_squares_rgb")]
        public string ColorSquaresRgb { get; set; }

        

        public enum SpecificationAttribute
        {
            Color,
            Width,
            AspectRatio,
            RimSize,
            LoadIndex,
            SpeedIndex,
            RunFlat,
            VehicleType,
            Temperature
        }
    }
}