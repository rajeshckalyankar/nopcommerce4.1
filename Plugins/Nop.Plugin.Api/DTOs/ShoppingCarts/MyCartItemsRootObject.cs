﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Nop.Web.Models.ShoppingCart;
using static Nop.Plugin.Api.Controllers.BaseApiController;

namespace Nop.Plugin.Api.DTOs.ShoppingCarts
{
    public class MyCartItemsRootObject : ISerializableObject
    {
        public MyCartItemsRootObject()
        {
            //ShoppingCartItems = new List<ShoppingCartItemDto>();
        }

        //public string ErrorCode { get; set; }
        //public string ErrorTitle { get; set; }
        //public string ErrorDesc { get; set; }

        [JsonProperty("Error")]
        public ErrorResponse Error;

        [JsonProperty("Data")]
        public ShoppingCartModel shoppingCartModel { get; set; }

        [JsonProperty("ShoppingCartTotal")]
        public String ShoppingCartTotal;

        public string GetPrimaryPropertyName()
        {
            return "ShoppingCart";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof (ShoppingCartModel);
        }
    }
}