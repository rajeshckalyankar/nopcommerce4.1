﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using static Nop.Plugin.Api.Controllers.BaseApiController;

namespace Nop.Plugin.Api.DTOs.ShoppingCarts
{
    public class ShoppingCartItemsRootObject : ISerializableObject
    {
        public ShoppingCartItemsRootObject()
        {
            ShoppingCartItems = new List<ShoppingCartItemDto>();
        }

        //public string ErrorCode { get; set; }
        //public string ErrorTitle { get; set; }
        //public string ErrorDesc { get; set; }

        [JsonProperty("Error")]
        public ErrorResponse Error;

        [JsonProperty("ShoppingCartItems")]
        public IList<ShoppingCartItemDto> ShoppingCartItems { get; set; }

        [JsonProperty("ShoppingCartTotal")]
        public String ShoppingCartTotal;

        public string GetPrimaryPropertyName()
        {
            return "ShoppingCartItems";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof (ShoppingCartItemDto);
        }

        [JsonProperty("TrackId")]
        public string TrackId;

    }
}