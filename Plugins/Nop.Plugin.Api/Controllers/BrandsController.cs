﻿using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Vendors;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.DTOs.Errors;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.JSON.Serializers;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Services.Vendors;
using Nop.Web.Factories;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Security;
using Nop.Web.Models.Catalog;

namespace Nop.Plugin.Api.Controllers
{
    //public partial class CatalogController : BasePublicController
    //[ApiController]
    //[Route("[controller]")]
    [ApiAuthorize(Policy = JwtBearerDefaults.AuthenticationScheme, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CatalogsController : BaseApiController
    {
        #region Fields

        private readonly CatalogSettings _catalogSettings;
        private readonly IAclService _aclService;
        private readonly ICatalogModelFactory _catalogModelFactory;
        private readonly ICategoryService _categoryService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ILocalizationService _localizationService;
        private readonly IManufacturerService _manufacturerService;
        private readonly IPermissionService _permissionService;
        private readonly IProductModelFactory _productModelFactory;
        private readonly IProductService _productService;
        private readonly IProductTagService _productTagService;
        private readonly IStoreContext _storeContext;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IVendorService _vendorService;
        private readonly IWebHelper _webHelper;
        private readonly IWorkContext _workContext;
        private readonly MediaSettings _mediaSettings;
        private readonly VendorSettings _vendorSettings;

        #endregion

        #region Ctor

        public CatalogsController(  CatalogSettings catalogSettings,
                                    IAclService aclService,
                                    ICatalogModelFactory catalogModelFactory,
                                    ICategoryService categoryService, 
                                    ICustomerActivityService customerActivityService,
                                    IGenericAttributeService genericAttributeService,
                                    ILocalizationService localizationService,
                                    IManufacturerService manufacturerService,
                                    IPermissionService permissionService, 
                                    IProductModelFactory productModelFactory,
                                    IProductService productService, 
                                    IProductTagService productTagService,
                                    IStoreContext storeContext,
                                    IStoreMappingService storeMappingService,
                                    IVendorService vendorService,
                                    IWebHelper webHelper,
                                    IWorkContext workContext, 
                                    MediaSettings mediaSettings,
                                    VendorSettings vendorSettings,
                                    IJsonFieldsSerializer jsonFieldsSerializer,
                                    ICustomerService customerService,
                                    IStoreService storeService,
                                    IDiscountService discountService,
                                    IPictureService pictureService
            ) : base(jsonFieldsSerializer, aclService, customerService, storeMappingService, storeService, discountService, customerActivityService, localizationService, pictureService)
        {
            this._catalogSettings = catalogSettings;
            this._aclService = aclService;
            this._catalogModelFactory = catalogModelFactory;
            this._categoryService = categoryService;
            this._customerActivityService = customerActivityService;
            this._genericAttributeService = genericAttributeService;
            this._localizationService = localizationService;
            this._manufacturerService = manufacturerService;
            this._permissionService = permissionService;
            this._productModelFactory = productModelFactory;
            this._productService = productService;
            this._productTagService = productTagService;
            this._storeContext = storeContext;
            this._storeMappingService = storeMappingService;
            this._vendorService = vendorService;
            this._webHelper = webHelper;
            this._workContext = workContext;
            this._mediaSettings = mediaSettings;
            this._vendorSettings = vendorSettings;
        }

        #endregion

        #region Brands

        [HttpPost]
        [Route("/api/GetBrandByID")]
        //[ProducesResponseType(typeof(CustomersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public virtual IActionResult GetBrandByID(int manufacturerId, CatalogPagingFilteringModel command)
        {
            var ErrorCode = "xxxx";
            var ErrorDesc = "BrandNotAvailable";
            var manufacturer = _manufacturerService.GetManufacturerById(manufacturerId);
            if (manufacturer == null || manufacturer.Deleted)
            {
                var errorsJson = GetJsonErrorObject(ErrorCode, ErrorDesc, "Error");
                return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
            }

            var notAvailable =
                //published?
                !manufacturer.Published ||
                //ACL (access control list) 
                !_aclService.Authorize(manufacturer) ||
                //Store mapping
                !_storeMappingService.Authorize(manufacturer);
            //Check whether the current user has a "Manage categories" permission (usually a store owner)
            //We should allows him (her) to use "Preview" functionality
            if (notAvailable && !_permissionService.Authorize(StandardPermissionProvider.ManageManufacturers))
            {
                var errorsJson = GetJsonErrorObject(ErrorCode, ErrorDesc, "Error");
                return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
            }
                       
            //activity log
            _customerActivityService.InsertActivity("PublicStore.ViewManufacturer",
                string.Format(_localizationService.GetResource("ActivityLog.PublicStore.ViewManufacturer"), manufacturer.Name), manufacturer);

            //model
            var model = _catalogModelFactory.PrepareManufacturerModel(manufacturer, command);

            //template
            var templateViewPath = _catalogModelFactory.PrepareManufacturerTemplateViewPath(manufacturer.ManufacturerTemplateId);
            return View(templateViewPath, model);
        }

        [HttpPost]
        [Route("/api/getAllBrands/{languageId?}")]
        //[ProducesResponseType(typeof(CustomersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public virtual IActionResult GetAllBrands(int? languageId = null)
        {
            var model = _catalogModelFactory.PrepareManufacturerAllModels(languageId);
            model = model.OrderBy(m => m.Name).ToList();

            var jToken = JToken.FromObject(model);              

            var jTokenResult = jToken.ToString();

            return new ErrorActionResult(jTokenResult, HttpStatusCode.OK);
                        
        }

        [HttpPost]
        [Route("/api/GetAllBrandsList")]
        //[ProducesResponseType(typeof(CustomersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public virtual IActionResult GetAllBrandsList()
        {
            //var manufacturer = _manufacturerService.GetAllManufacturers(showHidden: false);

            var manufacturers = _manufacturerService.GetAllManufacturers(showHidden: true);

            var jToken = JToken.FromObject(manufacturers);

            var jTokenResult = jToken.ToString();

            return new ErrorActionResult(jTokenResult, HttpStatusCode.OK);


            ////return Json(manufacturers);
            //var json = JsonConvert.SerializeObject(manufacturers);

            // //return new RawJsonActionResult(json);
            //return new ErrorActionResult(json, HttpStatusCode.OK);

            ////prepare grid model
            //var model = new ManufacturerListModel
            //{
            //    //fill in model values from the entity
            //    Data = manufacturers.Select(manufacturer => manufacturer.ToModel<ManufacturerModel>()),
            //    Total = manufacturers.TotalCount
            //};

            //return model;

        }


        #endregion

        //#region Categories

        //[HttpsRequirement(SslRequirement.No)]
        //public virtual IActionResult Category(int categoryId, CatalogPagingFilteringModel command)
        //{
        //    var category = _categoryService.GetCategoryById(categoryId);
        //    if (category == null || category.Deleted)
        //        return InvokeHttp404();

        //    var notAvailable =
        //        //published?
        //        !category.Published ||
        //        //ACL (access control list) 
        //        !_aclService.Authorize(category) ||
        //        //Store mapping
        //        !_storeMappingService.Authorize(category);
        //    //Check whether the current user has a "Manage categories" permission (usually a store owner)
        //    //We should allows him (her) to use "Preview" functionality
        //    if (notAvailable && !_permissionService.Authorize(StandardPermissionProvider.ManageCategories))
        //        return InvokeHttp404();

        //    //'Continue shopping' URL
        //    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, 
        //        NopCustomerDefaults.LastContinueShoppingPageAttribute, 
        //        _webHelper.GetThisPageUrl(false),
        //        _storeContext.CurrentStore.Id);

        //    //display "edit" (manage) link
        //    if (_permissionService.Authorize(StandardPermissionProvider.AccessAdminPanel) && _permissionService.Authorize(StandardPermissionProvider.ManageCategories))
        //        DisplayEditLink(Url.Action("Edit", "Category", new { id = category.Id, area = AreaNames.Admin }));

        //    //activity log
        //    _customerActivityService.InsertActivity("PublicStore.ViewCategory",
        //        string.Format(_localizationService.GetResource("ActivityLog.PublicStore.ViewCategory"), category.Name), category);

        //    //model
        //    var model = _catalogModelFactory.PrepareCategoryModel(category, command);

        //    //template
        //    var templateViewPath = _catalogModelFactory.PrepareCategoryTemplateViewPath(category.CategoryTemplateId);
        //    return View(templateViewPath, model);
        //}

        //#endregion




    }
}