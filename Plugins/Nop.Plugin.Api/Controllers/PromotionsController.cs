﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Widgets.NivoSlider;

using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Infrastructure;

using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.Delta;
using Nop.Plugin.Api.DTOs.OrderItems;
using Nop.Plugin.Api.DTOs.Orders;
using Nop.Plugin.Api.Factories;
using Nop.Plugin.Api.Helpers;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.ModelBinders;
using Nop.Plugin.Api.Models.OrdersParameters;
using Nop.Plugin.Api.Services;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Security;
using Nop.Services.Shipping;
using Nop.Services.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Nop.Plugin.Api.Controllers
{
    using DTOs.Errors;
    using JSON.Serializers;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Newtonsoft.Json;
    using Nop.Core.Caching;
    using Nop.Core.Configuration;
    using Nop.Core.Domain.Shipping;
    using Nop.Plugin.Api.Models.ShoppingCartsParameters;
    using Nop.Plugin.Shipping.FixedByWeightByTotal;
    using Nop.Plugin.Widgets.NivoSlider.Infrastructure.Cache;
    using Nop.Services.Configuration;
    using Nop.Web.Factories;
    using Nop.Web.Framework.Models;
    using Nop.Web.Models.ShoppingCart;

    [ApiAuthorize(Policy = JwtBearerDefaults.AuthenticationScheme, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PromotionsController : BaseApiController
    {
        private readonly IOrderApiService _orderApiService;
        private readonly IProductService _productService;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IOrderService _orderService;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IShippingService _shippingService;
        private readonly IDTOHelper _dtoHelper;
        private readonly IProductAttributeConverter _productAttributeConverter;
        private readonly IStoreContext _storeContext;
        private readonly IFactory<Order> _factory;
        private readonly IShoppingCartModelFactory _shoppingCartModelFactory;
        private readonly ISettingService _settingService;
        private readonly IWorkContext _workContext;
        private readonly ICustomerService _customerService;
        private readonly ILogger _logger;
        private readonly Nop.Web.Areas.Admin.Factories.IOrderModelFactory _orderModelFactory;
        private readonly IPermissionService _permissionService;
        private readonly IStaticCacheManager _cacheManager;
        private readonly IPictureService _pictureService;

        // We resolve the order settings this way because of the tests.
        // The auto mocking does not support concreate types as dependencies. It supports only interfaces.
        private OrderSettings _orderSettings;

        private OrderSettings OrderSettings => _orderSettings ?? (_orderSettings = EngineContext.Current.Resolve<OrderSettings>());

        public PromotionsController(IOrderApiService orderApiService,
            IJsonFieldsSerializer jsonFieldsSerializer,
            IAclService aclService,
            ICustomerService customerService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IDiscountService discountService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            IProductService productService,
            IFactory<Order> factory,
            IOrderProcessingService orderProcessingService,
            IOrderService orderService,
            IShoppingCartService shoppingCartService,
            IGenericAttributeService genericAttributeService,
            IStoreContext storeContext,
            IShippingService shippingService,
            IPictureService pictureService,
            IDTOHelper dtoHelper,
            IShoppingCartModelFactory shoppingCartModelFactory,
             ISettingService settingService,
             IWorkContext workContext,
             ILogger logger,
            IProductAttributeConverter productAttributeConverter,
            Nop.Web.Areas.Admin.Factories.IOrderModelFactory orderModelFactory,
             IStaticCacheManager cacheManager,
             IPermissionService permissionService)
            : base(jsonFieldsSerializer, aclService, customerService, storeMappingService,
                 storeService, discountService, customerActivityService, localizationService, pictureService)
        {
            _orderApiService = orderApiService;
            _factory = factory;
            _orderProcessingService = orderProcessingService;
            _orderService = orderService;
            _shoppingCartService = shoppingCartService;
            _genericAttributeService = genericAttributeService;
            _storeContext = storeContext;
            _shippingService = shippingService;
            _dtoHelper = dtoHelper;
            _productService = productService;
            _productAttributeConverter = productAttributeConverter;
            _shoppingCartModelFactory = shoppingCartModelFactory;
            _settingService = settingService;
            _workContext = workContext;
            _customerService = customerService;
            _logger = logger;
            _orderModelFactory = orderModelFactory;
            _permissionService = permissionService;
            _cacheManager = cacheManager;
            _pictureService = pictureService;
        }

        [HttpGet]
        [Route("/api/promotions")]
        //[ProducesResponseType(typeof(OrdersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult Promotions()
        {

            var nivoSliderSettings = _settingService.LoadSetting<NivoSliderSettings>(_storeContext.CurrentStore.Id);

            var model = new List<PublicInfoModelList>();

            if (nivoSliderSettings.Picture1Id != 0)
            {
                model.Add(new PublicInfoModelList()
                {
                    PictureUrl = GetPictureUrl(nivoSliderSettings.Picture1Id),
                    Text = nivoSliderSettings.Text1,
                    Link = nivoSliderSettings.Link1,
                    AltText = nivoSliderSettings.AltText1,
                });
            }

            if (nivoSliderSettings.Picture2Id != 0)
            {
                model.Add(new PublicInfoModelList()
                {
                    PictureUrl = GetPictureUrl(nivoSliderSettings.Picture2Id),
                    Text = nivoSliderSettings.Text2,
                    Link = nivoSliderSettings.Link2,
                    AltText = nivoSliderSettings.AltText2,
                });
            }

            if (nivoSliderSettings.Picture3Id != 0)
            {
                model.Add(new PublicInfoModelList()
                {
                    PictureUrl = GetPictureUrl(nivoSliderSettings.Picture3Id),
                    Text = nivoSliderSettings.Text3,
                    Link = nivoSliderSettings.Link3,
                    AltText = nivoSliderSettings.AltText3,
                });
            }

            if (nivoSliderSettings.Picture4Id != 0)
            {
                model.Add(new PublicInfoModelList()
                {
                    PictureUrl = GetPictureUrl(nivoSliderSettings.Picture4Id),
                    Text = nivoSliderSettings.Text4,
                    Link = nivoSliderSettings.Link4,
                    AltText = nivoSliderSettings.AltText4,
                });
            }

            if (nivoSliderSettings.Picture5Id != 0)
            {
                model.Add(new PublicInfoModelList()
                {
                    PictureUrl = GetPictureUrl(nivoSliderSettings.Picture5Id),
                    Text = nivoSliderSettings.Text5,
                    Link = nivoSliderSettings.Link5,
                    AltText = nivoSliderSettings.AltText5,
                });
            }

            //var model = new PublicInfoModel
            //{
            //    Picture1Url = GetPictureUrl(nivoSliderSettings.Picture1Id),
            //    Text1 = nivoSliderSettings.Text1,
            //    Link1 = nivoSliderSettings.Link1,
            //    AltText1 = nivoSliderSettings.AltText1,

            //    Picture2Url = GetPictureUrl(nivoSliderSettings.Picture2Id),
            //    Text2 = nivoSliderSettings.Text2,
            //    Link2 = nivoSliderSettings.Link2,
            //    AltText2 = nivoSliderSettings.AltText2,

            //    Picture3Url = GetPictureUrl(nivoSliderSettings.Picture3Id),
            //    Text3 = nivoSliderSettings.Text3,
            //    Link3 = nivoSliderSettings.Link3,
            //    AltText3 = nivoSliderSettings.AltText3,

            //    Picture4Url = GetPictureUrl(nivoSliderSettings.Picture4Id),
            //    Text4 = nivoSliderSettings.Text4,
            //    Link4 = nivoSliderSettings.Link4,
            //    AltText4 = nivoSliderSettings.AltText4,

            //    Picture5Url = GetPictureUrl(nivoSliderSettings.Picture5Id),
            //    Text5 = nivoSliderSettings.Text5,
            //    Link5 = nivoSliderSettings.Link5,
            //    AltText5 = nivoSliderSettings.AltText5
            //};

            //if (string.IsNullOrEmpty(model.Picture1Url) && string.IsNullOrEmpty(model.Picture2Url) &&
            //    string.IsNullOrEmpty(model.Picture3Url) && string.IsNullOrEmpty(model.Picture4Url) &&
            //    string.IsNullOrEmpty(model.Picture5Url))
                //no pictures uploaded
                //return Content("");

            return Json(model);
            //return View("~/Plugins/Widgets.NivoSlider/Views/PublicInfo.cshtml", model);
        }

        protected string GetPictureUrl(int pictureId)
        {
            var cacheKey = string.Format(ModelCacheEventConsumer.PICTURE_URL_MODEL_KEY, pictureId);
            return _cacheManager.Get(cacheKey, () =>
            {
                //little hack here. nulls aren't cacheable so set it to ""
                var url = _pictureService.GetPictureUrl(pictureId, showDefaultPicture: false) ?? "";
                return url;
            });
        }


    }

    // Nested classes.

    public class NivoSliderSettings : ISettings
    {
        public int Picture1Id { get; set; }
        public string Text1 { get; set; }
        public string Link1 { get; set; }
        public string AltText1 { get; set; }

        public int Picture2Id { get; set; }
        public string Text2 { get; set; }
        public string Link2 { get; set; }
        public string AltText2 { get; set; }

        public int Picture3Id { get; set; }
        public string Text3 { get; set; }
        public string Link3 { get; set; }
        public string AltText3 { get; set; }

        public int Picture4Id { get; set; }
        public string Text4 { get; set; }
        public string Link4 { get; set; }
        public string AltText4 { get; set; }

        public int Picture5Id { get; set; }
        public string Text5 { get; set; }
        public string Link5 { get; set; }
        public string AltText5 { get; set; }
    }

    // Model

    public class PublicInfoModel : BaseNopModel
    {
        public string Picture1Url { get; set; }
        public string Text1 { get; set; }
        public string Link1 { get; set; }
        public string AltText1 { get; set; }

        public string Picture2Url { get; set; }
        public string Text2 { get; set; }
        public string Link2 { get; set; }
        public string AltText2 { get; set; }

        public string Picture3Url { get; set; }
        public string Text3 { get; set; }
        public string Link3 { get; set; }
        public string AltText3 { get; set; }

        public string Picture4Url { get; set; }
        public string Text4 { get; set; }
        public string Link4 { get; set; }
        public string AltText4 { get; set; }

        public string Picture5Url { get; set; }
        public string Text5 { get; set; }
        public string Link5 { get; set; }
        public string AltText5 { get; set; }
    }

    public class PublicInfoModelList : BaseNopModel
    {

        public string PictureUrl { get; set; }
        [JsonProperty("id")]
        public string Text { get; set; }
        [JsonProperty("choice")]
        public string Link { get; set; }
        public string AltText { get; set; }
    }


}