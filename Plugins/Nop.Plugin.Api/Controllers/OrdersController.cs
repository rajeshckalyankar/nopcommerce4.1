﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Infrastructure;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.Delta;
using Nop.Plugin.Api.DTOs.OrderItems;
using Nop.Plugin.Api.DTOs.Orders;
using Nop.Plugin.Api.Factories;
using Nop.Plugin.Api.Helpers;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.ModelBinders;
using Nop.Plugin.Api.Models.OrdersParameters;
using Nop.Plugin.Api.Services;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Security;
using Nop.Services.Shipping;
using Nop.Services.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Nop.Plugin.Api.Controllers
{
    using DTOs.Errors;
    using JSON.Serializers;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Hosting;
    using Newtonsoft.Json;
    using Nop.Core.Data;
    using Nop.Core.Domain.Catalog;
    using Nop.Core.Domain.Messages;
    using Nop.Core.Domain.Payments;
    using Nop.Core.Domain.PushNotifications;
    using Nop.Core.Domain.Shipping;
    using Nop.Plugin.Api.Models.ShoppingCartsParameters;
    using Nop.Plugin.Shipping.FixedByWeightByTotal;
    using Nop.Services.Configuration;
    using Nop.Services.Messages;
    using Nop.Services.PushNotifications;
    using Nop.Web.Factories;
    using Nop.Web.Models.Catalog;
    using Nop.Web.Models.ShoppingCart; 
    using System.IO;
    using System.Text;
    using System.Xml;
    using System.Xml.Serialization;

    [ApiAuthorize(Policy = JwtBearerDefaults.AuthenticationScheme, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class OrdersController : BaseApiController
    {
        private readonly IOrderApiService _orderApiService;
        private readonly IProductService _productService;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IOrderService _orderService;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IShippingService _shippingService;
        private readonly IDTOHelper _dtoHelper;
        private readonly IProductAttributeConverter _productAttributeConverter;
        private readonly IStoreContext _storeContext;
        private readonly IFactory<Order> _factory;
        private readonly IShoppingCartModelFactory _shoppingCartModelFactory;
        private readonly ISettingService _settingService;
        private readonly IWorkContext _workContext;
        private readonly ICustomerService _customerService;
        private readonly ILogger _logger;
        private readonly Nop.Web.Areas.Admin.Factories.IOrderModelFactory _orderModelFactory;
        private readonly IRepository<Order> _orderRepository;
        private readonly IWorkflowMessageService _workflowMessageService;        
        private readonly IAddressService _addressService;
        private readonly ILocalizationService _localizationService;
		private readonly IPushNotificationService _pushNotificationService;
        private readonly IHostingEnvironment _hostingEnvironment;

        // We resolve the order settings this way because of the tests.
        // The auto mocking does not support concreate types as dependencies. It supports only interfaces.
        private OrderSettings _orderSettings;

        private OrderSettings OrderSettings => _orderSettings ?? (_orderSettings = EngineContext.Current.Resolve<OrderSettings>());

        public OrdersController(IOrderApiService orderApiService,
            IJsonFieldsSerializer jsonFieldsSerializer,
            IAclService aclService,
            ICustomerService customerService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IDiscountService discountService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            IProductService productService,
            IFactory<Order> factory,
            IOrderProcessingService orderProcessingService,
            IOrderService orderService,
            IShoppingCartService shoppingCartService,
            IGenericAttributeService genericAttributeService,
            IStoreContext storeContext,
            IShippingService shippingService,
            IPictureService pictureService,
            IDTOHelper dtoHelper,
            IShoppingCartModelFactory shoppingCartModelFactory,
            ISettingService settingService,
            IWorkContext workContext,
            ILogger logger,
            IProductAttributeConverter productAttributeConverter,
            Nop.Web.Areas.Admin.Factories.IOrderModelFactory orderModelFactory,
            IRepository<Order> orderRepository,
            IWorkflowMessageService workflowMessageService,
            IAddressService addressService,
            IPushNotificationService pushNotificationService,
            IHostingEnvironment hostingEnvironment
            )
            : base(jsonFieldsSerializer, aclService, customerService, storeMappingService,
                 storeService, discountService, customerActivityService, localizationService, pictureService)
        {
            _orderApiService = orderApiService;
            _factory = factory;
            _orderProcessingService = orderProcessingService;
            _orderService = orderService;
            _shoppingCartService = shoppingCartService;
            _genericAttributeService = genericAttributeService;
            _storeContext = storeContext;
            _shippingService = shippingService;
            _dtoHelper = dtoHelper;
            _productService = productService;
            _productAttributeConverter = productAttributeConverter;
            _shoppingCartModelFactory = shoppingCartModelFactory;
            _settingService = settingService;
            _workContext = workContext;
            _customerService = customerService;
            _logger = logger;
            _orderModelFactory = orderModelFactory;
            _orderRepository = orderRepository;
            _workflowMessageService = workflowMessageService;
            _addressService = addressService;
            _pushNotificationService = pushNotificationService;
            _localizationService = localizationService;
            _hostingEnvironment = hostingEnvironment;
        }

        /// <summary>
        /// Receive a list of all Orders
        /// </summary>
        /// <response code="200">OK</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/orders/{customer_id}/{page}/{limit}/{languageId?}")]
        [ProducesResponseType(typeof(OrdersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetOrders(int Customer_Id, int Page, int Limit, int? languageId = null)
        {
            
            ErrorResponse error = new ErrorResponse();
            OrdersParametersModel parameters = new OrdersParametersModel();
            GenericJsonResponse genericJsonResponse = new GenericJsonResponse();
            parameters.CustomerId = Customer_Id;
            parameters.Page = Page;
            parameters.Limit = Limit;
            //if (parameters.Page < Configurations.DefaultPageValue)
            //{
            //    //return Error(HttpStatusCode.BadRequest, "page", "Invalid page parameter");
            //    error.ErrorCode = "";
            //    error.ErrorTitle = "Error";
            //    error.ErrorDesc = "Invalid page parameter";
            //    genericJsonResponse.Error = error;
            //    genericJsonResponse.Data = null;
            //    return Json(genericJsonResponse);
            //} 

            //if (parameters.Limit < Configurations.MinLimit || parameters.Limit > Configurations.MaxLimit)
            //{
            //    //return Error(HttpStatusCode.BadRequest, "page", "Invalid limit parameter");
            //    error.ErrorCode = "";
            //    error.ErrorTitle = "Error";
            //    error.ErrorDesc = "Invalid limit parameter";
            //    genericJsonResponse.Error = error;
            //    genericJsonResponse.Data = null;
            //    return Json(genericJsonResponse);
            //}

            var storeId = _storeContext.CurrentStore.Id;

            var orders = _orderApiService.GetOrders(parameters.Ids, parameters.CreatedAtMin,
                parameters.CreatedAtMax,
                parameters.Limit, parameters.Page, parameters.SinceId,
                parameters.Status, parameters.PaymentStatus, parameters.ShippingStatus,
                parameters.CustomerId, storeId);
            IList<OrderDto> ordersAsDtos = orders.Select(x => _dtoHelper.PrepareOrderDTO_List(x, languageId)).ToList();
            CatalogPagingFilteringModel command = new CatalogPagingFilteringModel();
            var query = _orderRepository.Table;
            query = query.Where(order => order.CustomerId == Customer_Id && order.Deleted == false);
            command.PageSize = parameters.Limit;
            command.PageNumber = parameters.Page;
            command.TotalItems = query.Count();
            if (command.TotalItems % command.PageSize == 0)
            {
                command.TotalPages = command.TotalItems / command.PageSize;
            }
            else
            {
                command.TotalPages = command.TotalItems / command.PageSize;
                command.TotalPages += 1;
            }
            var ordersRootObject = new OrdersRootObject()
            {
                Orders = ordersAsDtos
            };
            if (query != null && query.Count() > 0)
            {
                error.ErrorCode = "2093";
                error.ErrorTitle = "Success";
                error.ErrorDesc = "OrdersListAvailable";
            }
            else
            {
                error.ErrorCode = "2094";
                error.ErrorTitle = "Error";
                error.ErrorDesc = "OrderListEmpty";
            }
            GetOrdersResponse getOrdersResponse = new GetOrdersResponse();
            getOrdersResponse.catalogPagingFilteringModel = command;
            getOrdersResponse.ordersRootObject = ordersRootObject;
            genericJsonResponse.Error = error;
            genericJsonResponse.Data = getOrdersResponse;
            return Json(genericJsonResponse);
            //var json = JsonFieldsSerializer.Serialize(ordersRootObject, parameters.Fields);
            //return new RawJsonActionResult(json);
        }

        
        /// <summary>
        /// Receive a count of all Orders
        /// </summary>
        /// <response code="200">OK</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/orders/count")]
        [ProducesResponseType(typeof(OrdersCountRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetOrdersCount(OrdersCountParametersModel parameters)
        {
            var storeId = _storeContext.CurrentStore.Id;

            var ordersCount = _orderApiService.GetOrdersCount(parameters.CreatedAtMin, parameters.CreatedAtMax, parameters.Status,
                                                              parameters.PaymentStatus, parameters.ShippingStatus, parameters.CustomerId, storeId);

            var ordersCountRootObject = new OrdersCountRootObject()
            {
                Count = ordersCount
            };

            return Ok(ordersCountRootObject);
        }         

        /// <summary>
        /// Retrieve order by spcified id
        /// </summary>
        ///   /// <param name="id">Id of the order</param>
        /// <param name="fields">Fields from the order you want your json to contain</param>
        /// <response code="200">OK</response>
        /// <response code="404">Not Found</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/orders/{id}/{languageId?}")]
        [ProducesResponseType(typeof(OrdersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetOrderById(int id, string fields = "", int? languageId = null)
        {
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }

            var order = _orderApiService.GetOrderById(id);

            if (order == null)
            {
                return Error(HttpStatusCode.NotFound, "order", "not found");
            }
            //Map Localized shipping address
            if (order.WarehouseId.HasValue && order.WarehouseId != 0)
            {
                int localizedAddressId = 0;
                var warehouse = _shippingService.GetWarehouseById(order.WarehouseId.Value);
                //Set only the matching one warehouse in the ProductWarehouseInventory
                foreach (var item in order.OrderItems)
                {
                    //Find the matching warehouse
                    List<ProductWarehouseInventory> warehouseInventory = item.Product.ProductWarehouseInventory.Where(x => x.WarehouseId == order.WarehouseId.Value).ToList();
                    //If nout found then create new ProductWarehouseInventory as this store must be returned in the result
                    if (warehouseInventory == null || warehouseInventory.Count == 0)
                    {
                        warehouseInventory = new List<ProductWarehouseInventory>()
                        {
                            new ProductWarehouseInventory()
                            {
                                  Id = -1,//Set to -1 as otherwise it returns very odd -ve no
                                  ProductId = item.Product.Id,
                                  WarehouseId = order.WarehouseId.Value,
                                  StockQuantity = 0,
                                  ReservedQuantity = 0,
                                  Warehouse = warehouse
                                 } };
                    }
                    item.Product.ProductWarehouseInventory = warehouseInventory;

                }
                if (languageId.HasValue)
                {
                    order.ShippingAddress = GetLocalizedAddresss(warehouse, out localizedAddressId, languageId);
                    if (localizedAddressId != 0)
                        order.ShippingAddressId = localizedAddressId;
                }

            }
            var ordersRootObject = new OrdersRootObject();

            var orderDto = _dtoHelper.PrepareOrderDTO_List(order, languageId);  // Optimized.
            ordersRootObject.Orders.Add(orderDto);

            var json = JsonFieldsSerializer.Serialize(ordersRootObject, fields);

            return new RawJsonActionResult(json);
        }


        //Get Local Address if it is not present then get standard address
        private Core.Domain.Common.Address GetLocalizedAddresss(Core.Domain.Shipping.Warehouse stores, out int localizedAddressId, int? languageId = null)
        {
            localizedAddressId = 0;
            var stdAddress = stores.AddressId > 0 ? (_addressService.GetAddressById(stores.AddressId)) : null;
            if (languageId.HasValue)
            {
                var localAddressId = _localizationService.GetLocalized(stores, x => x.AddressId, languageId);
                var localAddress = localAddressId > 0 ? (_addressService.GetAddressById(localAddressId)) : null;
                if (localAddress != null)
                {
                    if (stdAddress == null)
                        stdAddress = localAddress;
                    else
                    {
                        //If localAddress field is not null then assign that to address field       
                        localizedAddressId = localAddress.Id;
                        stdAddress.Address1 = !string.IsNullOrEmpty(localAddress.Address1) ? localAddress.Address1 : stdAddress.Address1;
                        stdAddress.Address2 = !string.IsNullOrEmpty(localAddress.Address2) ? localAddress.Address2 : stdAddress.Address2;
                        stdAddress.City = !string.IsNullOrEmpty(localAddress.City) ? localAddress.City : stdAddress.City;
                        stdAddress.Company = !string.IsNullOrEmpty(localAddress.Company) ? localAddress.Company : stdAddress.Company;
                        stdAddress.Country = (localAddress.Country != null) ? localAddress.Country : stdAddress.Country;
                        stdAddress.CountryId = (localAddress.CountryId.HasValue) ? localAddress.CountryId : stdAddress.CountryId;
                        stdAddress.County = !string.IsNullOrEmpty(localAddress.County) ? localAddress.County : stdAddress.County;
                        stdAddress.PhoneNumber = !string.IsNullOrEmpty(localAddress.PhoneNumber) ? localAddress.PhoneNumber : stdAddress.PhoneNumber;
                        stdAddress.StateProvince = (localAddress.StateProvince != null) ? localAddress.StateProvince : stdAddress.StateProvince;
                        stdAddress.StateProvinceId = (localAddress.StateProvinceId.HasValue) ? localAddress.StateProvinceId : stdAddress.StateProvinceId;
                        stdAddress.ZipPostalCode = !string.IsNullOrEmpty(localAddress.ZipPostalCode) ? localAddress.ZipPostalCode : stdAddress.ZipPostalCode;
                    }
                }
            }
            return stdAddress;
        }


        /// <summary>
        /// Retrieve all orders for customer
        /// </summary>
        /// <param name="customerId">Id of the customer whoes orders you want to get</param>
        /// <response code="200">OK</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/orders/customer/{customerId}/{languageId?}")]
        [ProducesResponseType(typeof(OrdersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetOrdersByCustomerId(int customerId, int? languageId = null)
        {
            IList<OrderDto> ordersForCustomer =
                _orderApiService.GetOrdersByCustomerId(customerId)
                .Select(x => _dtoHelper.PrepareOrderDTO(x, languageId))
                .Where(oOrder => oOrder.OrderStatus.ToUpper() != OrderStatus.Pending.ToString().ToUpper())
                .ToList();

            var ordersRootObject = new OrdersRootObject()
            {
                Orders = ordersForCustomer
            };

            return Ok(ordersRootObject);
        }

        public bool CheckProceedToCheckoutFlag(Customer customer)
        {
            var errorResponse = new ErrorResponse();
            try
            {
                var flag = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.ProceedToCheckout, _storeContext.CurrentStore.Id);

                if (flag == "true")
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        [HttpPost]
        [Route("/api/saveProceedToCheckout/{customerId}/{CurrentTrackId}")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult SaveProceedToCheckout(int customerId, string CurrentTrackId)
        {
            var errorResponse = new ErrorResponse();
            try
            {
                //  To, reset checkout data.
                var customer = CustomerService.GetCustomerById(customerId);

                if (customer == null)
                {
                    var errorsJson1 = GetJsonErrorObject("2042", "InvalidCustomerID", "Error");
                    return new ErrorActionResult(errorsJson1, HttpStatusCode.OK);
                }

                string SavedTrackId = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.TrackId, _storeContext.CurrentStore.Id);

                if (CurrentTrackId != SavedTrackId)
                {
                    _genericAttributeService.SaveAttribute<string>(customer, NopCustomerDefaults.ProceedToCheckout, null, _storeContext.CurrentStore.Id);
                    var errorModel = new OrderTotals
                    {
                        Data = null,
                        Error = new ErrorResponse
                        {
                            ErrorCode = "2070",
                            ErrorDesc = "CartIsModified",
                            ErrorTitle = "Error"
                        }
                    };
                    var errorModelJson = JsonConvert.SerializeObject(errorModel);

                    return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
                }
                //if (!CheckProceedToCheckoutFlag(customer))
                //{
                //    _genericAttributeService.SaveAttribute<string>(customer, NopCustomerDefaults.ProceedToCheckout, null, _storeContext.CurrentStore.Id);
                //    var errorModel = new OrderTotals
                //    {
                //        Data = null,
                //        Error = new ErrorResponse
                //        {
                //            ErrorCode = "2070",
                //            ErrorDesc = "CartIsModified",
                //            ErrorTitle = "Error"
                //        }
                //    };
                //    var errorModelJson = JsonConvert.SerializeObject(errorModel);

                //    return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
                //}

                //Validate each cart item for any warning.
                var errorResponseMsg = ValidateCartItem(customer);
                if (!string.IsNullOrEmpty(errorResponseMsg))
                {
                    var errorModel = new OrderTotals
                    {
                        Data = null,
                        Error = new ErrorResponse
                        {
                            ErrorCode = "2060",
                            ErrorDesc = errorResponseMsg,
                            ErrorTitle = "Error"
                        }
                    };
                    var errorModelJson = JsonConvert.SerializeObject(errorModel);
                    return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
                }

                //reset checkout data
                _customerService.ResetCheckoutData(customer, _storeContext.CurrentStore.Id, true);
                //

                _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.ProceedToCheckout, "true", _storeContext.CurrentStore.Id);

                errorResponse.ErrorCode = "2066";
                errorResponse.ErrorTitle = "SavedProceedToCheckout";
                errorResponse.ErrorDesc = "";

                var ModelJsonString = JsonConvert.SerializeObject(errorResponse);
                return new ErrorActionResult(ModelJsonString, HttpStatusCode.OK);

            }
            catch
            {
                errorResponse.ErrorCode = "2065";
                errorResponse.ErrorTitle = "ErrorProceedToCheckout";
                errorResponse.ErrorDesc = "Error";

                var ModelJsonString1 = JsonConvert.SerializeObject(errorResponse);
                return new ErrorActionResult(ModelJsonString1, HttpStatusCode.OK);
            }


        }

        [HttpPost]
        [Route("/api/saveSelectedDeliveryMethod/{customerId}/{deliveryMethodId}")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult SaveSelectedDeliveryMethod(int CustomerId, int DeliveryMethodId)
        {
            var errorResponse = new ErrorResponse();
            try
            {
                var customer = CustomerService.GetCustomerById(CustomerId);

                if (customer == null)
                {
                    var errorsJson1 = GetJsonErrorObject("2042", "InvalidCustomerID", "Error");
                    return new ErrorActionResult(errorsJson1, HttpStatusCode.OK);
                }

                if (!CheckProceedToCheckoutFlag(customer))
                {
                    var errorModel = new ErrorResponse
                    {
                        ErrorCode = "2070",
                        ErrorDesc = "CartIsModified",
                        ErrorTitle = "Error"
                    };
                    var errorModelJson = JsonConvert.SerializeObject(errorModel);
                    return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
                }

                //Validate each cart item for any warning.
                var errorResponseMsg = ValidateCartItem(customer);
                if (!string.IsNullOrEmpty(errorResponseMsg))
                {
                    var errorModel = new OrderTotals
                    {
                        Data = null,
                        Error = new ErrorResponse
                        {
                            ErrorCode = "2060",
                            ErrorDesc = errorResponseMsg,
                            ErrorTitle = "Error"
                        }
                    };
                    var errorModelJson = JsonConvert.SerializeObject(errorModel);
                    return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
                }

                var rateModels = _shippingService.GetAllShippingMethods().Select(shippingMethod => new FixedRateModel
                {
                    ShippingMethodId = shippingMethod.Id,
                    ShippingMethodName = shippingMethod.Name,
                    Rate = _settingService.GetSettingByKey<decimal>(string.Format(FixedByWeightByTotalDefaults.FixedRateSettingsKey, shippingMethod.Id)),
                    ShippingMethodDescription = shippingMethod.Description,
                    DisplayOrder = shippingMethod.DisplayOrder
                }).ToList();

                rateModels = rateModels.Where(c => c.ShippingMethodId == DeliveryMethodId).ToList();

                if (rateModels == null || rateModels.Count <= 0)
                {
                    errorResponse.ErrorCode = "2061";
                    errorResponse.ErrorTitle = "InValidSelectedDeliveryMethod";
                    errorResponse.ErrorDesc = "Error";

                    var ModelJsonString1 = JsonConvert.SerializeObject(errorResponse);
                    return new ErrorActionResult(ModelJsonString1, HttpStatusCode.OK);
                }

                var selectedShippingOption = rateModels.Select(r => new ShippingOption
                {
                    Name = r.ShippingMethodName,
                    Rate = r.Rate,
                    Description = r.ShippingMethodDescription,
                    ShippingRateComputationMethodSystemName = "Shipping.FixedByWeightByTotal"
                }).FirstOrDefault();

                _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.SelectedShippingOptionAttribute, selectedShippingOption, _storeContext.CurrentStore.Id);

                errorResponse.ErrorCode = "2062";
                errorResponse.ErrorTitle = "SavedSelectedDeliveryMethod";
                errorResponse.ErrorDesc = "";

                var ModelJsonString = JsonConvert.SerializeObject(errorResponse);
                return new ErrorActionResult(ModelJsonString, HttpStatusCode.OK);
            }
            catch
            {
                errorResponse.ErrorCode = "2061";
                errorResponse.ErrorTitle = "InValidSelectedDeliveryMethod";
                errorResponse.ErrorDesc = "Error";

                var ModelJsonString2 = JsonConvert.SerializeObject(errorResponse);
                return new ErrorActionResult(ModelJsonString2, HttpStatusCode.OK);
            }
        }


        [HttpPost]
        [Route("/api/SaveSelectedPaymentMethod/{customerId}/{paymentMethodName}")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult SaveSelectedPaymentMethod(int customerId, string paymentMethodName)
        {
            var customer = CustomerService.GetCustomerById(customerId);

            if (customer == null)
            {
                var errorsJson = GetJsonErrorObject("2042", "InvalidCustomerID", "Error");
                return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
            }
            if (!CheckProceedToCheckoutFlag(customer))
            {
                var errorModel = new ErrorResponse
                {
                    ErrorCode = "2070",
                    ErrorDesc = "CartIsModified",
                    ErrorTitle = "Error"
                };
                var errorModelJson = JsonConvert.SerializeObject(errorModel);
                return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
            }

            var errorResponse = new ErrorResponse();
            var valid = SaveSelectedPaymentMethodName(customerId, paymentMethodName);
            if (!valid)
            {
                errorResponse.ErrorCode = "2063";
                errorResponse.ErrorTitle = "InValidSelectedPaymentMethod";
                errorResponse.ErrorDesc = "Error";

                var ModelJsonString1 = JsonConvert.SerializeObject(errorResponse);
                return new ErrorActionResult(ModelJsonString1, HttpStatusCode.OK);
            }
            else
            {
                errorResponse.ErrorCode = "2064";
                errorResponse.ErrorTitle = "SavedSelectedPaymentMethod";
                errorResponse.ErrorDesc = "";

                var ModelJsonString = JsonConvert.SerializeObject(errorResponse);
                return new ErrorActionResult(ModelJsonString, HttpStatusCode.OK);
            }

        }

        // Do not call this method when order total is zero.
        public bool SaveSelectedPaymentMethodName(int customerId, string paymentMethodName)
        {
            var errorResponse = new ErrorResponse();
            try
            {
                var customer = CustomerService.GetCustomerById(customerId);

                if (customer == null)
                {
                    return false;
                }

                var ValidPaymentMethods = new List<string> { "knet", "credit card", "cod" };

                var valid = ValidPaymentMethods.Contains(paymentMethodName.ToLower());

                if (!valid)
                {
                    errorResponse.ErrorCode = "2063";
                    errorResponse.ErrorTitle = "InValidSelectedPaymentMethod";
                    errorResponse.ErrorDesc = "Error";

                    var ModelJsonString1 = JsonConvert.SerializeObject(errorResponse);
                    //return new ErrorActionResult(ModelJsonString1, HttpStatusCode.OK);
                    return false;
                }

                _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.SelectedPaymentMethodAttribute, paymentMethodName, _storeContext.CurrentStore.Id);

                errorResponse.ErrorCode = "2064";
                errorResponse.ErrorTitle = "SavedSelectedPaymentMethod";
                errorResponse.ErrorDesc = "";

                var ModelJsonString = JsonConvert.SerializeObject(errorResponse);
                //return new ErrorActionResult(ModelJsonString, HttpStatusCode.OK);
                return true;
            }
            catch
            {
                errorResponse.ErrorCode = "2063";
                errorResponse.ErrorTitle = "InValidSelectedPaymentMethod";
                errorResponse.ErrorDesc = "Error";

                var ModelJsonString2 = JsonConvert.SerializeObject(errorResponse);
                //return new ErrorActionResult(ModelJsonString2, HttpStatusCode.OK);
                return false;
            }
        }

        [HttpGet]
        [Route("/api/getOrderTotals/{customerId}")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult GetOrderTotals(int CustomerId)
        {
            // We doesn't have to check for value because this is done by the order validator.
            _logger.Warning("START: Try block ..");
            try
            {
                _logger.Warning("START: CustomerService.GetCustomerById(CustomerId);");
                var customer = CustomerService.GetCustomerById(CustomerId);

                _logger.Warning("START:  if (customer == null)");
                if (customer == null)
                {
                    var errorModel = new OrderTotals
                    {
                        Data = null,
                        Error = new ErrorResponse
                        {
                            ErrorCode = "2042",
                            ErrorDesc = "InvalidCustomerID",
                            ErrorTitle = "Error"
                        }
                    };
                    var errorModelJson = JsonConvert.SerializeObject(errorModel);

                    return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
                }

                _logger.Warning("START: var cart = customer.ShoppingCartItems");
                var cart = customer.ShoppingCartItems
                           .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                           .LimitPerStore(_storeContext.CurrentStore.Id)
                           .ToList();

                _logger.Warning("START: !CheckProceedToCheckoutFlag(customer)");
                if (!CheckProceedToCheckoutFlag(customer) || cart == null || cart.Count == 0)
                {
                    var errorModel = new OrderTotals
                    {
                        Data = null,
                        Error = new ErrorResponse
                        {
                            ErrorCode = "2070",
                            ErrorDesc = "CartIsModified",
                            ErrorTitle = "Error"
                        }
                    };
                    var errorModelJson = JsonConvert.SerializeObject(errorModel);

                    return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
                }

                //Validate each cart item for any warning.
                var errorResponseMsg = ValidateCartItem(customer);
                if (!string.IsNullOrEmpty(errorResponseMsg))
                {
                    var errorModel = new OrderTotals
                    {
                        Data = null,
                        Error = new ErrorResponse
                        {
                            ErrorCode = "2060",
                            ErrorDesc = errorResponseMsg,
                            ErrorTitle = "Error"
                        }
                    };
                    var errorModelJson = JsonConvert.SerializeObject(errorModel);
                    return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
                }

                //to clear discounts applied if any
                _genericAttributeService.SaveAttribute<string>(customer, NopCustomerDefaults.DiscountCouponCodeAttribute, null);

                _logger.Warning("START: Get flag: SetRewardPoints? ");
                var flag = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.SetRewardPoints, _storeContext.CurrentStore.Id);

                _logger.Warning("END: SetRewardPoints = " + flag);

                OrderTotalsModel orderTotalsModel;

                _logger.Warning("START: _shoppingCartModelFactory.PrepareOrderTotalsModelForReward(cart, true, true);");

                //if (flag == "true")
                //    OrderTotalsModel = _shoppingCartModelFactory.PrepareOrderTotalsModelForReward(cart, true, true);
                //else
                //    OrderTotalsModel = _shoppingCartModelFactory.PrepareOrderTotalsModelForReward(cart, true, false);

                // Apply usable reward points by default.
                _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.SetRewardPoints, "true", _storeContext.CurrentStore.Id);
                orderTotalsModel = _shoppingCartModelFactory.PrepareOrderTotalsModelForReward(cart, true, true);

                if (orderTotalsModel.RedeemedRewardPointsAmount != null)
                    orderTotalsModel.RedeemedRewardPointsAmount = orderTotalsModel.RedeemedRewardPointsAmount.Replace("-", "");

                var errorResponse = new ErrorResponse
                {
                    ErrorCode = "2067",
                    ErrorDesc = "SuccessGetOrderTotal",
                    ErrorTitle = ""
                };

                _logger.Warning("START: Set OrderTotalsModel: Data/Error. ");
                var model = new OrderTotals
                {
                    Data = orderTotalsModel,
                    Error = errorResponse
                };

                _logger.Warning("START: JsonConvert.SerializeObject(model); ");
                var ModelJsonString = JsonConvert.SerializeObject(model);

                _logger.Warning("START: return - OrderTotalsModel;" + Convert.ToString(ModelJsonString.Substring(0, 20)));
                return new ErrorActionResult(ModelJsonString, HttpStatusCode.OK);

            }
            catch (Exception ex)
            {
                _logger.Warning("Catch - START: Set OrderTotalsModel: Data=null/Error.  ");
                var errorModel = new OrderTotals
                {
                    Data = null,
                    Error = new ErrorResponse
                    {
                        ErrorCode = "2068",
                        ErrorDesc = "ErrorGetOrderTotal: Reason: " + Convert.ToString(ex.Message),
                        ErrorTitle = "Error"
                    }
                };
                _logger.Warning("Catch - START: JsonConvert.SerializeObject(model); ");
                var errorModelJson = JsonConvert.SerializeObject(errorModel);

                _logger.Warning("Catch - START: return - OrderTotalsModel;" + Convert.ToString(errorModelJson.Substring(0, 20)));
                return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
            }
        }


        [HttpPost]
        [Route("/api/orders")]
        [ProducesResponseType(typeof(OrdersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult CreateOrder([ModelBinder(typeof(JsonModelBinder<OrderDto>))] Delta<OrderDto> orderDelta)
        {
            var customer = new Customer();
            try
            {
                _logger.Warning("START: !ModelState.IsValid");
                // Here we display the errors if the validation has failed at some point.
                if (!ModelState.IsValid)
                {
                    _logger.Error("Error caught in ModelState.IsValid; ErrorCount = " + Convert.ToString(ModelState.ErrorCount));
                    var ordersRootObject1 = new OrdersRootObject
                    {
                        Orders = null,
                        Error = new ErrorResponse
                        {
                            ErrorCode = "2071",
                            ErrorDesc = "ErrorCreatedNewOrder",
                            ErrorTitle = "SpecificReason: ModelState.IsValid == false"
                        }
                    };
                    var ModelJsonString1 = JsonConvert.SerializeObject(ordersRootObject1);
                    return new ErrorActionResult(ModelJsonString1, HttpStatusCode.OK);
                }

                // We doesn't have to check for value because this is done by the order validator.
                customer = CustomerService.GetCustomerById(orderDelta.Dto.CustomerId.Value);

                if (customer == null)
                {
                    var errorModel = new OrderTotals
                    {
                        Data = null,
                        Error = new ErrorResponse
                        {
                            ErrorCode = "2042",
                            ErrorDesc = "InvalidCustomerID",
                            ErrorTitle = "Error"
                        }
                    };
                    var errorModelJson = JsonConvert.SerializeObject(errorModel);

                    return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
                }

                _logger.Warning("START: CheckProceedToCheckoutFlag");
                if (!CheckProceedToCheckoutFlag(customer))
                {
                    var errorModel = new OrderTotals
                    {
                        Data = null,
                        Error = new ErrorResponse
                        {
                            ErrorCode = "2070",
                            ErrorDesc = "CartIsModified",
                            ErrorTitle = "Error"
                        }
                    };
                    var errorModelJson = JsonConvert.SerializeObject(errorModel);

                    return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
                }

                //Validate each cart item for any warning.
                var errorResponseMsg = ValidateCartItem(customer);
                if (!string.IsNullOrEmpty(errorResponseMsg))
                {
                    var errorModel = new OrderTotals
                    {
                        Data = null,
                        Error = new ErrorResponse
                        {
                            ErrorCode = "2060",
                            ErrorDesc = errorResponseMsg,
                            ErrorTitle = "Error"
                        }
                    };
                    var errorModelJson = JsonConvert.SerializeObject(errorModel);
                    return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
                }

                var shippingRequired = false;

                _logger.Warning("START: if (orderDelta.Dto.OrderItems != null)");
                if (orderDelta.Dto.OrderItems != null)
                {
                    var shouldReturnError = AddOrderItemsToCart(orderDelta.Dto.OrderItems, customer, orderDelta.Dto.StoreId ?? _storeContext.CurrentStore.Id);
                    if (shouldReturnError)
                    {
                        return Error(HttpStatusCode.BadRequest);
                    }

                    shippingRequired = IsShippingAddressRequired(orderDelta.Dto.OrderItems);
                }

                _logger.Warning("START: if (shippingRequired)");
                if (shippingRequired)
                {
                    var isValid = true;

                    isValid &= SetShippingOption(orderDelta.Dto.ShippingRateComputationMethodSystemName,
                                                orderDelta.Dto.ShippingMethod,
                                                orderDelta.Dto.StoreId ?? _storeContext.CurrentStore.Id,
                                                customer,
                                                BuildShoppingCartItemsFromOrderItemDtos(orderDelta.Dto.OrderItems.ToList(),
                                                                                        customer.Id,
                                                                                        orderDelta.Dto.StoreId ?? _storeContext.CurrentStore.Id));

                    if (!isValid)
                    {
                        return Error(HttpStatusCode.BadRequest);
                    }
                }

                _logger.Warning("START: _factory.Initialize();");
                var newOrder = _factory.Initialize();

                _logger.Warning("START: orderDelta.Merge(newOrder);");
                orderDelta.Merge(newOrder);

                customer.BillingAddress = newOrder.BillingAddress;
                customer.ShippingAddress = newOrder.ShippingAddress;

                // If the customer has something in the cart it will be added too. Should we clear the cart first? 
                newOrder.Customer = customer;

                // The default value will be the currentStore.id, but if it isn't passed in the json we need to set it by hand.
                if (!orderDelta.Dto.StoreId.HasValue)
                {
                    newOrder.StoreId = _storeContext.CurrentStore.Id;
                }

                _logger.Warning("START: PlaceOrder(newOrder, customer);");

                int WarehouseId = 0;
                if (orderDelta.Dto.WarehouseId.HasValue)
                {
                    WarehouseId = orderDelta.Dto.WarehouseId.Value;
                }
                var placeOrderResult = PlaceOrder(newOrder, customer, orderDelta.Dto.StoreCode, WarehouseId, orderDelta.Dto.CouponCode, Convert.ToDecimal(orderDelta.Dto.CouponValue), Convert.ToInt32(orderDelta.Dto.RedeemedRewardPoints), Convert.ToDecimal(orderDelta.Dto.RedeemedRewardPointsValue));

                _logger.Warning("START: if (!placeOrderResult.Success)");
                if (!placeOrderResult.Success)
                {
                    foreach (var error in placeOrderResult.Errors)
                    {
                        ModelState.AddModelError("order placement", error);
                    }

                    var logDetails = "";//ModelState.Root.Errors.FirstOrDefault().ErrorMessage.ToString();                     
                    logDetails = placeOrderResult.Errors.FirstOrDefault();

                    _logger.Error("Error! Order Failed. Reason: " + logDetails);

                    //return new ErrorActionResult(errorJson, HttpStatusCode.OK);

                    //return Error(HttpStatusCode.BadRequest);

                    var ordersRootObjectError = new OrdersRootObject
                    {
                        Orders = null,
                        Error = new ErrorResponse
                        {
                            ErrorCode = "2071",
                            ErrorDesc = "ErrorCreatedNewOrder. Reason: " + logDetails,
                            ErrorTitle = "SpecificReason"
                        }
                    };
                    var ModelJsonError = JsonConvert.SerializeObject(ordersRootObjectError);
                    return new ErrorActionResult(ModelJsonError, HttpStatusCode.OK);

                }

                _logger.Warning("START: CustomerActivityService.InsertActivity(AddNewOrder");
                CustomerActivityService.InsertActivity("AddNewOrder",
                     LocalizationService.GetResource("ActivityLog.AddNewOrder"), newOrder);

                var ordersRootObject = new OrdersRootObject();

                _logger.Warning("START: _dtoHelper.PrepareOrderDTO(placeOrderResult.PlacedOrder);");
                var placedOrderDto = _dtoHelper.PrepareOrderDTO(placeOrderResult.PlacedOrder);

                _logger.Warning("START: ordersRootObject.Orders.Add(placedOrderDto);");

                ordersRootObject.Orders.Add(placedOrderDto);

                ordersRootObject.Error = new ErrorResponse
                {
                    ErrorCode = "2072",
                    ErrorDesc = "SuccessCreatedNewOrder",
                    ErrorTitle = ""
                };
                //To avoid error "Self referencing loop detected for property".
                ordersRootObject.Orders.ToList().ForEach(o => o.OrderItems.ToList().ForEach(oi => oi.Product.ProductWarehouseInventory = null));
                var ModelJsonString = JsonConvert.SerializeObject(ordersRootObject);
                _logger.Warning("START: return JSON Response.");

                #region Send email to customer


                if (newOrder.PaymentMethodSystemName.Equals("COD", StringComparison.InvariantCultureIgnoreCase))
                {
                    try
                    {
                        var orderPlacedCustomerNotificationQueuedEmailIds = _workflowMessageService
                            //.SendOrderPlacedCustomerNotification(newOrder, newOrder.CustomerLanguageId);
                            .SendOrderPlacedCustomerNotificationAlmailem(placeOrderResult.PlacedOrder, placeOrderResult.PlacedOrder.CustomerLanguageId);
                        if (orderPlacedCustomerNotificationQueuedEmailIds.Any())
                            AddOrderNote(placeOrderResult.PlacedOrder, $"\"Order placed\" email (to customer) has been queued. Queued email identifiers: {string.Join(", ", orderPlacedCustomerNotificationQueuedEmailIds)}.");
                    }
                    catch (Exception ex)
                    {
                        _logger.Error("Error occurred while queuing email for OrderID\t:" + placeOrderResult.PlacedOrder.Id, ex);
                    }
                }

                #endregion

                return new ErrorActionResult(ModelJsonString, HttpStatusCode.OK);

            }
            catch (Exception ex)
            {
                _logger.Error("Error caught in Catch block", ex, customer);
                var ordersRootObject = new OrdersRootObject
                {
                    Orders = null,
                    Error = new ErrorResponse
                    {
                        ErrorCode = "2071",
                        ErrorDesc = "ErrorCreatedNewOrder. Reason: " + ex.Message.ToString(),
                        ErrorTitle = "Error in Catch"
                    }
                };
                var ModelJsonString = JsonConvert.SerializeObject(ordersRootObject);
                return new ErrorActionResult(ModelJsonString, HttpStatusCode.OK);

            }
        }

        /// <summary>
        /// Add order note
        /// </summary>
        /// <param name="order">Order</param>
        /// <param name="note">Note text</param>
        protected virtual void AddOrderNote(Order order, string note)
        {
            order.OrderNotes.Add(new OrderNote
            {
                Note = note,
                DisplayToCustomer = false,
                CreatedOnUtc = DateTime.UtcNow
            });

            _orderService.UpdateOrder(order);
        }

        [HttpDelete]
        [Route("/api/orders/{id}")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult DeleteOrder(int id)
        {
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }

            var orderToDelete = _orderApiService.GetOrderById(id);

            if (orderToDelete == null)
            {
                return Error(HttpStatusCode.NotFound, "order", "not found");
            }

            _orderProcessingService.DeleteOrder(orderToDelete);

            //activity log
            CustomerActivityService.InsertActivity("DeleteOrder", LocalizationService.GetResource("ActivityLog.DeleteOrder"), orderToDelete);

            return new RawJsonActionResult("{}");
        }

        //[HttpPut]
        //[Route("/api/cancelOrder/{orderId}")]
        //[ProducesResponseType(typeof(OrdersRootObject), (int)HttpStatusCode.OK)]
        //[ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        //[ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        //[ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        //[ProducesResponseType(typeof(ErrorsRootObject), 422)]
        //public IActionResult CancelOrder(int orderId)
        //{
        //    var currentOrder = _orderApiService.GetOrderById(orderId);

        //    if (currentOrder == null)
        //    {
        //        var ordersErrorObject = new OrdersRootObject
        //        {
        //            Error = new ErrorResponse
        //            {
        //                ErrorCode = "2076",
        //                ErrorDesc = "SuccessCancellingOrder",
        //                ErrorTitle = ""
        //            },
        //            Orders = null
        //        };

        //        var errorJson = JsonFieldsSerializer.Serialize(ordersErrorObject, string.Empty);
        //        return new ErrorActionResult(errorJson, HttpStatusCode.OK);
        //    }

        //    currentOrder.OrderStatus = OrderStatus.Cancelled;
        //    currentOrder.UpdatedOnUtc = DateTime.UtcNow;

        //    _orderService.UpdateOrder(currentOrder);

        //    CustomerActivityService.InsertActivity("UpdateOrder",
        //         LocalizationService.GetResource("ActivityLog.UpdateOrder"), currentOrder);

        //    var ordersRootObject = new OrdersRootObject();

        //    var placedOrderDto = _dtoHelper.PrepareOrderDTO(currentOrder);

        //    ordersRootObject.Orders.Add(placedOrderDto);
        //    ordersRootObject.Error = new ErrorResponse
        //    {
        //        ErrorCode = "2076",
        //        ErrorDesc = "SuccessCancellingOrder",
        //        ErrorTitle=""
        //    };

        //    var json = JsonFieldsSerializer.Serialize(ordersRootObject, string.Empty);
        //    return new ErrorActionResult(json, HttpStatusCode.OK);
        //}

        //[HttpPut]
        //[Route("/api/cancelOrder/{orderId}")]
        //[ProducesResponseType(typeof(OrdersRootObject), (int)HttpStatusCode.OK)]
        //[ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        //[ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        //[ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        //[ProducesResponseType(typeof(ErrorsRootObject), 422)]
        //public IActionResult CancelOrder(int orderId)
        //{
        //    //if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
        //    //return AccessDeniedView();

        //    //try to get an order with the specified id
        //    GenericJsonResponse genericJsonResponse = new GenericJsonResponse();
        //    var order = _orderService.GetOrderById(orderId);
        //    ErrorResponse Error = new ErrorResponse();

        //    if (order == null)
        //    {
        //        Error.ErrorCode = "2075";
        //        Error.ErrorTitle = "Error";
        //        Error.ErrorDesc = "ErrorCancellingOrder";
        //        genericJsonResponse.Error = Error;
        //        genericJsonResponse.Data = null;
        //        return Json(genericJsonResponse);
        //    }
        //    //return RedirectToAction("List");

        //    try
        //    {
        //        _orderProcessingService.CancelOrder(order, false);
        //        //LogEditOrder(order.Id);
        //        IList<OrderDto> ordersForCustomer =
        //        _orderApiService.GetOrdersByCustomerId(order.CustomerId)
        //        .Select(x => _dtoHelper.PrepareOrderDTO(x))
        //        .Where(oOrder => oOrder.OrderStatus.ToUpper() != OrderStatus.Pending.ToString().ToUpper())
        //        .ToList();

        //        var ordersRootObject = new OrdersRootObject()
        //        {
        //            Orders = ordersForCustomer
        //        };
        //        //prepare model
        //        //var model = _orderModelFactory.PrepareOrderModel(null, order);
        //        //return Ok(ordersRootObject);
        //        //return View(model);
        //        Error.ErrorCode = "2076";
        //        Error.ErrorTitle = "";
        //        Error.ErrorDesc = "SuccessCancellingOrder";
        //        genericJsonResponse.Error = Error;
        //        genericJsonResponse.Data = ordersRootObject;
        //        return Json(genericJsonResponse);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex.Message, ex);

        //        Error.ErrorCode = "2075";
        //        Error.ErrorTitle = "Error";
        //        Error.ErrorDesc = "ErrorCancellingOrder";
        //        genericJsonResponse.Error = Error;
        //        genericJsonResponse.Data = null;
        //        return Json(genericJsonResponse);
        //    }
        //}

        [HttpPost]
        [Route("/api/cancelOrder/{orderId}")]
        [ProducesResponseType(typeof(OrdersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult CancelOrder(int orderId)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
            //return AccessDeniedView();

            //try to get an order with the specified id
            GenericJsonResponse genericJsonResponse = new GenericJsonResponse();
            var order = _orderService.GetOrderById(orderId);
            ErrorResponse Error = new ErrorResponse();

            if (order == null)
            {
                Error.ErrorCode = "2075";
                Error.ErrorTitle = "Error";
                Error.ErrorDesc = "ErrorCancellingOrder";
                genericJsonResponse.Error = Error;
                genericJsonResponse.Data = null;
                return Json(genericJsonResponse);
            }
            //return RedirectToAction("List");

            try
            {
                _orderProcessingService.CancelOrder(order, false);
                //LogEditOrder(order.Id);
                //IList<OrderDto> ordersForCustomer =
                //_orderApiService.GetOrdersByCustomerId(order.CustomerId)
                //.Select(x => _dtoHelper.PrepareOrderDTO(x))
                //.Where(oOrder => oOrder.OrderStatus.ToUpper() != OrderStatus.Pending.ToString().ToUpper())
                //.ToList();

                //var ordersRootObject = new OrdersRootObject()
                //{
                //    Orders = ordersForCustomer
                //};
                //prepare model
                //var model = _orderModelFactory.PrepareOrderModel(null, order);
                //return Ok(ordersRootObject);
                //return View(model);
                Error.ErrorCode = "2076";
                Error.ErrorTitle = "Success";
                Error.ErrorDesc = "SuccessCancellingOrder";
                genericJsonResponse.Error = Error;
                genericJsonResponse.Data = null;
                //genericJsonResponse.Data = ordersRootObject;
                return Json(genericJsonResponse);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message, ex);

                Error.ErrorCode = "2075";
                Error.ErrorTitle = "Error";
                Error.ErrorDesc = "ErrorCancellingOrder";
                genericJsonResponse.Error = Error;
                genericJsonResponse.Data = null;
                return Json(genericJsonResponse);
            }
        }

        [HttpPut]
        [Route("/api/orders/{id}")]
        [ProducesResponseType(typeof(OrdersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult UpdateOrder([ModelBinder(typeof(JsonModelBinder<OrderDto>))] Delta<OrderDto> orderDelta)
        {
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            var currentOrder = _orderApiService.GetOrderById(orderDelta.Dto.Id);

            if (currentOrder == null)
            {
                return Error(HttpStatusCode.NotFound, "order", "not found");
            }

            var customer = currentOrder.Customer;

            var shippingRequired = currentOrder.OrderItems.Any(item => !item.Product.IsFreeShipping);

            if (shippingRequired)
            {
                var isValid = true;

                if (!string.IsNullOrEmpty(orderDelta.Dto.ShippingRateComputationMethodSystemName) ||
                    !string.IsNullOrEmpty(orderDelta.Dto.ShippingMethod))
                {
                    var storeId = orderDelta.Dto.StoreId ?? _storeContext.CurrentStore.Id;

                    isValid &= SetShippingOption(orderDelta.Dto.ShippingRateComputationMethodSystemName ?? currentOrder.ShippingRateComputationMethodSystemName,
                        orderDelta.Dto.ShippingMethod,
                        storeId,
                        customer, BuildShoppingCartItemsFromOrderItems(currentOrder.OrderItems.ToList(), customer.Id, storeId));
                }

                if (isValid)
                {
                    currentOrder.ShippingMethod = orderDelta.Dto.ShippingMethod;
                }
                else
                {
                    return Error(HttpStatusCode.BadRequest);
                }
            }

            orderDelta.Merge(currentOrder);

            customer.BillingAddress = currentOrder.BillingAddress;
            customer.ShippingAddress = currentOrder.ShippingAddress;

            _orderService.UpdateOrder(currentOrder);

            CustomerActivityService.InsertActivity("UpdateOrder",
                 LocalizationService.GetResource("ActivityLog.UpdateOrder"), currentOrder);

            var ordersRootObject = new OrdersRootObject();

            var placedOrderDto = _dtoHelper.PrepareOrderDTO(currentOrder); 
            placedOrderDto.ShippingMethod = orderDelta.Dto.ShippingMethod;

            ordersRootObject.Orders.Add(placedOrderDto);

            var json = JsonFieldsSerializer.Serialize(ordersRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        private bool SetShippingOption(string shippingRateComputationMethodSystemName, string shippingOptionName, int storeId, Customer customer, List<ShoppingCartItem> shoppingCartItems)
        {
            var isValid = true;

            if (string.IsNullOrEmpty(shippingRateComputationMethodSystemName))
            {
                isValid = false;

                ModelState.AddModelError("shipping_rate_computation_method_system_name",
                    "Please provide shipping_rate_computation_method_system_name");
            }
            else if (string.IsNullOrEmpty(shippingOptionName))
            {
                isValid = false;

                ModelState.AddModelError("shipping_option_name", "Please provide shipping_option_name");
            }
            else
            {
                var shippingOptionResponse = _shippingService.GetShippingOptions(shoppingCartItems, customer.ShippingAddress, customer,
                        shippingRateComputationMethodSystemName, storeId);

                if (shippingOptionResponse.Success)
                {
                    var shippingOptions = shippingOptionResponse.ShippingOptions.ToList();

                    var shippingOption = shippingOptions
                        .Find(so => !string.IsNullOrEmpty(so.Name) && so.Name.Equals(shippingOptionName, StringComparison.InvariantCultureIgnoreCase));

                    _genericAttributeService.SaveAttribute(customer,
                        NopCustomerDefaults.SelectedShippingOptionAttribute,
                        shippingOption, storeId);
                }
                else
                {
                    isValid = false;

                    foreach (var errorMessage in shippingOptionResponse.Errors)
                    {
                        ModelState.AddModelError("shipping_option", errorMessage);
                    }
                }
            }

            return isValid;
        }

        private List<ShoppingCartItem> BuildShoppingCartItemsFromOrderItems(List<OrderItem> orderItems, int customerId, int storeId)
        {
            var shoppingCartItems = new List<ShoppingCartItem>();

            foreach (var orderItem in orderItems)
            {
                shoppingCartItems.Add(new ShoppingCartItem()
                {
                    ProductId = orderItem.ProductId,
                    CustomerId = customerId,
                    Quantity = orderItem.Quantity,
                    RentalStartDateUtc = orderItem.RentalStartDateUtc,
                    RentalEndDateUtc = orderItem.RentalEndDateUtc,
                    StoreId = storeId,
                    Product = orderItem.Product,
                    ShoppingCartType = ShoppingCartType.ShoppingCart
                });
            }

            return shoppingCartItems;
        }

        private List<ShoppingCartItem> BuildShoppingCartItemsFromOrderItemDtos(List<OrderItemDto> orderItemDtos, int customerId, int storeId)
        {
            var shoppingCartItems = new List<ShoppingCartItem>();

            foreach (var orderItem in orderItemDtos)
            {
                if (orderItem.ProductId != null)
                {
                    shoppingCartItems.Add(new ShoppingCartItem()
                    {
                        ProductId = orderItem.ProductId.Value, // required field
                        CustomerId = customerId,
                        Quantity = orderItem.Quantity ?? 1,
                        RentalStartDateUtc = orderItem.RentalStartDateUtc,
                        RentalEndDateUtc = orderItem.RentalEndDateUtc,
                        StoreId = storeId,
                        Product = _productService.GetProductById(orderItem.ProductId.Value),
                        ShoppingCartType = ShoppingCartType.ShoppingCart
                    });
                }
            }

            return shoppingCartItems;
        }

        private PlaceOrderResult PlaceOrder(Order newOrder, Customer customer, string StoreCode, int WarehouseId, string CouponCode, decimal CouponValue, int RedeemRewardPoints, decimal RedeemRewardPointsValue)
        {
            var processPaymentRequest = new ProcessPaymentRequest
            {
                StoreId = newOrder.StoreId,
                CustomerId = customer.Id,
                PaymentMethodSystemName = newOrder.PaymentMethodSystemName
            };


            var placeOrderResult = _orderProcessingService.PlaceOrder(processPaymentRequest, CouponCode, CouponValue, RedeemRewardPoints, RedeemRewardPointsValue, StoreCode, WarehouseId);

            return placeOrderResult;
        }

        private bool IsShippingAddressRequired(ICollection<OrderItemDto> orderItems)
        {
            var shippingAddressRequired = false;

            foreach (var orderItem in orderItems)
            {
                if (orderItem.ProductId != null)
                {
                    var product = _productService.GetProductById(orderItem.ProductId.Value);

                    shippingAddressRequired |= product.IsShipEnabled;
                }
            }

            return shippingAddressRequired;
        }

        private bool AddOrderItemsToCart(ICollection<OrderItemDto> orderItems, Customer customer, int storeId)
        {
            var shouldReturnError = false;

            foreach (var orderItem in orderItems)
            {
                if (orderItem.ProductId != null)
                {
                    var product = _productService.GetProductById(orderItem.ProductId.Value);

                    if (!product.IsRental)
                    {
                        orderItem.RentalStartDateUtc = null;
                        orderItem.RentalEndDateUtc = null;
                    }

                    var attributesXml = _productAttributeConverter.ConvertToXml(orderItem.Attributes.ToList(), product.Id);

                    var errors = _shoppingCartService.AddToCart(customer, product,
                        ShoppingCartType.ShoppingCart, storeId, attributesXml,
                        0M, orderItem.RentalStartDateUtc, orderItem.RentalEndDateUtc,
                        orderItem.Quantity ?? 1);

                    if (errors.Count > 0)
                    {
                        foreach (var error in errors)
                        {
                            ModelState.AddModelError("order", error);
                        }

                        shouldReturnError = true;
                    }
                }
            }

            return shouldReturnError;
        }

        #region Discount And RewardPoints     

        //Apply & Remove Reward Points..
        [HttpPost]
        [Route("/api/applyRewardPoints/{customerId}/{setRewardPoints}")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult ApplyRewardPoints(int CustomerId, string SetRewardPoints)
        {
            // We doesn't have to check for value because this is done by the order validator.
            try
            {
                var customer = CustomerService.GetCustomerById(CustomerId);

                if (customer == null)
                {
                    var errorModel = new OrderTotals
                    {
                        Data = null,
                        Error = new ErrorResponse
                        {
                            ErrorCode = "2042",
                            ErrorDesc = "InvalidCustomerID",
                            ErrorTitle = "Error"
                        }
                    };
                    var errorModelJson = JsonConvert.SerializeObject(errorModel);

                    return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
                }

                var cart = customer.ShoppingCartItems
                           .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                           .LimitPerStore(_storeContext.CurrentStore.Id)
                           .ToList();

                if (!CheckProceedToCheckoutFlag(customer) || cart == null || cart.Count == 0)
                {
                    var errorModel = new OrderTotals
                    {
                        Data = null,
                        Error = new ErrorResponse
                        {
                            ErrorCode = "2070",
                            ErrorDesc = "CartIsModified",
                            ErrorTitle = "Error"
                        }
                    };
                    var errorModelJson = JsonConvert.SerializeObject(errorModel);

                    return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
                }

                OrderTotalsModel orderTotalsModel;

                if (SetRewardPoints == "true")
                {
                    orderTotalsModel = _shoppingCartModelFactory.PrepareOrderTotalsModelForReward(cart, true, true);
                    _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.SetRewardPoints, "true", _storeContext.CurrentStore.Id);
                }
                else
                {
                    orderTotalsModel = _shoppingCartModelFactory.PrepareOrderTotalsModelForReward(cart, true, false);
                    _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.SetRewardPoints, "", _storeContext.CurrentStore.Id);
                }

                ErrorResponse errorResponse;
                if (SetRewardPoints == "true")
                {
                    errorResponse = new ErrorResponse
                    {
                        ErrorCode = "2077",
                        ErrorDesc = "SuccessApplyRewardPoints",
                        ErrorTitle = ""
                    };
                }
                else
                {
                    errorResponse = new ErrorResponse
                    {
                        ErrorCode = "2079",
                        ErrorDesc = "SuccessRemoveRewardPoints",
                        ErrorTitle = ""
                    };
                }

                if (orderTotalsModel.RedeemedRewardPointsAmount != null)
                    orderTotalsModel.RedeemedRewardPointsAmount = orderTotalsModel.RedeemedRewardPointsAmount.Replace("-", "");

                var model = new OrderTotals
                {
                    Data = orderTotalsModel,
                    Error = errorResponse
                };

                var ModelJsonString = JsonConvert.SerializeObject(model);
                return new ErrorActionResult(ModelJsonString, HttpStatusCode.OK);

            }
            catch (Exception ex)
            {
                OrderTotals errorModel;
                if (SetRewardPoints == "true")
                {
                    errorModel = new OrderTotals
                    {
                        Data = null,
                        Error = new ErrorResponse
                        {
                            ErrorCode = "2078",
                            ErrorDesc = "ErrorApplyRewardPoints: Reason: " + Convert.ToString(ex.Message),
                            ErrorTitle = "Error"
                        }
                    };
                }
                else
                {
                    errorModel = new OrderTotals
                    {
                        Data = null,
                        Error = new ErrorResponse
                        {
                            ErrorCode = "2080",
                            ErrorDesc = "ErrorRemoveRewardPoints: Reason: " + Convert.ToString(ex.Message),
                            ErrorTitle = "Error"
                        }
                    };
                }
                var errorModelJson = JsonConvert.SerializeObject(errorModel);
                return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
            }
        }

        [HttpPost]
        [Route("/api/applyDiscountCouponCode/{customerId}/{discountCouponCode}")]
        [ProducesResponseType(typeof(OrdersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult ApplyDiscountCouponCode(int CustomerId, string DiscountCouponCode)
        {
            try
            {
                //  To, reset checkout data.
                var customer = CustomerService.GetCustomerById(CustomerId);

                if (customer == null)
                {
                    var errorModel = new OrderTotals
                    {
                        Data = null,
                        Error = new ErrorResponse
                        {
                            ErrorCode = "2042",
                            ErrorDesc = "InvalidCustomerID",
                            ErrorTitle = "Error"
                        }
                    };
                    var errorModelJson = JsonConvert.SerializeObject(errorModel);

                    return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
                }

                //trim
                if (DiscountCouponCode == null)
                {
                    var errorModel = new OrderTotals
                    {
                        Data = null,
                        Error = new ErrorResponse
                        {
                            ErrorCode = "2090",
                            ErrorDesc = "InvalidCouponCode",
                            ErrorTitle = "Error"
                        }
                    };
                    var errorModelJson = JsonConvert.SerializeObject(errorModel);
                    return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
                }
                DiscountCouponCode = DiscountCouponCode.Trim();

                //cart
                var cart = customer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .LimitPerStore(_storeContext.CurrentStore.Id)
                    .ToList();

                if (!CheckProceedToCheckoutFlag(customer) || cart == null || cart.Count == 0)
                {
                    var errorModel = new OrderTotals
                    {
                        Data = null,
                        Error = new ErrorResponse
                        {
                            ErrorCode = "2070",
                            ErrorDesc = "CartIsModified",
                            ErrorTitle = "Error"
                        }
                    };
                    var errorModelJson = JsonConvert.SerializeObject(errorModel);

                    return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
                }

                var model = new ShoppingCartModel();
                if (!string.IsNullOrWhiteSpace(DiscountCouponCode))
                {
                    //we find even hidden records here. this way we can display a user-friendly message if it's expired
                    var discounts = DiscountService.GetAllDiscountsForCaching(couponCode: DiscountCouponCode, showHidden: true)
                        .Where(d => d.RequiresCouponCode)
                        .ToList();
                    if (discounts.Any())
                    {
                        var userErrors = new List<string>();
                        var anyValidDiscount = discounts.Any(discount =>
                        {
                            var validationResult = DiscountService.ValidateDiscount(discount, customer, new[] { DiscountCouponCode });
                            userErrors.AddRange(validationResult.Errors);

                            return validationResult.IsValid;
                        });

                        if (anyValidDiscount)
                        {
                            //valid
                            _customerService.ApplyDiscountCouponCode(customer, DiscountCouponCode);
                            model.DiscountBox.Messages.Add(LocalizationService.GetResource("ShoppingCart.DiscountCouponCode.Applied"));
                            model.DiscountBox.IsApplied = true;
                        }
                        else
                        {
                            if (userErrors.Any())
                            {
                                //some user errors
                                model.DiscountBox.Messages = userErrors;
                                var errorModel = new OrderTotals
                                {
                                    Data = null,
                                    Error = new ErrorResponse
                                    {
                                        ErrorCode = "2090",
                                        ErrorDesc = "InvalidCouponCode",
                                        ErrorTitle = "Error"
                                    }
                                };
                                var errorModelJson = JsonConvert.SerializeObject(errorModel);
                                return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
                            }
                            else
                            {
                                //general error text
                                model.DiscountBox.Messages.Add(LocalizationService.GetResource("ShoppingCart.DiscountCouponCode.WrongDiscount"));
                                var errorModel = new OrderTotals
                                {
                                    Data = null,
                                    Error = new ErrorResponse
                                    {
                                        ErrorCode = "2090",
                                        ErrorDesc = "InvalidCouponCode",
                                        ErrorTitle = "Error"
                                    }
                                };
                                var errorModelJson = JsonConvert.SerializeObject(errorModel);
                                return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
                            }
                        }
                    }
                    else
                    {
                        //discount cannot be found
                        model.DiscountBox.Messages.Add(LocalizationService.GetResource("ShoppingCart.DiscountCouponCode.WrongDiscount"));
                        var errorModel = new OrderTotals
                        {
                            Data = null,
                            Error = new ErrorResponse
                            {
                                ErrorCode = "2090",
                                ErrorDesc = "InvalidCouponCode",
                                ErrorTitle = "Error"
                            }
                        };
                        var errorModelJson = JsonConvert.SerializeObject(errorModel);
                        return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
                    }

                }
                else
                {
                    //empty coupon code
                    model.DiscountBox.Messages.Add(LocalizationService.GetResource("ShoppingCart.DiscountCouponCode.WrongDiscount"));
                    var errorModel = new OrderTotals
                    {
                        Data = null,
                        Error = new ErrorResponse
                        {
                            ErrorCode = "2090",
                            ErrorDesc = "InvalidCouponCode",
                            ErrorTitle = "Error"
                        }
                    };
                    var errorModelJson = JsonConvert.SerializeObject(errorModel);
                    return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
                }

                model = _shoppingCartModelFactory.PrepareShoppingCartModel(model, cart);

                var flag = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.SetRewardPoints, _storeContext.CurrentStore.Id);

                OrderTotalsModel orderTotalsModel;
                if (flag == "true")
                    orderTotalsModel = _shoppingCartModelFactory.PrepareOrderTotalsModelForReward(cart, true, true);
                else
                    orderTotalsModel = _shoppingCartModelFactory.PrepareOrderTotalsModelForReward(cart, true, false);

                var errorResponse = new ErrorResponse
                {
                    ErrorCode = "2086",
                    ErrorDesc = "SuccessApplyDiscountCouponCode",
                    ErrorTitle = ""
                };

                if (orderTotalsModel.RedeemedRewardPointsAmount != null)
                    orderTotalsModel.RedeemedRewardPointsAmount = orderTotalsModel.RedeemedRewardPointsAmount.Replace("-", "");

                var model1 = new OrderTotals
                {
                    Data = orderTotalsModel,
                    Error = errorResponse
                };
                var ModelJsonString = JsonConvert.SerializeObject(model1);
                return new ErrorActionResult(ModelJsonString, HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                var errorModel = new OrderTotals
                {
                    Data = null,
                    Error = new ErrorResponse
                    {
                        ErrorCode = "2087",
                        ErrorDesc = "ErrorApplyDiscountCouponCode: Reason: " + Convert.ToString(ex.Message),
                        ErrorTitle = "Error"
                    }
                };
                var errorModelJson = JsonConvert.SerializeObject(errorModel);
                return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
            }
        }

        [HttpPost]
        [Route("/api/removeDiscountCouponCode/{customerId}/{discountCouponCode}")]
        [ProducesResponseType(typeof(OrdersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public virtual IActionResult RemoveDiscountCoupon(int CustomerId, string DiscountCouponCode)
        {
            try
            {
                var customer = CustomerService.GetCustomerById(CustomerId);

                if (customer == null)
                {
                    var errorModel = new OrderTotals
                    {
                        Data = null,
                        Error = new ErrorResponse
                        {
                            ErrorCode = "2042",
                            ErrorDesc = "InvalidCustomerID",
                            ErrorTitle = "Error"
                        }
                    };
                    var errorModelJson = JsonConvert.SerializeObject(errorModel);
                    return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
                }

                var cart = customer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .LimitPerStore(_storeContext.CurrentStore.Id)
                    .ToList();

                if (!CheckProceedToCheckoutFlag(customer) || cart == null || cart.Count == 0)
                {
                    var errorModel = new OrderTotals
                    {
                        Data = null,
                        Error = new ErrorResponse
                        {
                            ErrorCode = "2070",
                            ErrorDesc = "CartIsModified",
                            ErrorTitle = "Error"
                        }
                    };
                    var errorModelJson = JsonConvert.SerializeObject(errorModel);
                    return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
                }

                var model = new ShoppingCartModel();

                //trim
                if (DiscountCouponCode == null)
                {
                    var errorModel = new OrderTotals
                    {
                        Data = null,
                        Error = new ErrorResponse
                        {
                            ErrorCode = "2090",
                            ErrorDesc = "InvalidCouponCode",
                            ErrorTitle = "Error"
                        }
                    };
                    var errorModelJson = JsonConvert.SerializeObject(errorModel);
                    return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
                }
                DiscountCouponCode = DiscountCouponCode.Trim();

                //get discount identifier
                if (!string.IsNullOrWhiteSpace(DiscountCouponCode))
                {
                    bool Found = false;
                    foreach (var code in _customerService.ParseAppliedDiscountCouponCodes(customer))
                    {
                        if (code.Equals(DiscountCouponCode.ToLower()))
                        {
                            _customerService.RemoveDiscountCouponCode(customer, DiscountCouponCode);
                            Found = true;
                            break;
                        }
                    }
                    if (!Found)
                    {
                        //Coupon code not found in list of applied coupons
                        var errorModel = new OrderTotals
                        {
                            Data = null,
                            Error = new ErrorResponse
                            {
                                ErrorCode = "2090",
                                ErrorDesc = "InvalidCouponCode",
                                ErrorTitle = "Error"
                            }
                        };
                        var errorModelJson = JsonConvert.SerializeObject(errorModel);
                        return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
                    }
                }
                else
                {
                    //empty coupon code
                    model.DiscountBox.Messages.Add(LocalizationService.GetResource("ShoppingCart.DiscountCouponCode.WrongDiscount"));
                    var errorModel = new OrderTotals
                    {
                        Data = null,
                        Error = new ErrorResponse
                        {
                            ErrorCode = "2090",
                            ErrorDesc = "InvalidCouponCode",
                            ErrorTitle = "Error"
                        }
                    };
                    var errorModelJson = JsonConvert.SerializeObject(errorModel);
                    return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
                }

                //model = _shoppingCartModelFactory.PrepareShoppingCartModel(model, cart);

                var flag = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.SetRewardPoints, _storeContext.CurrentStore.Id);

                OrderTotalsModel orderTotalsModel;

                if (flag == "true")
                    orderTotalsModel = _shoppingCartModelFactory.PrepareOrderTotalsModelForReward(cart, true, true);
                else
                    orderTotalsModel = _shoppingCartModelFactory.PrepareOrderTotalsModelForReward(cart, true, false);

                var errorResponse = new ErrorResponse
                {
                    ErrorCode = "2088",
                    ErrorDesc = "SuccessRemoveDiscountCoupon",
                    ErrorTitle = ""
                };

                if (orderTotalsModel.RedeemedRewardPointsAmount != null)
                    orderTotalsModel.RedeemedRewardPointsAmount = orderTotalsModel.RedeemedRewardPointsAmount.Replace("-", "");

                var model1 = new OrderTotals
                {
                    Data = orderTotalsModel,
                    Error = errorResponse
                };

                var ModelJsonString = JsonConvert.SerializeObject(model1);
                return new ErrorActionResult(ModelJsonString, HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                var errorModel = new OrderTotals
                {
                    Data = null,
                    Error = new ErrorResponse
                    {
                        ErrorCode = "2089",
                        ErrorDesc = "ErrorRemoveDiscountCoupon: Reason: " + Convert.ToString(ex.Message),
                        ErrorTitle = "Error"
                    }
                };
                var errorModelJson = JsonConvert.SerializeObject(errorModel);
                return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
            }
        }

        #endregion

        #region Validate Cart Item
        private string ValidateCartItem(Customer customer)
        {
            var details = customer.ShoppingCartItems;
            var error = new ErrorResponse();
            string errorMessage = "";
            int counter = 1;

            //validate individual cart items
            foreach (var sci in details)
            {
                var sciWarnings = _shoppingCartService.GetShoppingCartItemWarnings(customer,
                                                                                    sci.ShoppingCartType,
                                                                                    sci.Product,
                                                                                    _storeContext.CurrentStore.Id,
                                                                                    sci.AttributesXml,
                                                                                    sci.CustomerEnteredPrice,
                                                                                    sci.RentalStartDateUtc,
                                                                                    sci.RentalEndDateUtc,
                                                                                    sci.Quantity,
                                                                                    false,
                                                                                    sci.Id);
                if (sciWarnings.Any())
                    errorMessage += " 0" + counter + ". " + (sciWarnings.Aggregate(string.Empty, (current, next) => $"{current}{next}"));
                counter++;
            }
            return errorMessage;
        }
        #endregion


        #region SyncOrderStatusAPI

        [HttpPost]
        [Route("/api/syncOrderStatusAPI/")]
        //[ProducesResponseType(typeof(OrdersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public virtual IActionResult SyncOrderStatusAPI([FromBody]IList<OrderStatusSync> orderStatusSyncs)
        {
            // Order Completed strings.
            string orderCompleted_ShortTitle_En = _localizationService.GetResource("pushnotification.order_completed.shorttitle_en");
            string orderCompleted_LongTitle_En = _localizationService.GetResource("pushnotification.order_completed.longtitle_en");
            string orderCompleted_Contents_En = _localizationService.GetResource("pushnotification.order_completed.contents_en");

            string orderCompleted_ShortTitle_Ar = _localizationService.GetResource("pushnotification.order_completed.shorttitle_ar");
            string orderCompleted_LongTitle_Ar = _localizationService.GetResource("pushnotification.order_completed.longtitle_ar");
            string orderCompleted_Contents_Ar = _localizationService.GetResource("pushnotification.order_completed.contents_ar");
            
            // Order Cancelled strings.
            string orderCancelled_ShortTitle_En = _localizationService.GetResource("pushnotification.order_cancelled.shorttitle_en");
            string orderCancelled_LongTitle_En = _localizationService.GetResource("pushnotification.order_cancelled.longtitle_en");
            string orderCancelled_Contents_En = _localizationService.GetResource("pushnotification.order_cancelled.contents_en");

            string orderCancelled_ShortTitle_Ar = _localizationService.GetResource("pushnotification.order_cancelled.shorttitle_ar");
            string orderCancelled_LongTitle_Ar = _localizationService.GetResource("pushnotification.order_cancelled.longtitle_ar");
            string orderCancelled_Contents_Ar = _localizationService.GetResource("pushnotification.order_cancelled.contents_ar");

            

            var OrdersComplete = new OrderCompleteObject();
            var stringWriter = new System.IO.StringWriter();
            StringBuilder sb = new StringBuilder();
            string[] xmlData = new string[] { };
            List<PushNotification> lstPushNotifications = new List<PushNotification>();

            if (orderStatusSyncs.Count>0)
            {
                xmlData = new string[orderStatusSyncs.Count * 2];
            }
            
            int arrayIndex = 0;
            //var PN_ShortTitle = _localizationService.GetResource("Account.AccountActivation.OTPValidMinutes");
            try
            {

                if (orderStatusSyncs == null)
                {
                    var Error = new ErrorResponse
                    {
                        ErrorCode = "9001",
                        ErrorDesc = "OrderCompleteError: Request Parameter null.",
                        ErrorTitle = "Error"
                    };
                    var errorModelJson = JsonConvert.SerializeObject(Error);
                    return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
                }

                if (orderStatusSyncs.Count > 0)
                {
                    foreach (var orderStatusSync in orderStatusSyncs)
                    {
                        string _ShortTitle_En = String.Empty;
                        string _LongTitle_En = String.Empty;
                        string _Contents_En = String.Empty;

                        string _ShortTitle_Ar = String.Empty;
                        string _LongTitle_Ar = String.Empty;
                        string _Contents_Ar = String.Empty;
                                                
                        var order = _orderService.GetOrderByCustomOrderNumber(orderStatusSync.CustomOrderNumber);
                        if (order == null)
                        {
                            OrdersComplete.ResultStatus.Add(orderStatusSync.CustomOrderNumber, StatusResult.OrderNotFound);
                            //orderStatusSync.StatusResult = StatusResult.OrderNotFound;
                            orderStatusSync.OrderStatusSyncStatus = (int)StatusResult.OrderNotFound;
                            continue;
                        }

                        //check if order already complete
                        if (order.OrderStatus == OrderStatus.Complete)
                        {
                            OrdersComplete.ResultStatus.Add(orderStatusSync.CustomOrderNumber, StatusResult.AlreadyComplete);
                            orderStatusSync.OrderStatusSyncStatus = (int)StatusResult.AlreadyComplete;
                            continue;
                        }

                        //check if order already cancelled
                        if (order.OrderStatus == OrderStatus.Cancelled)
                        {
                            OrdersComplete.ResultStatus.Add(orderStatusSync.CustomOrderNumber, StatusResult.AlreadyCancelled);
                            orderStatusSync.OrderStatusSyncStatus = (int)StatusResult.AlreadyCancelled;
                            continue;
                        }

                        //See if given order status is defined, if yes then if it is complete or cancelled
                        if (!Enum.IsDefined(typeof(OrderStatus), orderStatusSync.OrderStatus))
                        {
                            OrdersComplete.ResultStatus.Add(orderStatusSync.CustomOrderNumber, StatusResult.OrderStatusNotValid);
                            orderStatusSync.OrderStatusSyncStatus = (int)StatusResult.OrderStatusNotValid;
                            continue;
                        }
                        if (Enum.IsDefined(typeof(OrderStatus), orderStatusSync.OrderStatus))
                        {
                            if (orderStatusSync.OrderStatus != OrderStatus.Complete.ToString() && orderStatusSync.OrderStatus != OrderStatus.Cancelled.ToString())
                            {
                                OrdersComplete.ResultStatus.Add(orderStatusSync.CustomOrderNumber, StatusResult.OrderStatusNotValid);
                                orderStatusSync.OrderStatusSyncStatus = (int)StatusResult.OrderStatusNotValid;
                                continue;
                            }
                        }

                        //if order status is complete
                        if (orderStatusSync.OrderStatus == OrderStatus.Complete.ToString())
                        {
                            _ShortTitle_En = String.Format(orderCompleted_ShortTitle_En, order.CustomOrderNumber);
                            _LongTitle_En = String.Format(orderCompleted_LongTitle_En, order.CustomOrderNumber);
                            _Contents_En = String.Format(orderCompleted_Contents_En, order.CustomOrderNumber);

                            _ShortTitle_Ar = String.Format(orderCompleted_ShortTitle_Ar, order.CustomOrderNumber);
                            _LongTitle_Ar = String.Format(orderCompleted_LongTitle_Ar, order.CustomOrderNumber);
                            _Contents_Ar = String.Format(orderCompleted_Contents_Ar, order.CustomOrderNumber);

                            //check if paid online.
                            if (order.PaymentStatusId == (int)PaymentStatus.Paid)
                            {
                                _orderProcessingService.SetOrderStatus(order, OrderStatus.Complete, false, false);

    							order.PN_Status = 1;
                                order.UpdatedOnUtc = orderStatusSync.UpdatedOnUTC;
                                _orderService.UpdateOrder(order);        
                                
                                OrdersComplete.ResultStatus.Add(orderStatusSync.CustomOrderNumber, StatusResult.Successful);
                                orderStatusSync.OrderStatusSyncStatus = (int)StatusResult.Successful;

                                // Start - Push Notification for completed orders.
                                PushNotification pushNotificationEn = new PushNotification
                                {
                                    CustomerId = order.CustomerId,
                                    Badge = 0,
                                    ShortTitle = _ShortTitle_En,
                                    LongTitle = _LongTitle_En,
                                    Contents = _Contents_En,
                                    Language = Convert.ToString((int)PushLanguage.En)
                                };

                                _pushNotificationService.InsertPushNotification(pushNotificationEn); 
                                _pushNotificationService.CreateXMLRowDataForPush(pushNotificationEn, ref sb);
                                xmlData[arrayIndex++] = Convert.ToString(sb);
                                sb.Clear();

                                PushNotification pushNotificationAr = new PushNotification
                                {
                                    CustomerId = order.CustomerId,
                                    Badge = 0,
                                    ShortTitle = _ShortTitle_Ar,
                                    LongTitle = _LongTitle_Ar,
                                    Contents = _Contents_Ar,
                                    Language = Convert.ToString((int)PushLanguage.Ar)
                                };
                                _pushNotificationService.InsertPushNotification(pushNotificationAr); 
                                _pushNotificationService.CreateXMLRowDataForPush(pushNotificationAr, ref sb);
                                xmlData[arrayIndex++] = Convert.ToString(sb);
                                sb.Clear();

                                // Add new generated PN in list.
                                lstPushNotifications.Add(pushNotificationEn);
                                lstPushNotifications.Add(pushNotificationAr);

                                // End - Push Notification for completed orders.

                            }
                            else
                            {
                                //check if given payment status exists if yes then if it is paid or not
                                if (!Enum.IsDefined(typeof(PaymentStatus), orderStatusSync.PaymentStatus))
                                {
                                    OrdersComplete.ResultStatus.Add(orderStatusSync.CustomOrderNumber, StatusResult.PaymentStatusNotValid);
                                    orderStatusSync.OrderStatusSyncStatus = (int)StatusResult.PaymentStatusNotValid;
                                    continue;
                                }

                                if ((Enum.IsDefined(typeof(PaymentStatus), orderStatusSync.PaymentStatus)) && orderStatusSync.PaymentStatus != PaymentStatus.Paid.ToString())
                                {
                                    OrdersComplete.ResultStatus.Add(orderStatusSync.CustomOrderNumber, StatusResult.PaymentStatusNotValid);
                                    orderStatusSync.OrderStatusSyncStatus = (int)StatusResult.PaymentStatusNotValid;
                                    continue;
                                }

                                //if given payment status is paid then mark order as paid and complete - COD
                                if (orderStatusSync.PaymentStatus == PaymentStatus.Paid.ToString())
                                {
                                    _orderProcessingService.MarkOrderAsPaid(order, false);

                                    _orderProcessingService.SetOrderStatus(order, OrderStatus.Complete, false, false);

                                    order.PN_Status = 1;
                                    order.UpdatedOnUtc = orderStatusSync.UpdatedOnUTC;
                                    _orderService.UpdateOrder(order);

                                    OrdersComplete.ResultStatus.Add(orderStatusSync.CustomOrderNumber, StatusResult.Successful);
                                    orderStatusSync.OrderStatusSyncStatus = (int)StatusResult.Successful;

                                    // Start - Push Notification for completed orders.
                                    PushNotification pushNotificationEn = new PushNotification
                                    {
                                        CustomerId = order.CustomerId,
                                        Badge = 0,
                                        ShortTitle = _ShortTitle_En,
                                        LongTitle = _LongTitle_En,
                                        Contents = _Contents_En,
                                        Language = Convert.ToString((int)PushLanguage.En)
                                    };

                                    _pushNotificationService.InsertPushNotification(pushNotificationEn);                                 
                                    _pushNotificationService.CreateXMLRowDataForPush(pushNotificationEn, ref sb);
                                    xmlData[arrayIndex++] = Convert.ToString(sb);
                                    sb.Clear();

                                    PushNotification pushNotificationAr = new PushNotification
                                    {
                                        CustomerId = order.CustomerId,
                                        Badge = 0,
                                        ShortTitle = _ShortTitle_Ar,
                                        LongTitle = _LongTitle_Ar,
                                        Contents = _Contents_Ar,
                                        Language = Convert.ToString((int)PushLanguage.Ar)
                                    };
                                    _pushNotificationService.InsertPushNotification(pushNotificationAr); 
                                    _pushNotificationService.CreateXMLRowDataForPush(pushNotificationAr, ref sb);
                                    xmlData[arrayIndex++] = Convert.ToString(sb);
                                    sb.Clear();

                                    // Add new generated PN in list.
                                    lstPushNotifications.Add(pushNotificationEn);
                                    lstPushNotifications.Add(pushNotificationAr);

                                    // End - Push Notification for completed orders.

                                }
                            }
                        }
                        else if (orderStatusSync.OrderStatus == OrderStatus.Cancelled.ToString()) //if given order status is cancelled
                        {
                            _orderProcessingService.CancelOrder(order, false, false);

                            order.PN_Status = 1;
                            order.UpdatedOnUtc = orderStatusSync.UpdatedOnUTC;
                            _orderService.UpdateOrder(order);

                            OrdersComplete.ResultStatus.Add(orderStatusSync.CustomOrderNumber, StatusResult.Successful);
                            orderStatusSync.OrderStatusSyncStatus = (int)StatusResult.Successful;

                            // Start - Push Notification for cancelled orders.

                            _ShortTitle_En = String.Format(orderCancelled_ShortTitle_En, order.CustomOrderNumber);
                            _LongTitle_En = String.Format(orderCancelled_LongTitle_En, order.CustomOrderNumber);
                            _Contents_En = String.Format(orderCancelled_Contents_En, order.CustomOrderNumber);

                            _ShortTitle_Ar = String.Format(orderCancelled_ShortTitle_Ar, order.CustomOrderNumber);
                            _LongTitle_Ar = String.Format(orderCancelled_LongTitle_Ar, order.CustomOrderNumber);
                            _Contents_Ar = String.Format(orderCancelled_Contents_Ar, order.CustomOrderNumber);

                            PushNotification pushNotificationEn = new PushNotification
                            {
                                CustomerId = order.CustomerId,
                                Badge = 0,
                                ShortTitle = _ShortTitle_En,
                                LongTitle = _LongTitle_En,
                                Contents = _Contents_En,
                                Language = Convert.ToString((int)PushLanguage.En)
                            };

                            _pushNotificationService.InsertPushNotification(pushNotificationEn);
                            _pushNotificationService.CreateXMLRowDataForPush(pushNotificationEn, ref sb);
                            xmlData[arrayIndex++] = Convert.ToString(sb);
                            sb.Clear();

                            PushNotification pushNotificationAr = new PushNotification
                            {
                                CustomerId = order.CustomerId,
                                Badge = 0,
                                ShortTitle = _ShortTitle_Ar,
                                LongTitle = _LongTitle_Ar,
                                Contents = _Contents_Ar,
                                Language = Convert.ToString((int)PushLanguage.Ar)
                            };
                            _pushNotificationService.InsertPushNotification(pushNotificationAr); 
                            _pushNotificationService.CreateXMLRowDataForPush(pushNotificationAr, ref sb);
                            xmlData[arrayIndex++] = Convert.ToString(sb);
                            sb.Clear();

                            // Add new generated PN in list.
                            lstPushNotifications.Add(pushNotificationEn);
                            lstPushNotifications.Add(pushNotificationAr);

                            // End - Push Notification for cancelled orders.

                        }
                    }

                    if (OrdersComplete.ResultStatus.Where(o => o.Value == StatusResult.Successful).Count() == orderStatusSyncs.Count)
                        OrdersComplete.Status = DTOs.Orders.StatusCode.Success;
                    else if (OrdersComplete.ResultStatus.Where(o => o.Value != StatusResult.Successful).Count() == orderStatusSyncs.Count)
                        OrdersComplete.Status = DTOs.Orders.StatusCode.Failed;
                    else
                        OrdersComplete.Status = DTOs.Orders.StatusCode.PartialSuccess;

                    OrdersComplete.OrderStatusSyncResult = orderStatusSyncs;

                     
                    var errorModelJson = JsonConvert.SerializeObject(OrdersComplete);

                    // Start - Call Push Notification WCF Service.
                    try
                    { 
                        PushAPIResponse pushAPIResponse = _pushNotificationService.CallPushNotificationService(xmlData);
                        if (pushAPIResponse.ErrorCode == "0" && pushAPIResponse.TransactionIDs.Count > 0)
                        {
                            _pushNotificationService.UpdatePushNotification(lstPushNotifications, pushAPIResponse.TransactionIDs);
                        }
                        else if (pushAPIResponse.ErrorCode == "1")
                        {
                            _logger.Error("An error occured in Push Notification Service processing: UserAuhthenticationFailed");
                        }
                        else if (pushAPIResponse.ErrorCode == "2")
                        {
                            _logger.Error("An error occured in Push Notification Service processing: ExceptionInProcess");
                        }
                        else if (pushAPIResponse.ErrorCode == "3")
                        {
                            _logger.Error("An error occured in Push Notification Service processing: EmptyXMLData");
                        }
                        else
                        {
                            _logger.Error("An unknown error occured in Push Notification Service processing.");
                        }
                    }
                    catch(Exception ex)
                    {
                        _logger.Error("An error occured in Push Notification Service processing", ex);
                    }
                    // End - Call Push Notification WCF Service.

                    return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);

                }
                else
                {
                    OrdersComplete.Status = DTOs.Orders.StatusCode.Failed;
                    OrdersComplete.Errors = new ErrorResponseData
                    {
                        ErrorCode = "9001",
                        ErrorDesc = "Order list is empty.",
                        ErrorTitle = "Error"
                    };
                    var errorModelJson = JsonConvert.SerializeObject(OrdersComplete);
                    return new ErrorActionResult(errorModelJson, HttpStatusCode.NoContent);
                }
            }
            catch (Exception ex)
            {
                OrdersComplete.Status = DTOs.Orders.StatusCode.Failed;
                OrdersComplete.Errors = new ErrorResponseData
                {
                    ErrorCode = "9002",
                    ErrorDesc = "OrderCompleteError: " + Convert.ToString(ex.Message),
                    ErrorTitle = "Error"
                };
                var errorModelJson = JsonConvert.SerializeObject(OrdersComplete);
                return new ErrorActionResult(errorModelJson, HttpStatusCode.InternalServerError);
            }
        }


        #endregion

        #region Reverting order if payment fails
        [HttpGet]
        [Route("/api/revertOrderAPI/{orderId}")]
        //[ProducesResponseType(typeof(OrdersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        //[ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult RevertOrderAPI(int OrderId)
        {
            RevertOrderResponse oRevertOrderResponse;
            try
            {
                Order order = _orderService.GetOrderById(OrderId);
                if (order == null)
                {
                    //throw new ArgumentNullException(nameof(order));
                    oRevertOrderResponse = new RevertOrderResponse
                    {
                        Data = false,
                        Error = new ErrorResponse
                        {
                            ErrorCode = "2099",
                            ErrorDesc = "InvalidOrderId",
                            ErrorTitle = "Error"
                        }
                    };
                    _logger.Error("Invalid OrderID: " + OrderId + " passed to RevertOrderAPI");
                    var responseModelJson = JsonConvert.SerializeObject(oRevertOrderResponse);
                    return new ErrorActionResult(responseModelJson, HttpStatusCode.OK);
                }

                if (_orderProcessingService.RevertOrder(order))
                {
                    oRevertOrderResponse = new RevertOrderResponse
                    {
                        Data = true,
                        Error = new ErrorResponse
                        {
                            ErrorCode = "2097",
                            ErrorDesc = "SuccessRevertOrder",
                            ErrorTitle = "Success"
                        }
                    };
                    _logger.InsertLog(Core.Domain.Logging.LogLevel.Debug, "Reverted order successfully with OrderID: " + OrderId, "Reverted order successfully with OrderID: " + OrderId);
                    var responseModelJson = JsonConvert.SerializeObject(oRevertOrderResponse);
                    return new ErrorActionResult(responseModelJson, HttpStatusCode.OK);
                }
                else
                {
                    oRevertOrderResponse = new RevertOrderResponse
                    {
                        Data = false,
                        Error = new ErrorResponse
                        {
                            ErrorCode = "2098",
                            ErrorDesc = "ErrorRevertOrder",
                            ErrorTitle = "Error"
                        }
                    };
                    _logger.Error("Error occrred while reverting order with OrderId: " + OrderId + ".");
                    var responseModelJson = JsonConvert.SerializeObject(oRevertOrderResponse);
                    return new ErrorActionResult(responseModelJson, HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {
                oRevertOrderResponse = new RevertOrderResponse
                {
                    Data = false,
                    Error = new ErrorResponse
                    {
                        ErrorCode = "2098",
                        ErrorDesc = "ErrorRevertOrder",
                        ErrorTitle = "Error"
                    }
                };
                _logger.Error("Error occurred in RevertOrderAPI for OrderId: " + OrderId + ".", ex);
                var responseModelJson = JsonConvert.SerializeObject(oRevertOrderResponse);
                return new ErrorActionResult(responseModelJson, HttpStatusCode.OK);
            }
        }
        #endregion

        //public void CreateXMLDataForPush(System.IO.StringWriter stringWriter, PushNotification pushNotification, ref StringBuilder sb)
        //{
        //    var myStringWriter = new System.IO.StringWriter();
        //    XmlSerializer serializer = new XmlSerializer(typeof(PushNotification));            
        //    serializer.Serialize(myStringWriter, pushNotification);
        //    string rowData = Convert.ToString(stringWriter);
        //    string rowData1 = rowData.Remove(0, rowData.IndexOf("<Id>"));
        //    // rowData.Replace("<?xml version=\"1.0\" encoding=\"utf - 16\"?>", "");
        //    //string ss =  rowData.Replace("<PushNotification xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">", "");
        //    string rowData2 = rowData1.Replace("</PushNotification>", "");
        //    sb.Append("<row>");
        //    sb.Append(rowData2);
        //    sb.Append("</row>");
        //}

        //private PushAPIResponse CallPushService(string[] xmlData)
        //{
        //    XmlDocument xmlDocument = new XmlDocument();
        //    string contentRootPath = _hostingEnvironment.ContentRootPath;
        //    string xmlPNConfigurationFilePath = contentRootPath + "/App_Data/Push.InstantFetchService.xml";

        //    xmlDocument.Load(xmlPNConfigurationFilePath);
        //    XmlElement root = xmlDocument.DocumentElement;
        //    XmlNodeList nodes = root.ChildNodes[0].ChildNodes;

        //    string uri = nodes.Cast<XmlNode>().FirstOrDefault(x => x.Name == "URI").InnerText;
        //    string apiUser = nodes.Cast<XmlNode>().FirstOrDefault(x => x.Name == "ApiUser").InnerText;
        //    string apiPassword = nodes.Cast<XmlNode>().FirstOrDefault(x => x.Name == "ApiPassword").InnerText;
        //    string serviceId = nodes.Cast<XmlNode>().FirstOrDefault(x => x.Name == "ServiceId").InnerText;
        //    string scheduleTime = nodes.Cast<XmlNode>().FirstOrDefault(x => x.Name == "ScheduleTime").InnerText;
        //    string isInstant = nodes.Cast<XmlNode>().FirstOrDefault(x => x.Name == "IsInstant").InnerText;

        //    string output = string.Empty;
        //    PushAPIResponse pushResponse = null;
        //    HttpWebRequest request = WebRequest.Create(uri) as HttpWebRequest;

        //    request.Credentials = CredentialCache.DefaultCredentials;
        //    request.Accept = "application/json";
        //    request.ContentType = "application/json";
        //    request.Method = "POST";

        //    //xmlData[0] = "<row><content_id>7</content_id><user>102</user><badge>1</badge><shorttitle>Case closure initiated</shorttitle><longtitle>Closure initiated for Case Test</longtitle><contents>Ritu Raje has initiated closure for Case Test.Please check the job details for more information</contents><language>1</language></row>";
        //    //xmlData[0] = "<row><content_id>3</content_id><user>102</user><badge>1</badge><shorttitle>ShortT</shorttitle><longtitle>LongT</longtitle><contents>First msg text</contents><language>1</language><msisdn /></row>";
        //    //xmlData[0] = "<row><content_id>6</content_id><user>1</user><badge>1</badge><shorttitle>Test from Medha</shorttitle><longtitle>Case CASE05 attended</longtitle><contents>Ritu Rajeshirke attended Case CASE05.Please check the job details for more information</contents><language>1</language></row>";
        //    //xmlData[1] = "<row> <content_id>3953</content_id><user>34</user><badge>555</badge><shorttitle>ShortT</shorttitle><longtitle>LongT</longtitle><contents>Second message text_24</contents><language>1</language><msisdn /></row>";

             
        //    object input = new
        //    {
        //        ApiUser = apiUser,
        //        ApiPassword = apiPassword,
        //        ServiceId = serviceId, 
        //        ScheduleTime = scheduleTime,
        //        IsInstant = isInstant,
        //        XmlDataList = xmlData
        //    };

        //    string inputJson = JsonConvert.SerializeObject(input);

        //    byte[] bytes = Encoding.UTF8.GetBytes(inputJson);
             
        //    try
        //    {
        //        using (Stream stream = request.GetRequestStream())
        //        {
        //            //obj.WriteObject(stream, input);
        //            stream.Write(bytes, 0, bytes.Length);
        //            stream.Close();
        //        }

        //        using (HttpWebResponse httpResponse = (HttpWebResponse)request.GetResponse())
        //        {
        //            using (Stream stream = httpResponse.GetResponseStream())
        //            {
        //                output = (new StreamReader(stream)).ReadToEnd();
        //                pushResponse = JsonConvert.DeserializeObject<PushAPIResponse>(output);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error("Exception in CallPushService(): " + ex);
        //    }
        //    return pushResponse;
        //}      

        //public IList<string> GetShoppingCartItemWarnings(Customer customer)
        //{

        //    //validate individual cart items
        //    foreach (var sci in details.Cart)
        //    {
        //        var sciWarnings = _shoppingCartService.GetShoppingCartItemWarnings(details.Customer,
        //            sci.ShoppingCartType, sci.Product, processPaymentRequest.StoreId, sci.AttributesXml,
        //            sci.CustomerEnteredPrice, sci.RentalStartDateUtc, sci.RentalEndDateUtc, sci.Quantity, false, sci.Id);
        //        if (sciWarnings.Any())
        //            throw new NopException(sciWarnings.Aggregate(string.Empty, (current, next) => $"{current}{next};"));
        //    }

        //    return "2";
        //}
    }

    #region Class for CompleteAPI

    /// <summary>
    /// Class which will given as the parameter in the request of CompleteOrder API
    /// </summary>
    /// 

    public class OrderStatusSync
    {
        #region Private variables

        //private int _order_id;
        //private string _custom_order_number;
        //private string _order_status;
        //private string _payment_status;
        //private DateTime _updated_on_utc;
        ////private StatusResult _status_result;

        #endregion


        #region Public variables

        #region Public variables

        public int OrderId { get; set; }

        public string CustomOrderNumber { get; set; }

        public string OrderStatus { get; set; }

        public string PaymentStatus { get; set; }

        public DateTime UpdatedOnUTC { get; set; }

        public int OrderStatusSyncStatus { get; set; }

        #endregion

        //public int OrderId
        //{
        //    get
        //    {
        //        return _order_id;
        //    }
        //    set
        //    {
        //        _order_id = value;
        //    }
        //}

        //public string CustomOrderNumber
        //{
        //    get
        //    {
        //        return _custom_order_number;
        //    }
        //    set
        //    {
        //        _custom_order_number = value;
        //    }
        //}

        //public string OrderStatus
        //{
        //    get
        //    {
        //        return _order_status;
        //    }
        //    set
        //    {
        //        _order_status = value;
        //    }
        //}

        //public string PaymentStatus
        //{
        //    get
        //    {
        //        return _payment_status;
        //    }
        //    set
        //    {
        //        _payment_status = value;
        //    }
        //}

        //public DateTime UpdatedOnUTC
        //{
        //    get
        //    {
        //        return _updated_on_utc;
        //    }
        //    set
        //    {
        //        _updated_on_utc = value;
        //    }
        //}

        ////public StatusResult StatusResult
        ////{
        ////    get
        ////    {
        ////        return _status_result;
        ////    }
        ////    set
        ////    {
        ////        _status_result = value;
        ////    }
        ////}

        //public int OrderStatusSyncStatus
        //{
        //    get; set;
        //}

        #endregion
    }

    //public class OrderStatusSync
    //{
    //    /// <summary>
    //    /// represents the order id.
    //    /// </summary>
    //    public int OrderId { get; set; }

    //    public string CustomOrderNumber { get; set; }

    //    public OrderStatus OrderStatusToAssign { get; set; }

    //    public PaymentStatus PaymentStatusToAssign { get; set; }

    //    public DateTime UpdatedOnUTC { get; set; }

    //    public StatusResult OrderStatusSyncStatus { get; set; }
    //}

    #endregion
}