using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Nop.Plugin.Api.Models;
using Nop.Web.Models.Payments;
using Microsoft.Extensions.Options;
using Nop.Core.Gateway;
using Nop.Core.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Plugin.Api.Utils;
using Microsoft.AspNetCore.WebUtilities;
using System.Text;
using System.Runtime.InteropServices;
using Nop.Plugin.Api.Constants;
using static Nop.Plugin.Api.Controllers.BaseApiController;
using System.Xml;
using static Nop.Plugin.Api.Controllers.PaymentsController;
using Microsoft.AspNetCore.Hosting;
using Nop.Core.Data;
using Nop.Plugin.Api.Services;
using Nop.Services.Localization;

namespace Nop.Plugin.Api.Controllers

{
    public class PaymentController : BaseController
    {

        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly Nop.Services.Logging.ILogger _logger;
        private readonly IPaymentApiService _paymentApiService;
        private readonly ILocalizationService _localizationService;

        public PaymentController(IOptions<GatewayApiConfig> gatewayApiConfig,
                                    GatewayApiClient gatewayApiClient,
                                    NVPApiClient nvpApiClient,
                                    IHostingEnvironment hostingEnvironment,
                                    Nop.Services.Logging.ILogger ologger,
                                    IPaymentApiService paymentApiService,
                                    ILocalizationService localizationService,
                                    ILogger<PaymentController> logger
                                ) : base(gatewayApiConfig, gatewayApiClient, nvpApiClient, logger)
        {
            _hostingEnvironment = hostingEnvironment;
            _logger = ologger;
            _paymentApiService = paymentApiService;
            _localizationService = localizationService;
        }

        [HttpGet("hostedCheckoutUrl/{orderId}/{orderTotal}/{transactionId}")]
        public IActionResult HostedCheckoutURL([FromRoute(Name = "orderId")] string orderId, [FromRoute(Name = "orderTotal")] string orderTotal, [FromRoute(Name = "transactionId")] string transactionId)
        {
            var errorModelJson = string.Empty;
            ErrorResponse errorResponse = new ErrorResponse();
            XmlDocument xmlDocument = new XmlDocument();
            var merchentProfile = new MerchentProfile();
            InitPaymentResponse initPaymentResponse = new InitPaymentResponse();

            try
            {
                int cc = 0;
                double tot = 0;
                bool isInt = int.TryParse(orderId, out cc);
                bool isDouble = double.TryParse(orderTotal, out tot);
                string errorMessage = String.Empty; ;

                var PaymentTransactionDetails = _paymentApiService.GetPaymentTransactionByTrackId(transactionId);

                if (PaymentTransactionDetails != null && isInt && isDouble)
                {                  
                    // Get Credit Card URL Valid Minuites. Check whether it is of data type - double else set 5.
                    double URLValidMins = 10;
                    var _UrlValidMins = _localizationService.GetResource("CreditCard.Payment.URLValidMinutes");
                    if (Double.TryParse(_UrlValidMins, out URLValidMins)) // IsDouble?
                    {
                        URLValidMins = Convert.ToDouble(_UrlValidMins);
                    }
                    else
                    {
                        URLValidMins = 10;
                    }

                    var CreatedDate = PaymentTransactionDetails.CreatedOnUtc;
                    var dateDiff = DateTime.UtcNow - CreatedDate;

                    if (dateDiff.TotalMinutes > URLValidMins)
                    {
                        errorMessage = "Payment url is expired.";
                    }
                    else if (Convert.ToString(PaymentTransactionDetails.OrderId) != orderId)
                    {
                        errorMessage = "Order Id is not valid in url.";
                    }
                   
                }
                else
                {
                    errorMessage = "Transaction Id is not valid in url.";
                }

                if (errorMessage != string.Empty)
                {
                    _logger.Error($"Hosted CheckoutURL Error : " + errorMessage);
                    return View(ViewNames.Error, new ErrorViewModel
                    {
                        RequestId = System.Diagnostics.Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                        Cause = errorMessage,
                        Message = errorMessage
                    });
                }

                string contentRootPath = _hostingEnvironment.ContentRootPath;
                string xmlPaymentGatewayConfigurationFilePath = contentRootPath + "/App_Data/PaymentGatewayConfiguration.xml";
                xmlDocument.Load(xmlPaymentGatewayConfigurationFilePath);
                XmlElement root = xmlDocument.DocumentElement;
                XmlNodeList nodes = root.ChildNodes;
                string GatewayName = string.Empty;
                string IsActive = string.Empty;

                _logger.Information("Start: Read file PaymentGatewayConfiguration.");
                foreach (XmlNode node in nodes)
                {
                    GatewayName = node["GatewayName"].InnerText;
                    if (GatewayName.ToUpper() == "CREDIT CARD" && node["IsActive"].InnerText.ToUpper() == "Y")
                    {
                        GatewayApiConfig.MerchantId = AES.Decrypt(node["MerchantId"].InnerText);  
                        GatewayApiConfig.Username = AES.Decrypt(node["Username"].InnerText); 
                        GatewayApiConfig.Password = AES.Decrypt(node["Password"].InnerText); 
                        GatewayApiConfig.GatewayUrl = node["MasterCardGatewayUrl"].InnerText;
                        GatewayApiConfig.Currency = node["Currency"].InnerText;                        
                        GatewayApiConfig.Version = node["Version"].InnerText; 
                        GatewayApiConfig.UseProxy = Convert.ToBoolean(node["UseProxy"].InnerText);
                        GatewayApiConfig.UseSsl = Convert.ToBoolean(node["UseSsl"].InnerText);

                        GatewayApiRequest gatewayApiRequest = new GatewayApiRequest
                        {
                            GatewayApiConfig = GatewayApiConfig,
                            ApiMethod = "POST",
                            ApiOperation = node["APIOperation"].InnerText,
                            interaction = node["Interaction"].InnerText,
                            OrderId = orderId,
                            TransactionId = transactionId,
                            OrderAmount = orderTotal,
                            OrderCurrency = node["Currency"].InnerText,
                            OrderDescription = Configurations.ORDER_DESCRIPTION_TEXT +  transactionId, //Passing for our ref. As we need it in response to change transaction status.

                        };

                        gatewayApiRequest.buildSessionRequestUrl();
                        gatewayApiRequest.buildPayload();
                        gatewayApiRequest.ApiMethod = GatewayApiClient.POST;

                        try
                        {
                            String response = GatewayApiClient.SendTransaction(gatewayApiRequest);

                            _logger.Information("Hosted Checkout Response : " + response);

                            CheckoutSessionModel checkoutSessionModel = CheckoutSessionModel.toCheckoutSessionModel(response);

                            ViewBag.CheckoutJsUrl = $@"{GatewayApiConfig.GatewayUrl}/checkout/version/{GatewayApiConfig.Version}/checkout.js";
                            ViewBag.MerchantId = GatewayApiConfig.MerchantId;
                            ViewBag.OrderId = gatewayApiRequest.OrderId;
                            ViewBag.CheckoutSession = checkoutSessionModel;
                            ViewBag.Currency = GatewayApiConfig.Currency;
                            ViewBag.OrderDescription = gatewayApiRequest.OrderDescription;
                            ViewBag.OrderAmount = gatewayApiRequest.OrderAmount;

                            ViewBag.MerchantName = node["MerchantName"].InnerText;
                            ViewBag.AddressLine1 = node["AddressLine1"].InnerText;
                            ViewBag.AddressLine2 = node["AddressLine2"].InnerText;
                            ViewBag.BillingAddress = node["BillingAddress"].InnerText;
                            ViewBag.OrderSummary = node["OrderSummary"].InnerText;
                        }
                        catch (Exception e)
                        {
                            _logger.Error($"Hosted Checkout error : {JsonConvert.SerializeObject(e)}");

                            return View(ViewNames.Error, new ErrorViewModel
                            {
                                RequestId = System.Diagnostics.Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                                Cause = e.InnerException != null ? e.InnerException.StackTrace : e.StackTrace,
                                Message = e.Message
                            });
                        }

                        return View(ViewNames.HostedCheckout);
                    }
                }

                _logger.Error("Credit Card payment gateway method not found in xml config file.");

                return View(ViewNames.Error, new ErrorViewModel
                {
                    RequestId = "NA",
                    Cause = "config error",
                    Message = "Credit Card payment gateway method not found in xml config file."
                });

            }
            catch (Exception ex)
            {
                _logger.Error($"Hosted Checkout error : {JsonConvert.SerializeObject(ex)}");

                return View(ViewNames.Error, new ErrorViewModel
                {
                    RequestId = System.Diagnostics.Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                    Cause = ex.InnerException != null ? ex.InnerException.StackTrace : ex.StackTrace,
                    Message = ex.Message
                });

            }
        }


        /// <summary>
        /// Display HOSTED CHECKOUT operation page, view HostedCheckout.cshtml
        /// </summary>
        [HttpGet("hostedCheckout")]
        [HttpGet("api/hostedCheckout")]
        public IActionResult ShowHostedCheckout()
        {
            Logger.LogInformation("Payment controller HostedCheckout action");

            #region Merchant gatewat details
            GatewayApiConfig.UseProxy = false;
            GatewayApiConfig.UseSsl = false;
            GatewayApiConfig.MerchantId = "900120901";
            GatewayApiConfig.Username = "merchant.900120901";
            GatewayApiConfig.Password = "bdc30713a6f4f5d3c00416809a50ac52";
            //GatewayApiConfig.Password = "d0c67f0263710651470dc35b14315b42";
            GatewayApiConfig.Currency = "KWD";
            GatewayApiConfig.GatewayUrl = "https://test-nbkpayment.mtf.gateway.mastercard.com";
            GatewayApiConfig.Version = "53";
            #endregion

            GatewayApiRequest gatewayApiRequest = new GatewayApiRequest();

            gatewayApiRequest.ApiMethod = "POST";
            gatewayApiRequest.ApiOperation = "CREATE_CHECKOUT_SESSION"; //PAY is used to indicate we have to do some payment operations.
            gatewayApiRequest.interaction = "PURCHASE";

            #region order details
            gatewayApiRequest.OrderId = "2019_241";
            gatewayApiRequest.TransactionId = "01234561006";
            gatewayApiRequest.OrderAmount = "341";
            gatewayApiRequest.OrderCurrency = "KWD";
            gatewayApiRequest.OrderDescription = "Online tyre purchase";
            #endregion

            gatewayApiRequest.GatewayApiConfig = GatewayApiConfig;
            gatewayApiRequest.buildSessionRequestUrl();
            gatewayApiRequest.buildPayload();

            gatewayApiRequest.ApiMethod = GatewayApiClient.POST;

            try
            {
                String response = GatewayApiClient.SendTransaction(gatewayApiRequest);

                Logger.LogInformation("HostedCheckout response : " + response);

                CheckoutSessionModel checkoutSessionModel = CheckoutSessionModel.toCheckoutSessionModel(response);

                ViewBag.CheckoutJsUrl = $@"{GatewayApiConfig.GatewayUrl}/checkout/version/{GatewayApiConfig.Version}/checkout.js";
                ViewBag.MerchantId = GatewayApiConfig.MerchantId;
                ViewBag.OrderId = gatewayApiRequest.OrderId;
                ViewBag.CheckoutSession = checkoutSessionModel;
                ViewBag.Currency = GatewayApiConfig.Currency;
                ViewBag.OrderDescription = gatewayApiRequest.OrderDescription;
                ViewBag.OrderAmount = gatewayApiRequest.OrderAmount;
            }
            catch (Exception e)
            {
                Logger.LogError($"Hosted Checkout error : {JsonConvert.SerializeObject(e)}");

                return View("Error", new ErrorViewModel
                {
                    RequestId = getRequestId(),
                    Cause = e.InnerException != null ? e.InnerException.StackTrace : e.StackTrace,
                    Message = e.Message
                });
            }
            return View(ViewNames.HostedCheckout);
            //return View("HostedCheckout");
        }

        /// <summary>
        /// Display PAY operation page, view Pay.cshtml
        /// </summary>
        [HttpGet("pay")]
        public IActionResult ShowPay()
        {
            Logger.LogInformation("Payment controller ShowPay action");

            GatewayApiRequest gatewayApiRequest = GatewayApiRequest.createSampleApiRequest(GatewayApiConfig);

            ViewBag.JavascriptSessionUrl = getSessionJsUrl(GatewayApiConfig);
            ViewBag.TestAndGoLiveUrl = getTestAndGoLiveDocumentationURL();

            return View("Pay", gatewayApiRequest);
        }

        /// <summary>
        /// Display PAY operation page, view Pay.cshtml
        /// </summary>
        [HttpGet("payWithToken")]
        public IActionResult ShowPayWithToken()
        {
            Logger.LogInformation("Payment controller ShowPayWithToken action");

            GatewayApiRequest gatewayApiRequest = GatewayApiRequest.createSampleApiRequest(GatewayApiConfig);

            ViewBag.JavascriptSessionUrl = getSessionJsUrl(GatewayApiConfig);
            ViewBag.TestAndGoLiveUrl = getTestAndGoLiveDocumentationURL();
            ViewBag.ApiDocumentation = $@"{GatewayApiConfig.GatewayUrl}/api/documentation/integrationGuidelines/supportedFeatures/pickAdditionalFunctionality/tokenization/tokenization.html";

            return View("PayWithToken", gatewayApiRequest);
        }

        /// <summary>
        /// Display AUTHORIZE operation page, view Authorize.cshtml
        /// </summary>
        [HttpGet("authorize")]
        public IActionResult ShowAuthorize()
        {
            Logger.LogInformation("Payment controller ShowAuthorize action");

            GatewayApiRequest gatewayApiRequest = GatewayApiRequest.createSampleApiRequest(GatewayApiConfig);

            ViewBag.JavascriptSessionUrl = getSessionJsUrl(GatewayApiConfig);
            ViewBag.TestAndGoLiveUrl = getTestAndGoLiveDocumentationURL();

            return View("Authorize", gatewayApiRequest);
        }

        /// <summary>
        /// Display CAPTURE operation page, view Capture.cshtml
        /// </summary>
        [HttpGet("capture")]
        public IActionResult ShowCapture()
        {
            Logger.LogInformation("Payment controller ShowCapture action");

            GatewayApiRequest gatewayApiRequest = GatewayApiRequest.createSampleApiRequest(GatewayApiConfig, "CAPTURE");

            return View("Capture", gatewayApiRequest);
        }

        /// <summary>
        /// Display REFUND operation page, view Refund.cshtml
        /// </summary>
        [HttpGet("refund")]
        public IActionResult ShowRefund()
        {
            Logger.LogInformation("Payment controller ShowRefund action");

            GatewayApiRequest gatewayApiRequest = GatewayApiRequest.createSampleApiRequest(GatewayApiConfig, "REFUND");

            return View("Refund", gatewayApiRequest);
        }

        /// <summary>
        /// Display VOID operation page, view Void.cshtml
        /// </summary>
        [HttpGet("void")]
        public IActionResult ShowVoid()
        {
            Logger.LogInformation("Payment controller ShowVoid action");

            GatewayApiRequest gatewayApiRequest = GatewayApiRequest.createSampleApiRequest(GatewayApiConfig, "VOID");

            return View("Void", gatewayApiRequest);
        }

        /// <summary>
        /// Display VERIFY operation page, view Verify.cshtml
        /// </summary>
        [HttpGet("verify")]
        public IActionResult ShowVerify()
        {
            Logger.LogInformation("Payment controller ShowVerify action");

            ViewBag.JavascriptSessionUrl = getSessionJsUrl(GatewayApiConfig);
            ViewBag.TestAndGoLiveUrl = getTestAndGoLiveDocumentationURL();

            return View("Verify");
        }

        /// <summary>
        /// Display RETRIEVE TRANSACTION operation page, view RetrieveTransaction.cshtml
        /// </summary>
        [HttpGet("retrieveOrder")]
        public IActionResult ShowRetrieveOrder()
        {
            Logger.LogInformation("Payment controller ShowRetrieveTransaction action");

            GatewayApiRequest gatewayApiRequest = GatewayApiRequest.createSampleApiRequest(GatewayApiConfig, "RETRIEVE_ORDER");

            return View("RetrieveOrder", gatewayApiRequest);
        }


        /// <summary>
        /// Display PAY with 3D Secure operation page, view PayWith3Ds.cshtml
        /// </summary>
        [HttpGet("payWith3ds")]
        public IActionResult ShowPayWith3ds()
        {
            Logger.LogInformation("Payment controller ShowPayWith3ds action");

            ViewBag.JavascriptSessionUrl = getSessionJsUrl(GatewayApiConfig);

            //documentation links
            ViewBag.DocumentationCreateSessionUrl = $@"{GatewayApiConfig.GatewayUrl}/api/documentation/apiDocumentation/rest-json/version/latest/operation/Session%3a%20Create%20Checkout%20Session.html";
            ViewBag.Documentation3dsEnrollmentUrl = $@"{GatewayApiConfig.GatewayUrl}/api/documentation/apiDocumentation/rest-json/version/latest/operation/3DS%3a%20%20Check%203DS%20Enrollment.html";
            ViewBag.Documentation3dsProcessResultUrl = $@"{GatewayApiConfig.GatewayUrl}/api/documentation/apiDocumentation/rest-json/version/latest/operation/3DS%3a%20Process%20ACS%20Result.html";
            ViewBag.DocumentationTransactionPayUrl = $@"{GatewayApiConfig.GatewayUrl}/api/documentation/apiDocumentation/rest-json/version/latest/operation/Transaction%3a%20%20Pay.html";
            ViewBag.DocumentationTransactionAuthorizeUrl = $@"{GatewayApiConfig.GatewayUrl}/api/documentation/apiDocumentation/rest-json/version/latest/operation/Transaction%3a%20%20Authorize.html";

            GatewayApiRequest gatewayApiRequest = GatewayApiRequest.createSampleApiRequest(GatewayApiConfig);

            return View("PayWith3ds", gatewayApiRequest);
        }


        /// <summary>
        /// Shows the pay with paypal page.
        /// </summary>
        /// <returns>The pay with paypal view</returns>
        [HttpGet("payWithPayPal")]
        public IActionResult ShowPayWithPaypal()
        {
            Logger.LogInformation("Payment controller ShowPayWithPayPal action");

            ViewBag.JavascriptSessionUrl = getSessionJsUrl(GatewayApiConfig);
            ViewBag.DocumentationPayPal = $@"{GatewayApiConfig.GatewayUrl}/api/documentation/integrationGuidelines/supportedFeatures/pickPaymentMethod/browserPayments/PayPal.html";

            GatewayApiRequest gatewayApiRequest = GatewayApiRequest.createSampleApiRequest(GatewayApiConfig);

            return View("PayWithPayPal", gatewayApiRequest);
        }




        /// <summary>
        /// Shows the pay with Union Pay page.
        /// </summary>
        /// <returns>The pay with UnionPay view</returns>
        [HttpGet("showPayWithUnionPaySecurePay")]
        public IActionResult ShowPayWithUnionPaySecurePay()
        {
            Logger.LogInformation("Payment controller ShowPayWithUnionPal action");

            ViewBag.JavascriptSessionUrl = getSessionJsUrl(GatewayApiConfig);
            ViewBag.DocumentationUnionPay = $@"{GatewayApiConfig.GatewayUrl}/api/documentation/integrationGuidelines/supportedFeatures/pickPaymentMethod/browserPayments/testDetails.html#x_SecurePayTest";

            GatewayApiRequest gatewayApiRequest = GatewayApiRequest.createSampleApiRequest(GatewayApiConfig);

            return View("PayWithUnionPay", gatewayApiRequest);
        }


        /// <summary>
        /// Gets the session js URL.
        /// </summary>
        /// <returns>The session js URL.</returns>
        /// <param name="gatewayApiConfig">Gateway API config.</param>
        private string getSessionJsUrl(GatewayApiConfig gatewayApiConfig)
        {
            return $@"{GatewayApiConfig.GatewayUrl}/form/version/{GatewayApiConfig.Version}/merchant/{GatewayApiConfig.MerchantId}/session.js";
        }

        /// <summary>
        /// Shows the pay through NVP page
        /// </summary>
        /// <returns>The pay through nvp.</returns>
        [HttpGet("showPayThroughNVP")]
        public IActionResult ShowPayThroughNVP()
        {
            Logger.LogInformation("Payment controller ShowPayThroughNVP action");

            ViewBag.JavascriptSessionUrl = getSessionJsUrl(GatewayApiConfig);
            ViewBag.TestAndGoLiveUrl = $@"{GatewayApiConfig.GatewayUrl}/api/documentation/integrationGuidelines/supportedFeatures/testAndGoLive.html";

            GatewayApiRequest gatewayApiRequest = GatewayApiRequest.createSampleApiRequest(GatewayApiConfig);

            return View("PayThroughNVP", gatewayApiRequest);
        }


        /// <summary>
        /// Shows the Masterpass view.
        /// </summary>
        /// <returns>The masterpass.</returns>
        [HttpGet("showMasterpass")]
        public IActionResult ShowMasterpass()
        {
            Logger.LogInformation("Payment controller ShowMasterpass action");

            ViewBag.JavascriptSessionUrl = getSessionJsUrl(GatewayApiConfig);


            //documentation links
            ViewBag.MasterpassCreateSession = $@"{GatewayApiConfig.GatewayUrl}/api/documentation/apiDocumentation/rest-json/version/latest/operation/Session%3a%20Create%20Session.html";
            ViewBag.MasterpassUpdateSession = $@"{GatewayApiConfig.GatewayUrl}/api/documentation/apiDocumentation/rest-json/version/latest/operation/Session%3a%20Update%20Session.html";
            ViewBag.MasterpassOpenWallet = $@"{GatewayApiConfig.GatewayUrl}/api/documentation/apiDocumentation/rest-json/version/latest/operation/Wallet%3a%20Open%20Wallet.html";
            ViewBag.MasterpassJSLibrary = $@"{GatewayApiConfig.GatewayUrl}/api/documentation/integrationGuidelines/supportedFeatures/pickPaymentMethod/masterPassOnline.html";
            ViewBag.MasterpassUpdateSessionFromWallet = $@"{GatewayApiConfig.GatewayUrl}/api/documentation/apiDocumentation/rest-json/version/latest/operation/Wallet%3a%20Update%20Session%20From%20Wallet.html";
            ViewBag.MasterpassPay = $@"{GatewayApiConfig.GatewayUrl}/api/documentation/apiDocumentation/rest-json/version/latest/operation/Transaction%3a%20%20Pay.html";
            ViewBag.MasterpassAuthorize = $@"{GatewayApiConfig.GatewayUrl}/api/documentation/apiDocumentation/rest-json/version/latest/operation/Transaction%3a%20%20Authorize.html";
            ViewBag.MasterpassFullDocumentation = $@"{GatewayApiConfig.GatewayUrl}/api/documentation/integrationGuidelines/supportedFeatures/pickPaymentMethod/masterPassOnline.html";


            string ORIGIN_RETURN_PAGE = "/masterpassResponse";
            string MASTERPASS_ONLINE = "MASTERPASS_ONLINE";

            GatewayApiRequest gatewayApiRequest = GatewayApiRequest.createSampleApiRequest(GatewayApiConfig);

            //build return URL
            string returnURL = Microsoft.AspNetCore.Http.Extensions.UriHelper.BuildAbsolute(Request.Scheme, Request.Host, Request.PathBase, ORIGIN_RETURN_PAGE);

            gatewayApiRequest.MasterpassOriginUrl = returnURL;
            gatewayApiRequest.MasterpassOnline = MASTERPASS_ONLINE;


            return View("PayWithMasterpass", gatewayApiRequest);
        }


        /// <summary>
        /// Gets the test and go live documentation URL.
        /// </summary>
        /// <returns>The test and go live.</returns>
        private string getTestAndGoLiveDocumentationURL()
        {
            //return $@"{GatewayApiConfig.GatewayUrl}/api/documentation/integrationGuidelines/supportedFeatures/testAndGoLive.html";
            return @"https://test-nbkpayment.mtf.gateway.mastercard.com/api/documentation/integrationGuidelines/supportedFeatures/testAndGoLive.html?locale=en_US";

        }


    }
}
