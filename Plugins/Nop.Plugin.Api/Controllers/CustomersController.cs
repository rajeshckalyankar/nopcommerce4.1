﻿using Newtonsoft.Json;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Gdpr;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.Tax;
using Nop.Core.Infrastructure;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.Delta;
using Nop.Plugin.Api.DTOs;
using Nop.Plugin.Api.DTOs.Customers;
using Nop.Plugin.Api.Factories;
using Nop.Plugin.Api.Helpers;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.MappingExtensions;
using Nop.Plugin.Api.ModelBinders;
using Nop.Plugin.Api.Models.CustomersParameters;
using Nop.Plugin.Api.Services;
using Nop.Services.Authentication;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.Events;
using Nop.Services.Gdpr;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Services.Tax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

//using System.ServiceModel;
//using System.ServiceModel.Web;


namespace Nop.Plugin.Api.Controllers
{
    using DTOs.Errors;
    using JSON.Serializers;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Options;
    using Nop.Core.Data;
    using Nop.Core.Domain.Common;
    using Nop.Plugin.Api.Models.Customer;
    using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
    using System.IO;

    //[ApiController]
    //[Route("[controller]")]
    [ApiAuthorize(Policy = JwtBearerDefaults.AuthenticationScheme, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CustomersController : BaseApiController
    {
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IEventPublisher _eventPublisher;
        private readonly IAuthenticationService _authenticationService;
        private readonly IWorkContext _workContext;
        private readonly ICustomerRegistrationService _customerRegistrationService;
        private readonly ICustomerService _customerService;
        private readonly ILocalizationService _localizationService;
        private readonly ICustomerApiService _customerApiService;
        private readonly ICustomerRolesHelper _customerRolesHelper;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IEncryptionService _encryptionService;
        private readonly ICountryService _countryService;
        private readonly IMappingHelper _mappingHelper;
        private readonly INewsLetterSubscriptionService _newsLetterSubscriptionService;
        private readonly ILanguageService _languageService;
        private readonly IFactory<Customer> _factory;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly Web.Factories.ICustomerModelFactory _customerModelFactory;
        private readonly Web.Areas.Admin.Factories.ICustomerModelFactory _customerModelFactoryAdmin;
        private readonly CustomerSettings _customerSettings;
        private readonly MvcJsonOptions _options;

        private readonly IStoreContext _storeContext;
        private readonly ITaxService _taxService;
        private readonly TaxSettings _taxSettings;
        private readonly DateTimeSettings _dateTimeSettings;

        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly LocalizationSettings _localizationSettings;
        private readonly IGdprService _gdprService;
        private readonly GdprSettings _gdprSettings;
        private readonly IWebHelper _webHelper;
        private readonly IAddressService _addressService;
        private readonly IEmailAccountService _emailAccountService;
        private readonly EmailAccountSettings _emailAccountSettings;
        private readonly IQueuedEmailService _queuedEmailService;
        private readonly ILogger _logger;
        private readonly INopFileProvider _fileProvider;

        private const string PHONE = "Phone";
        private const string EMAIL = "Email";

        // We resolve the customer settings this way because of the tests.
        // The auto mocking does not support concreate types as dependencies. It supports only interfaces.
        // private CustomerSettings _customerSettings;

        // private CustomerSettings CustomerSettings
        //{
        //    get
        //    {
        //        if (_customerSettings == null)
        //        {
        //            _customerSettings = EngineContext.Current.Resolve<CustomerSettings>();
        //        }

        //        return _customerSettings;
        //    }
        //}

        public CustomersController(
            ICustomerApiService customerApiService,
            IJsonFieldsSerializer jsonFieldsSerializer,
            IAclService aclService,
            ICustomerService customerService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IDiscountService discountService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            ICustomerRolesHelper customerRolesHelper,
            IGenericAttributeService genericAttributeService,
            IEncryptionService encryptionService,
            IFactory<Customer> factory,
            ICountryService countryService,
            IMappingHelper mappingHelper,
            INewsLetterSubscriptionService newsLetterSubscriptionService,
            IPictureService pictureService, ILanguageService languageService,
            ICustomerRegistrationService customerRegistrationService,
            IShoppingCartService shoppingCartService,
            IEventPublisher eventPublisher,
            IAuthenticationService authenticationService,
            IWorkContext workContext,
            CustomerSettings customerSettings,
            Web.Factories.ICustomerModelFactory customerModelFactory,
            Web.Areas.Admin.Factories.ICustomerModelFactory customerModelFactoryAdmin,
            IOptions<MvcJsonOptions> options,
            IStoreContext storeContext,
            ITaxService taxService,
            TaxSettings taxSettings,
            DateTimeSettings dateTimeSettings,
            IWorkflowMessageService workflowMessageService,
            LocalizationSettings localizationSettings,
            IGdprService gdprService,
            GdprSettings gdprSettings,
            IWebHelper webHelper,
            IAddressService addressService,
            IEmailAccountService emailAccountService,
            EmailAccountSettings emailAccountSettings,
            IQueuedEmailService queuedEmailService,
            INopFileProvider fileProvider,
            ILogger logger
            ) :
            base(jsonFieldsSerializer, aclService, customerService, storeMappingService, storeService, discountService, customerActivityService, localizationService, pictureService)
        {
            _customerApiService = customerApiService;
            _factory = factory;
            _countryService = countryService;
            _mappingHelper = mappingHelper;
            _newsLetterSubscriptionService = newsLetterSubscriptionService;
            _languageService = languageService;
            _encryptionService = encryptionService;
            _genericAttributeService = genericAttributeService;
            _customerRolesHelper = customerRolesHelper;
            _customerRegistrationService = customerRegistrationService;
            _shoppingCartService = shoppingCartService;
            _eventPublisher = eventPublisher;
            _authenticationService = authenticationService;
            _workContext = workContext;
            _localizationService = localizationService;
            _customerActivityService = customerActivityService;
            _customerModelFactory = customerModelFactory;
            _customerModelFactoryAdmin = customerModelFactoryAdmin;
            _customerSettings = customerSettings;
            _options = options.Value;
            _customerService = customerService;
            _storeContext = storeContext;
            _taxService = taxService;
            _taxSettings = taxSettings;
            _dateTimeSettings = dateTimeSettings;
            _gdprSettings = gdprSettings;
            _gdprService = gdprService;
            _workflowMessageService = workflowMessageService;
            _localizationSettings = localizationSettings;
            _webHelper = webHelper;
            _addressService = addressService;
            _emailAccountService = emailAccountService;
            _emailAccountSettings = emailAccountSettings;
            _queuedEmailService = queuedEmailService;
            _fileProvider = fileProvider;
            _logger = logger;
        }


        private bool IsInteger(string str)
        {

            Regex regex = new Regex(@"^[0-9]+$");
            try
            {
                if (String.IsNullOrWhiteSpace(str))
                {
                    return false;
                }
                if (!regex.IsMatch(str))
                {
                    return false;
                }

                return true;

            }
            catch (Exception ex)
            {

            }
            return false;
        }

        //T Deserialize<T>(MediaTypeFormatter formatter, string str) where T : class
        //{
        //    // Write the serialized string to a memory stream.
        //    Stream stream = new MemoryStream();
        //    StreamWriter writer = new StreamWriter(stream);
        //    writer.Write(str);
        //    writer.Flush();
        //    stream.Position = 0;
        //    // Deserialize to an object of type T
        //    return formatter.ReadFromStreamAsync(typeof(T), stream, null, null).Result as T;
        //}


        [HttpPost]
        [Route("/api/login")]
        [ProducesResponseType(typeof(CustomersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult Login([FromBody] Nop.Plugin.Api.Models.Customer.LoginClass model, string returnUrl, bool captchaValid)
        {
            var IsValidateByPhone = false;
            var ErrorCode = "2030";
            var ErrorDesc = "GenericError";
            try
            {
                //_logger.Warning("START: CheckUserName_Phone_Email?:- Username: " + model.Username);  //TEMP LOG..
                if (IsInteger(model.Username))
                    IsValidateByPhone = true;
                //_logger.Warning("END: CheckUserName_Phone_Email?:- Username: " + model.Username);  //TEMP LOG..
                if (ModelState.IsValid)
                {
                    //_logger.Warning("START: ValidateCustomer:- ModelState Is Valid: Username: " + model.Username); //TEMP LOG..
                    var loginResult = _customerRegistrationService.ValidateCustomer(model.Username, model.Password, IsValidateByPhone);
                    //_logger.Warning("END: ValidateCustomer:- Login Result: " + loginResult.ToString() +"Username: "+ model.Username);  //TEMP LOG..
                    //_logger.Warning("START: Login Case.");  //TEMP LOG..
                    switch (loginResult)
                    {
                        case CustomerLoginResults.Successful:
                            {
                                _logger.Warning("Login Case Successful.");  //TEMP LOG..
                                                                            // _logger.Warning("START: GetCustomerBy_Phone_Email");  //TEMP LOG..
                                var customer = IsValidateByPhone ?
                                                _customerService.GetCustomerByPhone(model.Username) :
                                                _customerService.GetCustomerByEmail(model.Username);

                                //migrate shopping cart
                                //_shoppingCartService.MigrateShoppingCart(_workContext.CurrentCustomer, customer, true);

                                //_logger.Warning("START: _authenticationService.SignIn");  //TEMP LOG..
                                //sign in new customer
                                _authenticationService.SignIn(customer, model.RememberMe);

                                //_logger.Warning("START: _eventPublisher.Publish");  //TEMP LOG..
                                //raise event       
                                _eventPublisher.Publish(new CustomerLoggedinEvent(customer));

                                // _logger.Warning("START: _customerActivityService.InsertActivity");  //TEMP LOG..
                                //activity log
                                _customerActivityService.InsertActivity(customer, "PublicStore.Login",
                                    _localizationService.GetResource("ActivityLog.PublicStore.Login"), customer);

                                //_logger.Warning("START: customerDto = _customerApiService.GetCustomerById");  //TEMP LOG..
                                var customerDto = _customerApiService.GetCustomerById(customer.Id);

                                //_logger.Warning("START: Create object customersRootObject");  //TEMP LOG..
                                var customersRootObject = new CustomersRootObject();
                                // _logger.Warning("START: customersRootObject.Customers.Add(customerDto);");  //TEMP LOG..
                                customersRootObject.Customers.Add(customerDto);
                                //_logger.Warning("START: JsonFieldsSerializer.Serialize(customersRootObject, string.Empty);");  //TEMP LOG..

                                //To avoid error "Self referencing loop detected for property".
                                customersRootObject.Customers.ToList().ForEach(o => o.ShoppingCartItems = null);

                                var json = JsonFieldsSerializer.Serialize(customersRootObject, string.Empty);
                                // _logger.Warning("START: For Verification, Get 200 chars of Customer JSON = " + json.Substring(0,200));  //TEMP LOG..
                                //_logger.Warning("START:  return new ErrorActionResult(json, HttpStatusCode.OK);");  //TEMP LOG..
                                return new ErrorActionResult(json, HttpStatusCode.OK);

                            }
                        case CustomerLoginResults.CustomerNotExist:
                            ErrorCode = "2001";
                            ErrorDesc = "CustomerNotExist";
                            _logger.Warning("Login Case: CustomerNotExist");  //TEMP LOG..
                            //ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.CustomerNotExist"));
                            break;
                        case CustomerLoginResults.Deleted:
                            ErrorCode = "2002";
                            ErrorDesc = "Deleted";
                            _logger.Warning("Login Case: CustomerDeleted");  //TEMP LOG..
                            //ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.Deleted"));
                            break;
                        case CustomerLoginResults.NotActive:
                            ErrorCode = "2003";
                            ErrorDesc = "NotActive";
                            _logger.Warning("Login Case: NotActive");  //TEMP LOG..
                            //ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.NotActive"));

                            //_logger.Warning("Login Case: NotActive: START: Get LoginCustomer By Phone_Email");  //TEMP LOG..
                            //Get users phone number to send OTP.
                            var loginCustomer = IsValidateByPhone ?
                                                _customerService.GetCustomerByPhone(model.Username) :
                                                _customerService.GetCustomerByEmail(model.Username);
                            //_logger.Warning("Login Case: NotActive: Create object-custModel");  //TEMP LOG..
                            var custModel = new Models.Customer.RegisterModel();
                            //_logger.Warning("Login Case: NotActive: Get users phone number");  //TEMP LOG..
                            custModel.Phone = IsValidateByPhone ?
                                              model.Username :
                                              _genericAttributeService.GetAttribute<string>(loginCustomer, NopCustomerDefaults.PhoneAttribute);

                            _logger.Warning("Login Case: NotActive:START: SendOTP()");  //TEMP LOG..
                            //Send OTP
                            if (SendOTP(loginCustomer, custModel, PHONE))
                            {
                                ErrorCode = "2003";
                                ErrorDesc = "NotActive"; // Msg: Your account verification is pending. Please enter the OTP sent on your registered mobile number.
                                _logger.Warning("Login Case: NotActive:START: SendOTP -> OTP Sent!!");  //TEMP LOG..
                            }
                            else
                            {
                                ErrorCode = "2032";
                                ErrorDesc = "NotActive_ErrorSendingOTP";
                                _logger.Warning("Login Case: NotActive:START: SendOTP -> Error Sending OTP!!");  //TEMP LOG..
                            }
                            _logger.Warning("Login Case: NotActive:END: SendOTP()");  //TEMP LOG..
                            break;
                        case CustomerLoginResults.NotRegistered:
                            ErrorCode = "2004";
                            ErrorDesc = "NotRegistered";
                            _logger.Warning("Login Case: NotRegistered");  //TEMP LOG..
                            //ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.NotRegistered"));
                            break;
                        case CustomerLoginResults.LockedOut:
                            ErrorCode = "2005";
                            ErrorDesc = "LockedOut";
                            _logger.Warning("Login Case: LockedOut");  //TEMP LOG..
                            //ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.LockedOut"));
                            break;
                        case CustomerLoginResults.WrongPassword:
                        default:
                            ErrorCode = "2006";
                            ErrorDesc = "WrongCredentials";
                            _logger.Warning("Login Case: WrongPassword_&_Default");  //TEMP LOG..
                            //ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials"));
                            break;
                    }
                    _logger.Warning("END: Login Case.");  //TEMP LOG..
                }

                var errorsJson = GetJsonErrorObject(ErrorCode, ErrorDesc, "Error");
                _logger.Error("Error! Event: Login. Reason: " + errorsJson);
                return new ErrorActionResult(errorsJson, HttpStatusCode.OK);

            }
            catch (Exception ex)
            {
                var errorsJson = GetJsonErrorObject(ErrorCode, "User: " + model.Username + "; " + ex.Message.ToString(), "Try_Catch_Error");
                _logger.Error("Login Exception caught in catch block! Reason: " + errorsJson, ex);
                return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
            }
        }

        public bool SendOTP(Customer customer, Models.Customer.RegisterModel model, string verificationType = PHONE)
        {
            const string secret = "051220180140IND";
            var totp = new PureOtp.Totp(System.Text.Encoding.UTF8.GetBytes(secret), totpSize: 4, step: 1);
            var otpCode = totp.ComputeTotp();

            //email validation message
            _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.AccountActivationTokenAttribute, otpCode);
            _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.AccountActivationTokenDateGeneratedAttribute, DateTime.Now);

            if (verificationType == PHONE)
            {
                //SEND OTP ON CUSTOMER MOBILE NUMBER.
                //model.NewPhone = To change phone number.
                var statusMessage = SendOTPOnPhone(String.IsNullOrWhiteSpace(model.NewPhone) ? model.Phone : model.NewPhone, otpCode);

                string statusCode = !String.IsNullOrWhiteSpace(statusMessage) && statusMessage.Length >= 2
                                            ? statusMessage.Substring(0, 2)
                                            : statusMessage;
                if (statusCode == "00")
                {
                    return true;
                }
                else
                {
                    _logger.Error("An error occured while sending OTP! Reason: " + statusMessage);
                }

            }
            else if (verificationType == EMAIL)
            {
                //send email
                _workflowMessageService.SendCustomerForgotPasswordRecoveryMessage(customer, _workContext.WorkingLanguage.Id);
                return true;

                // return SendOTPOnMail(customer, otpCode);
            }

            return false;

        }

        [HttpPost]
        [Route("/api/registerGuestUser")]
        [Route("/api/register")]
        //[ValidateCaptcha]
        //[ValidateHoneypot]
        //[PublicAntiForgery]
        ////available even when navigation is not allowed
        //[CheckAccessPublicStore(true)]
        public virtual IActionResult Register([FromBody] Models.Customer.RegisterModel model, string returnUrl, bool captchaValid)
        {
            //check whether registration is allowed
            if (_customerSettings.UserRegistrationType == UserRegistrationType.Disabled)
            {
                var errorsJson = GetJsonErrorObject("2012", "RegistrationDisabled", "Error");
                return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
            }


            //validate unique user
            if (_customerService.GetCustomerByEmail(model.Email) != null)
            {
                //return new RawJsonActionResult(_localizationService.GetResource("Account.Register.Errors.EmailAlreadyExists"));                 
                var errorsJson = GetJsonErrorObject("2013", "EmailAlreadyExists", "Error");
                return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
            }

            //validate unique user
            if (_customerService.GetCustomerByPhone(model.Phone) != null)
            {
                //return new RawJsonActionResult(_localizationService.GetResource("Account.Register.Errors.MobileAlreadyExists"));                 
                var errorsJson = GetJsonErrorObject("2014", "PhoneAlreadyExists", "Error");
                return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
            }
            //To validate Password
            Match matchResult = Regex.Match(model.Password, "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[~!@#$%^&*\\(\\)\\+\\/_=\\{\\}\\[\\]:\\;.,?<>\\|\'\"\\\\-])");
            if (!matchResult.Success || model.Password.Length < 8)
            {
                var errorsJson = GetJsonErrorObject("2016", "RegistrationFailed", "Error");
                return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
            }
            // To register a guest customer.
            if (model.GuestUserId > 0)
            {
                var GuestUser = _customerService.GetCustomerById(model.GuestUserId);

                if (GuestUser == null)
                {
                    var error2Json = GetJsonErrorObject("2042", "InvalidCustomerID", "Error");
                    return new ErrorActionResult(error2Json, HttpStatusCode.OK);
                }
                else
                {
                    _workContext.CurrentCustomer = GuestUser;
                }
            }
            else // To register a new user by registration.
            {
                if (_workContext.CurrentCustomer.IsRegistered())
                {
                    //Already registered customer. 
                    _authenticationService.SignOut();

                    //raise logged out event       
                    _eventPublisher.Publish(new CustomerLoggedOutEvent(_workContext.CurrentCustomer));

                    //Save a new record
                    _workContext.CurrentCustomer = _customerService.InsertGuestCustomer();
                }
                else
                {
                    _workContext.CurrentCustomer = _customerService.InsertGuestCustomer(); // Create new blank customer model to fill registration data. Do not use old guest userid, as it may be in use.
                }
            }

            var customer = _workContext.CurrentCustomer;

            // For ALMAILEM: For Guest User Register: Clear registering guest users all addresses.
            //if (model.GuestUserId > 0)
            {
                customer.CustomerAddressMappings = null;
                customer.BillingAddress = null;
                customer.ShippingAddress = null;
                customer.BillingAddressId = null;
                customer.ShippingAddressId = null;
            }


            customer.RegisteredInStoreId = _storeContext.CurrentStore.Id;

            //Issue: Error while Registering from Android:- 
            //Error! RegistrationFailed. 
            //Reason: Search engine can't be registered; IsSystemAccount = True; System Name = SearchEngine.
            //So, set below default values to overcome this error.

            customer.IsSystemAccount = false;
            customer.SystemName = null;

            if (ModelState.IsValid)
            {
                if (_customerSettings.UsernamesEnabled && model.Username != null)
                {
                    model.Username = model.Username.Trim();
                }

                var isApproved = _customerSettings.UserRegistrationType == UserRegistrationType.Standard;
                var registrationRequest = new CustomerRegistrationRequest(customer,
                    model.Email,
                    _customerSettings.UsernamesEnabled ? model.Username : model.Email,
                    model.Password,
                    _customerSettings.DefaultPasswordFormat,
                    _storeContext.CurrentStore.Id,
                    isApproved);
                var registrationResult = _customerRegistrationService.RegisterCustomer(registrationRequest);

                if (registrationResult.Success)
                {
                    //Insert Generic Attributes of Customer. [FirstName, LastName & Phone]
                    _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.FirstNameAttribute, model.FirstName);
                    _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.LastNameAttribute, model.LastName);
                    _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.PhoneAttribute, model.Phone);

                    // For ALMAILEM: For Guest User Register: Clear registering guest users all addresses.
                    //if (model.GuestUserId > 0)
                    {
                        customer.CustomerAddressMappings = null;
                        customer.BillingAddress = null;
                        customer.ShippingAddress = null;
                        customer.BillingAddressId = null;
                        customer.ShippingAddressId = null;
                    }

                    //login customer now
                    if (isApproved)
                        _authenticationService.SignIn(customer, true);

                    // For ALMAILEM: Do not add default address while registration.
                    ////insert default address (if possible)
                    //var defaultAddress = new Nop.Core.Domain.Common.Address
                    //{
                    //    FirstName = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.FirstNameAttribute),
                    //    LastName = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.LastNameAttribute),
                    //    Email = customer.Email,
                    //    Company = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.CompanyAttribute),
                    //    CountryId = _genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.CountryIdAttribute) > 0
                    //        ? (int?)_genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.CountryIdAttribute)
                    //        : null,
                    //    StateProvinceId = _genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.StateProvinceIdAttribute) > 0
                    //        ? (int?)_genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.StateProvinceIdAttribute)
                    //        : null,
                    //    County = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.CountyAttribute),
                    //    City = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.CityAttribute),
                    //    Address1 = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.StreetAddressAttribute),
                    //    Address2 = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.StreetAddress2Attribute),
                    //    ZipPostalCode = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.ZipPostalCodeAttribute),
                    //    PhoneNumber = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.PhoneAttribute),
                    //    FaxNumber = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.FaxAttribute),
                    //    CreatedOnUtc = customer.CreatedOnUtc
                    //};
                    //if (this._addressService.IsAddressValid(defaultAddress))
                    //{
                    //    //some validation
                    //    if (defaultAddress.CountryId == 0)
                    //        defaultAddress.CountryId = null;
                    //    if (defaultAddress.StateProvinceId == 0)
                    //        defaultAddress.StateProvinceId = null;
                    //    //set default address
                    //    //customer.Addresses.Add(defaultAddress);
                    //    customer.CustomerAddressMappings.Add(new CustomerAddressMapping { Address = defaultAddress });
                    //    customer.BillingAddress = defaultAddress;
                    //    customer.ShippingAddress = defaultAddress;
                    //    _customerService.UpdateCustomer(customer);
                    //}

                  
                    //raise event       
                    _eventPublisher.Publish(new CustomerRegisteredEvent(customer));

                    //_workflowMessageService.SendCustomerEmailValidationMessage(customer, _workContext.WorkingLanguage.Id);

                    if (SendOTP(customer, model, PHONE))
                    {
                       
                        //return new RawJsonActionResult("Registration is successful! An OTP has been sent to your registered mobile number.");
                        var errorsJson = GetJsonErrorObject("2011", "RegistrationSuccessfulOTPSent", "");
                        return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
                    }
                    else
                    {
                        //return new RawJsonActionResult("Registration is successful! An error occured while sending OTP.");
                        var errorsJson = GetJsonErrorObject("2015", "OTPResendingError", "Error");
                        return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
                    }

                }

                //errors
                foreach (var error in registrationResult.Errors)
                    ModelState.AddModelError("", error);
            }

            //return new RawJsonActionResult(ModelState.Root.Errors.FirstOrDefault().ErrorMessage.ToString());
            var logDetails = ModelState.Root.Errors.FirstOrDefault().ErrorMessage.ToString() + "; IsSystemAccount = " + customer.IsSystemAccount + "; System Name = " + Convert.ToString(customer.SystemName);

            var errorJson = GetJsonErrorObject("2016", "RegistrationFailed: " + logDetails, "Error");

            _logger.Error("Error! RegistrationFailed. Reason: " + logDetails);

            return new ErrorActionResult(errorJson, HttpStatusCode.OK);

        }

        [HttpPost]
        [Route("/api/createGuestUser")]
        public virtual IActionResult CreateGuestUser()
        {
            try
            {
                var customer = _customerService.InsertGuestCustomer();

                _workContext.CurrentCustomer = customer;

                var customerDto = _customerApiService.GetCustomerById(customer.Id);

                //customerDto.ErrorCode = "";
                //customerDto.ErrorTitle = "";
                //customerDto.ErrorDesc = "";

                var customersRootObject = new CustomersRootObject();
                customersRootObject.Customers.Add(customerDto);

                var json = JsonFieldsSerializer.Serialize(customersRootObject, string.Empty);

                _logger.Information("New guest user created with ID: " + customer.Id, customer: customer);

                return new ErrorActionResult(json, HttpStatusCode.OK);

            }
            catch
            {
                var errorsJson = GetJsonErrorObject("2030", "GenericError", "Error");
                return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
            }


        }


        //[HttpsRequirement(SslRequirement.Yes)]
        ////available even when navigation is not allowed
        //[CheckAccessPublicStore(true)]
        //public virtual IActionResult AccountActivation(string token, string email)

        [HttpPost]
        [Route("/api/verifyOtp")]
        [Route("/api/verifyOtpToChangePhone")]
        public virtual IActionResult VerifyOtp([FromBody] Models.Customer.RegisterModel model)
        {
            try
            {
                var customer = String.IsNullOrWhiteSpace(model.Email) ?
                                _customerService.GetCustomerByPhone(model.Phone) :
                                _customerService.GetCustomerByEmail(model.Email);

                var username = String.IsNullOrWhiteSpace(model.Email) ? PHONE : EMAIL;

                if (customer == null)
                {
                    //return new RawJsonActionResult(username + " does not exists.");
                    if (username == PHONE)
                    {
                        var errorJson = GetJsonErrorObject("2018", "MobileNoNotExist", "Error");
                        return new ErrorActionResult(errorJson, HttpStatusCode.OK);
                    }
                    else
                    {
                        var errorJson = GetJsonErrorObject("2019", "EmailIDNotExist", "Error");
                        return new ErrorActionResult(errorJson, HttpStatusCode.OK);
                    }
                }

                // Get OTP Valid Minuites. Check whether it is of data type - double else set 15.00.
                double ValidOTPMins = 15.00;
                var OTPValidMins = _localizationService.GetResource("Account.AccountActivation.OTPValidMinutes");
                if (Double.TryParse(OTPValidMins, out ValidOTPMins)) // IsDouble?
                {
                    ValidOTPMins = Convert.ToDouble(OTPValidMins);
                }
                else
                {
                    ValidOTPMins = 15.00;
                }

                var cTokenGeneratedDate = _genericAttributeService.GetAttribute<DateTime>(customer, NopCustomerDefaults.AccountActivationTokenDateGeneratedAttribute);

                var dateDiff = DateTime.Now - cTokenGeneratedDate;
                if (dateDiff.TotalMinutes > ValidOTPMins)
                {
                    var errorJson = GetJsonErrorObject("2031", "OTPExpired", "Error");
                    return new ErrorActionResult(errorJson, HttpStatusCode.OK);
                }

                //Get saved OTP from database.
                var cToken = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.AccountActivationTokenAttribute);

                if (string.IsNullOrEmpty(cToken))
                    return new RawJsonActionResult(_localizationService.GetResource("Account.AccountActivation.AlreadyActivated"));

                //Verify OTP
                if (!cToken.Equals(model.Otp, StringComparison.InvariantCultureIgnoreCase))
                {
                    //return new RawJsonActionResult("Invalid OTP");
                    var errorJson = GetJsonErrorObject("2017", "InvalidOTP", "Error");
                    return new ErrorActionResult(errorJson, HttpStatusCode.OK);
                }                

                if (!String.IsNullOrWhiteSpace(model.NewPhone)) //Change Phone Number.
                {
                    _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.PhoneAttribute, model.NewPhone);

                    //set OTP as blank; blank OTP indicates user is active;
                    _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.AccountActivationTokenAttribute, "");
                    _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.AccountActivationTokenDateGeneratedAttribute, "");
                    
                    var errorsJson = GetJsonErrorObject("2025", "PhoneNoChanged", "");
                    return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
                }
                else
                {
                    //activate user account
                    customer.Active = true;
                    _customerService.UpdateCustomer(customer);

                    //send welcome message
                    //_workflowMessageService.SendCustomerWelcomeMessage(customer, _workContext.WorkingLanguage.Id);

                    var customerDto = _customerApiService.GetCustomerById(customer.Id);
                    //var customerDto = customer.ToDto();

                    customerDto.ErrorCode = "2020";
                    customerDto.ErrorTitle = "AccountActivated";
                    customerDto.ErrorDesc = "Congratulations!";

                    var customersRootObject = new CustomersRootObject();
                    customersRootObject.Customers.Add(customerDto);

                    var json = JsonFieldsSerializer.Serialize(customersRootObject, string.Empty);

                    //notifications if OTP is success
                    if (_customerSettings.NotifyNewCustomerRegistration)
                        _workflowMessageService.SendCustomerRegisteredNotificationMessage(customer, _localizationSettings.DefaultAdminLanguageId);
                    //Send Notification to customer
                    _workflowMessageService.SendCustomerRegisteredNotificationMessage(customer, _localizationSettings.DefaultAdminLanguageId, true);

                    //set OTP as blank; blank OTP indicates user is active;
                    _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.AccountActivationTokenAttribute, "");
                    _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.AccountActivationTokenDateGeneratedAttribute, "");

                    return new ErrorActionResult(json, HttpStatusCode.OK);

                    //var errorsJson = GetJsonErrorObject("2020", "AccountActivated", "Congratulations!");
                    //return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
                }
            }
            catch
            {
                var errorsJson = GetJsonErrorObject("2030", "GenericError", "Error");
                return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
            }

        }

        [HttpPost]
        [Route("/api/resendOtp")]
        [Route("/api/changeMobile")]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public virtual IActionResult ResendOtp([FromBody] Models.Customer.RegisterModel model)
        {
            if (!String.IsNullOrWhiteSpace(model.NewPhone))
            {
                var customerExists = _customerService.GetCustomerByPhone(model.NewPhone);
                if (customerExists != null)
                {
                    var errorJson = GetJsonErrorObject("2026", "NewPhoneAlreadyExists", "Error");
                    return new ErrorActionResult(errorJson, HttpStatusCode.OK);
                }
            }

            var customer = String.IsNullOrWhiteSpace(model.Email) ?
                            _customerService.GetCustomerByPhone(model.Phone) :
                            _customerService.GetCustomerByEmail(model.Email);

            var username = String.IsNullOrWhiteSpace(model.Email) ? PHONE : EMAIL;
            if (customer == null)//|| !customer.Active || customer.Deleted)
            {
                if (username == PHONE)
                {
                    var errorJson = GetJsonErrorObject("2018", "MobileNoNotExist", "Error");
                    return new ErrorActionResult(errorJson, HttpStatusCode.OK);
                }
                else if (username == EMAIL)
                {
                    var errorJson = GetJsonErrorObject("2019", "EmailIDNotExist", "Error");
                    return new ErrorActionResult(errorJson, HttpStatusCode.OK);
                }
            }

            if (SendOTP(customer, model, username))
            {
                if (username == PHONE)
                {
                    var errorJson = GetJsonErrorObject("2011", "OTPSentOnPhone", "");
                    return new ErrorActionResult(errorJson, HttpStatusCode.OK);
                }
                else if (username == EMAIL)
                {
                    var errorJson = GetJsonErrorObject("2021", "OTPSentOnMail", "");
                    return new ErrorActionResult(errorJson, HttpStatusCode.OK);
                }
            }

            var errorsJson = GetJsonErrorObject("2015", "OTPResendingError", "Error");
            return new ErrorActionResult(errorsJson, HttpStatusCode.OK);

        }

        //Forgot Password
        [HttpPost]
        [Route("/api/forgotPasswordRecovery")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public virtual IActionResult ForgotPasswordRecovery([FromBody] Models.Customer.RegisterModel model)
        {
            try
            {
                //1. Verify Email ID
                var customer = _customerService.GetCustomerByEmail(model.Email);
                if (customer == null || !customer.Active || customer.Deleted)
                {
                    var error2Json = GetJsonErrorObject("2019", "EmailIDNotExist", "Error");
                    return new ErrorActionResult(error2Json, HttpStatusCode.OK);
                }
                // 2. Get OTP Valid Minuites. Check whether it is of data type - double else set 15.00.
                double ValidOTPMins;
                var OTPValidMins = _localizationService.GetResource("Account.AccountActivation.OTPValidMinutes");
                if (Double.TryParse(OTPValidMins, out ValidOTPMins)) // IsDouble?
                {
                    ValidOTPMins = Convert.ToDouble(OTPValidMins);
                }
                else
                {
                    ValidOTPMins = 15.00;
                }

                var cTokenGeneratedDate = _genericAttributeService.GetAttribute<DateTime>(customer, NopCustomerDefaults.AccountActivationTokenDateGeneratedAttribute);

                var dateDiff = DateTime.Now - cTokenGeneratedDate;
                if (dateDiff.TotalMinutes > ValidOTPMins)
                {
                    var error1Json = GetJsonErrorObject("2031", "OTPExpired", "Error");
                    return new ErrorActionResult(error1Json, HttpStatusCode.OK);
                }

                //2.1 Get saved OTP from database.
                var cToken = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.AccountActivationTokenAttribute);

                //3. Verify OTP
                if (!cToken.Equals(model.Otp, StringComparison.InvariantCultureIgnoreCase))
                {
                    var error1Json = GetJsonErrorObject("2017", "InvalidOTP", "Error");
                    return new ErrorActionResult(error1Json, HttpStatusCode.OK);
                }

                //4. At this point request is valid & OTP is verified. Now encrypt and save password.
                var customerPassword = new CustomerPassword
                {
                    Customer = customer,
                    PasswordFormat = _customerSettings.DefaultPasswordFormat,
                    CreatedOnUtc = DateTime.UtcNow
                };
                switch (_customerSettings.DefaultPasswordFormat)
                {
                    case PasswordFormat.Clear:
                        customerPassword.Password = model.Password;
                        break;
                    case PasswordFormat.Encrypted:
                        customerPassword.Password = _encryptionService.EncryptText(model.Password);
                        break;
                    case PasswordFormat.Hashed:
                        var saltKey = _encryptionService.CreateSaltKey(NopCustomerServiceDefaults.PasswordSaltKeySize);
                        customerPassword.PasswordSalt = saltKey;
                        customerPassword.Password = _encryptionService.CreatePasswordHash(model.Password, saltKey, _customerSettings.HashedPasswordFormat);
                        break;
                }

                _customerService.InsertCustomerPassword(customerPassword);

                //publish event
                _eventPublisher.Publish(new CustomerPasswordChangedEvent(customerPassword));

                //5. Set OTP as blank; which indicates user is Active;
                _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.AccountActivationTokenAttribute, "");
                _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.AccountActivationTokenDateGeneratedAttribute, "");

                var errorJson = GetJsonErrorObject("2022", "PasswordReset", "Password Reset");
                return new ErrorActionResult(errorJson, HttpStatusCode.OK);
            }
            catch
            {
                var errorJson = GetJsonErrorObject("2023", "PasswordResetError", "Error");
                return new ErrorActionResult(errorJson, HttpStatusCode.OK);
            }

        }


        public string SendOTPOnPhone(string Phone, string otpCode)
        {
            //var SMSBody = otpCode + + " is your OTP to complete the registration. It is valid for 15 minutes.";
            //var LangEnglish = "L";
            //var AccountID = 1178;
            //var Username = "almailemusr"; //"almalmtestuser";
            //var Password = "ALM@748";
            //var SenderID = "FCC";
            //var HttpAPILink = "http://62.215.226.174/fccsms_P.aspx?UID=";//UID=almalmtestuser&P=almelamtst48&S=FCC&G=965********&M=<Msg>&L=<Language>
            //var ErrorMsg = "An error occured while sending OTP, please try again later.";
            var ErrorMsg = _localizationService.GetResource("Account.AccountActivation.OtpSendingErrorMessage");

            try
            {
                var LangEnglish = "L";
                var Username = DataSettingsManager.LoadSettings(fileProvider: _fileProvider).SMSGateway_Username;
                var Password = DataSettingsManager.LoadSettings(fileProvider: _fileProvider).SMSGateway_Password;
                var SenderID = DataSettingsManager.LoadSettings(fileProvider: _fileProvider).SMSGateway_SenderID;
                var HttpAPILink = DataSettingsManager.LoadSettings(fileProvider: _fileProvider).SMSGateway_HttpAPILink;

                // {0} is your OTP to complete the registration. It is valid for {1} minutes.
                var SMSBody = string.Format(_localizationService.GetResource("Account.AccountActivation.OtpMessage"), otpCode, _localizationService.GetResource("Account.AccountActivation.OtpValidMinutes"));

                //SenderID = _localizationService.GetResource("smsaccount.senderid");
                Phone = "965" + Phone;
                HttpAPILink = HttpAPILink + Username + "&P=" + Password + "&S=" + SenderID + "&G=" + Phone + "&M=" + SMSBody + "&L=" + LangEnglish;

                HttpWebRequest objH = (HttpWebRequest)WebRequest.Create(HttpAPILink);
                objH.Method = "GET";
                objH.Timeout = 60000;
                HttpWebResponse oResponse = (HttpWebResponse)objH.GetResponse();
                if (oResponse != null)
                {
                    if (oResponse.StatusCode == HttpStatusCode.OK) // Web request executed successfully!
                    {
                        var response = new StreamReader(oResponse.GetResponseStream()).ReadToEnd();
                        return response;
                    }
                }
                return ErrorMsg;
            }
            catch (Exception exc)
            {
                _logger.Error("In Catch: An error occured while sending OTP! Message: " + exc.Message);
                return ErrorMsg;
            }

        }

        public bool SendOTPOnMail(Customer customer, string otpCode)
        {
            try
            {
                var emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);
                if (emailAccount == null)
                    emailAccount = _emailAccountService.GetAllEmailAccounts().FirstOrDefault();
                if (emailAccount == null)
                    return false;//throw new NopException("Email account can't be loaded");

                var CustomerFullName = _customerService.GetCustomerFullName(customer);
                var email = new QueuedEmail
                {
                    Priority = QueuedEmailPriority.High,
                    EmailAccountId = emailAccount.Id,
                    FromName = emailAccount.DisplayName,
                    From = emailAccount.Email,
                    ToName = CustomerFullName,
                    To = customer.Email,
                    Subject = _localizationService.GetResource("account.otpmail.subject"),
                    Body = string.Format(_localizationService.GetResource("account.otpmail.body"), CustomerFullName, otpCode),
                    CreatedOnUtc = DateTime.UtcNow,
                    DontSendBeforeDateUtc = null
                };
                _queuedEmailService.InsertQueuedEmail(email);
                return true;
                //SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.SendEmail.Queued"));
            }
            catch (Exception exc)
            {
                _logger.Error("In Catch: An error occured while sending OTP on email. Reason: " + exc.Message);
                return false;
            }

            //return RedirectToAction("Edit", new { id = customer.Id });
        }

        //This is a temp API for mobile developers to know OTP.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        [Route("/api/GetOtpFromNopDB")]
        public virtual IActionResult GetOtp([FromBody] Models.Customer.RegisterModel model)
        {
            var customer = String.IsNullOrWhiteSpace(model.Email) ?
                            _customerService.GetCustomerByPhone(model.Phone) :
                            _customerService.GetCustomerByEmail(model.Email);

            var username = String.IsNullOrWhiteSpace(model.Email) ? PHONE : EMAIL;
            if (customer == null)
            {
                if (username == PHONE)
                {
                    var errorJson = GetJsonErrorObject("2018", "MobileNoNotExist", "Error");
                    return new ErrorActionResult(errorJson, HttpStatusCode.OK);
                }
                else
                {
                    var errorJson = GetJsonErrorObject("2019", "EmailIDNotExist", "Error");
                    return new ErrorActionResult(errorJson, HttpStatusCode.OK);
                }
            }

            //Get saved OTP from database.
            var cToken = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.AccountActivationTokenAttribute); // test code..

            if (string.IsNullOrEmpty(cToken))
                return new RawJsonActionResult(_localizationService.GetResource("Account.AccountActivation.AlreadyActivated"));

            return new RawJsonActionResult(cToken);


        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        [Route("/api/logout")]
        public virtual IActionResult Logout(string Email)
        {

            //activity log
            _customerActivityService.InsertActivity(_workContext.CurrentCustomer, "PublicStore.Logout",
                    _localizationService.GetResource("ActivityLog.PublicStore.Logout"), _workContext.CurrentCustomer);

            //standard logout 
            _authenticationService.SignOut();

            //raise logged out event       
            _eventPublisher.Publish(new CustomerLoggedOutEvent(_workContext.CurrentCustomer));

            //var customer = _customerService.GetCustomerByEmail(Email); 

            //if (customer == null)
            //{
            //    var errorJson = GetJsonErrorObject("2019", "EmailIDNotExist", "Error");
            //    return new ErrorActionResult(errorJson, HttpStatusCode.OK);
            //}
            return new ErrorActionResult(_localizationService.GetResource("ActivityLog.PublicStore.Logout"), HttpStatusCode.OK);
        }

        [HttpPost]
        [Route("/api/changePassword")]
        public virtual IActionResult ChangePassword([FromBody]Models.Customer.ChangePasswordModel model)
        {
            try
            {
                if (model.NewPassword == model.OldPassword)
                {
                    var errorJson = GetJsonErrorObject("2027", "New&OldPasswordIsSame", "Error");
                    return new ErrorActionResult(errorJson, HttpStatusCode.OK);
                }
                var customer = String.IsNullOrWhiteSpace(model.Email) ?
                               _customerService.GetCustomerByPhone(model.Phone) :
                               _customerService.GetCustomerByEmail(model.Email);

                var username = String.IsNullOrWhiteSpace(model.Email) ? PHONE : EMAIL;
                if (customer == null)
                {
                    if (username == PHONE)
                    {
                        var errorJson = GetJsonErrorObject("2018", "MobileNoNotExist", "Error");
                        return new ErrorActionResult(errorJson, HttpStatusCode.OK);
                    }
                    else
                    {
                        var errorJson = GetJsonErrorObject("2019", "EmailIDNotExist", "Error");
                        return new ErrorActionResult(errorJson, HttpStatusCode.OK);
                    }
                }

                if (!PasswordsMatch(_customerService.GetCurrentPassword(customer.Id), model.OldPassword))
                {
                    var errorJson = GetJsonErrorObject("2028", "OldPasswordDoesntMatch", "Error");
                    return new ErrorActionResult(errorJson, HttpStatusCode.OK);
                }

                var changePasswordRequest = new ChangePasswordRequest(
                                                                        customer.Email,
                                                                        true,
                                                                        _customerSettings.DefaultPasswordFormat,
                                                                        model.NewPassword,
                                                                        model.OldPassword
                                                                     );
                var changePasswordResult = _customerRegistrationService.ChangePassword(changePasswordRequest);
                if (changePasswordResult.Success)
                {
                    var errorJson = GetJsonErrorObject("2046", "PasswordChanged", "");
                    return new ErrorActionResult(errorJson, HttpStatusCode.OK);
                }
                else
                {
                    var errorsJson = GetJsonErrorObject("2030", "GenericError", "Error");
                    return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
                }

            }
            catch
            {
                var errorsJson = GetJsonErrorObject("2030", "GenericError", "Error");
                return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
            }
        }

        protected bool PasswordsMatch(CustomerPassword customerPassword, string enteredPassword)
        {
            if (customerPassword == null || string.IsNullOrEmpty(enteredPassword))
                return false;

            var savedPassword = string.Empty;
            switch (customerPassword.PasswordFormat)
            {
                case PasswordFormat.Clear:
                    savedPassword = enteredPassword;
                    break;
                case PasswordFormat.Encrypted:
                    savedPassword = _encryptionService.EncryptText(enteredPassword);
                    break;
                case PasswordFormat.Hashed:
                    savedPassword = _encryptionService.CreatePasswordHash(enteredPassword, customerPassword.PasswordSalt, _customerSettings.HashedPasswordFormat);
                    break;
            }

            if (customerPassword.Password == null)
                return false;

            return customerPassword.Password.Equals(savedPassword);
        }

        [HttpPost]
        [Route("/api/updateProfile")]
        //Required parameters for this API: FirstName, LastName, Email & Id.
        //Fields to update: FirstName, LastName & Email.
        //Get customer by ID.
        public virtual IActionResult UpdateProfile([FromBody] BaseCustomerDto model)
        {
            var errorCode = "2029";
            var errorTitle = "ProfileUpdated";
            try
            {

                var customer = _customerService.GetCustomerById(model.Id);

                if (customer == null)
                {
                    var error2Json = GetJsonErrorObject("2042", "InvalidCustomerID", "Error");
                    return new ErrorActionResult(error2Json, HttpStatusCode.OK);
                }
                //Update First Name.
                if (model.FirstName != null)
                {
                    _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.FirstNameAttribute, model.FirstName);
                }
                //Update Last Name.
                if (model.LastName != null)
                {
                    _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.LastNameAttribute, model.LastName);
                }
                //Update Email.
                if (model.Email != null && customer.Email != model.Email)
                {
                    customer.Email = model.Email;
                    _customerService.UpdateCustomer(customer);
                    errorCode = "2047";
                    errorTitle = "ProfileUpdatedWithEmail";
                }
                //var errorJson = GetJsonErrorObject("2029", "ProfileUpdated", "");
                //return new ErrorActionResult(errorJson, HttpStatusCode.OK);

                var customerDto = _customerApiService.GetCustomerById(customer.Id);

                customerDto.ErrorCode = errorCode;
                customerDto.ErrorTitle = errorTitle;
                customerDto.ErrorDesc = "";

                var customersRootObject = new CustomersRootObject();
                customersRootObject.Customers.Add(customerDto);

                var json = JsonFieldsSerializer.Serialize(customersRootObject, string.Empty);

                return new ErrorActionResult(json, HttpStatusCode.OK);

            }
            catch
            {
                var error1Json = GetJsonErrorObject("2030", "GenericError", "Error");
                return new ErrorActionResult(error1Json, HttpStatusCode.OK);
            }
        }

        #region Addresses


        [HttpPost]
        [Route("/api/customerAddress")]
        [Route("/api/addNewAddress")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), 422)]
        public virtual IActionResult AddressCreate([FromBody] CustomerAddressModel model)
        {
            try
            {
                //try to get a customer with the specified id
                var customer = _customerService.GetCustomerById(model.CustomerId);
                if (customer == null)
                {
                    var errorsJson = GetJsonErrorObject("2042", "InvalidCustomerID", "Error");
                    return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
                }

                if (ModelState.IsValid)
                {
                    var address = model.Address.ToEntity<Address>();

                    address.CreatedOnUtc = DateTime.UtcNow;

                    //customer.Addresses.Add(address);
                    customer.CustomerAddressMappings.Add(new CustomerAddressMapping { Address = address });

                    customer.BillingAddress = address;
                    customer.ShippingAddress = address;

                    _customerService.UpdateCustomer(customer);

                    var errorsJson1 = GetJsonErrorObject("2053", "AddNewAddressSuccess", "");
                    //return new ErrorActionResult(errorsJson1, HttpStatusCode.OK);

                    //Get Address list from customer object. Pass success error code details.
                    var modelAddressList = GetAddressListByCustomer(customer, "2053", "AddNewAddressSuccess", "");

                    //Convert result to Json string.
                    var ModelJsonString = JsonConvert.SerializeObject(modelAddressList);

                    return new ErrorActionResult(ModelJsonString, HttpStatusCode.OK);
                }
                //var logDetails = ModelState.Root.Children.ToList().FirstOrDefault().Errors.FirstOrDefault().ErrorMessage.ToString();

                //if we got this far, something failed, redisplay form.
                var errorsJson2 = GetJsonErrorObject("2052", "AddNewAddressError", "Error");
                return new ErrorActionResult(errorsJson2, HttpStatusCode.OK);
            }
            catch
            {
                _logger.Error("Error! Event: Add new address.");
                var errorsJson = GetJsonErrorObject("2030", "GenericError", "Error");
                return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
            }
        }



        [HttpPost]
        //[Route("/api/customerAddress")]
        [Route("/api/editAddress")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), 422)]
        public virtual IActionResult AddressEdit([FromBody] CustomerAddressModel model)
        {
            try
            {
                //try to get a customer with the specified id
                var customer = _customerService.GetCustomerById(model.CustomerId);
                if (customer == null)
                {
                    var errorsJson = GetJsonErrorObject("2042", "InvalidCustomerID", "Error");
                    return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
                }

                //try to get an address with the specified id
                var address = _addressService.GetAddressById(model.Address.Id);
                if (address == null)
                {
                    var errorsJson1 = GetJsonErrorObject("2051", "InvalidAddressID", "Error");
                    return new ErrorActionResult(errorsJson1, HttpStatusCode.OK);
                }

                if (ModelState.IsValid)
                {
                    address = model.Address.ToEntity(address);
                    _addressService.UpdateAddress(address);

                    //var errorsJson2 = GetJsonErrorObject("2055", "UpdateAddressSuccess", "");
                    //return new ErrorActionResult(errorsJson2, HttpStatusCode.OK);

                    //Get Address list from customer object. Pass success error code details.
                    var modelAddressList = GetAddressListByCustomer(customer, "2055", "UpdateAddressSuccess", "");

                    //Convert result to Json string.
                    var ModelJsonString = JsonConvert.SerializeObject(modelAddressList);

                    return new ErrorActionResult(ModelJsonString, HttpStatusCode.OK);

                }

                //if we got this far, something failed, redisplay form
                var errorsJson3 = GetJsonErrorObject("2054", "UpdateAddressError", "Error");
                return new ErrorActionResult(errorsJson3, HttpStatusCode.OK);
            }
            catch
            {
                _logger.Error("Error! Event: Edit address.");
                var errorsJson4 = GetJsonErrorObject("2030", "GenericError", "Error");
                return new ErrorActionResult(errorsJson4, HttpStatusCode.OK);
            }
        }


        [HttpGet]
        [Route("/api/customerAddress/{CustomerId}")]
        [Route("/api/getAddressList/{CustomerId}")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public virtual IActionResult AddressesSelect(int CustomerId)
        {

            //try to get a customer with the specified id
            var customer = _customerService.GetCustomerById(CustomerId);
            if (customer == null)
            {
                var errorsJson = GetJsonErrorObject("2042", "InvalidCustomerID", "Error");
                return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
            }



            var model = GetAddressListByCustomer(customer, "", "", "");

            return Json(model);
        }


        [HttpPost]
        //[Route("/api/customerAddress/{customerId}/{addressId}")]
        [Route("/api/deleteAddress/{customerId}/{addressId}")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public virtual IActionResult AddressDelete(int customerId, int addressId)
        {

            try
            {
                //try to get a customer with the specified id
                var customer = _customerService.GetCustomerById(customerId);
                if (customer == null)
                {
                    var errorsJson1 = GetJsonErrorObject("2042", "InvalidCustomerID", "Error");
                    return new ErrorActionResult(errorsJson1, HttpStatusCode.OK);
                }

                //try to get an address with the specified id
                var address = customer.Addresses.FirstOrDefault(a => a.Id == addressId);
                if (address == null)
                {
                    var errorsJson2 = GetJsonErrorObject("2051", "InvalidAddressID", "Error");
                    return new ErrorActionResult(errorsJson2, HttpStatusCode.OK);
                }

                _customerService.RemoveCustomerAddress(customer, address);
                _customerService.UpdateCustomer(customer);

                //now delete the address record
                _addressService.DeleteAddress(address);

                var errorsJson3 = GetJsonErrorObject("2057", "DeleteAddressSuccess", "");
                //return new ErrorActionResult(errorsJson3, HttpStatusCode.OK);

                //Get Address list from customer object. Pass success error code details.
                var model = GetAddressListByCustomer(customer, "2057", "DeleteAddressSuccess", "");

                //Convert result to Json string.
                var ModelJsonString = JsonConvert.SerializeObject(model);

                return new ErrorActionResult(ModelJsonString, HttpStatusCode.OK);
            }
            catch
            {
                _logger.Error("Error! Event: Delete address.");
                var errorsJson = GetJsonErrorObject("2056", "DeleteAddressError", "Error");
                return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
            }
        }

        [HttpPost]
        [Route("/api/setBillingShippingAddress/{customerId}/{addressId}")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public virtual IActionResult SetBillingShippingAddress(int customerId, int addressId)
        {
            try
            {
                //try to get a customer with the specified id
                var customer = _customerService.GetCustomerById(customerId);
                if (customer == null)
                {
                    var errorsJson1 = GetJsonErrorObject("2042", "InvalidCustomerID", "Error");
                    return new ErrorActionResult(errorsJson1, HttpStatusCode.OK);
                }

                //try to get an address with the specified id
                var address = _addressService.GetAddressById(addressId);
                if (address == null)
                {
                    var errorsJson1 = GetJsonErrorObject("2051", "InvalidAddressID", "Error");
                    return new ErrorActionResult(errorsJson1, HttpStatusCode.OK);
                }

                var flag = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.ProceedToCheckout, _storeContext.CurrentStore.Id);
                if (flag != "true")
                {
                    var errorsJson = GetJsonErrorObject("2070", "CartIsModified", "Error");
                    return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
                }

                customer.BillingAddressId = addressId;
                customer.ShippingAddressId = addressId;

                _customerService.UpdateCustomer(customer);

                _logger.Information("Info! Billing & Shipping Address Updated for customer: " + customer.Id + "; Email: " + customer.Email + "; AddressID: " + addressId);

                //Get Address list from customer object. Pass success error code details.
                var model = GetAddressListByCustomer(customer, "2055", "UpdateAddressSuccess", "");
                //model.Data = null;
                List<Nop.Web.Areas.Admin.Models.Common.AddressModel> oAddressModelList = new List<Nop.Web.Areas.Admin.Models.Common.AddressModel>();
                Nop.Web.Areas.Admin.Models.Common.AddressModel oAddressModel = new Nop.Web.Areas.Admin.Models.Common.AddressModel();
                Address oAddress = customer.Addresses.FirstOrDefault(o => o.Id == addressId);
                oAddressModel.Id = oAddress.Id;

                oAddressModelList.Add(oAddressModel);

                model.Data = oAddressModelList;

                //model.Data = customer.Addresses.Select(a =>
                //{
                //    var addressModel = a.ToModel<Web.Areas.Admin.Models.Common.AddressModel>();
                //    return addressModel.;
                //});//.FirstOrDefault(c => c.Id==addressId);

                //Convert result to Json string.
                var ModelJsonString = JsonConvert.SerializeObject(model);

                return new ErrorActionResult(ModelJsonString, HttpStatusCode.OK);
            }
            catch
            {
                _logger.Error("Error! Event: Set Billing & Shipping Address.");
                var errorsJson = GetJsonErrorObject("2054", "UpdateAddressError", "Error");
                return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
            }
        }


        private CustomerAddressListModel GetAddressListByCustomer(Customer customer, string errorCode, string errorDesc, string errorTitle = "")
        {
            //get customer addresses
            var addresses = customer.Addresses
                .OrderByDescending(address => address.CreatedOnUtc).ThenByDescending(address => address.Id).ToList();

            /*Get only valid addresses. CASE: Guest User: While placing order; basic info is collected without Governorate & Address1, which should not be in list of registered user's address list.*/
            addresses = addresses.Where(a => a.Company != null && a.Address1 != null).ToList();

            var errorResponse = new ErrorResponse();
            errorResponse.ErrorCode = errorCode;
            errorResponse.ErrorDesc = errorDesc;
            errorResponse.ErrorTitle = errorTitle;

            var model = new CustomerAddressListModel
            {
                Data = addresses.Select(address =>
                {
                    //fill in model values from the entity        
                    var addressModel = address.ToModel<Nop.Web.Areas.Admin.Models.Common.AddressModel>();
                    return addressModel;
                }),
                Total = addresses.Count,
                Error = errorResponse
            };

            return model;

        }

        #endregion

        [HttpGet]
        [Route("/api/getRewardPointsHistory/{customerId}")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public virtual IActionResult RewardPointsHistorySelect(int customerId)//([FromBody] Web.Areas.Admin.Models.Customers.CustomerRewardPointsSearchModel searchModel)
        {
            var searchModel = new Web.Areas.Admin.Models.Customers.CustomerRewardPointsSearchModel();
            searchModel.CustomerId = customerId;
            //try to get a customer with the specified id
            var customer = _customerService.GetCustomerById(searchModel.CustomerId);
            if (customer == null)
            {
                var errorsJson1 = GetJsonErrorObject("2042", "InvalidCustomerID", "Error");
                return new ErrorActionResult(errorsJson1, HttpStatusCode.OK);
            }

            //prepare model
            var model = _customerModelFactoryAdmin.PrepareRewardPointsListModel(searchModel, customer);

            var json = JsonConvert.SerializeObject(model);
            //var json = JsonConvert.SerializeObject(model.Data.FirstOrDefault()); // To get the latest record of reward points.

            //var json1 = new JavaScriptSerializer().Serialize(model);
            return new ErrorActionResult(json, HttpStatusCode.OK);
        }
        [HttpGet]
        [Route("/api/userPreferredAppLanguage/{customerId}")]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public virtual IActionResult UserPreferredAppLanguage(int customerId)
        {
            try
            {
                var customer = _customerService.GetCustomerById(customerId);
                if (customer == null || customer.Deleted)
                {
                    var errorsJson2 = GetJsonErrorObject("2042", "InvalidCustomerID", "Error");
                    return new ErrorActionResult(errorsJson2, HttpStatusCode.OK);
                }

                var LanguageId = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.UserPreferredAppLanguage);

                // return new RawJsonActionResult(LanguageId);

                var errorsJson = LanguageId;
                return new ErrorActionResult(errorsJson, HttpStatusCode.OK);

            }
            catch
            {
                var errorsJson = GetJsonErrorObject("2030", "GenericError", "Error");
                return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
            }
        }


        [HttpPost]
        [Route("/api/userPreferredAppLanguage/{customerId}/{languageId}")]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public virtual IActionResult UserPreferredAppLanguage(int customerId, int languageId)
        {
            try
            {
                var customer = _customerService.GetCustomerById(customerId);
                if (customer == null || customer.Deleted)
                {
                    var errorsJson2 = GetJsonErrorObject("2042", "InvalidCustomerID", "Error");
                    return new ErrorActionResult(errorsJson2, HttpStatusCode.OK);
                }

                _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.UserPreferredAppLanguage, languageId);

                var errorsJson = GetJsonErrorObject("2048", "SetAppLanguageSuccessfully", "");
                return new ErrorActionResult(errorsJson, HttpStatusCode.OK);

            }
            catch
            {
                var errorsJson = GetJsonErrorObject("2030", "GenericError", "Error");
                return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
            }
        }




        //**********************************************************************************************//
        /// <summary>
        /// Retrieve all customers of a shop
        /// </summary>
        /// <response code="200">OK</response>
        /// <response code="400">Bad request</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/customers")]
        [ProducesResponseType(typeof(CustomersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetCustomers(CustomersParametersModel parameters)
        {
            if (parameters.Limit < Configurations.MinLimit || parameters.Limit > Configurations.MaxLimit)
            {
                return Error(HttpStatusCode.BadRequest, "limit", "Invalid limit parameter");
            }

            if (parameters.Page < Configurations.DefaultPageValue)
            {
                return Error(HttpStatusCode.BadRequest, "page", "Invalid request parameters");
            }

            var allCustomers = _customerApiService.GetCustomersDtos(parameters.CreatedAtMin, parameters.CreatedAtMax, parameters.Limit, parameters.Page, parameters.SinceId);

            var customersRootObject = new CustomersRootObject()
            {
                Customers = allCustomers
            };

            var json = JsonFieldsSerializer.Serialize(customersRootObject, parameters.Fields);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// Retrieve customer by spcified id
        /// </summary>
        /// <param name="id">Id of the customer</param>
        /// <param name="fields">Fields from the customer you want your json to contain</param>
        /// <response code="200">OK</response>
        /// <response code="404">Not Found</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/customers/{id}")]
        [ProducesResponseType(typeof(CustomersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetCustomerById(int id, string fields = "")
        {
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }

            var customer = _customerApiService.GetCustomerById(id);

            if (customer == null)
            {
                return Error(HttpStatusCode.NotFound, "customer", "not found");
            }

            var customersRootObject = new CustomersRootObject();
            customersRootObject.Customers.Add(customer);

            var json = JsonFieldsSerializer.Serialize(customersRootObject, fields);

            return new RawJsonActionResult(json);
        }


        /// <summary>
        /// Get a count of all customers
        /// </summary>
        /// <response code="200">OK</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/customers/count")]
        [ProducesResponseType(typeof(CustomersCountRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        public IActionResult GetCustomersCount()
        {
            var allCustomersCount = _customerApiService.GetCustomersCount();

            var customersCountRootObject = new CustomersCountRootObject()
            {
                Count = allCustomersCount
            };

            return Ok(customersCountRootObject);
        }

        /// <summary>
        /// Search for customers matching supplied query
        /// </summary>
        /// <response code="200">OK</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/customers/search")]
        [ProducesResponseType(typeof(CustomersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult Search(CustomersSearchParametersModel parameters)
        {
            if (parameters.Limit <= Configurations.MinLimit || parameters.Limit > Configurations.MaxLimit)
            {
                return Error(HttpStatusCode.BadRequest, "limit", "Invalid limit parameter");
            }

            if (parameters.Page <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "page", "Invalid page parameter");
            }

            var customersDto = _customerApiService.Search(parameters.Query, parameters.Order, parameters.Page, parameters.Limit);

            var customersRootObject = new CustomersRootObject()
            {
                Customers = customersDto
            };

            var json = JsonFieldsSerializer.Serialize(customersRootObject, parameters.Fields);

            return new RawJsonActionResult(json);
        }

        [HttpPost]
        [Route("/api/customers")]
        [ProducesResponseType(typeof(CustomersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult CreateCustomer([ModelBinder(typeof(JsonModelBinder<CustomerDto>))] Delta<CustomerDto> customerDelta)
        {
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            //If the validation has passed the customerDelta object won't be null for sure so we don't need to check for this.

            // Inserting the new customer
            var newCustomer = _factory.Initialize();
            customerDelta.Merge(newCustomer);

            foreach (var address in customerDelta.Dto.Addresses)
            {
                // we need to explicitly set the date as if it is not specified
                // it will default to 01/01/0001 which is not supported by SQL Server and throws and exception
                if (address.CreatedOnUtc == null)
                {
                    address.CreatedOnUtc = DateTime.UtcNow;
                }
                newCustomer.Addresses.Add(address.ToEntity());
            }

            CustomerService.InsertCustomer(newCustomer);

            InsertFirstAndLastNameGenericAttributes(customerDelta.Dto.FirstName, customerDelta.Dto.LastName, newCustomer);

            if (!string.IsNullOrEmpty(customerDelta.Dto.LanguageId) && int.TryParse(customerDelta.Dto.LanguageId, out var languageId)
                && _languageService.GetLanguageById(languageId) != null)
            {
                _genericAttributeService.SaveAttribute(newCustomer, NopCustomerDefaults.LanguageIdAttribute, languageId);
            }

            //password
            if (!string.IsNullOrWhiteSpace(customerDelta.Dto.Password))
            {
                AddPassword(customerDelta.Dto.Password, newCustomer);
            }

            // We need to insert the entity first so we can have its id in order to map it to anything.
            // TODO: Localization
            // TODO: move this before inserting the customer.
            if (customerDelta.Dto.RoleIds.Count > 0)
            {
                AddValidRoles(customerDelta, newCustomer);

                CustomerService.UpdateCustomer(newCustomer);
            }

            // Preparing the result dto of the new customer
            // We do not prepare the shopping cart items because we have a separate endpoint for them.
            var newCustomerDto = newCustomer.ToDto();

            // This is needed because the entity framework won't populate the navigation properties automatically
            // and the country will be left null. So we do it by hand here.
            PopulateAddressCountryNames(newCustomerDto);

            // Set the fist and last name separately because they are not part of the customer entity, but are saved in the generic attributes.
            newCustomerDto.FirstName = customerDelta.Dto.FirstName;
            newCustomerDto.LastName = customerDelta.Dto.LastName;

            newCustomerDto.LanguageId = customerDelta.Dto.LanguageId;

            //activity log
            CustomerActivityService.InsertActivity("AddNewCustomer", LocalizationService.GetResource("ActivityLog.AddNewCustomer"), newCustomer);

            var customersRootObject = new CustomersRootObject();

            customersRootObject.Customers.Add(newCustomerDto);

            var json = JsonFieldsSerializer.Serialize(customersRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        [HttpPut]
        [Route("/api/customers/{id}")]
        [ProducesResponseType(typeof(CustomersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult UpdateCustomer([ModelBinder(typeof(JsonModelBinder<CustomerDto>))] Delta<CustomerDto> customerDelta)
        {
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            // Updateting the customer
            var currentCustomer = _customerApiService.GetCustomerEntityById(customerDelta.Dto.Id);

            if (currentCustomer == null)
            {
                return Error(HttpStatusCode.NotFound, "customer", "not found");
            }

            customerDelta.Merge(currentCustomer);

            if (customerDelta.Dto.RoleIds.Count > 0)
            {
                // Remove all roles
                while (currentCustomer.CustomerRoles.Count > 0)
                {
                    currentCustomer.CustomerRoles.Remove(currentCustomer.CustomerRoles.First());
                }

                AddValidRoles(customerDelta, currentCustomer);
            }

            if (customerDelta.Dto.Addresses.Count > 0)
            {
                var currentCustomerAddresses = currentCustomer.Addresses.ToDictionary(address => address.Id, address => address);

                foreach (var passedAddress in customerDelta.Dto.Addresses)
                {
                    var addressEntity = passedAddress.ToEntity();

                    if (currentCustomerAddresses.ContainsKey(passedAddress.Id))
                    {
                        _mappingHelper.Merge(passedAddress, currentCustomerAddresses[passedAddress.Id]);
                    }
                    else
                    {
                        currentCustomer.Addresses.Add(addressEntity);
                    }
                }
            }

            CustomerService.UpdateCustomer(currentCustomer);

            InsertFirstAndLastNameGenericAttributes(customerDelta.Dto.FirstName, customerDelta.Dto.LastName, currentCustomer);


            if (!string.IsNullOrEmpty(customerDelta.Dto.LanguageId) && int.TryParse(customerDelta.Dto.LanguageId, out var languageId)
                && _languageService.GetLanguageById(languageId) != null)
            {
                _genericAttributeService.SaveAttribute(currentCustomer, NopCustomerDefaults.LanguageIdAttribute, languageId);
            }

            //password
            if (!string.IsNullOrWhiteSpace(customerDelta.Dto.Password))
            {
                AddPassword(customerDelta.Dto.Password, currentCustomer);
            }

            // TODO: Localization

            // Preparing the result dto of the new customer
            // We do not prepare the shopping cart items because we have a separate endpoint for them.
            var updatedCustomer = currentCustomer.ToDto();

            // This is needed because the entity framework won't populate the navigation properties automatically
            // and the country name will be left empty because the mapping depends on the navigation property
            // so we do it by hand here.
            PopulateAddressCountryNames(updatedCustomer);

            // Set the fist and last name separately because they are not part of the customer entity, but are saved in the generic attributes.
            var firstNameGenericAttribute = _genericAttributeService.GetAttributesForEntity(currentCustomer.Id, typeof(Customer).Name)
                .FirstOrDefault(x => x.Key == "FirstName");

            if (firstNameGenericAttribute != null)
            {
                updatedCustomer.FirstName = firstNameGenericAttribute.Value;
            }

            var lastNameGenericAttribute = _genericAttributeService.GetAttributesForEntity(currentCustomer.Id, typeof(Customer).Name)
                .FirstOrDefault(x => x.Key == "LastName");

            if (lastNameGenericAttribute != null)
            {
                updatedCustomer.LastName = lastNameGenericAttribute.Value;
            }

            var languageIdGenericAttribute = _genericAttributeService.GetAttributesForEntity(currentCustomer.Id, typeof(Customer).Name)
                .FirstOrDefault(x => x.Key == "LanguageId");

            if (languageIdGenericAttribute != null)
            {
                updatedCustomer.LanguageId = languageIdGenericAttribute.Value;
            }

            //activity log
            CustomerActivityService.InsertActivity("UpdateCustomer", LocalizationService.GetResource("ActivityLog.UpdateCustomer"), currentCustomer);

            var customersRootObject = new CustomersRootObject();

            customersRootObject.Customers.Add(updatedCustomer);

            var json = JsonFieldsSerializer.Serialize(customersRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        [HttpDelete]
        [Route("/api/customers/{id}")]
        [GetRequestsErrorInterceptorActionFilter]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult DeleteCustomer(int id)
        {
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }

            var customer = _customerApiService.GetCustomerEntityById(id);

            if (customer == null)
            {
                return Error(HttpStatusCode.NotFound, "customer", "not found");
            }

            CustomerService.DeleteCustomer(customer);

            //remove newsletter subscription (if exists)
            foreach (var store in StoreService.GetAllStores())
            {
                var subscription = _newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmailAndStoreId(customer.Email, store.Id);
                if (subscription != null)
                    _newsLetterSubscriptionService.DeleteNewsLetterSubscription(subscription);
            }

            //activity log
            CustomerActivityService.InsertActivity("DeleteCustomer", LocalizationService.GetResource("ActivityLog.DeleteCustomer"), customer);

            return new RawJsonActionResult("{}");
        }

        private void InsertFirstAndLastNameGenericAttributes(string firstName, string lastName, Customer newCustomer)
        {
            // we assume that if the first name is not sent then it will be null and in this case we don't want to update it
            if (firstName != null)
            {
                _genericAttributeService.SaveAttribute(newCustomer, NopCustomerDefaults.FirstNameAttribute, firstName);
            }

            if (lastName != null)
            {
                _genericAttributeService.SaveAttribute(newCustomer, NopCustomerDefaults.LastNameAttribute, lastName);
            }
        }

        private void AddValidRoles(Delta<CustomerDto> customerDelta, Customer currentCustomer)
        {
            IList<CustomerRole> validCustomerRoles =
                _customerRolesHelper.GetValidCustomerRoles(customerDelta.Dto.RoleIds).ToList();

            // Add all newly passed roles
            foreach (var role in validCustomerRoles)
            {
                currentCustomer.CustomerRoles.Add(role);
            }
        }

        private void PopulateAddressCountryNames(CustomerDto newCustomerDto)
        {
            foreach (var address in newCustomerDto.Addresses)
            {
                SetCountryName(address);
            }

            if (newCustomerDto.BillingAddress != null)
            {
                SetCountryName(newCustomerDto.BillingAddress);
            }

            if (newCustomerDto.ShippingAddress != null)
            {
                SetCountryName(newCustomerDto.ShippingAddress);
            }
        }

        private void SetCountryName(AddressDto address)
        {
            if (string.IsNullOrEmpty(address.CountryName) && address.CountryId.HasValue)
            {
                var country = _countryService.GetCountryById(address.CountryId.Value);
                address.CountryName = country.Name;
            }
        }

        private void AddPassword(string newPassword, Customer customer)
        {
            // TODO: call this method before inserting the customer.
            var customerPassword = new CustomerPassword
            {
                Customer = customer,
                PasswordFormat = _customerSettings.DefaultPasswordFormat,
                CreatedOnUtc = DateTime.UtcNow
            };

            switch (_customerSettings.DefaultPasswordFormat)
            {
                case PasswordFormat.Clear:
                    {
                        customerPassword.Password = newPassword;
                    }
                    break;
                case PasswordFormat.Encrypted:
                    {
                        customerPassword.Password = _encryptionService.EncryptText(newPassword);
                    }
                    break;
                case PasswordFormat.Hashed:
                    {
                        var saltKey = _encryptionService.CreateSaltKey(5);
                        customerPassword.PasswordSalt = saltKey;
                        customerPassword.Password = _encryptionService.CreatePasswordHash(newPassword, saltKey, _customerSettings.HashedPasswordFormat);
                    }
                    break;
            }

            CustomerService.InsertCustomerPassword(customerPassword);

            // TODO: remove this.
            CustomerService.UpdateCustomer(customer);
        }


    }
}