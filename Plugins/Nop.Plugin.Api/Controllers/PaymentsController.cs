﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Core.Infrastructure;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Factories;
using Nop.Plugin.Api.Helpers;
using Nop.Plugin.Api.Services;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Shipping;
using Nop.Services.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Nop.Plugin.Api.Controllers
{
    using DTOs.Errors;
    using Nop.Plugin.Api.Models;
    using JSON.Serializers;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc.Controllers;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Routing;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;
    using Nop.Core.Configuration;
    using Nop.Core.Data;
    using Nop.Core.Domain.Payments;
    using Nop.Core.Gateway;
    using Nop.Plugin.Api.Constants;
    using Nop.Plugin.Api.DataAccess;
    using Nop.Plugin.Api.Domain;
    using Nop.Plugin.Api.DTOs.Payments;
    using Nop.Plugin.Api.JSON.ActionResults;
    using Nop.Services.Configuration;
    using Nop.Services.Helpers;
    using Nop.Web.Factories;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;
    using System.Xml;
    using Nop.Web.Models.Payments;

    [ApiAuthorize(Policy = JwtBearerDefaults.AuthenticationScheme, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PaymentsController : BaseApiController
    {
        private readonly IOrderApiService _orderApiService;
        private readonly IProductService _productService;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IOrderService _orderService;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IShippingService _shippingService;
        private readonly IDTOHelper _dtoHelper;
        private readonly IProductAttributeConverter _productAttributeConverter;
        private readonly IStoreContext _storeContext;
        private readonly IFactory<PaymentTransaction> _factory;
        private readonly IShoppingCartModelFactory _shoppingCartModelFactory;
        private readonly ISettingService _settingService;
        private readonly IWorkContext _workContext;
        private readonly ICustomerService _customerService;
        private readonly ILogger _logger;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IPaymentApiService _paymentApiService;
        protected readonly GatewayApiClient GatewayApiClient;
        protected readonly GatewayApiConfig GatewayApiConfig;

        // We resolve the order settings this way because of the tests.
        // The auto mocking does not support concreate types as dependencies. It supports only interfaces.
        private OrderSettings _orderSettings;

        private OrderSettings OrderSettings => _orderSettings ?? (_orderSettings = EngineContext.Current.Resolve<OrderSettings>());

        public PaymentsController(IOrderApiService orderApiService,
            IJsonFieldsSerializer jsonFieldsSerializer,
            IAclService aclService,
            ICustomerService customerService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IDiscountService discountService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            IProductService productService,
            IFactory<PaymentTransaction> factory,
            IOrderProcessingService orderProcessingService,
            IOrderService orderService,
            IShoppingCartService shoppingCartService,
            IGenericAttributeService genericAttributeService,
            IStoreContext storeContext,
            IShippingService shippingService,
            IPictureService pictureService,
            IDTOHelper dtoHelper,
            IShoppingCartModelFactory shoppingCartModelFactory,
             ISettingService settingService,
             IWorkContext workContext,
             ILogger logger,
            IProductAttributeConverter productAttributeConverter,
            IHostingEnvironment hostingEnvironment,
            IPaymentApiService paymentApiService,
            GatewayApiClient gatewayApiClient,
            IOptions<GatewayApiConfig> gatewayApiConfig
            )
            : base(jsonFieldsSerializer, aclService, customerService, storeMappingService,
                 storeService, discountService, customerActivityService, localizationService, pictureService)
        {
            _orderApiService = orderApiService;
            _factory = factory;
            _orderProcessingService = orderProcessingService;
            _orderService = orderService;
            _shoppingCartService = shoppingCartService;
            _genericAttributeService = genericAttributeService;
            _storeContext = storeContext;
            _shippingService = shippingService;
            _dtoHelper = dtoHelper;
            _productService = productService;
            _productAttributeConverter = productAttributeConverter;
            _shoppingCartModelFactory = shoppingCartModelFactory;
            _settingService = settingService;
            _workContext = workContext;
            _customerService = customerService;
            _logger = logger;
            _hostingEnvironment = hostingEnvironment;
            _paymentApiService = paymentApiService;
            GatewayApiClient = gatewayApiClient;
            GatewayApiConfig = gatewayApiConfig.Value;
        }


        public enum ResponseCodes
        {
            [Description("Success Response Code")]
            Success = 0,
            [Description("Invalid Parameters Error Response Code")]
            InvalidParameters = 1,
            [Description("Exception Response Code")]
            Exception = 2,
            [Description("Invalid Page Request Exception Response Code")]
            InvalidPageRequestException = 3,
            [Description("Request Timeout Exception Response Code")]
            RequestTimeoutException = 4
        }

        public enum Actions
        {
            [Description("Purchase Action")]
            Purchase = 1
        }

        public enum CurrencyCodes
        {
            [Description("Kuwaiti Dinar")]
            KD = 414
        }

        public enum Languages
        {
            [Description("English")]
            ENG = 0,
            [Description("Arabic")]
            ARA = 1
        }

        public enum Environments
        {
            [Description("Debug Environment")]
            DEBUG = 0,
            [Description("Production Environment")]
            PRODUCTION = 1,
            [Description("Staging Environment")]
            STAGING = 2
        }

        /// <summary>
        /// Initialize Payment
        /// </summary>
        /// <response code="200">OK</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        [HttpPost]
        [Route("/api/initPayment")]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult InitPayment([FromBody] InitPaymentModel initPaymentModel)
        {
            var errorModelJson = string.Empty;
            ErrorResponse errorResponse = new ErrorResponse();
            XmlDocument xmlDocument = new XmlDocument();
            var merchentProfile = new MerchentProfile();
            InitPaymentResponse initPaymentResponse = new InitPaymentResponse();
            KNetAPIResponse KnetApiResponse = new KNetAPIResponse();
            int cc = 0;
            double tot = 0;
            bool InvalidParameters = false;
            bool isInt = int.TryParse(initPaymentModel.CurrencyCode, out cc);
            bool isDouble = double.TryParse(initPaymentModel.OrderTotal, out tot);
            if (initPaymentModel.PaymentMethod.ToUpper() == "KNET" &&
                    (String.IsNullOrEmpty(initPaymentModel.CurrencyCode) ||
                       String.IsNullOrEmpty(initPaymentModel.LangId) ||
                       String.IsNullOrEmpty(initPaymentModel.OrderId) ||
                       String.IsNullOrEmpty(initPaymentModel.OrderTotal) ||
                       String.IsNullOrEmpty(initPaymentModel.PaymentMethod) ||
                       !isInt ||
                       !isDouble
                     )
               )
            {
                InvalidParameters = true;
            }
            else if (initPaymentModel.PaymentMethod.ToUpper() == "CREDIT CARD" &&
                   (  String.IsNullOrEmpty(initPaymentModel.OrderId) ||
                      String.IsNullOrEmpty(initPaymentModel.OrderTotal) ||
                      String.IsNullOrEmpty(initPaymentModel.PaymentMethod) ||
                      !isDouble
                    )
               )
            {
                InvalidParameters = true;
            }

            if (InvalidParameters)
            {
                initPaymentResponse = new InitPaymentResponse
                {
                    Error = new ErrorResponse
                    {
                        ErrorCode = "2085",
                        ErrorTitle = "Error",
                        ErrorDesc = "InvalidParameters"
                    },
                    Data = null
                };
                errorModelJson = JsonConvert.SerializeObject(initPaymentResponse);
                return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
            }

            Order order = _orderService.GetOrderById(Convert.ToInt32(initPaymentModel.OrderId));            

            if (order == null)
            {
                initPaymentResponse = new InitPaymentResponse
                {
                    Error = new ErrorResponse
                    {
                        ErrorCode = "2099",
                        ErrorDesc = "InvalidOrderId",
                        ErrorTitle = "Error"
                    },
                    Data = null
                };
                errorModelJson = JsonConvert.SerializeObject(initPaymentResponse);
                return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
            }

            try
            {
                //..\Presentation\Nop.Web\App_Data\PaymentGatewayConfiguration.xml
                string contentRootPath = _hostingEnvironment.ContentRootPath;
                string xmlPaymentGatewayConfigurationFilePath = contentRootPath + "/App_Data/PaymentGatewayConfiguration.xml";
                //xmlDocument.Load(HostingEnvironment.MapPath("~/PaymentGatewayConfiguration.xml"));
                xmlDocument.Load(xmlPaymentGatewayConfigurationFilePath);
                XmlElement root = xmlDocument.DocumentElement;
                XmlNodeList nodes = root.ChildNodes;
                string GatewayName = string.Empty;
                string IsActive = string.Empty;
                _logger.Information("Start: Read file PaymentGatewayConfiguration.");
                foreach (XmlNode node in nodes)
                {
                    GatewayName = node["GatewayName"].InnerText;
                    if (GatewayName.ToUpper() == initPaymentModel.PaymentMethod.ToUpper())
                    {
                        if (GatewayName.ToUpper() == "KNET" && node["IsActive"].InnerText.ToUpper() == "Y")
                        {
                            _logger.Information("Generate TrackID.");
                            string TrackId = GeneralHelper.GenerateTrackId(initPaymentModel.OrderId);

                            _logger.Information("Initialize _factory");
                            var oPaymentTransaction = _factory.Initialize();

                            //if (oPaymentDataAccess.AddKNETPaymentTransaction(initPaymentModel.OrderId, out TrackId))
                            if (oPaymentTransaction != null)
                            {
                                oPaymentTransaction.OrderId = Convert.ToInt32(initPaymentModel.OrderId);
                                oPaymentTransaction.TrackId = TrackId;
                                oPaymentTransaction.TransactionStatus = (int)PaymentTransactionStatus.INITIATED;

                                _logger.Information("Insert Payment Transaction");
                                _paymentApiService.InsertPaymentTransaction(oPaymentTransaction);

                                _logger.Information("Set merchentProfile from xml config file.");
                                merchentProfile = new MerchentProfile
                                {
                                    ResourceKey = AES.Decrypt(node["ResourceKey"].InnerText),
                                    merchantId = AES.Decrypt(node["MerchantId"].InnerText),
                                    password = AES.Decrypt(node["Password"].InnerText),
                                    responseURL = node["ResponseURL"].InnerText,
                                    errorURL = node["ErrorURL"].InnerText,
                                    KnetRequestURL = node["KNetURL"].InnerText,
                                    action = Convert.ToInt32(node["Action"].InnerText),
                                    amt = Convert.ToDouble(initPaymentModel.OrderTotal),
                                    currencyCode = Convert.ToInt32(initPaymentModel.CurrencyCode),
                                    langId = initPaymentModel.LangId,
                                    trackId = TrackId
                                };

                                bool IsError = false;
                                _logger.Information("Start: Call to FrameKNETRequest(merchentProfile);");
                                StringBuilder reqParamBuilder = FrameKNETRequest(merchentProfile);
                                _logger.Information("End: Call to FrameKNETRequest(merchentProfile);");

                                if (reqParamBuilder != null)
                                {
                                    string EncMerchentProfile = null;
                                    _logger.Information("Call to EncryptKNETMerchentProfile(..);");
                                    if (EncryptKNETMerchentProfile(reqParamBuilder.ToString(), merchentProfile.ResourceKey, out EncMerchentProfile))
                                    {
                                        _logger.Information("Encrypted KNET Merchent Profile!");
                                        _logger.Information("Generate payment url.");

                                        StringBuilder objsb = new StringBuilder("");
                                        objsb.Append(merchentProfile.KnetRequestURL);
                                        objsb.Append("&trandata=");
                                        objsb.Append(EncMerchentProfile);
                                        objsb.Append("&errorURL=" + merchentProfile.errorURL + "&responseURL=" + merchentProfile.responseURL);
                                        objsb.Append("&tranportalId=" + merchentProfile.merchantId);

                                        _logger.Information("Return success API response: SuccessInitPayment.");
                                        initPaymentResponse = new InitPaymentResponse
                                        {
                                            Error = new ErrorResponse
                                            {
                                                ErrorCode = "2084",
                                                ErrorTitle = "SuccessInitPayment",
                                                ErrorDesc = "Success"
                                            },
                                            Data = objsb.ToString()
                                        };
                                        break;
                                    }
                                    else
                                    {
                                        _logger.Information("Failed to Encrypt KNET Merchent Profile.");
                                        IsError = true;
                                    }
                                }
                                else
                                {
                                    _logger.Information("Failed to build reqPramBuilder.");
                                    IsError = true;
                                }

                                if (IsError)
                                {
                                    _logger.Information("Return error API response: ErrorBuildingPaymentRequest");
                                    initPaymentResponse.Error = new ErrorResponse
                                    {
                                        ErrorCode = "2095",
                                        ErrorDesc = "ErrorBuildingPaymentRequest",
                                        ErrorTitle = "Error"

                                    };
                                    initPaymentResponse.Data = null;
                                    break;
                                }
                            }
                            else
                            {
                                _logger.Information("Return error API response: ErrorSavingPaymentRequest");
                                initPaymentResponse = new InitPaymentResponse
                                {
                                    Error = new ErrorResponse
                                    {
                                        ErrorCode = "2096",
                                        ErrorTitle = "ErrorSavingPaymentRequest",
                                        ErrorDesc = "Error"
                                    },
                                    Data = null
                                };
                                break;
                            }

                            //initPaymentResponse = KNETRequestPaymentUrl(merchentProfile);

                            //errorModelJson = JsonConvert.SerializeObject(initPaymentResponse);
                            //return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
                        }
                        else if (GatewayName.ToUpper() == "CREDIT CARD" && node["IsActive"].InnerText.ToUpper() == "Y")
                        {                            
                            string BaseURL = node["BaseURL"].InnerText;

                            _logger.Information("Generate TrackID.");
                            string TrackId = GeneralHelper.GenerateTrackId(initPaymentModel.OrderId);

                            _logger.Information("Initialize _factory");
                            var oPaymentTransaction = _factory.Initialize();

                            if (oPaymentTransaction != null)
                            {
                                oPaymentTransaction.OrderId = Convert.ToInt32(initPaymentModel.OrderId);
                                oPaymentTransaction.TrackId = TrackId;
                                oPaymentTransaction.TransactionStatus = (int)PaymentTransactionStatus.INITIATED;

                                _logger.Information("Insert into Payment Transaction");
                                _paymentApiService.InsertPaymentTransaction(oPaymentTransaction);
                                 
                                bool IsError = false;

                                _logger.Information("Generate Credit Card payment url.");

                                StringBuilder objsb = new StringBuilder(BaseURL);
                                objsb.Append("/hostedCheckoutUrl/" + initPaymentModel.OrderId + "/" + initPaymentModel.OrderTotal + "/" + TrackId);

                                if (objsb != null)
                                    { 
                                        _logger.Information("Return success API response: SuccessInitPayment.");
                                        initPaymentResponse = new InitPaymentResponse
                                        {
                                            Error = new ErrorResponse
                                            {
                                                ErrorCode = "2084",
                                                ErrorTitle = "SuccessInitPayment",
                                                ErrorDesc = "Success"
                                            },
                                            Data = objsb.ToString()
                                        };
                                        break;
                                    }
                                    else
                                    {
                                        _logger.Information("Failed to Generate Credit Card payment url.");
                                        IsError = true;
                                    }
                                 

                                if (IsError)
                                {
                                    _logger.Information("Return error API response: ErrorBuildingPaymentRequest");
                                    initPaymentResponse.Error = new ErrorResponse
                                    {
                                        ErrorCode = "2095",
                                        ErrorDesc = "ErrorBuildingPaymentRequest",
                                        ErrorTitle = "Error"

                                    };
                                    initPaymentResponse.Data = null;
                                    break;
                                }
                            }
                            else
                            {
                                _logger.Information("Return error API response: ErrorSavingPaymentRequest");
                                initPaymentResponse = new InitPaymentResponse
                                {
                                    Error = new ErrorResponse
                                    {
                                        ErrorCode = "2096",
                                        ErrorTitle = "ErrorSavingPaymentRequest",
                                        ErrorDesc = "Error"
                                    },
                                    Data = null
                                };
                                break;
                            }
                        }
                        else
                        {
                            _logger.Error("Invalid Payment Gateway Method.");
                            initPaymentResponse.Error = new ErrorResponse
                            {
                                ErrorCode = "2081",
                                ErrorDesc = "InvalidPaymentGatewayMethod",
                                ErrorTitle = "Error"

                            };
                            initPaymentResponse.Data = null;

                            errorModelJson = JsonConvert.SerializeObject(initPaymentResponse);
                            return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
                        }
                    }
                    else
                    {
                        _logger.Error("Invalid Payment Gateway Method.");
                        initPaymentResponse.Error = new ErrorResponse
                        {
                            ErrorCode = "2081",
                            ErrorDesc = "InvalidPaymentGatewayMethod",
                            ErrorTitle = "Error"

                        };
                        initPaymentResponse.Data = null;
                        //return Json(initPaymentResponse);
                    }
                }
                errorModelJson = JsonConvert.SerializeObject(initPaymentResponse);
                return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);

            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in PaymentController:(InitPayment). \nException: " + ex.Message + "\nStackTrace: " + ex.StackTrace);
                initPaymentResponse = new InitPaymentResponse
                {
                    Error = new ErrorResponse
                    {
                        ErrorCode = "2030",
                        ErrorTitle = "Error",
                        ErrorDesc = "GenericError"
                    },
                    Data = null
                };
                errorModelJson = JsonConvert.SerializeObject(initPaymentResponse);
                return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
            }
        }

        /// <summary>
        /// Initialize Credit Card Payment
        /// </summary>
        /// <response code="200">OK</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        [HttpPost]
        [Route("/api/initPaymentMasterCard")]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult InitPaymentMasterCard([FromBody] InitPaymentMasterCardModel initPaymentModel)
        {
            var errorModelJson = string.Empty;
            ErrorResponse errorResponse = new ErrorResponse();
            XmlDocument xmlDocument = new XmlDocument();
            var merchentProfile = new MerchentProfile();
            InitPaymentResponse initPaymentResponse = new InitPaymentResponse();
            int cc = 0;
            double tot = 0;
            bool isDouble = double.TryParse(initPaymentModel.OrderTotal, out tot);
            if (String.IsNullOrEmpty(initPaymentModel.OrderId) ||
                String.IsNullOrEmpty(initPaymentModel.OrderTotal) ||
                !isDouble)
            {
                initPaymentResponse = new InitPaymentResponse
                {
                    Error = new ErrorResponse
                    {
                        ErrorCode = "2085",
                        ErrorTitle = "Error",
                        ErrorDesc = "InvalidParameters"
                    },
                    Data = null
                };
                errorModelJson = JsonConvert.SerializeObject(initPaymentResponse);
                return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
            }

            Order order = _orderService.GetOrderById(Convert.ToInt32(initPaymentModel.OrderId));

            if (order == null)
            {
                initPaymentResponse = new InitPaymentResponse
                {
                    Error = new ErrorResponse
                    {
                        ErrorCode = "2099",
                        ErrorDesc = "InvalidOrderId",
                        ErrorTitle = "Error"
                    },
                    Data = null
                };
                errorModelJson = JsonConvert.SerializeObject(initPaymentResponse);
                return new ErrorActionResult(errorModelJson, HttpStatusCode.OK);
            }

            try
            {
                string contentRootPath = _hostingEnvironment.ContentRootPath;
                string xmlPaymentGatewayConfigurationFilePath = contentRootPath + "/App_Data/PaymentGatewayConfiguration.xml";
                xmlDocument.Load(xmlPaymentGatewayConfigurationFilePath);
                XmlElement root = xmlDocument.DocumentElement;
                XmlNodeList nodes = root.ChildNodes;
                string GatewayName = string.Empty;
                string IsActive = string.Empty;

                _logger.Information("Start: Read file PaymentGatewayConfiguration.");
                foreach (XmlNode node in nodes)
                {
                    GatewayName = node["GatewayName"].InnerText;
                    if (GatewayName.ToUpper() == "CREDIT CARD" && node["IsActive"].InnerText.ToUpper() == "Y")
                    {
                        string TransactionId = GeneralHelper.GenerateTrackId(initPaymentModel.OrderId);
                        
                        var oPaymentTransaction = _factory.Initialize();

                        if (oPaymentTransaction != null)
                        {
                            oPaymentTransaction.OrderId = Convert.ToInt32(initPaymentModel.OrderId);
                            oPaymentTransaction.TrackId = TransactionId;
                            oPaymentTransaction.TransactionStatus = (int)PaymentTransactionStatus.INITIATED;

                            _paymentApiService.InsertPaymentTransaction(oPaymentTransaction);

                            GatewayApiConfig.MerchantId = AES.Decrypt(node["MerchantId"].InnerText); //"900120901";
                            GatewayApiConfig.Username = AES.Decrypt(node["Username"].InnerText);// "merchant.900120901";
                            GatewayApiConfig.Password = AES.Decrypt(node["Password"].InnerText); // "bdc30713a6f4f5d3c00416809a50ac52";
                            GatewayApiConfig.GatewayUrl = node["MasterCardGatewayUrl"].InnerText; // "https://test-nbkpayment.mtf.gateway.mastercard.com";
                            GatewayApiConfig.Currency = node["Currency"].InnerText; // "KWD";                            
                            GatewayApiConfig.Version = node["Version"].InnerText; // "53";
                            GatewayApiConfig.UseProxy = Convert.ToBoolean(node["UseProxy"].InnerText);
                            GatewayApiConfig.UseSsl = Convert.ToBoolean(node["UseSsl"].InnerText);

                            GatewayApiRequest gatewayApiRequest = new GatewayApiRequest
                            {
                                GatewayApiConfig = GatewayApiConfig,
                                ApiMethod = "POST",
                                ApiOperation = node["APIOperation"].InnerText,
                                interaction = node["Interaction"].InnerText,
                                OrderId = initPaymentModel.OrderId,
                                TransactionId = TransactionId,
                                OrderAmount = initPaymentModel.OrderTotal,
                                OrderCurrency = node["Currency"].InnerText,
                                OrderDescription = "NA"
                            };

                            gatewayApiRequest.buildSessionRequestUrl();
                            gatewayApiRequest.buildPayload();
                            gatewayApiRequest.ApiMethod = GatewayApiClient.POST;

                            try
                            {
                                String response = GatewayApiClient.SendTransaction(gatewayApiRequest);

                                _logger.Information("Hosted Checkout Response : " + response);

                                CheckoutSessionModel checkoutSessionModel = CheckoutSessionModel.toCheckoutSessionModel(response);

                                ViewBag.CheckoutJsUrl = $@"{GatewayApiConfig.GatewayUrl}/checkout/version/{GatewayApiConfig.Version}/checkout.js";
                                ViewBag.MerchantId = GatewayApiConfig.MerchantId;
                                ViewBag.OrderId = gatewayApiRequest.OrderId;
                                ViewBag.CheckoutSession = checkoutSessionModel;
                                ViewBag.Currency = GatewayApiConfig.Currency;
                                ViewBag.OrderDescription = gatewayApiRequest.OrderDescription;
                                ViewBag.OrderAmount = gatewayApiRequest.OrderAmount;

                                ViewBag.MerchantName = node["MerchantName"].InnerText;
                                ViewBag.AddressLine1 = node["AddressLine1"].InnerText;
                                ViewBag.AddressLine2 = node["AddressLine2"].InnerText;
                                ViewBag.BillingAddress = node["BillingAddress"].InnerText;
                                ViewBag.OrderSummary = node["OrderSummary"].InnerText;
                            }
                            catch (Exception e)
                            {
                                _logger.Error($"Hosted Checkout error : {JsonConvert.SerializeObject(e)}");

                                return View(ViewNames.Error, new ErrorViewModel
                                {
                                    RequestId = System.Diagnostics.Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                                    Cause = e.InnerException != null ? e.InnerException.StackTrace : e.StackTrace,
                                    Message = e.Message
                                });
                            }

                        }

                        //var outstr = View(ViewNames.HostedCheckout);
                        //var view = this.View(ViewNames.HostedCheckout);
                        //var html = view.ToHtml(this.HttpContext);

                        return View(ViewNames.HostedCheckout);
                    }
                }

                _logger.Error("Credit Card payment gateway method not found in xml config file.");

                return View(ViewNames.Error, new ErrorViewModel
                {
                    RequestId = "NA",
                    Cause = "config error",
                    Message = "Credit Card payment gateway method not found in xml config file."
                });

            }
            catch (Exception ex)
            {
                _logger.Error($"Hosted Checkout error : {JsonConvert.SerializeObject(ex)}");

                return View(ViewNames.Error, new ErrorViewModel
                {
                    RequestId = System.Diagnostics.Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                    Cause = ex.InnerException != null ? ex.InnerException.StackTrace : ex.StackTrace,
                    Message = ex.Message
                });

            }
        }

         

        /// <summary>
        /// This method frames parameters to be passed to KNET using merchent profile.
        /// </summary>
        /// <param name="oMerchentProfile"></param>
        /// <returns></returns>
        StringBuilder FrameKNETRequest(MerchentProfile oMerchentProfile)
        {

            string con = "&";
            StringBuilder objsb = new StringBuilder();
            try
            {

                objsb.Append("id=");
                objsb.Append(oMerchentProfile.merchantId);
                objsb.Append(con);
                objsb.Append("password=");
                objsb.Append(oMerchentProfile.password);
                objsb.Append(con);
                objsb.Append("action=");
                objsb.Append(oMerchentProfile.action);
                objsb.Append(con);
                objsb.Append("langid=");
                objsb.Append(oMerchentProfile.langId);
                objsb.Append(con);
                objsb.Append("currencycode=");
                objsb.Append(oMerchentProfile.currencyCode);
                objsb.Append(con);
                objsb.Append("amt=");
                objsb.Append(oMerchentProfile.amt);
                objsb.Append(con);

                objsb.Append("responseURL=");
                objsb.Append(oMerchentProfile.responseURL);
                objsb.Append(con);


                objsb.Append("errorURL=");
                objsb.Append(oMerchentProfile.errorURL);
                objsb.Append(con);
                objsb.Append("trackid=");
                objsb.Append(oMerchentProfile.trackId);
                objsb.Append(con);

                objsb.Append("udf1=");
                objsb.Append("");
                objsb.Append(con);
                objsb.Append("udf2=");
                objsb.Append("");
                objsb.Append(con);
                objsb.Append("udf3=");
                objsb.Append("");
                objsb.Append(con);
                objsb.Append("udf4=");
                objsb.Append("");
                objsb.Append(con);
                objsb.Append("udf5=");
                objsb.Append("");

            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in PaymentController:(EncryptKNETMerchentProfile). \nException: " + ex.Message + "\nStackTrace: " + ex.StackTrace);
                objsb = null;
            }
            return objsb;
        }

        /// <summary>
        /// This method encrypts KNET merchent profile request parameters which are to be passed by using resource key
        /// </summary>
        /// <param name="tranData"></param>
        /// <param name="resourceKey"></param>
        /// <param name="EncryptedString"></param>
        /// <returns>Boolean representing whether encryption is successful or not</returns>
        private bool EncryptKNETMerchentProfile(string tranData, string resourceKey, out string EncryptedString)
        {
            bool bReturn = false;
            EncryptedString = string.Empty;

            try
            {
                _logger.Information("Encrypting KNET Merchent Profile..");
                var plainBytes = Encoding.UTF8.GetBytes(tranData);
                byte[] EncryptedBytes = null;
                if (Encrypt(plainBytes, resourceKey, out EncryptedBytes))
                {
                    if (ByteArrayToHexString(EncryptedBytes, out EncryptedString))
                    {
                        bReturn = true;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in PaymentController:(EncryptKNETMerchentProfile). \nException: " + ex.Message + "\nStackTrace: " + ex.StackTrace);
            }
            return bReturn;
        }

        /// <summary>
        /// This function converts byte array to Hex string.
        /// </summary>
        /// <param name="Bytes"></param>
        /// <param name="HexString"></param>
        /// <returns>Boolean representing whether byte array to Hex string conversion is successful or not</returns>
        private bool ByteArrayToHexString(byte[] Bytes, out string HexString)
        {
            bool bReturn = false;
            HexString = string.Empty;
            try
            {
                StringBuilder Result = new StringBuilder(Bytes.Length * 2);
                string HexAlphabet = "0123456789abcdef";

                foreach (byte B in Bytes)
                {
                    Result.Append(HexAlphabet[(int)(B >> 4)]);
                    Result.Append(HexAlphabet[(int)(B & 0xF)]);
                    HexString = Result.ToString();
                }

                bReturn = true;
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in PaymentController:(ByteArrayToHexString). \nException: " + ex.Message + "\nStackTrace: " + ex.StackTrace);
            }
            return bReturn;
        }

        /// <summary>
        /// Forms RijndaelManaged object using given secret key.
        /// </summary>
        /// <param name="secretKey"></param>
        /// <param name="oRijndaelManaged"></param>
        /// <returns>Boolean representing whether operation was successful or not</returns>
        private bool GetRijndaelManaged(String secretKey, out RijndaelManaged oRijndaelManaged)
        {
            bool bReturn = false;
            oRijndaelManaged = null;
            try
            {
                var keyBytes = new byte[16];
                var secretKeyBytes = Encoding.ASCII.GetBytes(secretKey);
                Array.Copy(secretKeyBytes, keyBytes, Math.Min(keyBytes.Length, secretKeyBytes.Length));

                oRijndaelManaged = new RijndaelManaged
                {
                    Mode = CipherMode.CBC,
                    //  Padding = PaddingMode.PKCS5,

                    KeySize = 128,
                    BlockSize = 128,
                    Key = keyBytes,
                    IV = keyBytes

                };
                bReturn = true;
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in PaymentController:(GetRijndaelManaged). \nException: " + ex.Message + "\nStackTrace: " + ex.StackTrace);
            }
            return bReturn;
        }

        /// <summary>
        /// This method encrypts the byte array passed by making use of resource key and creating encryptor.
        /// </summary>
        /// <param name="plainBytes"></param>
        /// <param name="resourceKey"></param>
        /// <param name="EncryptedBytes"></param>
        /// <returns>Boolean representing whether encryption is successful or not</returns>
        private bool Encrypt(byte[] plainBytes, string resourceKey, out byte[] EncryptedBytes)
        {
            bool bReturn = false;
            EncryptedBytes = null;
            try
            {
                RijndaelManaged rijndaelManaged = null;
                if (GetRijndaelManaged(resourceKey, out rijndaelManaged))
                {
                    EncryptedBytes = rijndaelManaged.CreateEncryptor()
                        .TransformFinalBlock(plainBytes, 0, plainBytes.Length);
                    bReturn = true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in PaymentController:(Encrypt). \nException: " + ex.Message + "\nStackTrace: " + ex.StackTrace);
            }
            return bReturn;
        }

        /// <summary>
        /// Returns the list of available payment gateways.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("api/getPaymentMethods")]
        public IActionResult GetPaymentMethods()
        {
            GatewayListResponse gatewayListResponse = new GatewayListResponse();
            var jsonModel = string.Empty;
            try
            {
                string contentRootPath = _hostingEnvironment.ContentRootPath;
                string xmlPaymentGatewayConfigurationFilePath = contentRootPath + "/App_Data/PaymentGatewayConfiguration.xml";
                var gatewayList = _orderApiService.GetGatewayList(xmlPaymentGatewayConfigurationFilePath);
                {
                    gatewayListResponse.Error = null;
                    gatewayListResponse.Data = gatewayList;
                };
                jsonModel = JsonConvert.SerializeObject(gatewayListResponse);
                return new ErrorActionResult(jsonModel, HttpStatusCode.OK);
            }
            catch (Exception)
            {
                ErrorResponse Error = new ErrorResponse
                {
                    ErrorCode = "2030",
                    ErrorTitle = "Error",
                    ErrorDesc = "GenericError"
                };
                gatewayListResponse.Error = Error;
                gatewayListResponse.Data = null;
                jsonModel = JsonConvert.SerializeObject(gatewayListResponse);
                return new ErrorActionResult(jsonModel, HttpStatusCode.OK);
            }
        }

        public enum ErrorCodes
        {
            [Description("Success Code")]
            Success = 0,
            [Description("Error Code")]
            Error = 1,
        }

        public InitPaymentResponse KNETRequestPaymentUrl(MerchentProfile mp)
        {
            var requestPaymentResponseModel = new InitPaymentResponse();
            KNetAPIResponse apiResponse = new KNetAPIResponse();
            try
            {
                if (!ValidateParameters(mp.merchantId, mp.password, mp.action, mp.amt, mp.currencyCode,
                    mp.langId, mp.responseURL, mp.errorURL, mp.trackId))
                {
                    requestPaymentResponseModel = new InitPaymentResponse
                    {
                        Error = new ErrorResponse
                        {
                            ErrorCode = "2085",
                            ErrorTitle = "Error",
                            ErrorDesc = "InvalidParameters"
                        },
                        Data = null
                    };
                    return requestPaymentResponseModel; //throw new InvalidParametersException("Parameters are invalid");
                }
                else
                {
                    string url = mp.KnetRequestURL;

                    NameValueCollection parametersCollection = new NameValueCollection();
                    parametersCollection.Add("id", mp.merchantId.Trim());
                    parametersCollection.Add("passwordhash", mp.password.Trim());
                    parametersCollection.Add("action", mp.action.ToString().Trim());
                    parametersCollection.Add("amt", mp.amt.ToString("f3").Trim());
                    parametersCollection.Add("currencycode", mp.currencyCode.ToString().Trim());
                    parametersCollection.Add("langid", mp.langId.Trim());
                    parametersCollection.Add("responseURL", mp.responseURL.Trim());
                    parametersCollection.Add("errorURL", mp.errorURL.Trim());
                    parametersCollection.Add("trackId", mp.trackId.Trim());
                    ////string parameters = "id=" + merchantId + "&password=" + password + "&action=" + action + "&amt=" + amt + "&currencycode=" + currencyCode + "&langid=" + langId + "&responseURL=~/Success.html&errorURL=~/Error.html&trackId=" + trackId;
                    //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                    // // Skip validation of SSL/TLS certificate
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                    List<string> parametersList = new List<string>();
                    using (WebClient client = new WebClient())
                    {
                        byte[] response = client.UploadValues(url, parametersCollection);
                        var objResponse = Encoding.UTF8.GetString(response);

                        parametersList = objResponse.Split(':').ToList();
                        apiResponse.PaymentID = parametersList[0];
                        int pid;
                        bool isInt = int.TryParse(apiResponse.PaymentID.Substring(0, 3), out pid);
                        if (!isInt)
                        {
                            requestPaymentResponseModel = new InitPaymentResponse
                            {
                                Error = new ErrorResponse
                                {
                                    ErrorCode = "2083",
                                    ErrorTitle = "Error",
                                    ErrorDesc = "InvalidPageRequest"
                                },
                                Data = null
                            };
                            return requestPaymentResponseModel; //throw new InvalidPageRequestException("Invalid page request");
                        }
                        //apiResponse.ResponseCode = Convert.ToInt32(ResponseCodes.Success);
                        //apiResponse.ResponseMessage = "Success!";
                        apiResponse.PaymentURL = objResponse.Remove(0, apiResponse.PaymentID.Length + 1);
                        apiResponse.PaymentURL += "paymentID=" + apiResponse.PaymentID;

                        ////Save response data to table.
                        //AddKnetTransaction(apiResponse.PaymentID, mp.trackId);

                        requestPaymentResponseModel = new InitPaymentResponse
                        {
                            Error = new ErrorResponse
                            {
                                ErrorCode = "2084",
                                ErrorTitle = "",
                                ErrorDesc = "SuccessInitPayment"
                            },
                            Data = apiResponse
                        };
                    }
                    return requestPaymentResponseModel;
                }
            }
            //catch (InvalidPageRequestException ex)
            //{
            //    apiResponse.ResponseCode = Convert.ToInt32(ResponseCodes.InvalidPageRequestException);
            //    apiResponse.ResponseMessage = ex.Message;
            //}
            //catch (InvalidParametersException ex)
            //{
            //    apiResponse.ResponseCode = Convert.ToInt32(ResponseCodes.InvalidParameters);
            //    apiResponse.ResponseMessage = ex.Message;
            //}
            //catch (RequestTimeoutException ex)
            //{
            //    apiResponse.ResponseCode = Convert.ToInt32(ResponseCodes.RequestTimeoutException);
            //    apiResponse.ResponseMessage = ex.Message;
            //}
            catch (Exception ex)
            {
                requestPaymentResponseModel = new InitPaymentResponse
                {
                    Error = new ErrorResponse
                    {
                        ErrorCode = "2030",
                        ErrorTitle = "Error",
                        ErrorDesc = "GenericError"
                    },
                    Data = null
                };
                return requestPaymentResponseModel;
            }

        }

        private bool ValidateParameters(string merchantId, string password, int action, double amt, int currencyCode, string langId, string responseURL, string errorURL, string trackId)
        {
            bool result;
            result = !(String.IsNullOrEmpty(merchantId.Trim())) &&
                !(String.IsNullOrEmpty(password.Trim())) &&
                !(String.IsNullOrEmpty(action.ToString())) &&
                !(String.IsNullOrEmpty(amt.ToString())) &&
                (amt > 0 ? true : false) &&
                Enum.IsDefined(typeof(CurrencyCodes), currencyCode) &&
                !(String.IsNullOrEmpty(langId.Trim())) &&
                Enum.IsDefined(typeof(Languages), langId.ToUpper().Trim()) &&
                !(String.IsNullOrEmpty(responseURL.Trim())) &&
                !(String.IsNullOrEmpty(errorURL.Trim())) &&
                !(String.IsNullOrEmpty(trackId.Trim()));

            return result;
        }


        private void AddKnetTransaction(string PaymentId, string TrackID)
        {
            DataSettings dataSettings = DataSettingsManager.LoadSettings();
            if (!dataSettings?.IsValid ?? true)
                return;

            string connectionStringFromNop = dataSettings.DataConnectionString;

            //"Data Source=D-1240\\SQLEXPRESS;Initial Catalog=nopCommerceDEV;Integrated Security=False;Persist Security Info=False;User ID=sa;Password=$pring123"
            //SqlConnection sqlConnection = new SqlConnection("Data Source=D-1193\\SQLEXPRESS2014;Initial Catalog=nopCommerceDEV;Integrated Security=False;Persist Security Info=False;User ID=sa;Password=$pring123");
            SqlConnection sqlConnection = new SqlConnection(connectionStringFromNop);
            SqlCommand sqlCommand;
            try
            {
                sqlConnection.Open();
                sqlCommand = new SqlCommand("Add_KNET_Transaction", sqlConnection);
                sqlCommand.Parameters.AddWithValue("@p_auth", DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@p_date", DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@p_postdate", DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@p_ref", DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@p_payment_id", PaymentId);
                sqlCommand.Parameters.AddWithValue("@p_responsecode", DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@p_result", DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@p_track_id", TrackID);
                sqlCommand.Parameters.AddWithValue("@p_tran_id", DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@p_eci", DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@p_udf1", DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@p_udf2", DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@p_udf3", DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@p_udf4", DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@p_udf5", DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@p_errortext", DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@p_errorcode", DBNull.Value);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                sqlConnection.Close();
            }
        }

        ///// <summary>
        ///// Gets called from KNET API in case of Success.
        ///// </summary>
        ///// <param name="formData"></param>
        ///// <returns></returns>
        //[HttpPost]
        //[Route("/api/KNETSuccess")]
        //public void KNETSuccess([FromForm] string paymentid,[FromForm] string trackid, [FromForm] string tranid, [FromForm] string trandata, [FromForm] string auth, [FromForm(Name ="ref")] string rf, [FromForm] string amt, [FromForm] string result, [FromForm] string udf1, [FromForm] string udf2, [FromForm] string udf3, [FromForm] string udf4, [FromForm] string udf5, [FromForm] string postdate, [FromForm] string avr, [FromForm] string authRespCode)
        //{
        //    //KNETTransaction knetTransaction = new KNETTransaction();
        //    ////string BaseUrl = WebConfigurationManager.AppSettings["BaseUrl"];
        //    //var bodyStream = new StreamReader(HttpContext.Current.Request.InputStream);
        //    //bodyStream.BaseStream.Seek(0, SeekOrigin.Begin);
        //    //var bodyText = bodyStream.ReadToEnd();
        //    //var uri = HttpContext.Current.Request.Url.AbsoluteUri + "?" + bodyText;
        //    //NameValueCollection nameValueCollection = new Uri(uri).ParseQueryString();
        //    //int rowsAffected = 0;
        //    //string PaymentId = nameValueCollection.Get("paymentid");
        //    //string Result = nameValueCollection.Get("result");
        //    //rowsAffected = knetTransaction.UpdateSuccessKNETTransaction(nameValueCollection.Get("paymentid"), nameValueCollection.Get("result"), nameValueCollection.Get("auth"), nameValueCollection.Get("ref"), nameValueCollection.Get("tranid"), nameValueCollection.Get("postdate"), nameValueCollection.Get("trackid"), nameValueCollection.Get("responsecode"), nameValueCollection.Get("eci"));
        //    //string url = string.Empty;
        //    //if (nameValueCollection.Get("result").ToString().ToUpper() == ResultCodes.CAPTURED.ToString().ToUpper())
        //    //{
        //    //    url = "REDIRECT=" + BaseUrl + "SuccessPage.aspx?paymentId=" + PaymentId + "&Result=" + Result;
        //    //}
        //    //else
        //    //{
        //    //    url = "REDIRECT=" + BaseUrl + "ErrorPage.aspx?paymentId=" + PaymentId + "&Result=" + Result;
        //    //}
        //    //return url;
        //}

        /// <summary>
        /// Gets called from KNET API in case of Failure
        /// </summary>
        /// <param name="PaymentId"></param>
        /// <param name="ErrorCode"></param>
        /// <param name="ErrorText"></param>
        /// <returns></returns>
        //[HttpGet]
        //[Route("/api/KNETFailure")]
        //public void KNETFailure([FromQuery] string PaymentId, [FromQuery] string ErrorCode = null, [FromQuery] string ErrorText = null)
        //{
        //    //string BaseUrl = WebConfigurationManager.AppSettings["BaseUrl"];
        //    //string url = string.Empty;
        //    //if (ErrorCode != null)
        //    //{
        //    //    url = BaseUrl + "ErrorPage.aspx?paymentId=" + PaymentId + "&ErrorCode=" + ErrorCode;
        //    //}
        //    //else
        //    //{
        //    //    url = BaseUrl + "ErrorPage.aspx?paymentId=" + PaymentId + "&ErrorText=" + ErrorText;
        //    //}
        //    //return "REDIRECT=" + url;
        //}


        public class KNetAPIResponse
        {
            public string PaymentID { get; set; }
            public string PaymentURL { get; set; }
        }

        public class MerchentProfile
        {
            public string ResourceKey = "";
            public string merchantId = "";
            public string password = "";
            public int action = 0;
            public double amt = 0;
            public int currencyCode = 0;
            public string langId = "";
            public string responseURL = "";
            public string errorURL = "";
            public string trackId = "";
            public string environment = "";
            public string KnetRequestURL;
        }

        public class InitPaymentModel
        {
            public string OrderId;
            public string PaymentMethod;
            public string LangId;
            public string CurrencyCode;
            public string OrderTotal;
        }

        public class InitPaymentMasterCardModel
        {
            public string OrderId;
            public string OrderTotal;
        }

        public class InitPaymentResponse
        {
            public ErrorResponse Error;
            public Object Data;
        }

        public class GatewayListResponse
        {
            public ErrorResponse Error;
            public Object Data;
        }
    }
}