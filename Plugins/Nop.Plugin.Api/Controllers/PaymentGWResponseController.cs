﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Policy;
using System.Text;
using System.Web;
using System.Xml;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Logging;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Plugin.Api.DataAccess;
using Nop.Plugin.Api.Factories;
using Nop.Plugin.Api.Helpers;
using Nop.Plugin.Api.JSON.Serializers;
using Nop.Plugin.Api.Services;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Shipping;
using Nop.Services.Stores;
using Nop.Web.Areas.Admin.Factories;

namespace Nop.Plugin.Api.Controllers
{
    public class PaymentGWResponseController : BaseApiController
    {
        private readonly IOrderApiService _orderApiService;
        private readonly IProductService _productService;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IOrderService _orderService;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IShippingService _shippingService;
        private readonly IDTOHelper _dtoHelper;
        private readonly IProductAttributeConverter _productAttributeConverter;
        private readonly IStoreContext _storeContext;
        private readonly IFactory<KNETPaymentTransaction> _KNETPaymentTransactionFactory;
        private readonly IShoppingCartModelFactory _shoppingCartModelFactory;
        private readonly ISettingService _settingService;
        private readonly IWorkContext _workContext;
        private readonly ICustomerService _customerService;
        private readonly ILogger _logger;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IEmailAccountService _emailAccountService;
        private readonly IEmailSender _emailSender;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IPaymentApiService _paymentApiService;

        public PaymentGWResponseController(IOrderApiService orderApiService,
            IJsonFieldsSerializer jsonFieldsSerializer,
            IAclService aclService,
            ICustomerService customerService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IDiscountService discountService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            IProductService productService,
            IFactory<KNETPaymentTransaction> oKNETPaymentTransactionFactory,
            IOrderProcessingService orderProcessingService,
            IOrderService orderService,
            IShoppingCartService shoppingCartService,
            IGenericAttributeService genericAttributeService,
            IStoreContext storeContext,
            IShippingService shippingService,
            IPictureService pictureService,
            IDTOHelper dtoHelper,
            IShoppingCartModelFactory shoppingCartModelFactory,
             ISettingService settingService,
             IWorkContext workContext,
             ILogger logger,
            IProductAttributeConverter productAttributeConverter,
            IHostingEnvironment hostingEnvironment,
            IEmailAccountService emailAccountService,
            IWorkflowMessageService workflowMessageService,
            IPaymentApiService paymentApiService
            )
            : base(jsonFieldsSerializer, aclService, customerService, storeMappingService,
                 storeService, discountService, customerActivityService, localizationService, pictureService)
        {
            _orderApiService = orderApiService;
            _KNETPaymentTransactionFactory = oKNETPaymentTransactionFactory;
            _orderProcessingService = orderProcessingService;
            _orderService = orderService;
            _shoppingCartService = shoppingCartService;
            _genericAttributeService = genericAttributeService;
            _storeContext = storeContext;
            _shippingService = shippingService;
            _dtoHelper = dtoHelper;
            _productService = productService;
            _productAttributeConverter = productAttributeConverter;
            _shoppingCartModelFactory = shoppingCartModelFactory;
            _settingService = settingService;
            _workContext = workContext;
            _customerService = customerService;
            _logger = logger;
            _hostingEnvironment = hostingEnvironment;
            _emailAccountService = emailAccountService;
            _workflowMessageService = workflowMessageService;
            _paymentApiService = paymentApiService;
        }

        [HttpPost]
        [Route("/api/KNETResponse")]
        //public string KNETResponse([FromForm] string PaymentID, [FromForm] string TrackID, [FromForm] string TranID, [FromForm] string trandata, [FromForm] string Auth, [FromForm(Name = "ref")] string Ref, [FromForm] string amt, [FromForm] string result, [FromForm] string udf1, [FromForm] string udf2, [FromForm] string udf3, [FromForm] string udf4, [FromForm] string udf5, [FromForm] string postdate, [FromForm] string avr, [FromForm] string authRespCode, [FromForm] string Error, [FromForm] string ErrorText)
        public ActionResult KNETResponse([FromForm] string trandata, [FromForm] string Error, [FromForm] string ErrorText)
        {
            bool IsResultSaved = false;
            bool IsResponseParsed = false;
            string PaymentID = null, TrackID = null, TranID = null, Auth = null, Ref = null, Result = null, PostDate = null, UDF1 = null, UDF2 = null, UDF3 = null, UDF4 = null, UDF5 = null, Avr = null, AuthRespCode = null;
            Double? Amt = null;
            string strResp = string.Empty;
            int ResultStatus = -1;
            Order order = null;
            try
            {
                _logger.InsertLog(LogLevel.Debug, "KNET payment response received:\nTranData\t: " + trandata + "\nError\t:" + Error + "\nErrorText\t:" + ErrorText);

                if (String.IsNullOrEmpty(ErrorText)) //Error text is not found hence go for checking result.
                {
                    XmlDocument xmlDocument = new XmlDocument();
                    string contentRootPath = _hostingEnvironment.ContentRootPath;
                    string xmlPaymentGatewayConfigurationFilePath = contentRootPath + "/App_Data/PaymentGatewayConfiguration.xml";

                    xmlDocument.Load(xmlPaymentGatewayConfigurationFilePath);
                    XmlElement root = xmlDocument.DocumentElement;
                    XmlNodeList nodes = root.ChildNodes;

                    XmlNode KNETXmlNode = nodes.Cast<XmlNode>().FirstOrDefault(oXmlNode => oXmlNode["GatewayName"].InnerText.ToUpper() == "KNET" && oXmlNode["IsActive"].InnerText.ToUpper() == "Y");
                    string ResourceKey = AES.Decrypt(KNETXmlNode["ResourceKey"].InnerText);
                    string DecTranData = Decrypt(trandata, ResourceKey);

                    var dicDecryptedParams = HttpUtility.ParseQueryString(DecTranData);

                    Result = dicDecryptedParams["result"];

                    switch (Result)
                    {
                        case KNETTransactionResult.CAPTURED:
                        case KNETTransactionResult.NOT_CAPTURED:
                        case KNETTransactionResult.DENIED_BY_RISK:
                        case KNETTransactionResult.HOST_TIMEOUT:
                            PaymentID = dicDecryptedParams["paymentid"];
                            Auth = dicDecryptedParams["auth"];
                            Avr = dicDecryptedParams["avr"];
                            Ref = dicDecryptedParams["ref"];
                            TranID = dicDecryptedParams["tranid"];
                            PostDate = dicDecryptedParams["postdate"];
                            TrackID = dicDecryptedParams["trackid"];
                            UDF1 = dicDecryptedParams["udf1"];
                            UDF2 = dicDecryptedParams["udf2"];
                            UDF3 = dicDecryptedParams["udf3"];
                            UDF4 = dicDecryptedParams["udf4"];
                            UDF5 = dicDecryptedParams["udf5"];
                            Amt = Convert.ToDouble(dicDecryptedParams["amt"]);
                            AuthRespCode = dicDecryptedParams["authRespCode"];

                            _logger.Information("KNET response parsed. Details:\n" +
                                        "\nPaymentID\t:" + PaymentID +
                                        "\nAuth\t:" + Auth +
                                        "\nAvr\t:" + Avr +
                                        "\nRef\t:" + Ref +
                                        "\nTranID\t:" + TranID +
                                        "\nPostDate\t:" + PostDate +
                                        "\nTrackID\t:" + TrackID +
                                        "\nUDF1\t:" + UDF1 +
                                        "\nUDF2\t:" + UDF2 +
                                        "\nUDF3\t:" + UDF3 +
                                        "\nUDF4\t:" + UDF4 +
                                        "\nUDF5\t:" + UDF5 +
                                        "\nAmt\t:" + Amt.Value +
                                        "\nAuthRespCode\t:" + AuthRespCode);

                            IsResponseParsed = true;
                            break;

                        case KNETTransactionResult.CANCELED:
                            PaymentID = dicDecryptedParams["paymentid"];
                            Auth = dicDecryptedParams["auth"];
                            Avr = dicDecryptedParams["avr"];
                            Ref = dicDecryptedParams["ref"];
                            PostDate = dicDecryptedParams["postdate"];
                            TrackID = dicDecryptedParams["trackid"];
                            UDF1 = dicDecryptedParams["udf1"];
                            UDF2 = dicDecryptedParams["udf2"];
                            UDF3 = dicDecryptedParams["udf3"];
                            UDF4 = dicDecryptedParams["udf4"];
                            UDF5 = dicDecryptedParams["udf5"];
                            Amt = Convert.ToDouble(dicDecryptedParams["amt"]);

                            _logger.Information("KNET response parsed. Details:\n" +
                                        "\nPaymentID\t:" + PaymentID +
                                        "\nAuth\t:" + Auth +
                                        "\nAvr\t:" + Avr +
                                        "\nRef\t:" + Ref +
                                        "\nPostDate\t:" + PostDate +
                                        "\nTrackID\t:" + TrackID +
                                        "\nUDF1\t:" + UDF1 +
                                        "\nUDF2\t:" + UDF2 +
                                        "\nUDF3\t:" + UDF3 +
                                        "\nUDF4\t:" + UDF4 +
                                        "\nUDF5\t:" + UDF5 +
                                        "\nAmt\t:" + Amt.Value);

                            IsResponseParsed = true;
                            break;

                        default:
                            _logger.Error("Error occurred in KNET transaction response parsing. Unidentified result returned from KNET as: " + Result);
                            break;
                    }

                    if (IsResponseParsed)
                    {
                        //Nop.Plugin.Api.Models.PaymentParameters.KNETPaymentTransaction oKNETPaymentTransaction = new Nop.Plugin.Api.Models.PaymentParameters.KNETPaymentTransaction(PaymentID, TrackID, TranID, Auth, Ref, Result, UDF1, UDF2, UDF3, UDF4, UDF5, Amt, PostDate, Avr, AuthRespCode, null, null);
                        //PaymentDataAccess oPaymentDataAccess = new PaymentDataAccess(_logger);

                        var oPaymentTransaction = _paymentApiService.GetPaymentTransactionByTrackId(TrackID);

                        order = oPaymentTransaction.Order;

                        var oKNETPaymentTransaction = _KNETPaymentTransactionFactory.Initialize();

                        oKNETPaymentTransaction.Amt = Amt;
                        oKNETPaymentTransaction.Auth = Auth;
                        oKNETPaymentTransaction.AuthRespCode = AuthRespCode;
                        oKNETPaymentTransaction.Avr = Avr;
                        oKNETPaymentTransaction.ErrorCode = Error;
                        oKNETPaymentTransaction.ErrorText = ErrorText;
                        oKNETPaymentTransaction.PaymentId = PaymentID;

                        oKNETPaymentTransaction.PaymentTransactionId = oPaymentTransaction.Id;
                        oKNETPaymentTransaction.PaymentTransaction = oPaymentTransaction;
                        oKNETPaymentTransaction.PaymentTransaction.PaymentTransactionStatus = PaymentTransactionStatus.SUCCESS;

                        oKNETPaymentTransaction.PaymentTransaction.Order.UpdatedOnUtc = DateTime.UtcNow;
                        oKNETPaymentTransaction.PaymentTransaction.Order.PaymentStatus = Result.ToUpper().Equals(KNETTransactionResult.CAPTURED) ? PaymentStatus.Paid : PaymentStatus.Failed;
                        oKNETPaymentTransaction.PaymentTransaction.Order.OrderStatus = Result.ToUpper().Equals(KNETTransactionResult.CAPTURED) ? OrderStatus.Processing : OrderStatus.Pending;
                        oKNETPaymentTransaction.PaymentTransaction.Order.CaptureTransactionId = TranID;
                        oKNETPaymentTransaction.PaymentTransaction.Order.CaptureTransactionResult = Result; 
                        oKNETPaymentTransaction.PaymentTransaction.Order.AuthorizationTransactionCode = AuthRespCode; 
                        oKNETPaymentTransaction.PaymentTransaction.Order.PaidDateUtc = DateTime.UtcNow;
                        oKNETPaymentTransaction.PaymentTransaction.Order.PaymentId = PaymentID;

                        oKNETPaymentTransaction.PostDate = PostDate;
                        oKNETPaymentTransaction.Ref = Ref;
                        oKNETPaymentTransaction.Result = Result;
                        oKNETPaymentTransaction.TranId = TranID;
                        oKNETPaymentTransaction.UDF1 = UDF1;
                        oKNETPaymentTransaction.UDF2 = UDF2;
                        oKNETPaymentTransaction.UDF3 = UDF3;
                        oKNETPaymentTransaction.UDF4 = UDF4;
                        oKNETPaymentTransaction.UDF5 = UDF5;

                        _paymentApiService.UpdateKNETPaymentTransaction(oKNETPaymentTransaction);                        

                        IsResultSaved = true;

                        _logger.Information("Successfully saved result from KNET into DB. Following are the details:" +
                            "\nPaymentID\t:" + PaymentID +
                            "\nTrackID\t:" + TrackID +
                            "\nTranID\t:" + TranID +
                            "\nTranData\t:" + trandata +
                            "\nTranData\t:" + trandata +
                            "\nAuth\t:" + Auth +
                            "\nRef\t:" + Ref +
                            "\nAmt\t:" + Amt.Value +
                            "\nResult\t:" + Result +
                            "\nUDF1\t:" + UDF1 +
                            "\nUDF2\t:" + UDF2 +
                            "\nUDF3\t:" + UDF3 +
                            "\nUDF4\t:" + UDF4 +
                            "\nUDF5\t:" + UDF5 +
                            "\nPostDate\t:" + PostDate +
                            "\nAvr\t:" + Avr +
                            "\nAuthRespCode\t:" + AuthRespCode +
                            "\nPaymentStatus\t:" + oKNETPaymentTransaction.PaymentTransaction.Order.PaymentStatus.ToString());

                        

                        switch (Result)
                        {
                            case KNETTransactionResult.CAPTURED:
                                strResp = "Your transaction for amount " + Amt.Value + " is successful. \nPlease note your Transaction ID: " + TranID + " and Payment ID: " + PaymentID + ".";
                                ResultStatus = (int)KNETTransactionResultEnum.CAPTURED;
                                break;

                            case KNETTransactionResult.NOT_CAPTURED:
                                strResp = "Your transaction got failed.";
                                ResultStatus = (int)KNETTransactionResultEnum.NOT_CAPTURED;
                                break;

                            case KNETTransactionResult.DENIED_BY_RISK:
                                strResp = "Transaction denied.";
                                ResultStatus = (int)KNETTransactionResultEnum.DENIED_BY_RISK;
                                break;

                            case KNETTransactionResult.HOST_TIMEOUT:
                                strResp = "Transaction timed out.";
                                ResultStatus = (int)KNETTransactionResultEnum.HOST_TIMEOUT;
                                break;

                            case KNETTransactionResult.CANCELED:
                                strResp = "Transaction canceled.";
                                ResultStatus = (int)KNETTransactionResultEnum.CANCELED;
                                break;

                            default:
                                strResp = "Sorry! Something went wrong!";
                                ResultStatus = (int)KNETTransactionResultEnum.DEFAULT;
                                break;
                        }
                    }
                    else
                    {
                        _logger.Error("Error occurred while parsing response from KNET. Details:\nTranData\t: " + trandata + "\nError\t:" + Error + "\nErrorText\t:" + ErrorText);
                        strResp = "Sorry! Something went wrong!";
                    }
                }
                else
                {
                    _logger.Error("Error occurred in KNET transaction: " + ErrorText);
                }


            }
            catch (Exception ex)
            {
                strResp = "Sorry! Something went wrong!";
                try
                {
                    if (IsResultSaved)
                    {
                        _logger.Error("Successfully saved results from KNET into DB but error occurred after that. Following are the details:" +
                            "\nPaymentID\t:" + PaymentID +
                            "\nTrackID\t:" + TrackID +
                            "\nTranID\t:" + TranID +
                            "\nTranData\t:" + trandata +
                            "\nTranData\t:" + trandata +
                            "\nAuth\t:" + Auth +
                            "\nRef\t:" + Ref +
                            "\nAmt\t:" + Amt.Value +
                            "\nResult\t:" + Result +
                            "\nUDF1\t:" + UDF1 +
                            "\nUDF2\t:" + UDF2 +
                            "\nUDF3\t:" + UDF3 +
                            "\nUDF4\t:" + UDF4 +
                            "\nUDF5\t:" + UDF5 +
                            "\nPostDate\t:" + PostDate +
                            "\nAvr\t:" + Avr +
                            "\nAuthRespCode\t:" + AuthRespCode,
                            ex);
                    }
                    else
                    {
                        _logger.Error("Error occurred in processing success response from KNET. Details:\nTranData\t: " + trandata + "\nError\t:" + Error + "\nErrorText\t:" + ErrorText, ex);
                    }
                }
                catch (Exception innerEx)
                {
                    _logger.Error("Error occurred in processing response from KNET. Following are the details:\nTranData\t: " + trandata + "\nError\t:" + Error + "\nErrorText\t:" + ErrorText, innerEx);
                }
            }
             
            if (Result == KNETTransactionResult.CAPTURED)
            {
                try
                {
                    var orderPlacedCustomerNotificationQueuedEmailIds = _workflowMessageService
                            .SendOrderPlacedCustomerNotificationAlmailem(order, order.CustomerLanguageId);
                    if (orderPlacedCustomerNotificationQueuedEmailIds.Any())
                        AddOrderNote(order, $"\"Order placed\" email (to customer) has been queued. Queued email identifiers: {string.Join(", ", orderPlacedCustomerNotificationQueuedEmailIds)}.");
                }
                catch (Exception ex)
                {
                    _logger.Error("Error occurred while queuing email for OrderID\t:" + order.Id, ex);
                }

                //return Redirect(ReturnBaseURL() + "PaymentGatewayResponse/Success");
            }
            //else
            //    return Redirect(ReturnBaseURL() + "PaymentGatewayResponse/Error");

            return Redirect(ReturnBaseURL() + "PaymentGatewayResponse/Success/" + Result);
        }

        #region Decrypt
        public String Decrypt(String plainText, String key)
        {
            return Decrypt(StringToByteArray(plainText), GetRijndaelManaged(key));
        }
        private string Decrypt(byte[] plainBytes, RijndaelManaged rijndaelManaged)
        {
            ICryptoTransform decryptor = rijndaelManaged.CreateDecryptor(rijndaelManaged.Key, rijndaelManaged.IV);
            using (MemoryStream msDecrypt = new MemoryStream(plainBytes))
            {
                using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                {
                    using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                    {
                        return srDecrypt.ReadToEnd();
                    }
                }
            }
        }

        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }

        public static string ByteArrayToHexString(byte[] Bytes)
        {
            StringBuilder Result = new StringBuilder(Bytes.Length * 2);
            string HexAlphabet = "0123456789abcdef";

            foreach (byte B in Bytes)
            {
                Result.Append(HexAlphabet[(int)(B >> 4)]);
                Result.Append(HexAlphabet[(int)(B & 0xF)]);
            }

            return Result.ToString();
        }

        private RijndaelManaged GetRijndaelManaged(String secretKey)
        {
            var keyBytes = new byte[16];
            var secretKeyBytes = Encoding.ASCII.GetBytes(secretKey);
            Array.Copy(secretKeyBytes, keyBytes, Math.Min(keyBytes.Length, secretKeyBytes.Length));

            return new RijndaelManaged
            {
                Mode = CipherMode.CBC,
                //  Padding = PaddingMode.PKCS5,

                KeySize = 128,
                BlockSize = 128,
                Key = keyBytes,
                IV = keyBytes

            };
        }

        #endregion

        #region
        public string ReturnBaseURL()
        {
            XmlDocument xmlDocument = new XmlDocument();
            string contentRootPath = _hostingEnvironment.ContentRootPath;
            string xmlPaymentGatewayConfigurationFilePath = contentRootPath + "/App_Data/PaymentGatewayConfiguration.xml";
            //xmlDocument.Load(HostingEnvironment.MapPath("~/PaymentGatewayConfiguration.xml"));
            xmlDocument.Load(xmlPaymentGatewayConfigurationFilePath);
            XmlElement root = xmlDocument.DocumentElement;
            XmlNodeList nodes = root.ChildNodes;

            string BaseURL = string.Empty;
            string GatewayName;
            foreach (XmlNode node in nodes)
            {
                GatewayName = node["GatewayName"].InnerText;
                if (GatewayName.ToUpper() == "KNET" && node["IsActive"].InnerText.ToUpper() == "Y")
                {
                    BaseURL = node["BaseURL"].InnerText;
                }
            }

            return BaseURL;
        }
        #endregion

        [HttpGet]
        [Route("/api/KNETFailure")]
        public ActionResult KNETFailure([FromQuery] string PaymentId, [FromQuery] string trackID, [FromQuery] string ErrorCode = null, [FromQuery] string ErrorText = null)
        {
            bool IsResultSaved = false;
            try
            {

                if (!String.IsNullOrEmpty(trackID))
                {
                    _logger.Information("KNET payment failure for:\nTrackID\t: " + trackID + "\nPaymentID\t: " + PaymentId + "\nErrorCode\t:" + ErrorCode + "\nErrorText\t:" + ErrorText);

                    var oPaymentTransaction = _paymentApiService.GetPaymentTransactionByTrackId(trackID);

                    var oKNETPaymentTransaction = _KNETPaymentTransactionFactory.Initialize();

                    oKNETPaymentTransaction.ErrorCode = ErrorCode;
                    oKNETPaymentTransaction.ErrorText = ErrorText;
                    oKNETPaymentTransaction.PaymentId = PaymentId;

                    oKNETPaymentTransaction.PaymentTransactionId = oPaymentTransaction.Id;
                    oKNETPaymentTransaction.PaymentTransaction = oPaymentTransaction;
                    oKNETPaymentTransaction.PaymentTransaction.PaymentTransactionStatus = PaymentTransactionStatus.FAILED;

                    oKNETPaymentTransaction.PaymentTransaction.Order.UpdatedOnUtc = DateTime.UtcNow;
                    oKNETPaymentTransaction.PaymentTransaction.Order.PaymentStatus = PaymentStatus.Failed;
                    oKNETPaymentTransaction.PaymentTransaction.Order.OrderStatus = OrderStatus.Pending;
                    oKNETPaymentTransaction.PaymentTransaction.Order.PaymentId = PaymentId;

                    _paymentApiService.UpdateKNETPaymentTransaction(oKNETPaymentTransaction);

                    IsResultSaved = true;
                    _logger.Information("KNET Payment Failure. Successfully saved result from KNET into DB. Following are the details:\n" +
                        "PaymentID\t:" + PaymentId +
                        "\nTrackID\t:" + trackID +
                        "\nErrorCode\t:" + ErrorCode +
                        "\nErrorText\t:" + ErrorText);
                }
                else
                {
                    _logger.Error("Failure response received from KNET. ErrorText: " + ErrorText);
                }
            }
            catch (Exception ex)
            {
                try
                {
                    if (IsResultSaved)
                    {
                        _logger.Error("Successfully saved results from KNET into DB but error occurred after that. Following are the result parameters from the request:\n" +
                            "PaymentID\t:" + PaymentId +
                            "\nTrackID\t:" + trackID +
                            "\nErrorCode\t:" + ErrorCode +
                            "\nErrorText\t:" + ErrorText,
                            ex);
                    }
                    else
                    {
                        _logger.Error("Error occurred in processing failure response from KNET. Following are the result parameters from the request:\n" +
                            "PaymentID\t:" + PaymentId +
                            "\nTrackID\t:" + trackID +
                            "\nErrorCode\t:" + ErrorCode +
                            "\nErrorText\t:" + ErrorText,
                            ex);
                    }
                }
                catch (Exception innerEx)
                {
                    _logger.Error("Error occurred in processing success response from KNET. Following are the result parameters from the request:\n" +
                            "PaymentID\t:" + PaymentId +
                            "\nTrackID\t:" + trackID +
                            "\nErrorCode\t:" + ErrorCode +
                            "\nErrorText\t:" + ErrorText,
                            innerEx);
                }
            }             
            return Redirect(ReturnBaseURL() + "PaymentGatewayResponse/Error"); 
        }

        /// <summary>
        /// Add order note
        /// </summary>
        /// <param name="order">Order</param>
        /// <param name="note">Note text</param>
        protected virtual void AddOrderNote(Order order, string note)
        {
            order.OrderNotes.Add(new OrderNote
            {
                Note = note,
                DisplayToCustomer = false,
                CreatedOnUtc = DateTime.UtcNow
            });

            _orderService.UpdateOrder(order);
        }
    }
}
