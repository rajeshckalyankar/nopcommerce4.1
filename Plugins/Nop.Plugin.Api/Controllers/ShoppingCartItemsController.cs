﻿using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.Delta;
using Nop.Plugin.Api.DTOs.ShoppingCarts;
using Nop.Plugin.Api.Factories;
using Nop.Plugin.Api.Helpers;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.ModelBinders;
using Nop.Plugin.Api.Models.ShoppingCartsParameters;
using Nop.Plugin.Api.Services;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Nop.Plugin.Api.Controllers
{
    using DTOs.Errors;
    using JSON.Serializers;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Mvc;
    using Newtonsoft.Json;
    using Nop.Core.Domain.Catalog;
    using Nop.Core.Domain.Customers;
    using Nop.Plugin.Api.DTOs.Stores;
    using Nop.Plugin.Shipping.FixedByWeightByTotal;
    using Nop.Services.Common;
    using Nop.Services.Configuration;
    using Nop.Services.Shipping;
    using Nop.Web.Factories;
    using Nop.Web.Models.Catalog;

    [ApiAuthorize(Policy = JwtBearerDefaults.AuthenticationScheme, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ShoppingCartItemsController : BaseApiController
    {
        private readonly IShoppingCartItemApiService _shoppingCartItemApiService;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IProductService _productService;
        private readonly IFactory<ShoppingCartItem> _factory;
        private readonly IProductAttributeConverter _productAttributeConverter;
        private readonly IDTOHelper _dtoHelper;
        private readonly IStoreContext _storeContext;
        private readonly IWorkContext _workContext;
        private readonly ILocalizationService _localizationService;
        private readonly IBackInStockSubscriptionService _backInStockSubscriptionService;
        private readonly CatalogSettings _catalogSettings;
        private readonly IPermissionService _permissionService;
        private readonly ISettingService _settingService;
        private readonly IShippingService _shippingService;
        private readonly IAddressService _addressService;
        private readonly ILogger _logger;
        private readonly ICustomerService _customerService;
        private readonly IShoppingCartModelFactory _shoppingCartModelFactory;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IOrderApiService _orderApiService;

        [JsonProperty("StockAvailableStoreList")]
        public IList<int> ShowroomIDs { get; set; }

        public ShoppingCartItemsController(IShoppingCartItemApiService shoppingCartItemApiService,
            IJsonFieldsSerializer jsonFieldsSerializer,
            IAclService aclService,
            ICustomerService customerService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IDiscountService discountService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            IShoppingCartService shoppingCartService,
            IProductService productService,
            IFactory<ShoppingCartItem> factory,
            IPictureService pictureService,
            IProductAttributeConverter productAttributeConverter,
            IDTOHelper dtoHelper,
            IWorkContext workContext,
            IBackInStockSubscriptionService backInStockSubscriptionService,
            CatalogSettings catalogSettings,
            IPermissionService permissionService,
            ISettingService settingService,
            IShippingService shippingService,
            ILogger logger,
            IAddressService addressService,
            IShoppingCartModelFactory shoppingCartModelFactory,
            IGenericAttributeService genericAttributeService,
            IStoreContext storeContext,
            IOrderApiService orderApiService)
            : base(jsonFieldsSerializer,
                     aclService,
                     customerService,
                     storeMappingService,
                     storeService,
                     discountService,
                     customerActivityService,
                     localizationService,
                     pictureService
                  )
        {
            _shoppingCartItemApiService = shoppingCartItemApiService;
            _shoppingCartService = shoppingCartService;
            _productService = productService;
            _factory = factory;
            _productAttributeConverter = productAttributeConverter;
            _dtoHelper = dtoHelper;
            _storeContext = storeContext;
            _workContext = workContext;
            _localizationService = localizationService;
            _backInStockSubscriptionService = backInStockSubscriptionService;
            _catalogSettings = catalogSettings;
            _permissionService = permissionService;
            _settingService = settingService;
            _shippingService = shippingService;
            _logger = logger;
            _addressService = addressService;
            _customerService = customerService;
            _shoppingCartModelFactory = shoppingCartModelFactory;
            _genericAttributeService = genericAttributeService;
            _orderApiService = orderApiService;
        }

        public ShoppingCartItemsRootObject GetMyShoppingCart(int customerId, string trackId, int? languageId = null)
        {

            IList<ShoppingCartItem> shoppingCartItems = _shoppingCartItemApiService.GetShoppingCartItems(customerId);

            if (shoppingCartItems == null)
            {
                //return Error(HttpStatusCode.NotFound, "shopping_cart_item", "not found");
                var shoppingCartErrorObject = new ShoppingCartItemsRootObject()
                {
                    Error = new ErrorResponse
                    {
                        ErrorCode = "2045",
                        ErrorDesc = "CartIsEmpty",
                        ErrorTitle = "Error"
                    },
                    ShoppingCartItems = null,
                    ShoppingCartTotal = null
                };
                return shoppingCartErrorObject;
            }

            var shoppingCartItemsDtos = shoppingCartItems
                .Select(shoppingCartItem => _dtoHelper.PrepareShoppingCartItemDTO(shoppingCartItem, languageId))
                .ToList();          

            var shoppingCartsRootObject = new ShoppingCartItemsRootObject()
            {
                ShoppingCartItems = shoppingCartItemsDtos,
                ShoppingCartTotal = Convert.ToString(shoppingCartItemsDtos.Sum((c => Convert.ToDouble(c.ProductDto.ProductPriceDetails.PriceValue) * Convert.ToDouble(c.Quantity)))),
                TrackId = trackId
            };


            return shoppingCartsRootObject;
        }

        public ShoppingCartItemsRootObject GetMyShoppingCartOptimized(int customerId, string trackId, int Page, int Limit, int? languageId = null)
        {
            string shoppingCartTotal = "";
            IList <ShoppingCartItem> shoppingCartItems = _shoppingCartItemApiService.GetShoppingCartItems(customerId, page: Page, limit: Limit);
            
            if (shoppingCartItems == null)
            {
                //return Error(HttpStatusCode.NotFound, "shopping_cart_item", "not found");
                var shoppingCartErrorObject = new ShoppingCartItemsRootObject()
                {
                    Error = new ErrorResponse
                    {
                        ErrorCode = "2045",
                        ErrorDesc = "CartIsEmpty",
                        ErrorTitle = "Error"
                    },
                    ShoppingCartItems = null,
                    ShoppingCartTotal = null
                };
                return shoppingCartErrorObject;
            }

            ////Added this block, to calculate Cart Total.  
            if (customerId > 0)
            {
                IList<ShoppingCartItem> MyShoppingCartItems = _shoppingCartItemApiService.GetShoppingCartItems(customerId);
                var myShoppingCartItemsDtos = MyShoppingCartItems.Select(shoppingCartItem => _dtoHelper.PrepareShoppingCartItemDTOOptimized(shoppingCartItem, true, languageId)).ToList();
                shoppingCartTotal = Convert.ToString(myShoppingCartItemsDtos.Sum((c => Convert.ToDouble(c.ProductDto.ProductPriceDetails.PriceValue) * Convert.ToDouble(c.Quantity))));
            }
            //

            var shoppingCartItemsDtos = shoppingCartItems
                .Select(shoppingCartItem => _dtoHelper.PrepareShoppingCartItemDTOOptimized(shoppingCartItem, languageId : languageId))
                .ToList();
            
            var shoppingCartsRootObject = new ShoppingCartItemsRootObject()
            {
                ShoppingCartItems = shoppingCartItemsDtos,
                ShoppingCartTotal = shoppingCartTotal,// Convert.ToString(shoppingCartItemsDtos.Sum((c => Convert.ToDouble(c.ProductDto.ProductPriceDetails.PriceValue) * Convert.ToDouble(c.Quantity)))),
                TrackId = trackId
            };


            return shoppingCartsRootObject;
        }

        /// <summary>
        /// Receive a list of all shopping cart items
        /// </summary>
        /// <response code="200">OK</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/shopping_cart_items")]
        [ProducesResponseType(typeof(ShoppingCartItemsRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetShoppingCartItems(ShoppingCartItemsParametersModel parameters)
        {
            if (parameters.Limit < Configurations.MinLimit || parameters.Limit > Configurations.MaxLimit)
            {
                return Error(HttpStatusCode.BadRequest, "limit", "invalid limit parameter");
            }

            if (parameters.Page < Configurations.DefaultPageValue)
            {
                return Error(HttpStatusCode.BadRequest, "page", "invalid page parameter");
            }

            IList<ShoppingCartItem> shoppingCartItems = _shoppingCartItemApiService.GetShoppingCartItems(customerId: null,
                                                                                                         createdAtMin: parameters.CreatedAtMin,
                                                                                                         createdAtMax: parameters.CreatedAtMax,
                                                                                                         updatedAtMin: parameters.UpdatedAtMin,
                                                                                                         updatedAtMax: parameters.UpdatedAtMax,
                                                                                                         limit: parameters.Limit,
                                                                                                         page: parameters.Page);

            var shoppingCartItemsDtos = shoppingCartItems.Select(shoppingCartItem =>
            {
                return _dtoHelper.PrepareShoppingCartItemDTO(shoppingCartItem);
            }).ToList();

            var shoppingCartsRootObject = new ShoppingCartItemsRootObject()
            {
                ShoppingCartItems = shoppingCartItemsDtos
            };

            var json = JsonFieldsSerializer.Serialize(shoppingCartsRootObject, parameters.Fields);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// Receive a list of all shopping cart items by customer id
        /// </summary>
        /// <param name="customerId">Id of the customer whoes shopping cart items you want to get</param>
        /// <param name="parameters"></param>
        /// <response code="200">OK</response>
        /// <response code="400">Bad Request</response>
        /// <response code="404">Not Found</response>
        /// <response code="401">Unauthorized</response>
        [HttpPost]
        //[Route("/api/getCartItemsByCustomerId/{customerId}/{setProceedToCheckoutFlag}")]
        //[Route("/api/shoppingCartItems/{customerId}/{setProceedToCheckoutFlag}")]
        [Route("/api/getCartItemsByCustomerId/{customerId}/{languageId?}")]
        [Route("/api/shoppingCartItems/{customerId}/{languageId?}")]
        [ProducesResponseType(typeof(ShoppingCartItemsRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetShoppingCartItemsByCustomerId(int customerId, ShoppingCartItemsForCustomerParametersModel parameters, int? languageId = null)
        {
            try
            {
                if (customerId <= Configurations.DefaultCustomerId)
                {
                    var errorsJson = GetJsonErrorObject("2042", "InvalidCustomerID", "Error");
                    return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
                }

                var customer = CustomerService.GetCustomerById(customerId);

                if (customer == null)
                {
                    var errorsJson = GetJsonErrorObject("2042", "InvalidCustomerID", "Error");
                    return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
                }

                if (parameters.Limit < Configurations.MinLimit || parameters.Limit > Configurations.MaxLimit)
                {
                    return Error(HttpStatusCode.BadRequest, "limit", "invalid limit parameter");
                }

                if (parameters.Page < Configurations.DefaultPageValue)
                {
                    return Error(HttpStatusCode.BadRequest, "page", "invalid page parameter");
                }

                ////Now, we are not setting flag "setProceedToCheckoutFlag" = true, from API - ViewCart. So commenting..
                //if (setProceedToCheckoutFlag != null && setProceedToCheckoutFlag !="" && setProceedToCheckoutFlag.ToLower() == "true")
                //{
                //    _genericAttributeService.SaveAttribute(customer, NopCustomerDefaults.ProceedToCheckout, "true", _storeContext.CurrentStore.Id);
                //}

                string TrackId = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.TrackId, _storeContext.CurrentStore.Id);
                var myShoppingCart = GetMyShoppingCart(customer.Id, TrackId, languageId);

               
                var json = JsonFieldsSerializer.Serialize(myShoppingCart, parameters.Fields);
                return new ErrorActionResult(json, HttpStatusCode.OK);

                #region
                // ////Start: For Almailem: Commenting below code, as this response does not give TOTAL AMOUNT of an item in cart.
                // IList<ShoppingCartItem> shoppingCartItems = _shoppingCartItemApiService.GetShoppingCartItems(customerId,
                //                                                                                              parameters.CreatedAtMin,
                //                                                                                              parameters.CreatedAtMax, parameters.UpdatedAtMin,
                //                                                                                              parameters.UpdatedAtMax, parameters.Limit,
                //                                                                                              parameters.Page);

                // if (shoppingCartItems == null)
                // {
                //     return Error(HttpStatusCode.NotFound, "shopping_cart_item", "not found");
                // }

                // var shoppingCartItemsDtos = shoppingCartItems
                //     .Select(shoppingCartItem => _dtoHelper.PrepareShoppingCartItemDTO(shoppingCartItem))
                //     .ToList();

                // var shoppingCartsRootObject = new ShoppingCartItemsRootObject()
                // {
                //     ShoppingCartItems = shoppingCartItemsDtos
                // };

                // var json = JsonFieldsSerializer.Serialize(shoppingCartsRootObject, parameters.Fields);
                //// return new ErrorActionResult(json, HttpStatusCode.OK);
                // ////End.

                // //// Start: Use below response to get TOTAL.
                // var cart = customer.ShoppingCartItems
                //             .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                //             .LimitPerStore(_storeContext.CurrentStore.Id)
                //             .ToList();
                // var model = new ShoppingCartModel();

                // model.ShoppingCartTotal = 0.00M;

                // model = _shoppingCartModelFactory.PrepareShoppingCartModel(model, cart);

                // //model.ShoppingCartTotal = model.Items.Sum(c => Convert.ToDouble(c.SubTotal));
                // model.DiscountBox = null;
                // model.GiftCardBox = null;
                // model.OrderReviewData = null;

                // var myCart = new MyCartItemsRootObject
                // {
                //     Error = null,
                //     shoppingCartModel = model
                // };

                // var jsons = JsonConvert.SerializeObject(myCart);
                //return new ErrorActionResult(jsons, HttpStatusCode.OK);
                ////END.
                #endregion
            }
            catch
            {
                var myCart = new ShoppingCartItemsRootObject
                {
                    Error = new ErrorResponse
                    {
                        ErrorCode = "2030",
                        ErrorDesc = "GenericError",
                        ErrorTitle = "Error"
                    },
                    ShoppingCartItems = null,
                    ShoppingCartTotal = null
                };
                var myErrJson = JsonConvert.SerializeObject(myCart);
                return new ErrorActionResult(myErrJson, HttpStatusCode.OK);
                //var errorsJson = GetJsonErrorObject("2030", "GenericError", "Error");
                //return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
            }
        }

        /// <summary>
        /// Receive a list of all shopping cart items by customer id
        /// </summary>
        /// <param name="customerId">Id of the customer whoes shopping cart items you want to get</param>
        /// <param name="parameters"></param>
        /// <response code="200">OK</response>
        /// <response code="400">Bad Request</response>
        /// <response code="404">Not Found</response>
        /// <response code="401">Unauthorized</response>
        [HttpPost]
        [Route("/api/viewCartItemsByCustomerId/{customerId}/{page}/{limit}/{languageId?}")]
        [ProducesResponseType(typeof(ShoppingCartItemsRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult ViewCartItemsByCustomerId(int customerId, int page, int limit, int? languageId = null)
        {
            GenericJsonResponse genericJsonResponse = new GenericJsonResponse();
            ErrorResponse errorResponse = new ErrorResponse();
            try
            {
                if (customerId <= Configurations.DefaultCustomerId)
                {
                    errorResponse.ErrorCode = "2042";
                    errorResponse.ErrorTitle = "Error";
                    errorResponse.ErrorDesc = "InvalidCustomerID";
                    genericJsonResponse.Data = null;
                    genericJsonResponse.Error = errorResponse;
                    return Json(genericJsonResponse);
                }

                var customer = CustomerService.GetCustomerById(customerId);

                if (customer == null)
                {
                    errorResponse.ErrorCode = "2042";
                    errorResponse.ErrorTitle = "Error";
                    errorResponse.ErrorDesc = "InvalidCustomerID";
                    genericJsonResponse.Data = null;
                    genericJsonResponse.Error = errorResponse;
                    return Json(genericJsonResponse);
                }
                
                string TrackId = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.TrackId, _storeContext.CurrentStore.Id);
                var myShoppingCart = GetMyShoppingCartOptimized(customer.Id, TrackId, page, limit, languageId);
                CatalogPagingFilteringModel command = new CatalogPagingFilteringModel();
                IList<ShoppingCartItem> shoppingCartItems = _shoppingCartItemApiService.GetShoppingCartItems(customerId);

                if (shoppingCartItems == null || shoppingCartItems.Count() <= 0)
                {
                    errorResponse.ErrorCode = "2045";
                    errorResponse.ErrorTitle = "Error";
                    errorResponse.ErrorDesc = "CartIsEmpty";
                    genericJsonResponse.Data = null;
                    genericJsonResponse.Error = errorResponse;
                    return Json(genericJsonResponse);
                }
                
                if (shoppingCartItems != null && shoppingCartItems.Count() > 0)
                {
                    errorResponse = null;
                }

                command.TotalItems = shoppingCartItems.Count;
                command.PageSize = limit;
                command.PageNumber = page;
                if (command.TotalItems % command.PageSize == 0)
                {
                    command.TotalPages = command.TotalItems / command.PageSize;
                }
                else
                {
                    command.TotalPages = command.TotalItems / command.PageSize;
                    command.TotalPages += 1;
                }
                ViewCartResponse viewCartResponse = new ViewCartResponse();
                viewCartResponse.catalogPagingFilteringModel = command;
                viewCartResponse.shoppingCartItemsRootObject = myShoppingCart;
                genericJsonResponse.Data = viewCartResponse;
                genericJsonResponse.Error = errorResponse;
                return Json(genericJsonResponse);
            }
            catch (Exception ex)
            {
                errorResponse.ErrorCode = "2030";
                errorResponse.ErrorTitle = "Error";
                errorResponse.ErrorDesc = "GenericError";
                genericJsonResponse.Data = null;
                genericJsonResponse.Error = errorResponse;
                return Json(genericJsonResponse);
            }
        }

        [HttpPost]
        [Route("/api/addToCart")]
        [Route("/api/shoppingCartItems")]
        [ProducesResponseType(typeof(ShoppingCartItemsRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), 422)]
        public IActionResult CreateShoppingCartItem([ModelBinder(typeof(JsonModelBinder<ShoppingCartItemDto>))] Delta<ShoppingCartItemDto> shoppingCartItemDelta)
        {
            string TrackId = string.Empty;
            try
            {
                // Here we display the errors if the validation has failed at some point.
                if (!ModelState.IsValid)
                {
                    //return Error();
                    var logDetails = ModelState.Root.Children.ToList().FirstOrDefault().Errors.FirstOrDefault().ErrorMessage.ToString();

                    var errorJson = GetJsonErrorObject("2034", "AddToCartError. Reason: " + logDetails, "Error");

                    _logger.Error("Error! Event: Add to cart. Reason: " + logDetails);

                    return new ErrorActionResult(errorJson, HttpStatusCode.OK);
                }

                var newShoppingCartItem = _factory.Initialize();
                shoppingCartItemDelta.Merge(newShoppingCartItem);

                // We know that the product id and customer id will be provided because they are required by the validator.
                // TODO: validate
                var product = _productService.GetProductById(newShoppingCartItem.ProductId);

                if (product == null)
                {
                    var errorsJson = GetJsonErrorObject("2043", "InvalidProductID", "Error");
                    return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
                }

                var customer = CustomerService.GetCustomerById(newShoppingCartItem.CustomerId);

                if (customer == null)
                {
                    var errorsJson = GetJsonErrorObject("2042", "InvalidCustomerID", "Error");
                    return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
                }

                //reset checkout data
                _customerService.ResetCheckoutData(customer, _storeContext.CurrentStore.Id);

                var shoppingCartType = (ShoppingCartType)Enum.Parse(typeof(ShoppingCartType), shoppingCartItemDelta.Dto.ShoppingCartType);

                if (!product.IsRental)
                {
                    newShoppingCartItem.RentalStartDateUtc = null;
                    newShoppingCartItem.RentalEndDateUtc = null;
                }

                var attributesXml = _productAttributeConverter.ConvertToXml(shoppingCartItemDelta.Dto.Attributes, product.Id);

                var currentStoreId = _storeContext.CurrentStore.Id;

                var warnings = _shoppingCartService.AddToCart(customer, product, shoppingCartType, currentStoreId, attributesXml, 0M,
                                            newShoppingCartItem.RentalStartDateUtc, newShoppingCartItem.RentalEndDateUtc,
                                            shoppingCartItemDelta.Dto.Quantity ?? 1);
                var shoppingCartItem = _shoppingCartService.FindShoppingCartItemInTheCart(customer.ShoppingCartItems.ToList(),
                shoppingCartType, product);
                int newQuantity = 0;
                if (shoppingCartItem != null)
                    newQuantity = shoppingCartItem.Quantity + Convert.ToInt32(shoppingCartItemDelta.Dto.Quantity);
                else
                    newQuantity = Convert.ToInt32(shoppingCartItemDelta.Dto.Quantity);
                if (warnings.Count > 0)
                {
                    foreach (var warning in warnings)
                    {
                        ModelState.AddModelError("shopping cart item", warning);
                    }

                    var logDetails = ModelState.Root.Children.ToList().FirstOrDefault().Errors.FirstOrDefault().ErrorMessage.ToString();

                    //01. MaximumQuantityError:
                    var warningMaximumQuantity = "";
                    if (newQuantity > product.OrderMaximumQuantity)
                    {
                        warningMaximumQuantity = (string.Format(_localizationService.GetResource("ShoppingCart.MaximumQuantity"), product.OrderMaximumQuantity));
                        if (warningMaximumQuantity == logDetails)
                        {
                            //var warningJson = GetJsonErrorObject("2091", "MaximumQuantityError. Reason: " + logDetails, "Error");

                            _logger.Error("Error! Event: Add to cart. Reason: " + warningMaximumQuantity);

                            var myCart = new ShoppingCartItemsRootObject
                            {
                                Error = new ErrorResponse
                                {
                                    ErrorCode = "2091",
                                    ErrorDesc = "MaximumQuantityError",
                                    ErrorTitle = "Error"
                                },
                                ShoppingCartItems = null,
                                ShoppingCartTotal = null
                            };
                            var myErrJson2 = JsonConvert.SerializeObject(myCart);
                            return new ErrorActionResult(myErrJson2, HttpStatusCode.OK);
                        }
                    }
                    //

                    //02. QuantityExceedsStockOnHand:
                    var warningQuantityExceedsStock = "";
                    var maximumQuantityCanBeAdded = _productService.GetTotalStockQuantity(product);
                    if (maximumQuantityCanBeAdded < newQuantity)
                    {
                        if (maximumQuantityCanBeAdded > 0)
                        {
                            warningQuantityExceedsStock = (string.Format(_localizationService.GetResource("ShoppingCart.QuantityExceedsStock"), maximumQuantityCanBeAdded));

                            if (warningQuantityExceedsStock == logDetails)
                            {
                                //var warningJson = GetJsonErrorObject("2069", "QuantityExceedsStockOnHand. Reason: " + logDetails, "Error");

                                _logger.Error("Error! Event: Add to cart. Reason: " + warningQuantityExceedsStock);

                                var myCart = new ShoppingCartItemsRootObject
                                {
                                    Error = new ErrorResponse
                                    {
                                        ErrorCode = "2069",
                                        ErrorDesc = "QuantityExceedsStockOnHand",
                                        ErrorTitle = "Error"
                                    },
                                    ShoppingCartItems = null,
                                    ShoppingCartTotal = null
                                };
                                var myErrJson1 = JsonConvert.SerializeObject(myCart);
                                return new ErrorActionResult(myErrJson1, HttpStatusCode.OK);
                            }
                        }
                        else
                        {
                            var myCart = new ShoppingCartItemsRootObject
                            {
                                Error = new ErrorResponse
                                {
                                    ErrorCode = "2092",
                                    ErrorDesc = "ProductOutOfStock",
                                    ErrorTitle = "Error"
                                },
                                ShoppingCartItems = null,
                                ShoppingCartTotal = null
                            };
                            var myErrJson3 = JsonConvert.SerializeObject(myCart);
                            return new ErrorActionResult(myErrJson3, HttpStatusCode.OK);
                        }

                    }
                    //

                    var myCartError = new ShoppingCartItemsRootObject
                    {
                        Error = new ErrorResponse
                        {
                            ErrorCode = "2034",
                            ErrorDesc = "AddToCartError",
                            ErrorTitle = "Error"
                        },
                        ShoppingCartItems = null,
                        ShoppingCartTotal = null
                    };
                    var myErrJson = JsonConvert.SerializeObject(myCartError);
                    return new ErrorActionResult(myErrJson, HttpStatusCode.OK);

                    //var errorJson = GetJsonErrorObject("2034", "AddToCartError. Reason: " + logDetails, "Error");
                    //_logger.Error("Error! Event: Add to cart. Reason: " + logDetails);
                    //return new ErrorActionResult(errorJson, HttpStatusCode.OK);

                }

                TrackId = GenerateTrackId();
                //Save TrackId to GenericAttribute
                _genericAttributeService.SaveAttribute<string>(customer, NopCustomerDefaults.TrackId, TrackId, _storeContext.CurrentStore.Id);
                //var myShoppingCart = GetMyShoppingCart(customer.Id, TrackId);

                //if (myShoppingCart.Error == null)
                //{
                //    myShoppingCart.Error = new ErrorResponse
                //    {
                //        ErrorCode = "2035",
                //        ErrorDesc = "AddToCartSuccess",
                //        ErrorTitle = ""
                //    };
                //}
                ShoppingCartItemsRootObject myShoppingCart = new ShoppingCartItemsRootObject();
                myShoppingCart.TrackId = TrackId;
                myShoppingCart.Error = new ErrorResponse
                {
                    ErrorCode = "2035",
                    ErrorDesc = "AddToCartSuccess",
                    ErrorTitle = ""
                };
                var json = JsonFieldsSerializer.Serialize(myShoppingCart, "");
                return new ErrorActionResult(json, HttpStatusCode.OK);

                #region
                ////Start: For Almailem: Commenting below code, as this response does not give TOTAL AMOUNT of an item in cart.
                //else
                //{
                //    //// the newly added shopping cart item should be the last one
                //    //newShoppingCartItem = customer.ShoppingCartItems.LastOrDefault();
                //}

                //// Preparing the result dto of the new product category mapping
                //var newShoppingCartItemDto = _dtoHelper.PrepareShoppingCartItemDTO(newShoppingCartItem);

                //var shoppingCartsRootObject = new ShoppingCartItemsRootObject();

                //shoppingCartsRootObject.ShoppingCartItems.Add(newShoppingCartItemDto);

                //shoppingCartsRootObject.ErrorCode = "2035";
                //shoppingCartsRootObject.ErrorDesc = "AddToCartSuccess";
                //shoppingCartsRootObject.ErrorTitle = "";

                //var json = JsonFieldsSerializer.Serialize(shoppingCartsRootObject, string.Empty);
                ////return new ErrorActionResult(json, HttpStatusCode.OK);                

                ////// Start: Use below response to get TOTAL.
                //var cart = customer.ShoppingCartItems
                //            .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                //            .LimitPerStore(_storeContext.CurrentStore.Id)
                //            .ToList();
                //var model = new ShoppingCartModel();
                //model = _shoppingCartModelFactory.PrepareShoppingCartModel(model, cart);

                //model.DiscountBox = null;
                //model.GiftCardBox = null;
                //model.OrderReviewData = null;

                //var myCart = new MyCartItemsRootObject
                //{
                //    Error = new ErrorResponse
                //    {
                //        ErrorCode = "2035",
                //        ErrorDesc = "AddToCartSuccess",
                //        ErrorTitle = ""
                //    },
                //    shoppingCartModel = model
                //};

                //var myJson = JsonConvert.SerializeObject(myCart);
                //return new ErrorActionResult(myJson, HttpStatusCode.OK);
                //////END.
                #endregion
            }
            catch
            {
                _logger.Error("Error! Event: Add to Cart.");
                var myCart = new ShoppingCartItemsRootObject
                {
                    Error = new ErrorResponse
                    {
                        ErrorCode = "2034",
                        ErrorDesc = "AddToCartError",
                        ErrorTitle = "Error"
                    },
                    ShoppingCartItems = null,
                    ShoppingCartTotal = null
                };
                var myErrJson = JsonConvert.SerializeObject(myCart);
                return new ErrorActionResult(myErrJson, HttpStatusCode.OK);

                //var errorsJson = GetJsonErrorObject("2034", "AddToCartError", "Error");
                //return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
            }
        }

        [HttpPost]
        [Route("/api/updateCartItemByItemId/{id}")]
        //[Route("/api/shoppingCartItems/{id}")]
        [ProducesResponseType(typeof(ShoppingCartItemsRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult UpdateShoppingCartItem([ModelBinder(typeof(JsonModelBinder<ShoppingCartItemDto>))] Delta<ShoppingCartItemDto> shoppingCartItemDelta)
        {
            string TrackId = string.Empty;
            try
            {
                // Here we display the errors if the validation has failed at some point.
                ////if (!ModelState.IsValid)
                ////{
                ////    var logDetails = ModelState.Root.Children.ToList().FirstOrDefault().Errors.FirstOrDefault().ErrorMessage.ToString();

                ////    var errorJson = GetJsonErrorObject("2036", "UpdateCartError. Reason: " + logDetails, "Error");

                ////    _logger.Error("Error! Event: Update cart. Reason: " + logDetails);

                ////    return new ErrorActionResult(errorJson, HttpStatusCode.OK);
                ////}

                // We kno that the id will be valid integer because the validation for this happens in the validator which is executed by the model binder.
                var shoppingCartItemForUpdate = _shoppingCartItemApiService.GetShoppingCartItem(shoppingCartItemDelta.Dto.Id);

                if (shoppingCartItemForUpdate == null)
                {
                    var errorsJson = GetJsonErrorObject("2044", "InvalidCartItemID", "Error");
                    return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
                }

                int oldQuantity = shoppingCartItemForUpdate.Quantity;

                //  To, reset checkout data.
                var customer = CustomerService.GetCustomerById(shoppingCartItemForUpdate.CustomerId);

                if (customer == null)
                {
                    var errorsJson = GetJsonErrorObject("2042", "InvalidCustomerID", "Error");
                    return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
                }

                //reset checkout data
                _customerService.ResetCheckoutData(customer, _storeContext.CurrentStore.Id);
                //

                shoppingCartItemDelta.Merge(shoppingCartItemForUpdate);

                if (!shoppingCartItemForUpdate.Product.IsRental)
                {
                    shoppingCartItemForUpdate.RentalStartDateUtc = null;
                    shoppingCartItemForUpdate.RentalEndDateUtc = null;
                }

                if (shoppingCartItemDelta.Dto.Attributes != null)
                {
                    shoppingCartItemForUpdate.AttributesXml = _productAttributeConverter.ConvertToXml(shoppingCartItemDelta.Dto.Attributes, shoppingCartItemForUpdate.Product.Id);
                }

                // The update time is set in the service.
                var warnings = _shoppingCartService.UpdateShoppingCartItem(shoppingCartItemForUpdate.Customer, shoppingCartItemForUpdate.Id,
                    shoppingCartItemForUpdate.AttributesXml, shoppingCartItemForUpdate.CustomerEnteredPrice,
                    shoppingCartItemForUpdate.RentalStartDateUtc, shoppingCartItemForUpdate.RentalEndDateUtc,
                    shoppingCartItemForUpdate.Quantity);

                var shoppingCartItem = _shoppingCartService.FindShoppingCartItemInTheCart(customer.ShoppingCartItems.ToList(),
                shoppingCartItemForUpdate.ShoppingCartType, shoppingCartItemForUpdate.Product);
                int newQuantity = Convert.ToInt32(shoppingCartItemDelta.Dto.Quantity);

                if (warnings.Count > 0)
                {
                    //To avoid quantity update in case of any warnings. 
                    shoppingCartItemForUpdate.Quantity = oldQuantity;
                    foreach (var warning in warnings)
                    {
                        ModelState.AddModelError("shopping cart item", warning);
                    }

                    var logDetails = ModelState.Root.Children.ToList().FirstOrDefault().Errors.FirstOrDefault().ErrorMessage.ToString();

                    //01. QuantityExceedsStockOnHand:
                    var warningQuantityExceedsStock = "";
                    var maximumQuantityCanBeAdded = _productService.GetTotalStockQuantity(shoppingCartItemForUpdate.Product);
                    if (maximumQuantityCanBeAdded < newQuantity)
                    {
                        if (maximumQuantityCanBeAdded > 0)
                        {
                            warningQuantityExceedsStock = (string.Format(_localizationService.GetResource("ShoppingCart.QuantityExceedsStock"), maximumQuantityCanBeAdded));

                            if (warningQuantityExceedsStock == logDetails)
                            {
                                //var warningJson = GetJsonErrorObject("2069", "QuantityExceedsStockOnHand. Reason: " + logDetails, "Error");

                                _logger.Error("Error! Event: Update cart. Reason: " + warningQuantityExceedsStock);

                                var myCart = new ShoppingCartItemsRootObject
                                {
                                    Error = new ErrorResponse
                                    {
                                        ErrorCode = "2069",
                                        ErrorDesc = "QuantityExceedsStockOnHand",
                                        ErrorTitle = "Error"
                                    },
                                    ShoppingCartItems = null,
                                    ShoppingCartTotal = null
                                };
                                var myErrJson1 = JsonConvert.SerializeObject(myCart);
                                return new ErrorActionResult(myErrJson1, HttpStatusCode.OK);
                            }
                        }
                        else
                        {
                            var myCart = new ShoppingCartItemsRootObject
                            {
                                Error = new ErrorResponse
                                {
                                    ErrorCode = "2092",
                                    ErrorDesc = "ProductOutOfStock",
                                    ErrorTitle = "Error"
                                },
                                ShoppingCartItems = null,
                                ShoppingCartTotal = null
                            };
                            var myErrJson3 = JsonConvert.SerializeObject(myCart);
                            return new ErrorActionResult(myErrJson3, HttpStatusCode.OK);
                        }

                    }

                    var errorJson = GetJsonErrorObject("2036", "UpdateCartError. Reason: " + logDetails, "Error");

                    _logger.Error("Error! Event: Update cart. Reason: " + logDetails);

                    return new ErrorActionResult(errorJson, HttpStatusCode.OK);
                }

                TrackId = GenerateTrackId();
                //Save TrackId to GenericAttribute
                _genericAttributeService.SaveAttribute<string>(customer, NopCustomerDefaults.TrackId, TrackId, _storeContext.CurrentStore.Id);
                //var myShoppingCart = GetMyShoppingCart(customer.Id, TrackId);

                //if (myShoppingCart.Error == null)
                //{
                //    myShoppingCart.Error = new ErrorResponse
                //    {
                //        ErrorCode = "2037",
                //        ErrorDesc = "UpdateToCartSuccess",
                //        ErrorTitle = ""
                //    };
                //}
                ShoppingCartItemsRootObject myShoppingCart = new ShoppingCartItemsRootObject();
                myShoppingCart.TrackId = TrackId;
                myShoppingCart.Error = new ErrorResponse
                {
                    ErrorCode = "2037",
                    ErrorDesc = "UpdateToCartSuccess",
                    ErrorTitle = ""
                };
                var json = JsonFieldsSerializer.Serialize(myShoppingCart, "");
                return new ErrorActionResult(json, HttpStatusCode.OK);

                #region
                ////Start: For Almailem: Commenting below code, as this response does not give TOTAL AMOUNT of an item in cart.
                ////else
                ////{
                ////    shoppingCartItemForUpdate = _shoppingCartItemApiService.GetShoppingCartItem(shoppingCartItemForUpdate.Id);
                ////}

                ////// Preparing the result dto of the new product category mapping
                ////var newShoppingCartItemDto = _dtoHelper.PrepareShoppingCartItemDTO(shoppingCartItemForUpdate);

                ////var shoppingCartsRootObject = new ShoppingCartItemsRootObject();

                ////shoppingCartsRootObject.ShoppingCartItems.Add(newShoppingCartItemDto);
                ////shoppingCartsRootObject.ErrorCode = "2037";
                ////shoppingCartsRootObject.ErrorDesc = "UpdateCartSuccess";
                ////shoppingCartsRootObject.ErrorTitle = "";

                ////var json = JsonFieldsSerializer.Serialize(shoppingCartsRootObject, string.Empty);

                ////return new RawJsonActionResult(json);
                ///

                //// Start: Use below response to get TOTAL.
                //var cart = customer.ShoppingCartItems
                //            .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                //            .LimitPerStore(_storeContext.CurrentStore.Id)
                //            .ToList();
                //var model = new ShoppingCartModel();
                //model = _shoppingCartModelFactory.PrepareShoppingCartModel(model, cart);

                //model.DiscountBox = null;
                //model.GiftCardBox = null;
                //model.OrderReviewData = null;

                //var myCart = new MyCartItemsRootObject
                //{
                //    Error = new ErrorResponse
                //    {
                //        ErrorCode = "2037",
                //        ErrorDesc = "UpdateToCartSuccess",
                //        ErrorTitle = ""
                //    },
                //    shoppingCartModel = model
                //};

                //var myJson = JsonConvert.SerializeObject(myCart);
                //return new ErrorActionResult(myJson, HttpStatusCode.OK);
                ////END.
                #endregion
            }
            catch
            {
                _logger.Error("Error! Event: Update Cart Item.");
                var myCart = new ShoppingCartItemsRootObject
                {
                    Error = new ErrorResponse
                    {
                        ErrorCode = "2036",
                        ErrorDesc = "UpdateCartError",
                        ErrorTitle = "Error"
                    },
                    ShoppingCartItems = null,
                    ShoppingCartTotal = null
                };
                var myErrJson = JsonConvert.SerializeObject(myCart);
                return new ErrorActionResult(myErrJson, HttpStatusCode.OK);

                //var errorsJson = GetJsonErrorObject("2036", "UpdateCartError", "Error");
                //return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
            }
        }

        [HttpPost]
        [Route("/api/deleteCartItemByItemId/{id}")]
        //[Route("/api/shoppingCartItems/{id}")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult DeleteShoppingCartItem(int id)
        {
            string TrackId = string.Empty;
            try
            {
                if (id <= 0)
                {
                    var errorsJson2 = GetJsonErrorObject("2044", "InvalidCartItemID", "Error");
                    return new ErrorActionResult(errorsJson2, HttpStatusCode.OK);
                }

                var shoppingCartItemForDelete = _shoppingCartItemApiService.GetShoppingCartItem(id);

                if (shoppingCartItemForDelete == null)
                {
                    var errorsJson2 = GetJsonErrorObject("2044", "InvalidCartItemID", "Error");
                    return new ErrorActionResult(errorsJson2, HttpStatusCode.OK);
                }

                //  To, reset checkout data.
                var customer = CustomerService.GetCustomerById(shoppingCartItemForDelete.CustomerId);

                if (customer == null)
                {
                    var errorsJson1 = GetJsonErrorObject("2042", "InvalidCustomerID", "Error");
                    return new ErrorActionResult(errorsJson1, HttpStatusCode.OK);
                }

                //reset checkout data
                _customerService.ResetCheckoutData(customer, _storeContext.CurrentStore.Id);
                //

                _shoppingCartService.DeleteShoppingCartItem(shoppingCartItemForDelete);
                TrackId = GenerateTrackId();
                //Save TrackId to GenericAttribute
                _genericAttributeService.SaveAttribute<string>(customer, NopCustomerDefaults.TrackId, TrackId, _storeContext.CurrentStore.Id);
                //var myShoppingCart = GetMyShoppingCart(customer.Id, TrackId);

                //if (myShoppingCart.Error == null)
                //{
                //    myShoppingCart.Error = new ErrorResponse
                //    {
                //        ErrorCode = "2039",
                //        ErrorDesc = "DeleteToCartSuccess",
                //        ErrorTitle = ""
                //    };
                //}
                ShoppingCartItemsRootObject myShoppingCart = new ShoppingCartItemsRootObject();
                myShoppingCart.TrackId = TrackId;
                myShoppingCart.Error = new ErrorResponse
                {
                    ErrorCode = "2039",
                    ErrorDesc = "DeleteToCartSuccess",
                    ErrorTitle = ""
                };
                var json = JsonFieldsSerializer.Serialize(myShoppingCart, "");
                return new ErrorActionResult(json, HttpStatusCode.OK);


                //activity log
                //CustomerActivityService.InsertActivity("DeleteShoppingCartItem", LocalizationService.GetResource("ActivityLog.DeleteShoppingCartItem"), shoppingCartItemForDelete);

                //var errorsJson = GetJsonErrorObject("2039", "DeleteCartSuccess", "");
                //return new ErrorActionResult(errorsJson, HttpStatusCode.OK);


                //// Start: Use below response to get TOTAL.
                //var cart = customer.ShoppingCartItems
                //            .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                //            .LimitPerStore(_storeContext.CurrentStore.Id)
                //            .ToList();
                //var model = new ShoppingCartModel();
                //model = _shoppingCartModelFactory.PrepareShoppingCartModel(model, cart);

                //model.DiscountBox = null;
                //model.GiftCardBox = null;
                //model.OrderReviewData = null;

                //var myCart = new MyCartItemsRootObject
                //{
                //    Error = new ErrorResponse
                //    {
                //        ErrorCode = "2039",
                //        ErrorDesc = "DeleteToCartSuccess",
                //        ErrorTitle = ""
                //    },
                //    shoppingCartModel = model
                //};

                //var myJson = JsonConvert.SerializeObject(myCart);
                //return new ErrorActionResult(myJson, HttpStatusCode.OK);
                ////END.
            }
            catch
            {
                _logger.Error("Error! Event: Delete Cart Item.");

                var myCart = new ShoppingCartItemsRootObject
                {
                    Error = new ErrorResponse
                    {
                        ErrorCode = "2038",
                        ErrorDesc = "DeleteCartError",
                        ErrorTitle = "Error"
                    },
                    ShoppingCartItems = null,
                    ShoppingCartTotal = null
                };
                var myErrJson = JsonConvert.SerializeObject(myCart);
                return new ErrorActionResult(myErrJson, HttpStatusCode.OK);
                //var errorsJson = GetJsonErrorObject("2038", "DeleteCartError", "Error");
                //return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
            }
        }

        [HttpPost]
        [Route("/api/requestStock")]
        [ProducesResponseType(typeof(ShoppingCartItemsRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), 422)]
        public virtual IActionResult requestStock([FromBody] BackInStockSubscription model) //SubscribePopupPOST(int productId)
        {
            try
            {
                var product = _productService.GetProductById(model.ProductId);
                if (product == null || product.Deleted)
                {
                    var errorsJson2 = GetJsonErrorObject("2043", "InvalidProductID", "Error");
                    return new ErrorActionResult(errorsJson2, HttpStatusCode.OK);
                }

                var customer = CustomerService.GetCustomerById(model.CustomerId);

                if (customer == null || customer.Deleted)
                {
                    var errorsJson2 = GetJsonErrorObject("2042", "InvalidCustomerID", "Error");
                    return new ErrorActionResult(errorsJson2, HttpStatusCode.OK);
                }
                _workContext.CurrentCustomer = customer;

                if (!_workContext.CurrentCustomer.IsRegistered())
                {
                    var errorsJson = GetJsonErrorObject("2033", "UserIsNotRegistered", "Error");
                    return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
                }

                //if (product.ManageInventoryMethod == ManageInventoryMethod.ManageStock &&
                //    product.BackorderMode == BackorderMode.NoBackorders &&
                //    product.AllowBackInStockSubscriptions &&
                //    _productService.GetTotalStockQuantity(product) <= 0)
                if (_productService.GetTotalStockQuantity(product) <= 0)
                {
                    //out of stock
                    var subscription = _backInStockSubscriptionService
                        .FindSubscription(_workContext.CurrentCustomer.Id, product.Id, _storeContext.CurrentStore.Id);
                    if (subscription != null)
                    {
                        //subscription already exists
                        //unsubscribe
                        _backInStockSubscriptionService.DeleteSubscription(subscription);

                        //return Json(new
                        //{
                        //    result = "Unsubscribed"
                        //});
                    }

                    //subscription does not exist
                    //subscribe
                    if (_backInStockSubscriptionService
                        .GetAllSubscriptionsByCustomerId(_workContext.CurrentCustomer.Id, _storeContext.CurrentStore.Id, 0, 1)
                        .TotalCount >= _catalogSettings.MaximumBackInStockSubscriptions)
                    {
                        var logDetails1 = string.Format(_localizationService.GetResource("BackInStockSubscriptions.MaxSubscriptions"), _catalogSettings.MaximumBackInStockSubscriptions);
                        _logger.Error("Error! Event: Request stock. Reason: " + logDetails1);
                        var errorsJson2 = GetJsonErrorObject("2040", "RequestStockError. Reason: " + logDetails1, "Error");
                        return new ErrorActionResult(errorsJson2, HttpStatusCode.OK);
                        //return Json(new
                        //{
                        //    result = string.Format(_localizationService.GetResource("BackInStockSubscriptions.MaxSubscriptions"), _catalogSettings.MaximumBackInStockSubscriptions)
                        //});
                    }
                    subscription = new BackInStockSubscription
                    {
                        Customer = _workContext.CurrentCustomer,
                        Product = product,
                        StoreId = _storeContext.CurrentStore.Id,
                        CreatedOnUtc = DateTime.UtcNow,
                        Quantity = model.Quantity
                    };
                    _backInStockSubscriptionService.InsertSubscription(subscription);

                    var errorsJson = GetJsonErrorObject("2041", "RequestStockSuccess", "");
                    return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
                }

                //subscription not possible
                var logDetails = _localizationService.GetResource("BackInStockSubscriptions.NotAllowed");
                _logger.Error("Error! Event: Request stock. Reason: " + logDetails);
                var errorsJson1 = GetJsonErrorObject("2040", "RequestStockError. Reason: " + logDetails, "Error");
                return new ErrorActionResult(errorsJson1, HttpStatusCode.OK);
                // return Content(_localizationService.GetResource("BackInStockSubscriptions.NotAllowed"));
            }
            catch
            {
                _logger.Error("Error! Event: Request stock.");
                var errorsJson = GetJsonErrorObject("2040", "RequestStockError", "Error");
                return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
            }

        }

        [HttpGet]
        [Route("/api/getDeliveryTypes")]
        [ProducesResponseType(typeof(ShoppingCartItemsRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), 422)]
        public IActionResult FixedShippingRateList()
        {
            var rateModels = _shippingService.GetAllShippingMethods().Select(shippingMethod => new FixedRateModel
            {
                ShippingMethodId = shippingMethod.Id,
                ShippingMethodName = shippingMethod.Name,
                Rate = _settingService.GetSettingByKey<decimal>(string.Format(FixedByWeightByTotalDefaults.FixedRateSettingsKey, shippingMethod.Id)),
                ShippingMethodDescription = shippingMethod.Description,
                DisplayOrder = shippingMethod.DisplayOrder
            }).ToList();

            return Json(rateModels);
        }


        [HttpGet]
        [Route("/api/getStoreList/{languageId?}")]
        [ProducesResponseType(typeof(ShoppingCartItemsRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), 422)]
        public IActionResult GetStoreList(int? languageId = null)
        {
            //var storeList = _shippingService.GetAllWarehouses();
            //return Json(storeList);

            var storeList = _shippingService.GetAllWarehouses().Select(stores => new StoreDetails
            {
                Id = stores.Id,
                Name = _localizationService.GetLocalized(stores, x => x.Name, languageId),
                StoreCode = stores.StoreCode,
                StoreTimings = stores.StoreTimings,
                Features = stores.Features,
                Latitude = stores.Latitude,
                Longitude = stores.Longitude,
                Governorate = _localizationService.GetLocalized(stores, x => x.Governorate, languageId),
                AdminComment = stores.AdminComment,
                AddressId = _localizationService.GetLocalized(stores, x => x.AddressId, languageId),
                IsWarehouse = stores.IsWarehouse,
                StoreAddress = GetLocalizedAddresss(stores,languageId)
            }).Where(stores => !stores.IsWarehouse).OrderBy(o => o.Name).ToList();

            return Json(storeList);

            //var rateModels = _shippingService.GetAllShippingMethods().Select(shippingMethod => new FixedRateModel
            //{
            //    ShippingMethodId = shippingMethod.Id,
            //    ShippingMethodName = shippingMethod.Name,
            //    Rate = _settingService.GetSettingByKey<decimal>(string.Format(FixedByWeightByTotalDefaults.FixedRateSettingsKey, shippingMethod.Id)),
            //    ShippingMethodDescription = shippingMethod.Description,
            //    DisplayOrder = shippingMethod.DisplayOrder
            //}).ToList();

            //return Json(rateModels);

        }

        //Get Local Address if it is not present then get standard address
        private Core.Domain.Common.Address GetLocalizedAddresss(Core.Domain.Shipping.Warehouse stores, int? languageId = null)
        {
            var stdAddress = stores.AddressId > 0 ? (_addressService.GetAddressById(stores.AddressId)) : null;
            if (languageId.HasValue)
            {
                var localAddressId = _localizationService.GetLocalized(stores, x => x.AddressId, languageId);
                var localAddress = localAddressId > 0 ? (_addressService.GetAddressById(localAddressId)) : null;
                if(localAddress != null)
                {
                    if (stdAddress == null)
                        stdAddress = localAddress;
                    else
                    {
                        //If localAddress field is not null then assign that to address field
                        stdAddress.Address1 = !string.IsNullOrEmpty(localAddress.Address1) ? localAddress.Address1 : stdAddress.Address1;
                        stdAddress.Address2 = !string.IsNullOrEmpty(localAddress.Address2) ? localAddress.Address2 : stdAddress.Address2;
                        stdAddress.City = !string.IsNullOrEmpty(localAddress.City) ? localAddress.City : stdAddress.City;
                        stdAddress.Company = !string.IsNullOrEmpty(localAddress.Company) ? localAddress.Company : stdAddress.Company;
                        stdAddress.Country = (localAddress.Country != null) ? localAddress.Country : stdAddress.Country;
                        stdAddress.CountryId = (localAddress.CountryId.HasValue) ? localAddress.CountryId : stdAddress.CountryId;
                        stdAddress.County = !string.IsNullOrEmpty(localAddress.County) ? localAddress.County : stdAddress.County;
                        stdAddress.PhoneNumber = !string.IsNullOrEmpty(localAddress.PhoneNumber) ? localAddress.PhoneNumber : stdAddress.PhoneNumber;
                        stdAddress.StateProvince = (localAddress.StateProvince != null) ? localAddress.StateProvince : stdAddress.StateProvince;
                        stdAddress.StateProvinceId = (localAddress.StateProvinceId.HasValue) ? localAddress.StateProvinceId : stdAddress.StateProvinceId;
                        stdAddress.ZipPostalCode = !string.IsNullOrEmpty(localAddress.ZipPostalCode) ? localAddress.ZipPostalCode : stdAddress.ZipPostalCode;
                    }
                }
            }
            return stdAddress;
        }

        [HttpGet]
        [Route("/api/getStockAvailableStoreList/{customerId}/{showroomId}")]
        [Route("/api/getStockWiseStoreList/{customerId}/{showroomId}")]
        [ProducesResponseType(typeof(ShoppingCartItemsRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), 422)]
        public IActionResult GetStockWiseStoreList(int customerId, int showroomId)
        {
            var storesRootObject = new WarehouseRootObject();
            var customer = CustomerService.GetCustomerById(customerId);
            var jsonResponse = "";

            if (customer == null)
            {
                storesRootObject.ErrorCode = "2042";
                storesRootObject.ErrorTitle = "InvalidCustomerID";
                storesRootObject.ErrorDesc = "Error";

                jsonResponse = JsonFieldsSerializer.Serialize(storesRootObject, "");
                return new RawJsonActionResult(jsonResponse);

                //var errorsJson = GetJsonErrorObject("2042", "InvalidCustomerID", "Error");
                //return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
            }
            if (customer.ShoppingCartItems == null || customer.ShoppingCartItems.Count <= 0)
            {
                //var errorsJson = GetJsonErrorObject("2045", "CartIsEmpty", "Error");
                //return new ErrorActionResult(errorsJson, HttpStatusCode.OK);

                storesRootObject.ErrorCode = "2045";
                storesRootObject.ErrorTitle = "CartIsEmpty";
                storesRootObject.ErrorDesc = "Error";

                jsonResponse = JsonFieldsSerializer.Serialize(storesRootObject, "");
                return new RawJsonActionResult(jsonResponse);
            }

            //var shoppingCart = customer.ShoppingCartItems
            //                    .Select(c => c.Product.ProductWarehouseInventory
            //                    .Where(pwi => pwi.WarehouseId == showroomId && (pwi.StockQuantity - pwi.ReservedQuantity) >= c.Quantity )).ToList();

            bool found = (customer.ShoppingCartItems != null || customer.ShoppingCartItems.Count > 0) ?
                customer.ShoppingCartItems
                .All(oShoppingCartItem =>
                    oShoppingCartItem.Product.ProductWarehouseInventory
                    .Any(oProductWarehouseInventory =>
                        oProductWarehouseInventory.WarehouseId == showroomId &&
                        oProductWarehouseInventory.StockQuantity - oProductWarehouseInventory.ReservedQuantity >= oShoppingCartItem.Quantity
                    )
                 ) : false;


            if (!found)
            {
                var storeList = _shippingService.GetAllWarehouses().Select(stores => new StoreDetails
                {
                    Id = stores.Id,
                    Name = stores.Name,
                    StoreCode = stores.StoreCode,
                    StoreTimings = stores.StoreTimings,
                    Features = stores.Features,
                    Latitude = stores.Latitude,
                    Longitude = stores.Longitude,
                    Governorate = stores.Governorate,
                    AdminComment = stores.AdminComment,
                    AddressId = stores.AddressId,
                    IsWarehouse = stores.IsWarehouse,
                    StoreAddress = stores.AddressId > 0 ? (_addressService.GetAddressById(stores.AddressId)) : null
                }).Where(stores => !stores.IsWarehouse).ToList();

                var suggestedStores = storeList
                            .Where(oStore =>
                                customer.ShoppingCartItems
                                .All(oShoppingCartItem =>
                                    oShoppingCartItem.Product.ProductWarehouseInventory
                                    .Any(oProductWarehouseInventory =>
                                        oProductWarehouseInventory.WarehouseId == oStore.Id &&
                                        oProductWarehouseInventory.StockQuantity - oProductWarehouseInventory.ReservedQuantity >= oShoppingCartItem.Quantity
                                    )
                                 )
                              );
                ShowroomIDs = suggestedStores.Select(oSuggestedStores => oSuggestedStores.Id).ToList();

                if (ShowroomIDs.Count > 0)
                {
                    storesRootObject.ShowroomIDs = ShowroomIDs;
                    storesRootObject.ErrorCode = "2059";
                    storesRootObject.ErrorTitle = "";
                    storesRootObject.ErrorDesc = "ProductAvailable";
                }
                else
                {
                    storesRootObject.ErrorCode = "2058";
                    storesRootObject.ErrorTitle = "ProductUnavailable";
                    storesRootObject.ErrorDesc = "Error";
                }

                jsonResponse = JsonFieldsSerializer.Serialize(storesRootObject, "");

                return new RawJsonActionResult(jsonResponse);

                // return Json(ShowroomIDs); 
            }

            ShowroomIDs = new List<int> { showroomId };

            var storeRootObject = new WarehouseRootObject()
            {
                ShowroomIDs = ShowroomIDs,
                ErrorCode = "2059",
                ErrorTitle = "",
                ErrorDesc = "ProductAvailable"
            };

            jsonResponse = JsonFieldsSerializer.Serialize(storeRootObject, "");

            return new RawJsonActionResult(jsonResponse);

        }

        [HttpGet]
        [Route("/api/getStockAvailabilityWiseStoreLists/{customerId}/{languageId?}")]
        [ProducesResponseType(typeof(ShoppingCartItemsRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), 422)]
        public IActionResult GetStockAvailabilityWiseStoreLists(int customerId, int? languageId = null)
        {
            GenericJsonResponse genericJsonResponse = new GenericJsonResponse();
            ErrorResponse errorResponse = new ErrorResponse();
            try
            {
                var storesRootObject = new WarehouseRootObject();
                var customer = CustomerService.GetCustomerById(customerId);
                StockwiseStoreList stockwiseStoreList = new StockwiseStoreList();
                int showroomId = 0;

                if (customer == null)
                {
                    errorResponse.ErrorCode = "2042";
                    errorResponse.ErrorTitle = "InvalidCustomerID";
                    errorResponse.ErrorDesc = "Error";
                    genericJsonResponse.Error = errorResponse;
                    genericJsonResponse.Data = null;
                    return Json(genericJsonResponse);

                    //var errorsJson = GetJsonErrorObject("2042", "InvalidCustomerID", "Error");
                    //return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
                }
                if (customer.ShoppingCartItems == null || customer.ShoppingCartItems.Count <= 0)
                {
                    //var errorsJson = GetJsonErrorObject("2045", "CartIsEmpty", "Error");
                    //return new ErrorActionResult(errorsJson, HttpStatusCode.OK);

                    errorResponse.ErrorCode = "2045";
                    errorResponse.ErrorTitle = "CartIsEmpty";
                    errorResponse.ErrorDesc = "Error";
                    genericJsonResponse.Error = errorResponse;
                    genericJsonResponse.Data = null;
                    return Json(genericJsonResponse);
                }

                //var shoppingCart = customer.ShoppingCartItems
                //                    .Select(c => c.Product.ProductWarehouseInventory
                //                    .Where(pwi => pwi.WarehouseId == showroomId && (pwi.StockQuantity - pwi.ReservedQuantity) >= c.Quantity )).ToList();

                bool found = (customer.ShoppingCartItems != null || customer.ShoppingCartItems.Count > 0) ?
                    customer.ShoppingCartItems
                    .All(oShoppingCartItem =>
                        oShoppingCartItem.Product.ProductWarehouseInventory
                        .Any(oProductWarehouseInventory =>
                            oProductWarehouseInventory.WarehouseId == showroomId &&
                            oProductWarehouseInventory.StockQuantity - oProductWarehouseInventory.ReservedQuantity >= oShoppingCartItem.Quantity
                        )
                     ) : false;

                var suggestedStores = new List<StoreDetails>();
                if (!found)
                {
                    var storeList = _shippingService.GetAllWarehouses().Select(stores => new StoreDetails
                    {
                        Id = stores.Id,
                        Name = _localizationService.GetLocalized(stores, x => x.Name, languageId),
                        StoreCode = stores.StoreCode,
                        StoreTimings = stores.StoreTimings,
                        Features = stores.Features,
                        Latitude = stores.Latitude,
                        Longitude = stores.Longitude,
                        Governorate = _localizationService.GetLocalized(stores, x => x.Governorate, languageId),
                        AdminComment = stores.AdminComment,
                        AddressId = stores.AddressId,
                        IsWarehouse = stores.IsWarehouse,
                        StoreAddress = GetLocalizedAddresss(stores, languageId)
                    }).Where(stores => !stores.IsWarehouse).ToList();
                    
                    suggestedStores = storeList
                                .Where(oStore =>
                                    customer.ShoppingCartItems
                                    .All(oShoppingCartItem =>
                                        oShoppingCartItem.Product.ProductWarehouseInventory
                                        .Any(oProductWarehouseInventory =>
                                            oProductWarehouseInventory.WarehouseId == oStore.Id &&
                                            oProductWarehouseInventory.StockQuantity - oProductWarehouseInventory.ReservedQuantity >= oShoppingCartItem.Quantity
                                        )
                                     )
                                  ).ToList();
                    //ShowroomIDs = suggestedStores.Select(oSuggestedStores => oSuggestedStores.Id).ToList();
                    List<StoreDetails> lstStoreNotHavingItem = new List<StoreDetails>();
                    errorResponse.ErrorCode = "2058";
                    errorResponse.ErrorTitle = "Error";
                    errorResponse.ErrorDesc = "ProductUnavailable";
                    if (storeList != null)
                    {
                        lstStoreNotHavingItem = storeList.Except(suggestedStores).ToList();
                    }
                    if (suggestedStores != null && suggestedStores.Count > 0)
                    {
                        //storesRootObject.ShowroomIDs = ShowroomIDs;
                        stockwiseStoreList.fullyAvailableStoreDetails = suggestedStores;
                        errorResponse.ErrorCode = "2059";
                        errorResponse.ErrorTitle = "Success";
                        errorResponse.ErrorDesc = "ProductAvailable";
                    }
                    if (lstStoreNotHavingItem != null && lstStoreNotHavingItem.Count > 0)
                    {
                        stockwiseStoreList.partiallyAvailableStoreDetails = lstStoreNotHavingItem;
                    }
                    //if ((stockwiseStoreList.fullyAvailableStoreDetails == null || stockwiseStoreList.fullyAvailableStoreDetails.Count == 0) && (stockwiseStoreList.partiallyAvailableStoreDetails == null || stockwiseStoreList.partiallyAvailableStoreDetails.Count == 0))
                    //{
                    //    errorResponse.ErrorCode = "2058";
                    //    errorResponse.ErrorTitle = "Error";
                    //    errorResponse.ErrorDesc = "ProductUnavailable";
                    //}
                    //return Json(genericJsonResponse);

                    // return Json(ShowroomIDs); 
                }
                genericJsonResponse.Error = errorResponse;
                genericJsonResponse.Data = stockwiseStoreList;
                return Json(genericJsonResponse);
            }
            catch (Exception ex)
            {
                errorResponse.ErrorCode = "2030";
                errorResponse.ErrorTitle = "Error";
                errorResponse.ErrorDesc = "GenericError";
                genericJsonResponse.Error = errorResponse;
                genericJsonResponse.Data = null;
                return Json(genericJsonResponse);
            }
        }

        [HttpGet]
        [Route("/api/getShowroomListByProductID/{orderId}")]
        [ProducesResponseType(typeof(ShoppingCartItemsRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), 422)]
        public IActionResult GetShowroomListByProductID(int orderId)
        {
            GenericJsonResponse genericJsonResponse = new GenericJsonResponse();
            ErrorResponse errorResponse = new ErrorResponse();
            try
            {
                var storesRootObject = new WarehouseRootObject();
                var order = _orderApiService.GetOrderById(orderId);
                StockwiseStoreList stockwiseStoreList = new StockwiseStoreList();
                int showroomId = 0;
                if (order == null)
                {
                    errorResponse.ErrorCode = "";
                    errorResponse.ErrorTitle = "InvalidOrderID";
                    errorResponse.ErrorDesc = "Error";
                    genericJsonResponse.Error = errorResponse;
                    genericJsonResponse.Data = null;
                    return Json(genericJsonResponse);

                    //var errorsJson = GetJsonErrorObject("2042", "InvalidCustomerID", "Error");
                    //return new ErrorActionResult(errorsJson, HttpStatusCode.OK);
                }
                if (order.OrderItems == null || order.OrderItems.Count <= 0)
                {
                    //var errorsJson = GetJsonErrorObject("2045", "CartIsEmpty", "Error");
                    //return new ErrorActionResult(errorsJson, HttpStatusCode.OK);

                    errorResponse.ErrorCode = "2094";
                    errorResponse.ErrorTitle = "Error";
                    errorResponse.ErrorDesc = "OrderListEmpty";
                    genericJsonResponse.Error = errorResponse;
                    genericJsonResponse.Data = null;
                    return Json(genericJsonResponse);
                }

                //var shoppingCart = customer.ShoppingCartItems
                //                    .Select(c => c.Product.ProductWarehouseInventory
                //                    .Where(pwi => pwi.WarehouseId == showroomId && (pwi.StockQuantity - pwi.ReservedQuantity) >= c.Quantity )).ToList();

                bool found = (order.OrderItems != null || order.OrderItems.Count > 0) ?
                    order.OrderItems
                    .All(oShoppingCartItem =>
                        oShoppingCartItem.Product.ProductWarehouseInventory
                        .Any(oProductWarehouseInventory =>
                            oProductWarehouseInventory.WarehouseId == showroomId &&
                            oProductWarehouseInventory.StockQuantity - oProductWarehouseInventory.ReservedQuantity >= oShoppingCartItem.Quantity
                        )
                     ) : false;

                var suggestedStores = new List<StoreDetails>();
                if (!found)
                {
                    var storeList = _shippingService.GetAllWarehouses().Select(stores => new StoreDetails
                    {
                        Id = stores.Id,
                        Name = stores.Name,
                        StoreCode = stores.StoreCode,
                        StoreTimings = stores.StoreTimings,
                        Features = stores.Features,
                        Latitude = stores.Latitude,
                        Longitude = stores.Longitude,
                        Governorate = stores.Governorate,
                        AdminComment = stores.AdminComment,
                        AddressId = stores.AddressId,
                        StoreAddress = stores.AddressId > 0 ? (_addressService.GetAddressById(stores.AddressId)) : null
                    }).ToList();

                    suggestedStores = storeList
                                .Where(oStore =>
                                    order.OrderItems
                                    .All(oOrderItems =>
                                        oOrderItems.Product.ProductWarehouseInventory
                                        .Any(oProductWarehouseInventory =>
                                            oProductWarehouseInventory.WarehouseId == oStore.Id &&
                                            oProductWarehouseInventory.StockQuantity - oProductWarehouseInventory.ReservedQuantity >= oOrderItems.Quantity
                                        )
                                     )
                                  ).ToList();
                    //ShowroomIDs = suggestedStores.Select(oSuggestedStores => oSuggestedStores.Id).ToList();
                    //List<StoreDetails> lstStoreNotHavingItem = new List<StoreDetails>();
                    errorResponse.ErrorCode = "2058";
                    errorResponse.ErrorTitle = "Error";
                    errorResponse.ErrorDesc = "ProductUnavailable";
                    //if (storeList != null)
                    //{
                    //    lstStoreNotHavingItem = storeList.Except(suggestedStores).ToList();
                    //}
                    if (suggestedStores != null && suggestedStores.Count > 0)
                    {
                        //storesRootObject.ShowroomIDs = ShowroomIDs;
                        stockwiseStoreList.fullyAvailableStoreDetails = suggestedStores;
                        errorResponse.ErrorCode = "2059";
                        errorResponse.ErrorTitle = "Success";
                        errorResponse.ErrorDesc = "ProductAvailable";
                    }
                    //if (lstStoreNotHavingItem != null && lstStoreNotHavingItem.Count > 0)
                    //{
                    //    stockwiseStoreList.partiallyAvailableStoreDetails = lstStoreNotHavingItem;
                    //}
                    //if ((stockwiseStoreList.fullyAvailableStoreDetails == null || stockwiseStoreList.fullyAvailableStoreDetails.Count == 0) && (stockwiseStoreList.partiallyAvailableStoreDetails == null || stockwiseStoreList.partiallyAvailableStoreDetails.Count == 0))
                    //{
                    //    errorResponse.ErrorCode = "2058";
                    //    errorResponse.ErrorTitle = "Error";
                    //    errorResponse.ErrorDesc = "ProductUnavailable";
                    //}
                    //return Json(genericJsonResponse);

                    // return Json(ShowroomIDs); 
                }
                genericJsonResponse.Error = errorResponse;
                genericJsonResponse.Data = stockwiseStoreList;
                return Json(genericJsonResponse);
            }
            catch (Exception ex)
            {
                errorResponse.ErrorCode = "2030";
                errorResponse.ErrorTitle = "Error";
                errorResponse.ErrorDesc = "GenericError";
                genericJsonResponse.Error = errorResponse;
                genericJsonResponse.Data = null;
                return Json(genericJsonResponse);
            }
        }

        [HttpGet]
        [Route("/api/checkAvailability/{customerId}/{showroomId}")]
        [ProducesResponseType(typeof(ShoppingCartItemsRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), 422)]
        public IActionResult checkAvailability(int customerId, int showroomId)
        {
            GenericJsonResponse genericJsonResponse = new GenericJsonResponse();

            return Json(genericJsonResponse);
        }

        /// <summary>
        /// Returns a 4 digit random number as string
        /// </summary>
        /// <returns></returns>
        public string GenerateTrackId()
        {
            const string secret = "051220180140IND";
            var totp = new PureOtp.Totp(System.Text.Encoding.UTF8.GetBytes(secret), totpSize: 4, step: 1);
            var TrackId = totp.ComputeTotp();
            return TrackId.ToString();
        }
    }
}