using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
//using Microsoft.Extensions.Logging;
using Nop.Web.Models.Payments;
using Nop.Plugin.Api.Models;
using Microsoft.Extensions.Options;
using Nop.Core.Gateway;
using Nop.Core.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Plugin.Api.Utils;
using Microsoft.AspNetCore.WebUtilities;
using System.Text;
using System.Runtime.InteropServices;
using Nop.Plugin.Api.Constants;
using Nop.Services.Orders;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Plugin.Api.Services;
using Nop.Plugin.Api.Factories;
using Nop.Services.Logging;
using Nop.Core.Domain.Logging;
using Nop.Services.Messages;
using System.Xml;
using Microsoft.AspNetCore.Hosting;

namespace Nop.Plugin.Api.Controllers

{
    /// <summary>
    /// Controller responsible to centralize the process and result methods
    /// </summary>
    public class PaymentApiController : BaseController
    {
        private readonly IOrderService _orderService;
        private readonly ILogger _logger;
        private readonly IPaymentApiService _paymentApiService;
        private readonly IFactory<MasterCardPaymentTransaction> _MasterCardPaymentTransactionFactory;
        private readonly IFactory<KNETPaymentTransaction> _KNETPaymentTransactionFactory;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IHostingEnvironment _hostingEnvironment;

        public PaymentApiController(IOptions<GatewayApiConfig> gatewayApiConfig,
                                    GatewayApiClient gatewayApiClient,
                                    NVPApiClient nvpApiClient,
                                    IOrderService orderService,
                                    ILogger ologger,
                                    IFactory<MasterCardPaymentTransaction> oMasterCardPaymentTransactionFactory,
                                    IFactory<KNETPaymentTransaction> oKNETPaymentTransactionFactory,
                                    IPaymentApiService paymentApiService,
                                    IWorkflowMessageService workflowMessageService,
                                    IHostingEnvironment hostingEnvironment,
                                    Microsoft.Extensions.Logging.ILogger<PaymentApiController> logger
                                ) : base(gatewayApiConfig, gatewayApiClient, nvpApiClient, logger)
        {
            _orderService = orderService;
            _logger = ologger;
            _paymentApiService = paymentApiService;
            _KNETPaymentTransactionFactory = oKNETPaymentTransactionFactory;
            _MasterCardPaymentTransactionFactory = oMasterCardPaymentTransactionFactory;
            _workflowMessageService = workflowMessageService;
            _hostingEnvironment = hostingEnvironment;
        }


        /// <summary>
        /// This method receives the callback from the Hosted Checkout redirect. It looks up the order using the RETRIEVE_ORDER operation and
        /// displays either the receipt or an error page.
        /// </summary>
        /// <param name="orderId">needed to retrieve order</param>
        /// <param name="result">Result of Hosted Checkout operation (success or error) - sent from hostedCheckout.html complete() callback</param>
        /// <returns>IActionResult for hosted checkout receipt page or error page</returns>
        [HttpGet("PaymentGatewayResponse/{result}/{orderId}"), HttpGet("PaymentGatewayResponse/{result}/{orderId}/{sessionId}"), HttpGet("hostedCheckout/{orderId}/{result}"), HttpGet("hostedCheckout/{orderId}/{result}/{sessionId}")]
        public IActionResult HostedCheckoutReceipt([FromRoute(Name = "orderId")] string orderId, [FromRoute(Name = "result")] string result, [FromRoute(Name = "sessionId")] string sessionId)
        {
            bool IsSuccess = false;
            //_logger.Information($"PaymentApiController HostedCheckoutReceipt action orderId {orderId} result {result} sessionId {sessionId}");
            _logger.InsertLog(LogLevel.Information, $"PaymentApiController HostedCheckoutReceipt action orderId {orderId} result {result} sessionId {sessionId}");

            if (result == "SUCCESS")
            {
                GatewayApiRequest gatewayApiRequest = new GatewayApiRequest(GatewayApiConfig)
                {
                    ApiOperation = "RETRIEVE_ORDER",
                    OrderId = orderId,
                    ApiMethod = GatewayApiClient.GET
                };

                gatewayApiRequest.buildOrderUrl();


                string response = GatewayApiClient.SendTransaction(gatewayApiRequest);

                _logger.InsertLog(LogLevel.Information, $"Hosted checkout retrieve order response {response}");

                //parse response
                TransactionResponseModel transactionResponseModel = null;
                try
                {
                    _logger.InsertLog(LogLevel.Information, $"Creating TransactionResponseModel..");
                    transactionResponseModel = TransactionResponseModel.toTransactionResponseModel(response);

                    _logger.InsertLog(LogLevel.Information, $"Start: SaveMasterCardResponse()");
                    IsSuccess = SaveCreditCardResponse(response, transactionResponseModel);

                    if (IsSuccess)
                    {
                        _logger.InsertLog(LogLevel.Information, $"End: SUCCESS in SaveMasterCardResponse()");
                    }
                    else
                    {
                        _logger.InsertLog(LogLevel.Information, $"End: FAILURE in SaveMasterCardResponse()");
                    }                    

                }
                catch (Exception e)
                {
                    _logger.InsertLog(LogLevel.Error, $"Hosted Checkout Receipt error : {JsonConvert.SerializeObject(e)}");

                    return View(ViewNames.Error, new ErrorViewModel
                    {
                        RequestId = getRequestId(),
                        Cause = e.InnerException != null ? e.InnerException.StackTrace : e.StackTrace,
                        Message = e.Message
                    });
                }

                return View(ViewList.GetValueOrDefault("Receipt"), transactionResponseModel);
            }
            else
            {
                _logger.InsertLog(LogLevel.Error, $"The payment was unsuccessful {result}");
                return View(ViewNames.Error, new ErrorViewModel
                {
                    RequestId = getRequestId(),
                    Cause = "Payment was unsuccessful",
                    Message = "There was a problem completing your transaction."
                });
            }
        }


        public bool SaveCreditCardResponse(string response, TransactionResponseModel transactionResponseModel)
        {
            bool IsResultSaved = false;
            bool IsResponseParsed = false;
            string PaymentID = null, TrackID = null, MasterCardTransactionId = null, Receipt = null, Result = null, Status = null, AcquirerCode = null, AcquirerMessage = null, GatewayCode = null, AuthorizationCode = null, ErrorCode = null, ErrorText = null, TransactionCreationTime = null, CreatedOnUtc = null;
            string OrderId = null, OrderCurrency = null, OrderDescription = null;
            Double? OrderAmount = null;
            string strResp = string.Empty;
            int ResultStatus = -1;
            Order order = null;
            try
            {
                _logger.InsertLog(LogLevel.Information, "Credit Card Payment Response received: " + response);

                if (String.IsNullOrEmpty(ErrorText)) //Error text is not found hence go for checking result.
                {

                    switch (transactionResponseModel.Status)
                    {
                        case MasterCardTransactionResult.CAPTURED:
                        case MasterCardTransactionResult.NOT_CAPTURED:
                        case MasterCardTransactionResult.DENIED_BY_RISK:
                        case MasterCardTransactionResult.HOST_TIMEOUT:

                            JObject jObject = JObject.Parse(response);
                            var transactionList = jObject["transaction"];

                            GatewayCode = transactionList[0]["response"]["gatewayCode"].ToObject<String>();
                            OrderId = transactionList[0]["order"]["id"].ToObject<String>();
                            OrderAmount = Convert.ToDouble(transactionList[0]["order"]["amount"].ToObject<String>());
                            OrderCurrency = transactionList[0]["order"]["currency"].ToObject<String>();
                            OrderDescription = transactionList[0]["order"]["description"].ToObject<String>();

                            Receipt = transactionList[0]["transaction"]["receipt"].ToObject<String>();
                            PaymentID = transactionList[0]["transaction"]["receipt"].ToObject<String>();
                            MasterCardTransactionId = transactionList[0]["transaction"]["acquirer"]["transactionId"].ToObject<String>();
                            TrackID = OrderDescription.Replace(Configurations.ORDER_DESCRIPTION_TEXT, "");

                            TransactionCreationTime = transactionList[0]["order"]["creationTime"].ToObject<String>();
                            Status = transactionList[0]["order"]["status"].ToObject<String>(); // CAPTURED/NOT CAPTURED
                            Result = transactionList[0]["result"].ToObject<String>(); // SUCCESS/FAILURE
                            AcquirerCode = transactionList[0]["response"]["acquirerCode"].ToObject<String>();
                            AcquirerMessage = transactionList[0]["response"]["acquirerMessage"].ToObject<String>();

                            AuthorizationCode = Result == "SUCCESS" ? transactionList[0]["transaction"]["authorizationCode"].ToObject<String>() : "";

                            ErrorCode = String.Empty;
                            ErrorText = String.Empty;

                            _logger.Information("Credit Card response parsed. Details:\n" +
                                                "\nGatewayCode\t:" + GatewayCode +
                                                "\nOrderId\t:" + OrderId +
                                                "\nOrderAmount\t:" + OrderAmount.Value +
                                                "\nOrderCurrency\t:" + OrderCurrency +
                                                "\nOrderDescription\t:" + OrderDescription + 
                                                "\nReceipt\t:" + Receipt +
                                                "\nMasterCardTransactionId\t:" + MasterCardTransactionId +
                                                "\nTransactionCreationTime\t:" + TransactionCreationTime +
                                                "\nStatus\t:" + Status  +
                                                "\nResult\t:" + Result +
                                                "\nAcquirerCode\t:" + AcquirerCode +
                                                "\nAcquirerMessage\t:" + AcquirerMessage +
                                                "\nAuthorizationCode\t:" + AuthorizationCode
                                               );

                            IsResponseParsed = true;
                            break;

                        case MasterCardTransactionResult.CANCELED:
                            _logger.Information("Credit Card transaction is cancelled.");                             
                            break;

                        default:
                            _logger.Error("Error occurred in Credit Card payment response parsing. Unidentified result returned from Credit Card as: " + Status);
                            break;
                    }

                    if (IsResponseParsed)
                    {
                        var oPaymentTransaction = _paymentApiService.GetPaymentTransactionByTrackId(TrackID);

                        order = oPaymentTransaction.Order;

                        var _masterCardPaymentTransaction = _MasterCardPaymentTransactionFactory.Initialize();
                          
                        _masterCardPaymentTransaction.GatewayCode = GatewayCode;
                        _masterCardPaymentTransaction.OrderId = OrderId;
                        _masterCardPaymentTransaction.OrderAmount = OrderAmount;
                        _masterCardPaymentTransaction.OrderCurrency = OrderCurrency;
                        _masterCardPaymentTransaction.OrderDescription = OrderDescription;
                        _masterCardPaymentTransaction.Receipt = Receipt; 
                        _masterCardPaymentTransaction.TransactionId = MasterCardTransactionId; 
                        _masterCardPaymentTransaction.TransactionCreationTime = Convert.ToDateTime(TransactionCreationTime);
                        _masterCardPaymentTransaction.Status = Status;
                        _masterCardPaymentTransaction.Result = Result;
                        _masterCardPaymentTransaction.AcquirerCode = AcquirerCode;
                        _masterCardPaymentTransaction.AcquirerMessage = AcquirerMessage;
                        _masterCardPaymentTransaction.ErrorCode = ErrorCode;
                        _masterCardPaymentTransaction.ErrorText = ErrorText;

                        _masterCardPaymentTransaction.PaymentTransactionId = oPaymentTransaction.Id;
                        _masterCardPaymentTransaction.PaymentTransaction = oPaymentTransaction;
                        _masterCardPaymentTransaction.PaymentTransaction.PaymentTransactionStatus = PaymentTransactionStatus.SUCCESS;

                        _masterCardPaymentTransaction.PaymentTransaction.Order.UpdatedOnUtc = DateTime.UtcNow;
                        _masterCardPaymentTransaction.PaymentTransaction.Order.PaymentStatus = Status.ToUpper().Equals(MasterCardTransactionResult.CAPTURED) ? PaymentStatus.Paid : PaymentStatus.Failed;
                        _masterCardPaymentTransaction.PaymentTransaction.Order.OrderStatus = Status.ToUpper().Equals(MasterCardTransactionResult.CAPTURED) ? OrderStatus.Processing : OrderStatus.Pending;
                        _masterCardPaymentTransaction.PaymentTransaction.Order.CaptureTransactionId = MasterCardTransactionId;
                        _masterCardPaymentTransaction.PaymentTransaction.Order.CaptureTransactionResult = Status;
                        _masterCardPaymentTransaction.PaymentTransaction.Order.PaidDateUtc = Convert.ToDateTime(TransactionCreationTime);
                        _masterCardPaymentTransaction.PaymentTransaction.Order.PaymentId = PaymentID;

                        _paymentApiService.InsertMasterCardPaymentTransaction(_masterCardPaymentTransaction);

                        order.UpdatedOnUtc = DateTime.UtcNow;
                        order.PaymentStatus = Status.ToUpper().Equals(MasterCardTransactionResult.CAPTURED) ? PaymentStatus.Paid : PaymentStatus.Failed;
                        order.OrderStatus = Status.ToUpper().Equals(MasterCardTransactionResult.CAPTURED) ? OrderStatus.Processing : OrderStatus.Pending;
                        order.CaptureTransactionId = MasterCardTransactionId;
                        order.CaptureTransactionResult = Status;
                        order.PaidDateUtc = Convert.ToDateTime(TransactionCreationTime);
                        order.PaymentId = PaymentID;

                        _orderService.UpdateOrder(order);

                        IsResultSaved = true;

                        _logger.Information("Successfully saved Credit Card response into DB.");


                        switch (Status)
                        {
                            case MasterCardTransactionResult.CAPTURED:
                                strResp = "Your transaction for amount " + OrderAmount.Value + " is successful. \nPlease note your Transaction ID: " + MasterCardTransactionId + " and Receipt ID: " + Receipt  + ".";
                                ResultStatus = (int)MasterCardTransactionResultEnum.CAPTURED;
                                break;

                            case MasterCardTransactionResult.NOT_CAPTURED:
                                strResp = "Your transaction is failed.";
                                ResultStatus = (int)MasterCardTransactionResultEnum.NOT_CAPTURED;
                                break;

                            case MasterCardTransactionResult.DENIED_BY_RISK:
                                strResp = "Transaction denied.";
                                ResultStatus = (int)MasterCardTransactionResultEnum.DENIED_BY_RISK;
                                break;

                            case MasterCardTransactionResult.HOST_TIMEOUT:
                                strResp = "Transaction timed out.";
                                ResultStatus = (int)MasterCardTransactionResultEnum.TIME_OUT;
                                break;

                            case MasterCardTransactionResult.CANCELED:
                                strResp = "Transaction canceled.";
                                ResultStatus = (int)MasterCardTransactionResultEnum.CANCELED;
                                break;

                            default:
                                strResp = "Sorry! Something went wrong!";
                                ResultStatus = (int)MasterCardTransactionResultEnum.DEFAULT;
                                break;
                        }
                        _logger.Information("Transaction Result: "+ ResultStatus + " | Message: " + strResp);
                    }
                    else
                    {
                        _logger.Error("Error occurred while parsing response from MasterCard."); // Details:\nTranData\t: " + trandata + "\nError\t:" + Error + "\nErrorText\t:" + ErrorText);                        
                    }
                }
                else
                {
                    _logger.Error("Error occurred in Credit Card transaction: "); // + ErrorText);
                    return false;
                }


            }
            catch (Exception ex)
            { 
                try
                {
                    if (IsResultSaved)
                    {
                        _logger.Information("Successfully saved Credit Card response into DB, but an error occurred after record saved.: " + ex.Message, ex);
                    }
                    else
                    {
                        _logger.Error("Error occurred in processing success response from MasterCard.: " + ex.Message, ex); // Details:\nTranData\t: " + trandata + "\nError\t:" + Error + "\nErrorText\t:" + ErrorText, ex);
                    }
                }
                catch (Exception innerEx)
                {
                    _logger.Error("Error occurred in processing success response from MasterCard.: " + innerEx.Message, innerEx); //  Following are the details:\nTranData\t: " + trandata + "\nError\t:" + Error + "\nErrorText\t:" + ErrorText, innerEx);
                }
                return false;
            }

            if (Status == MasterCardTransactionResult.CAPTURED)
            {
                try
                {
                    var orderPlacedCustomerNotificationQueuedEmailIds = _workflowMessageService
                            .SendOrderPlacedCustomerNotificationAlmailem(order, order.CustomerLanguageId);
                    if (orderPlacedCustomerNotificationQueuedEmailIds.Any())
                        AddOrderNote(order, $"\"Order placed\" email (to customer) has been queued. Queued email identifiers: {string.Join(", ", orderPlacedCustomerNotificationQueuedEmailIds)}.");

                    _logger.Information("Order placed email queued for OrderID\t: " + order.CustomOrderNumber);
                }
                catch (Exception ex)
                {
                    _logger.Error("Error occurred while queuing email for OrderID\t:" + order.CustomOrderNumber, ex);
                }
                //return true;
                //return Redirect(ReturnBaseURL() + "PaymentGatewayResponse/Success");
            }
            return true;
            //else
            //return false;
            // return Redirect(ReturnBaseURL() + "PaymentGatewayResponse/Error");
        }

        /// <summary>
        /// Add order note
        /// </summary>
        /// <param name="order">Order</param>
        /// <param name="note">Note text</param>
        protected virtual void AddOrderNote(Order order, string note)
        {
            order.OrderNotes.Add(new OrderNote
            {
                Note = note,
                DisplayToCustomer = false,
                CreatedOnUtc = DateTime.UtcNow
            });

            _orderService.UpdateOrder(order);
        }

        public string ReturnBaseURL()
        {
            XmlDocument xmlDocument = new XmlDocument();
            string contentRootPath = _hostingEnvironment.ContentRootPath;
            string xmlPaymentGatewayConfigurationFilePath = contentRootPath + "/App_Data/PaymentGatewayConfiguration.xml"; 
            xmlDocument.Load(xmlPaymentGatewayConfigurationFilePath);
            XmlElement root = xmlDocument.DocumentElement;
            XmlNodeList nodes = root.ChildNodes;

            string BaseURL = string.Empty;
            string GatewayName;
            foreach (XmlNode node in nodes)
            {
                GatewayName = node["GatewayName"].InnerText;
                if (GatewayName.ToUpper() == "CREDIT CARD" && node["IsActive"].InnerText.ToUpper() == "Y")
                {
                    BaseURL = node["BaseURL"].InnerText;
                }
            }

            return BaseURL;
        }

        /// <summary>
        /// This method handles the response from the CHECK_3DS_ENROLLMENT operation. If the card is enrolled, the response includes the HTML for the issuer's authentication form, to be injected into secureIdPayerAuthenticationForm.html.
        /// Otherwise, it displays an error.
        /// <summary> 
        /// <param name="gatewayApiRequest">needed to retrieve various data to complete API operation</param>
        /// <returns>IActionResult - displays issuer authentication form or error page</returns>
        [HttpPost("check3dsEnrollment")]
        public IActionResult Check3dsEnrollment([FromBody] GatewayApiRequest gatewayApiRequest)
        {
            _logger.Information($"PaymentApiController Check3dsEnrollment action SessionId {JsonConvert.SerializeObject(gatewayApiRequest)} gatewayApiRequest.SessionId {gatewayApiRequest.SessionId}");

            gatewayApiRequest.GatewayApiConfig = GatewayApiConfig;

            // Retrieve session
            gatewayApiRequest.buildSessionRequestUrl();
            gatewayApiRequest.ApiMethod = GatewayApiClient.GET;

            _logger.Information($"gatewayApiRequest {JsonConvert.SerializeObject(gatewayApiRequest)}");

            string response = GatewayApiClient.SendTransaction(gatewayApiRequest);

            _logger.Information("Get session response -- " + response);

            //validate transaction response
            if (JsonHelper.isErrorMessage(response))
            {
                return View("Error", ErrorViewModel.toErrorViewModel(getRequestId(), response));
            }


            CheckoutSessionModel checkoutSessionModel = CheckoutSessionModel.toCheckoutSessionModel(response);

            _logger.Information($@"checkoutSession.Id {checkoutSessionModel.Id} gatewayApiRequest.SessionId {gatewayApiRequest.SessionId}");

            string secureId = IdUtils.generateSampleId();
            gatewayApiRequest.SecureId = secureId;

            gatewayApiRequest.buildSecureIdRequestUrl();
            gatewayApiRequest.buildPayload();
            gatewayApiRequest.ApiMethod = GatewayApiClient.PUT;


            //add values in session to use it after processing response
            setSessionValue("secureId", secureId);
            setSessionValue("sessionId", checkoutSessionModel.Id);
            setSessionValue("amount", gatewayApiRequest.OrderAmount);
            setSessionValue("currency", gatewayApiRequest.OrderCurrency);
            setSessionValue("orderId", gatewayApiRequest.OrderId);
            setSessionValue("transactionId", gatewayApiRequest.TransactionId);

            response = GatewayApiClient.SendTransaction(gatewayApiRequest);

            _logger.Information($"SecureId response {response}");

            //validate transaction response
            if (JsonHelper.isErrorMessage(response))
            {
                return View("Error", ErrorViewModel.toErrorViewModel(getRequestId(), response));
            }

            //parse response
            SecureIdEnrollmentResponseModel model = null;
            try
            {
                model = SecureIdEnrollmentResponseModel.toSecureIdEnrollmentResponseModel(Request, response);
            }
            catch (Exception e)
            {
                _logger.Error($"Check3dsEnrollment error : {JsonConvert.SerializeObject(e)}");

                return View("Error", new ErrorViewModel
                {
                    RequestId = getRequestId(),
                    Cause = e.InnerException != null ? e.InnerException.StackTrace : e.StackTrace,
                    Message = e.Message
                });
            }


            //check process result 
            _logger.Information($"SecureIdEnrollmentResponseModel {JsonConvert.SerializeObject(model)}");

            if (model.Status != "CARD_ENROLLED")
            {
                _logger.Error($"Check3dsEnrollment was unsuccessful, status {model.Status}");
                return View("Error", new ErrorViewModel
                {
                    RequestId = getRequestId(),
                    Cause = model.Status,
                    Message = "Card not enrolled in 3DS."
                });
            }

            return View(ViewList.GetValueOrDefault("SecureIdPayerAuthenticationForm"), model);
        }

        /// <summary>
        /// This method handles to capture the form['PaRes'] response and send and AUTORIZE call using the information got from the 3DS response.
        /// <summary> 
        /// <param name="PaRes">Needed to retrieve token id to complete API operation</param>
        /// <returns>IActionResult - displays the result or error page</returns>
        [HttpPost("process3ds")]
        public IActionResult Process3dsAuthenticationResult()
        {
            String responseView = ViewList.GetValueOrDefault("ApiResponse");

            //cons
            String AUTHORIZE = "AUTHORIZE";
            String PROCESS_ACS_RESULT = "PROCESS_ACS_RESULT";

            //get secure / session information from session
            String secureId = getSessionValueAsString("secureId");
            String sessionId = getSessionValueAsString("sessionId");
            String amount = getSessionValueAsString("amount");
            String currency = getSessionValueAsString("currency");
            String orderId = getSessionValueAsString("orderId");
            String transactionId = getSessionValueAsString("transactionId");

            //remove values from session
            removeSessionValue("secureId");
            removeSessionValue("sessionId");
            removeSessionValue("amount");
            removeSessionValue("currency");
            removeSessionValue("orderId");
            removeSessionValue("transactionId");

            // Retrieve Payment Authentication Response (PaRes) from request
            String paRes = this.Request.Form["PaRes"];

            //init aux variables
            String response = null;
            GatewayApiRequest gatewayApiRequest = null;


            // Process Access Control Server (ACS) result
            gatewayApiRequest = new GatewayApiRequest();

            gatewayApiRequest.GatewayApiConfig = GatewayApiConfig;
            gatewayApiRequest.ApiMethod = GatewayApiClient.POST;
            gatewayApiRequest.PaymentAuthResponse = paRes;
            gatewayApiRequest.SecureId = secureId;
            gatewayApiRequest.ApiOperation = PROCESS_ACS_RESULT;
            gatewayApiRequest.buildPayload();
            gatewayApiRequest.buildSecureIdRequestUrl();

            response = GatewayApiClient.SendTransaction(gatewayApiRequest);

            //validate transaction response
            if (JsonHelper.isErrorMessage(response))
            {
                return View("Error", ErrorViewModel.toErrorViewModel(getRequestId(), response));
            }

            //parse response to domain
            SecureIdEnrollmentResponseModel model = null;
            try
            {
                model = SecureIdEnrollmentResponseModel.toSecureIdEnrollmentResponseModel(Request, response);
                _logger.Information($"SecureIdEnrollmentResponseModel {JsonConvert.SerializeObject(model)}");

            }
            catch (Exception e)
            {
                _logger.Error($"Process3dsAuthenticationResult error : {JsonConvert.SerializeObject(e)}");

                return View("Error", new ErrorViewModel
                {
                    RequestId = getRequestId(),
                    Cause = e.InnerException != null ? e.InnerException.StackTrace : e.StackTrace,
                    Message = e.Message
                });
            }


            //create 'authorize' API request in case of SUCCESS response
            if (model.Status != null && "AUTHENTICATION_FAILED" != model.Status)
            {

                //build authorize request
                gatewayApiRequest = new GatewayApiRequest();
                gatewayApiRequest.GatewayApiConfig = GatewayApiConfig;
                gatewayApiRequest.ApiMethod = GatewayApiClient.PUT;
                gatewayApiRequest.ApiOperation = AUTHORIZE;

                gatewayApiRequest.SessionId = sessionId;
                gatewayApiRequest.SecureId3D = secureId;
                gatewayApiRequest.OrderCurrency = currency;
                gatewayApiRequest.OrderAmount = amount;
                gatewayApiRequest.TransactionId = transactionId;
                gatewayApiRequest.OrderId = orderId;

                gatewayApiRequest.buildPayload();
                gatewayApiRequest.buildRequestUrl();

                //call api
                response = GatewayApiClient.SendTransaction(gatewayApiRequest);

                //build response view
                this.buildViewData(gatewayApiRequest, response);


            }
            else
            {
                //return error view 
                return View("Error", new ErrorViewModel
                {
                    RequestId = getRequestId(),
                    Cause = model.Status,
                    Message = "3DS Authentication failed."
                });
            }

            return View(responseView);
        }


        /// <summary>
        /// This method processes the API request for server-to-server operations. These are operations that would not commonly be invoked via a user interacting with the browser, but a system event (CAPTURE, REFUND, VOID).
        /// <summary>
        /// <param name="gatewayApiRequest">contains info on how to construct API call</param>
        /// <returns>IActionResult for api response page or error page</returns>
        [HttpPost("process"), HttpGet("process") ]
        public IActionResult Process(GatewayApiRequest gatewayApiRequest)
        {
            _logger.Information($"PaymentApiController Process action gatewayApiRequest {JsonConvert.SerializeObject(gatewayApiRequest)}");

            #region Merchant gateway details
            GatewayApiConfig.UseProxy = false;
            GatewayApiConfig.UseSsl = false;
            GatewayApiConfig.MerchantId = "900120901";
            GatewayApiConfig.Username = "merchant.900120901";
            GatewayApiConfig.Password = "bdc30713a6f4f5d3c00416809a50ac52";
            //GatewayApiConfig.Password = "d0c67f0263710651470dc35b14315b42";
            GatewayApiConfig.Currency = "KWD";
            GatewayApiConfig.GatewayUrl = "https://test-nbkpayment.mtf.gateway.mastercard.com";
            GatewayApiConfig.Version = "53";
            #endregion

            gatewayApiRequest.ApiMethod = "PUT";
            gatewayApiRequest.ApiOperation = "PAY"; //PAY is used to indicate we have to do some payment operations.

            #region order details
            gatewayApiRequest.OrderId = "2019_234";
            gatewayApiRequest.TransactionId = "012345678912";
            gatewayApiRequest.OrderAmount = "300";
            gatewayApiRequest.OrderCurrency = "KWD";
            gatewayApiRequest.OrderDescription = "This is a testing order.";
            #endregion

            gatewayApiRequest.CorrelationId = "MyCorrelationId";

            #region test card details - taken from mastercard only
            gatewayApiRequest.CardNumber = "5111111111111118";
            gatewayApiRequest.ExpiryMonth = "05";
            gatewayApiRequest.ExpiryYear = "21";
            gatewayApiRequest.SecurityCode = "100";
            gatewayApiRequest.SourceType = "CARD"; //[ACH, ALIPAY, CARD, EBT_CARD, GIFT_CARD, SCHEME_TOKEN]
            #endregion


            gatewayApiRequest.GatewayApiConfig = GatewayApiConfig;

            //retrieve order doesnt require transaction information on the url
            if ("RETRIEVE_ORDER" == gatewayApiRequest.ApiOperation)
            {
                gatewayApiRequest.buildOrderUrl();
            }
            else
            {
                gatewayApiRequest.buildRequestUrl();
            }

            gatewayApiRequest.buildPayload();

            string response = GatewayApiClient.SendTransaction(gatewayApiRequest);

            buildViewData(gatewayApiRequest, response);

            return View(ViewList.GetValueOrDefault("ApiResponse"));
        }

        /// <summary>
        /// This method calls the INTIATE_BROWSER_PAYMENT operation, which returns a URL to the provider's website. The user is redirected to this URL, where the purchase is completed.
        /// </summary>
        /// <param name="gatewayApiRequest">contains info on how to construct API call</param>
        /// <returns>IActionResult for api response page or error page</returns>
        [HttpPost("processHostedSession")]
        public IActionResult ProcessHostedSession(GatewayApiRequest gatewayApiRequest)
        {
            _logger.Information($"PaymentApiController ProcessHostedSession action gatewayApiRequest {JsonConvert.SerializeObject(gatewayApiRequest)}");

            #region Merchant gateway details
            GatewayApiConfig.UseProxy = false;
            GatewayApiConfig.UseSsl = false;
            GatewayApiConfig.MerchantId = "900120901";
            GatewayApiConfig.Username = "merchant.900120901";
            GatewayApiConfig.Password = "bdc30713a6f4f5d3c00416809a50ac52";
            //GatewayApiConfig.Password = "d0c67f0263710651470dc35b14315b42";
            GatewayApiConfig.Currency = "KWD";
            GatewayApiConfig.GatewayUrl = "https://test-nbkpayment.mtf.gateway.mastercard.com";
            GatewayApiConfig.Version = "53";
            #endregion

            gatewayApiRequest.ApiMethod = "POST";
            gatewayApiRequest.ApiOperation = "CREATE_CHECKOUT_SESSION"; //PAY is used to indicate we have to do some payment operations.
            gatewayApiRequest.interaction = "PURCHASE"; // "AUTHORIZE"; //PURCHASE

            #region order details
            gatewayApiRequest.OrderId = "2019_234";
            gatewayApiRequest.TransactionId = "01234567890";
            gatewayApiRequest.OrderAmount = "450";
            gatewayApiRequest.OrderCurrency = "KWD";
            gatewayApiRequest.OrderDescription = "This is a testing order.";
            #endregion

            gatewayApiRequest.CorrelationId = "MyCorrelationId";

            gatewayApiRequest.GatewayApiConfig = GatewayApiConfig;
            gatewayApiRequest.buildRequesSessiontUrl();
            gatewayApiRequest.buildPayload();

            string response = GatewayApiClient.SendTransaction(gatewayApiRequest);

            buildViewData(gatewayApiRequest, response);

            return View(ViewList.GetValueOrDefault("ApiResponse"));
        }

        [HttpPost("tokenize")]
        public IActionResult tokenize([FromBody] GatewayApiRequest gatewayApiRequest)
        {
            _logger.Information($"PaymentApiController ProcessHostedSession action gatewayApiRequest {JsonConvert.SerializeObject(gatewayApiRequest)}");

            //update session with order details
            GatewayApiRequest gatewayUpdateSessionRequest = new GatewayApiRequest(GatewayApiConfig);
            gatewayUpdateSessionRequest.ApiMethod = GatewayApiClient.PUT;

            //update the url appending session id
            gatewayUpdateSessionRequest.buildSessionRequestUrl(gatewayApiRequest.SessionId);

            gatewayUpdateSessionRequest.OrderId = gatewayApiRequest.OrderId;
            gatewayUpdateSessionRequest.OrderCurrency = gatewayApiRequest.OrderCurrency;
            gatewayUpdateSessionRequest.OrderAmount = gatewayApiRequest.OrderAmount;

            //build payload with order info
            gatewayUpdateSessionRequest.buildPayload();

            String response = GatewayApiClient.SendTransaction(gatewayUpdateSessionRequest);

            _logger.Information($"Tokenize updated session {response}");

            //validate transaction response
            if (JsonHelper.isErrorMessage(response))
            {
                return View("Error", ErrorViewModel.toErrorViewModel(getRequestId(), response));
            }

            //generate token
            GatewayApiRequest gatewayGenerateTokenRequest = new GatewayApiRequest(GatewayApiConfig);
            gatewayGenerateTokenRequest.SessionId = gatewayApiRequest.SessionId;
            gatewayGenerateTokenRequest.ApiMethod = GatewayApiClient.POST;
            gatewayGenerateTokenRequest.buildPayload();
            gatewayGenerateTokenRequest.buildTokenUrl();

            response = GatewayApiClient.SendTransaction(gatewayGenerateTokenRequest);

                //validate token response
                if (JsonHelper.isErrorMessage(response))
                {
                    return View("Error", ErrorViewModel.toErrorViewModel(getRequestId(), response));
                }

            //convert json to model
            TokenResponse tokenResponse = TokenResponse.ToTokenResponse(response);

            //payment with token
            GatewayApiRequest gatewayGeneratePaymentRequest = new GatewayApiRequest(GatewayApiConfig);
            gatewayGeneratePaymentRequest.ApiOperation = "PAY";
            gatewayGeneratePaymentRequest.ApiMethod = GatewayApiClient.PUT;

            gatewayGeneratePaymentRequest.Token = tokenResponse.Token;
            gatewayGeneratePaymentRequest.SessionId = gatewayApiRequest.SessionId;
            gatewayGeneratePaymentRequest.OrderId = gatewayApiRequest.OrderId;
            gatewayGeneratePaymentRequest.TransactionId = gatewayApiRequest.TransactionId;

            gatewayGeneratePaymentRequest.buildPayload();
            gatewayGeneratePaymentRequest.buildRequestUrl();

            response = GatewayApiClient.SendTransaction(gatewayGeneratePaymentRequest);

            buildViewData(gatewayGeneratePaymentRequest, response);

            return View(ViewList.GetValueOrDefault("ApiResponse"));
        }
        

        /// <summary>
        /// Processes the payment with PayPal.
        /// </summary>
        /// <returns>The payment with pay pal.</returns>
        /// <param name="gatewayApiRequest">Gateway API request.</param>
        [HttpPost("processPaymentWithPayPal")]
        public IActionResult ProcessPaymentWithPayPal(GatewayApiRequest gatewayApiRequest)
        {
            //enrich params with paypal information
            gatewayApiRequest.SourceType = "PAYPAL";
            gatewayApiRequest.BrowserPaymentOperation = "PAY";
            gatewayApiRequest.BrowserPaymentPaymentConfirmation = "CONFIRM_AT_PROVIDER";

            //get redirect paypal page
            return browserPayment(gatewayApiRequest);
        }

        /// <summary>
        /// Generic method used for redirec to Browsers payments url.
        /// </summary>
        /// <returns>The payment page.</returns>
        /// <param name="gatewayApiRequest">Gateway API request.</param>
        private IActionResult browserPayment(GatewayApiRequest gatewayApiRequest)
        {

            //cons
            string INITIATE_BROWSER_PAYMENT = "INITIATE_BROWSER_PAYMENT";
            string callbackController = "/browserPaymentReceipt";

            //build api request
            gatewayApiRequest.GatewayApiConfig = GatewayApiConfig;
            gatewayApiRequest.ApiOperation = INITIATE_BROWSER_PAYMENT;
            gatewayApiRequest.ApiMethod = GatewayApiClient.PUT;

            //Build Redirect url

            //Concating the transaction and order ids. It will be used to retrieve the payment result on callback
            string returnURL = Microsoft.AspNetCore.Http.Extensions.UriHelper.BuildAbsolute(Request.Scheme, Request.Host, Request.PathBase, callbackController);

            //add query string to return url
            returnURL = QueryHelpers.AddQueryString(returnURL, "transactionId", gatewayApiRequest.TransactionId);
            returnURL = QueryHelpers.AddQueryString(returnURL, "orderId", gatewayApiRequest.OrderId);

            gatewayApiRequest.ReturnUrl = returnURL;

            gatewayApiRequest.buildRequestUrl();
            gatewayApiRequest.buildPayload();

            _logger.Information($"PaymentApiController action gatewayApiRequest {JsonConvert.SerializeObject(gatewayApiRequest)}");


            string response = GatewayApiClient.SendTransaction(gatewayApiRequest);

            //validate transaction response
            if (JsonHelper.isErrorMessage(response))
            {
                return View("Error", ErrorViewModel.toErrorViewModel(getRequestId(), response));
            }

            //parse response
            InitiateBrowserPaymentResponse initiateResponse = null;
            try
            {
                initiateResponse = InitiateBrowserPaymentResponse.toInitiateBrowserPaymentResponse(response);

            }
            catch (Exception e)
            {
                _logger.Error($"BrowserPayment error : {JsonConvert.SerializeObject(e)}");

                return View("Error", new ErrorViewModel
                {
                    RequestId = getRequestId(),
                    Cause = e.InnerException != null ? e.InnerException.StackTrace : e.StackTrace,
                    Message = e.Message
                });
            }

            //check result
            if ("SUCCESS" != initiateResponse.Result)
            {

                _logger.Information($"Browser controller action error response {response}");

                return View("Error", new ErrorViewModel
                {
                    RequestId = getRequestId(),
                    Cause = initiateResponse.Result,
                    Message = "Browser Payment error."
                });
            }

            //redirect to partner browser payment page 
            return Redirect(initiateResponse.RedirectUrl);
        }



        /// <summary>
        /// Capture the browsers payment callback
        /// </summary>
        /// <returns>The payment receipt.</returns>
        [HttpGet("browserPaymentReceipt"), HttpPost("browserPaymentReceipt")]
        public IActionResult browserPaymentReceipt(String transactionId, String orderId)
        {

            //get order id from page parameter

            //create a gateway parameters request to retrieve the transaction result
            GatewayApiRequest gatewayApiRequest = new GatewayApiRequest();
            gatewayApiRequest.GatewayApiConfig = GatewayApiConfig;
            gatewayApiRequest.ApiMethod = GatewayApiClient.GET;
            gatewayApiRequest.ApiOperation = "RETRIEVE_TRANSACTION";
            gatewayApiRequest.OrderId = orderId;

            gatewayApiRequest.buildOrderUrl();
            gatewayApiRequest.buildPayload();


            string response = GatewayApiClient.SendTransaction(gatewayApiRequest);

            //validate transaction response
            if (JsonHelper.isErrorMessage(response))
            {
                return View("Error", ErrorViewModel.toErrorViewModel(getRequestId(), response));
            }


            //parse response to model
            TransactionResponseModel model = null;
            try
            {
                model = TransactionResponseModel.toTransactionResponseModel(response);
            }
            catch (Exception e)
            {
                _logger.Error($"browserPaymentReceipt error : {JsonConvert.SerializeObject(e)}");

                return View("Error", new ErrorViewModel
                {
                    RequestId = getRequestId(),
                    Cause = e.InnerException != null ? e.InnerException.StackTrace : e.StackTrace,
                    Message = e.Message
                });
            }

            return View(ViewList.GetValueOrDefault("Receipt"), model);
        }



        /// <summary>
        /// Processes the payment with UnionPay.
        /// </summary>
        /// <returns>The payment with UnionPay .</returns>
        /// <param name="gatewayApiRequest">Gateway API request.</param>
        [HttpPost("processPaymentWithUnionPay")]
        public IActionResult ProcessPaymentWithUnionPay(GatewayApiRequest gatewayApiRequest)
        {
            //enrich params with Union Pay information
            gatewayApiRequest.SourceType = "UNION_PAY";
            gatewayApiRequest.BrowserPaymentOperation = "AUTHORIZE";

            //get redirect Union Pay page
            return browserPayment(gatewayApiRequest);
        }


        /// <summary>
        /// Processes the Pay through NVP.
        /// </summary>
        /// <returns>The pay through nvp.</returns>
        /// <param name="gatewayApiRequest">Gateway API request.</param>
        [HttpPost("processPayThroughNVP")]
        public IActionResult ProcessPayThroughNVP([FromBody] GatewayApiRequest gatewayApiRequest)
        {
            _logger.Information($"PaymentApiController Process action gatewayApiRequest {JsonConvert.SerializeObject(gatewayApiRequest)}");

            gatewayApiRequest.GatewayApiConfig = GatewayApiConfig;
            gatewayApiRequest.ApiMethod = NVPApiClient.POST;
            gatewayApiRequest.ContentType = NVPApiClient.CONTENT_TYPE;

            string response = NVPApiClient.SendTransaction(gatewayApiRequest);

            buildViewDataNVP(gatewayApiRequest, response);

            return View(ViewList.GetValueOrDefault("ApiResponse"));

        }


        /// <summary>
        /// Processes the masterpass pre requirements before shows the masterbutton.
        /// </summary>
        /// <returns>The masterpass.</returns>
        /// <param name="gatewayApiRequest">Gateway API request.</param>
        [HttpPost("processMasterpass")]
        public IActionResult ProcessMasterpass(GatewayApiRequest gatewayApiRequest)
        {
            _logger.Information($"PaymentApiController Process Master Pass action gatewayApiRequest {JsonConvert.SerializeObject(gatewayApiRequest)}");
            String response = null;

            // Create session to use with OPEN_WALLET operation
            GatewayApiRequest gatewaySessionRequest = new GatewayApiRequest(GatewayApiConfig);

            gatewaySessionRequest.buildSessionRequestUrl();
            gatewaySessionRequest.ApiMethod = GatewayApiClient.POST;
            response = GatewayApiClient.SendTransaction(gatewaySessionRequest);

            //validate transaction response
            if (JsonHelper.isErrorMessage(response))
            {
                return View("Error", ErrorViewModel.toErrorViewModel(getRequestId(), response));
            }

            //convert json to model
            CheckoutSessionModel checkoutSessionModel = CheckoutSessionModel.toCheckoutSessionModel(response);

            _logger.Information($"Masterpass hostedSession created {response}");

            // Call UPDATE_SESSION to add order information to session
            //update http verb
            GatewayApiRequest gatewayUpdateSessionRequest = new GatewayApiRequest(GatewayApiConfig);
            gatewayUpdateSessionRequest.ApiMethod = GatewayApiClient.PUT;

            //update the url appending session id
            gatewayUpdateSessionRequest.buildSessionRequestUrl(checkoutSessionModel.Id);

            gatewayUpdateSessionRequest.OrderId = gatewayApiRequest.OrderId;
            gatewayUpdateSessionRequest.OrderCurrency = gatewayApiRequest.OrderCurrency;
            gatewayUpdateSessionRequest.OrderAmount = gatewayApiRequest.OrderAmount;

            //build payload with order info
            gatewayUpdateSessionRequest.buildPayload();

            response = GatewayApiClient.SendTransaction(gatewayUpdateSessionRequest);

            _logger.Information($"Masterpass updated session {response}");

            //validate transaction response
            if (JsonHelper.isErrorMessage(response))
            {
                return View("Error", ErrorViewModel.toErrorViewModel(getRequestId(), response));
            }


            // Call OPEN_WALLET to retrieve Masterpass configuration
            //It will use session URL
            GatewayApiRequest gatewayOpenWalletRequest = new GatewayApiRequest(GatewayApiConfig);

            gatewayOpenWalletRequest.buildSessionRequestUrl(checkoutSessionModel.Id);
            gatewayOpenWalletRequest.ApiMethod = GatewayApiClient.POST;

            gatewayOpenWalletRequest.MasterpassOnline = gatewayApiRequest.MasterpassOnline;
            gatewayOpenWalletRequest.MasterpassOriginUrl = gatewayApiRequest.MasterpassOriginUrl;

            gatewayOpenWalletRequest.OrderCurrency = gatewayApiRequest.OrderCurrency;
            gatewayOpenWalletRequest.OrderAmount = gatewayApiRequest.OrderAmount;

            gatewayOpenWalletRequest.buildPayload();

            response = GatewayApiClient.SendTransaction(gatewayOpenWalletRequest);

            //validate transaction response
            if (JsonHelper.isErrorMessage(response))
            {
                return View("Error", ErrorViewModel.toErrorViewModel(getRequestId(), response));
            }


            //parse response to model
            MasterpassWalletResponse masterpassWalletResponse = null;
            try
            {
                masterpassWalletResponse = MasterpassWalletResponse.toMasterpassWalletResponse(response);
            }
            catch (Exception e)
            {
                _logger.Error($"ProcessMasterpass error : {JsonConvert.SerializeObject(e)}");

                return View("Error", new ErrorViewModel
                {
                    RequestId = getRequestId(),
                    Cause = e.InnerException != null ? e.InnerException.StackTrace : e.StackTrace,
                    Message = e.Message
                });
            }

            _logger.Information($"Masterpass wallet configuration {response}");

            // Save this value in HttpSession to retrieve after returning from issuer authentication form
            setSessionValue("sessionId", checkoutSessionModel.Id);
            setSessionValue("amount", gatewayApiRequest.OrderAmount);
            setSessionValue("currency", gatewayApiRequest.OrderCurrency);
            setSessionValue("orderId", gatewayApiRequest.OrderId);

            //set values for view usage
            ViewBag.requestToken = masterpassWalletResponse.RequestToken;
            ViewBag.merchantCheckoutId = masterpassWalletResponse.MerchantCheckoutId;
            ViewBag.allowedCardTypes = masterpassWalletResponse.AllowedCardTypes;

            return View(ViewList.GetValueOrDefault("MasterpassButton"));
        }



        /// <summary>
        /// Process Masterpasses callback response.
        /// </summary>
        /// <returns>The response.</returns>
        /// <param name="oauth_token">Oauth token.</param>
        /// <param name="oauth_verifier">Oauth verifier.</param>
        /// <param name="checkoutId">Checkout identifier.</param>
        /// <param name="checkout_resource_url">Checkout resource URL.</param>
        /// <param name="mpstatus">Mpstatus.</param>
        [HttpGet("masterpassResponse"), HttpPost("masterpassResponse")]
        public IActionResult masterpassResponse(String oauth_token, String oauth_verifier, String checkoutId,
                                                String checkout_resource_url, String mpstatus)
        {

            String UPDATE_SESSION_FROM_WALLET = "UPDATE_SESSION_FROM_WALLET";
            String WALLET_PROVIDER = "MASTERPASS_ONLINE";

            //get session values
            String sessionId = getSessionValueAsString("sessionId");
            String amount = getSessionValueAsString("amount");
            String currency = getSessionValueAsString("currency");
            String orderId = getSessionValueAsString("orderId");

            //remove session values
            removeSessionValue("sessionId");
            removeSessionValue("amount");
            removeSessionValue("currency");
            removeSessionValue("orderId");

            // UPDATE_SESSION_FROM_WALLET - Retrieve payment details from wallet using session ID
            GatewayApiRequest gatewayApiRequest = new GatewayApiRequest(GatewayApiConfig);
            gatewayApiRequest.ApiMethod = GatewayApiClient.POST;
            gatewayApiRequest.ApiOperation = UPDATE_SESSION_FROM_WALLET;
            gatewayApiRequest.MasterpassOnline = WALLET_PROVIDER;

            gatewayApiRequest.MasterpassOauthToken = oauth_token;
            gatewayApiRequest.MasterpassOauthVerifier = oauth_verifier;
            gatewayApiRequest.MasterpassCheckoutUrl = checkout_resource_url;

            gatewayApiRequest.buildSessionRequestUrl(sessionId);

            //build json 
            gatewayApiRequest.buildPayload();

            string response = GatewayApiClient.SendTransaction(gatewayApiRequest);

            _logger.Information($"Masterpass update session : {response}");

            //validate transaction response
            if (JsonHelper.isErrorMessage(response))
            {
                return View("Error", ErrorViewModel.toErrorViewModel(getRequestId(), response));
            }


            // Make a payment using the session
            // Construct API request
            GatewayApiRequest gatewayPayApiRequest = new GatewayApiRequest(GatewayApiConfig);
            gatewayPayApiRequest.ApiMethod = GatewayApiClient.PUT;
            gatewayPayApiRequest.ApiOperation = "PAY";
            gatewayPayApiRequest.SessionId = sessionId;

            //order info
            gatewayPayApiRequest.OrderAmount = amount;
            gatewayPayApiRequest.OrderId = orderId;
            gatewayPayApiRequest.OrderCurrency = currency;
            gatewayPayApiRequest.TransactionId = IdUtils.generateSampleId();

            //build payload
            gatewayPayApiRequest.buildPayload();

            gatewayPayApiRequest.buildRequestUrl();

            response = GatewayApiClient.SendTransaction(gatewayPayApiRequest);

            _logger.Information($"Masterpass PAY operation : {response}");

            //validate transaction response
            if (JsonHelper.isErrorMessage(response))
            {
                return View("Error", ErrorViewModel.toErrorViewModel(getRequestId(), response));
            }


            //parse response to default transaction response model
            TransactionResponseModel model = null;
            try
            {
                model = TransactionResponseModel.fromMasterpassResponseToTransactionResponseModel(response);
            }
            catch (Exception e)
            {
                _logger.Error($"MasterpassResponse error : {JsonConvert.SerializeObject(e)}");

                return View("Error", new ErrorViewModel
                {
                    RequestId = getRequestId(),
                    Cause = e.InnerException != null ? e.InnerException.StackTrace : e.StackTrace,
                    Message = e.Message
                });
            }

            return View(ViewList.GetValueOrDefault("Receipt"), model);
        }


        [HttpGet("successerror")]
        public string SuccessError()
        {
            return "This is Success Error API";
        }
    }
}
