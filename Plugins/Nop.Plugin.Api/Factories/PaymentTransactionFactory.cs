﻿using Nop.Core.Domain.Payments;
using Nop.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Api.Factories
{
    public class PaymentTransactionFactory : IFactory<PaymentTransaction>
    {
        public PaymentTransaction Initialize()
        {
            var paymentTransaction = new PaymentTransaction();

            paymentTransaction.CreatedOnUtc = DateTime.UtcNow;

            return paymentTransaction;
        }
    }
}
