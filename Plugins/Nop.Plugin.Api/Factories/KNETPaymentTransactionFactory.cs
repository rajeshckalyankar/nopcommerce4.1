﻿using Nop.Core.Domain.Payments;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Api.Factories
{
    public class KNETPaymentTransactionFactory : IFactory<KNETPaymentTransaction>
    {
        public KNETPaymentTransaction Initialize()
        {
            var oKNETPaymentTransaction = new KNETPaymentTransaction();

            oKNETPaymentTransaction.CreatedOnUtc = DateTime.UtcNow;

            return oKNETPaymentTransaction;
        }
    }
}
