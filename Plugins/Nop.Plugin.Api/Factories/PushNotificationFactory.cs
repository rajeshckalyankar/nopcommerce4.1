﻿using Nop.Core.Domain.Orders;
using Nop.Core.Domain.PushNotifications;
using Nop.Services.PushNotifications;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Api.Factories
{
    public class PushNotificationFactory : IFactory<PushNotification>
    {
        public PushNotification Initialize()
        {
            var oPushNotification = new PushNotification();

            oPushNotification.CreatedOnUtc = DateTime.UtcNow;

            return oPushNotification;
        }
    }
}
