﻿using System;
using System.Collections.Generic;
using System.Text;
using Nop.Core.Domain.Payments;

namespace Nop.Plugin.Api.Factories
{
    public class MasterCardPaymentTransactionFactory : IFactory<MasterCardPaymentTransaction>
    {
        public MasterCardPaymentTransaction Initialize()
        {
            var oMasterCardPaymentTransaction = new MasterCardPaymentTransaction();

            oMasterCardPaymentTransaction.CreatedOnUtc = DateTime.UtcNow;

            return oMasterCardPaymentTransaction;
        }
    }
}
