using System;

namespace Nop.Plugin.Api.Utils
{
    public class IdUtils
    {
        public static string generateSampleId()
        {
            return Guid.NewGuid().ToString().Replace("-", string.Empty).Substring(0, 10);
        }
    }
}
