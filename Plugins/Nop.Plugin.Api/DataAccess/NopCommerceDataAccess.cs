﻿using System;
using System.Collections.Generic;
using System.Text;
using Nop.Core.Data;
using Nop.Services.Logging;

namespace Nop.Plugin.Api.DataAccess
{
    public class NopCommerceDataAccess
    {
        protected string ConnectionString = ""; //TODO-1 Read connection string from configuration.
        protected readonly ILogger _logger;

        public NopCommerceDataAccess(ILogger logger)
        {
            _logger = logger;
            try
            {
                DataSettings dataSettings = DataSettingsManager.LoadSettings();
                if (dataSettings != null && dataSettings.IsValid)
                {
                    ConnectionString = dataSettings.DataConnectionString;
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error occurred while reading connection string from data settings.", ex);
            }
            
        }
    }

    enum SPResult
    {

        /// <summary>
        /// Exception occurred in SP. Control goes to catch block in SP.
        /// </summary>
        SP_EXCEPTION = -2,

        /// <summary>
        /// Unidentified error in SP execution
        /// </summary>
        ERROR = -1,

        /// <summary>
        /// SP execution success
        /// </summary>
        SUCCESS = 0,

        /// <summary>
        /// User defined error occurred in SP execution.
        /// </summary>
        FAILURE = 1
        
    }
}
