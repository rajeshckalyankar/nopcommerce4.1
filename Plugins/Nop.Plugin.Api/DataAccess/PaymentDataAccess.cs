﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Nop.Core.Domain.Logging;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Plugin.Api.Models.PaymentParameters;
using Nop.Services.Logging;

namespace Nop.Plugin.Api.DataAccess
{
    public class PaymentDataAccess : NopCommerceDataAccess
    {
        public PaymentDataAccess(ILogger logger) : base(logger)
        {
        }

        /// <summary>
        /// This function adds payment transaction at the time of initiating payment using KNET GW.
        /// </summary>
        /// <param name="OrderId"></param>
        /// <returns></returns>
        public bool AddKNETPaymentTransaction(string OrderId, out string TrackID)
        {
            bool bReturn = false;
            TrackID = string.Empty;
            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(ConnectionString))
                {
                    oSqlConnection.Open();

                    using (SqlCommand oSqlCommand = new SqlCommand("ADD_KNET_TRANSACTION", oSqlConnection))
                    {
                        SPResult SPResult;
                        oSqlCommand.CommandType = CommandType.StoredProcedure;

                        oSqlCommand.Parameters.Add("@pOrderId", SqlDbType.Int).Value = Convert.ToInt32(OrderId);
                        oSqlCommand.Parameters.Add("@pPaymentTransactionStatus", SqlDbType.Int).Value = Convert.ToInt32(PaymentTransactionStatus.INITIATED);
                        oSqlCommand.Parameters.Add("@pTrackID", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                        oSqlCommand.Parameters.Add("@pSPResult", SqlDbType.Int).Direction = ParameterDirection.Output;

                        oSqlCommand.ExecuteNonQuery();

                        SPResult = (SPResult)Convert.ToInt32(oSqlCommand.Parameters["@pSPResult"].Value);

                        switch (SPResult)
                        {
                            case SPResult.SP_EXCEPTION:
                                //Error gets logged in Log table from the catch block of SP.
                                break;

                            case SPResult.ERROR:
                                _logger.Error("Unidentified error occurred in exeucution of SP ADD_KNET_TRANSACTION.");
                                break;

                            case SPResult.FAILURE:
                                _logger.Error("Error occurred while adding KNET payment transaction.");
                                break;

                            case SPResult.SUCCESS:
                                TrackID = oSqlCommand.Parameters["@pTrackID"].Value.ToString();
                                bReturn = true;
                                _logger.InsertLog(LogLevel.Debug, "Successfully added KNET payment transaction.");
                                break;

                            default:
                                _logger.Error("Unidentified result from exeucution of SP ADD_KNET_TRANSACTION.");
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred while adding KNET payment transaction.", ex);
            }
            return bReturn;
        }

        /// <summary>
        /// This function saves the result from KNET GW into database.
        /// </summary>
        /// <param name="oKNETPaymentTransaction"></param>
        /// <returns></returns>
        public bool SaveKNETTransactionResult(Nop.Plugin.Api.Models.PaymentParameters.KNETPaymentTransaction oKNETPaymentTransaction, OrderStatus oOrderStatus, PaymentStatus oPaymentStatus, PaymentTransactionStatus oPaymentTransactionStatus)
        {
            bool bReturn = false;
            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(ConnectionString))
                {
                    oSqlConnection.Open();

                    using (SqlCommand oSqlCommand = new SqlCommand("SAVE_KNET_TRANSACTION_RESULT", oSqlConnection))
                    {
                        SPResult SPResult;
                        oSqlCommand.CommandType = CommandType.StoredProcedure;

                        if (String.IsNullOrEmpty(oKNETPaymentTransaction.PaymentID))
                        {
                            oSqlCommand.Parameters.Add("@pPaymentID", SqlDbType.VarChar, 100).Value = DBNull.Value;
                        }
                        else
                        {
                            oSqlCommand.Parameters.Add("@pPaymentID", SqlDbType.VarChar, 100).Value = oKNETPaymentTransaction.PaymentID;
                        }

                        oSqlCommand.Parameters.Add("@pTrackID", SqlDbType.VarChar, 100).Value = oKNETPaymentTransaction.TrackID;

                        if (String.IsNullOrEmpty(oKNETPaymentTransaction.TranID))
                        {
                            oSqlCommand.Parameters.Add("@pTranID", SqlDbType.VarChar, 100).Value = DBNull.Value;
                        }
                        else
                        {
                            oSqlCommand.Parameters.Add("@pTranID", SqlDbType.VarChar, 100).Value = oKNETPaymentTransaction.TranID;
                        }

                        if (String.IsNullOrEmpty(oKNETPaymentTransaction.Auth))
                        {
                            oSqlCommand.Parameters.Add("@pAuth", SqlDbType.VarChar, 100).Value = DBNull.Value;
                        }
                        else
                        {
                            oSqlCommand.Parameters.Add("@pAuth", SqlDbType.VarChar, 100).Value = oKNETPaymentTransaction.Auth;
                        }

                        if (String.IsNullOrEmpty(oKNETPaymentTransaction.Ref))
                        {
                            oSqlCommand.Parameters.Add("@pRef", SqlDbType.VarChar, 100).Value = DBNull.Value;
                        }
                        else
                        {
                            oSqlCommand.Parameters.Add("@pRef", SqlDbType.VarChar, 100).Value = oKNETPaymentTransaction.Ref;
                        }

                        if (String.IsNullOrEmpty(oKNETPaymentTransaction.Result))
                        {
                            oSqlCommand.Parameters.Add("@pResult", SqlDbType.VarChar, 100).Value = DBNull.Value;
                        }
                        else
                        {
                            oSqlCommand.Parameters.Add("@pResult", SqlDbType.VarChar, 100).Value = oKNETPaymentTransaction.Result;
                        }

                        if (String.IsNullOrEmpty(oKNETPaymentTransaction.UDF1))
                        {
                            oSqlCommand.Parameters.Add("@pUDF1", SqlDbType.VarChar, 100).Value = DBNull.Value;
                        }
                        else
                        {
                            oSqlCommand.Parameters.Add("@pUDF1", SqlDbType.VarChar, 100).Value = oKNETPaymentTransaction.UDF1;
                        }

                        if (String.IsNullOrEmpty(oKNETPaymentTransaction.UDF2))
                        {
                            oSqlCommand.Parameters.Add("@pUDF2", SqlDbType.VarChar, 100).Value = DBNull.Value;
                        }
                        else
                        {
                            oSqlCommand.Parameters.Add("@pUDF2", SqlDbType.VarChar, 100).Value = oKNETPaymentTransaction.UDF2;
                        }

                        if (String.IsNullOrEmpty(oKNETPaymentTransaction.UDF3))
                        {
                            oSqlCommand.Parameters.Add("@pUDF3", SqlDbType.VarChar, 100).Value = DBNull.Value;
                        }
                        else
                        {
                            oSqlCommand.Parameters.Add("@pUDF3", SqlDbType.VarChar, 100).Value = oKNETPaymentTransaction.UDF3;
                        }

                        if (String.IsNullOrEmpty(oKNETPaymentTransaction.UDF4))
                        {
                            oSqlCommand.Parameters.Add("@pUDF4", SqlDbType.VarChar, 100).Value = DBNull.Value;
                        }
                        else
                        {
                            oSqlCommand.Parameters.Add("@pUDF4", SqlDbType.VarChar, 100).Value = oKNETPaymentTransaction.UDF4;
                        }

                        if (String.IsNullOrEmpty(oKNETPaymentTransaction.UDF5))
                        {
                            oSqlCommand.Parameters.Add("@pUDF5", SqlDbType.VarChar, 100).Value = DBNull.Value;
                        }
                        else
                        {
                            oSqlCommand.Parameters.Add("@pUDF5", SqlDbType.VarChar, 100).Value = oKNETPaymentTransaction.UDF5;
                        }

                        if (!oKNETPaymentTransaction.Amt.HasValue)
                        {
                            oSqlCommand.Parameters.Add("@pAmt", SqlDbType.Decimal).Value = DBNull.Value;
                        }
                        else
                        {
                            oSqlCommand.Parameters.Add("@pAmt", SqlDbType.Decimal).Value = oKNETPaymentTransaction.Amt.Value;
                        }

                        if (String.IsNullOrEmpty(oKNETPaymentTransaction.PostDate))
                        {
                            oSqlCommand.Parameters.Add("@pPostDate", SqlDbType.VarChar, 30).Value = DBNull.Value;
                        }
                        else
                        {
                            oSqlCommand.Parameters.Add("@pPostDate", SqlDbType.VarChar, 30).Value = oKNETPaymentTransaction.PostDate;
                        }

                        if (String.IsNullOrEmpty(oKNETPaymentTransaction.Avr))
                        {
                            oSqlCommand.Parameters.Add("@pAvr", SqlDbType.VarChar, 100).Value = DBNull.Value;
                        }
                        else
                        {
                            oSqlCommand.Parameters.Add("@pAvr", SqlDbType.VarChar, 100).Value = oKNETPaymentTransaction.Avr;
                        }

                        if (String.IsNullOrEmpty(oKNETPaymentTransaction.AuthRespCode))
                        {
                            oSqlCommand.Parameters.Add("@pAuthRespCode", SqlDbType.VarChar, 100).Value = DBNull.Value;
                        }
                        else
                        {
                            oSqlCommand.Parameters.Add("@pAuthRespCode", SqlDbType.VarChar, 100).Value = oKNETPaymentTransaction.AuthRespCode;
                        }

                        if (String.IsNullOrEmpty(oKNETPaymentTransaction.ErrorCode))
                        {
                            oSqlCommand.Parameters.Add("@pErrorCode", SqlDbType.VarChar, 100).Value = DBNull.Value;
                        }
                        else
                        {
                            oSqlCommand.Parameters.Add("@pErrorCode", SqlDbType.VarChar, 100).Value = oKNETPaymentTransaction.ErrorCode;
                        }

                        if (String.IsNullOrEmpty(oKNETPaymentTransaction.ErrorText))
                        {
                            oSqlCommand.Parameters.Add("@pErrorText", SqlDbType.VarChar, 1000).Value = DBNull.Value;
                        }
                        else
                        {
                            oSqlCommand.Parameters.Add("@pErrorText", SqlDbType.VarChar, 1000).Value = oKNETPaymentTransaction.ErrorText;
                        }

                        oSqlCommand.Parameters.Add("@pPaymentStatus", SqlDbType.Int).Value = Convert.ToInt32(oPaymentStatus);

                        oSqlCommand.Parameters.Add("@pOrderStatus", SqlDbType.Int).Value = Convert.ToInt32(oOrderStatus);

                        oSqlCommand.Parameters.Add("@pPaymentTransactionStatus", SqlDbType.Int).Value = Convert.ToInt32(oPaymentTransactionStatus);

                        oSqlCommand.Parameters.Add("@pSPResult", SqlDbType.Int).Direction = ParameterDirection.Output;

                        oSqlCommand.ExecuteNonQuery();

                        SPResult = (SPResult)Convert.ToInt32(oSqlCommand.Parameters["@pSPResult"].Value);

                        switch (SPResult)
                        {
                            case SPResult.SP_EXCEPTION:
                                //Error gets logged in Log table from the catch block of SP.
                                break;

                            case SPResult.ERROR:
                                _logger.Error("Unidentified error occurred in exeucution of SP SAVE_KNET_TRANSACTION_RESULT.");
                                break;

                            case SPResult.FAILURE:
                                _logger.Error("Error occurred while saving KNET transaction result.");
                                break;

                            case SPResult.SUCCESS:
                                bReturn = true;
                                _logger.InsertLog(LogLevel.Debug, "Successfully saved KNET transaction result.");
                                break;

                            default:
                                _logger.Error("Unidentified result from exeucution of SP SAVE_KNET_TRANSACTION_RESULT.");
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred while saving KNET transaction result.", ex);
            }
            return bReturn;
        }
    }
}
