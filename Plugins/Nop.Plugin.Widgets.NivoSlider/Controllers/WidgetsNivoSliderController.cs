﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Configuration;
using Nop.Plugin.Widgets.NivoSlider.Models;
using Nop.Services.Catalog;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Widgets.NivoSlider.Controllers
{
    public enum PromotionalListEnum
    {
        Product = 1,
        Brand = 2
    }

    [Area(AreaNames.Admin)]
    public class WidgetsNivoSliderController : BasePluginController
    {
        private readonly IStoreContext _storeContext;
        private readonly IPermissionService _permissionService;
        private readonly IPictureService _pictureService;
        private readonly ISettingService _settingService;
        private readonly ILocalizationService _localizationService;
        private readonly IProductService _productService;
        private readonly IManufacturerService _manufacturerService;

        public WidgetsNivoSliderController(IStoreContext storeContext,
            IPermissionService permissionService,
            IPictureService pictureService,
            ISettingService settingService,
            ICacheManager cacheManager,
            ILocalizationService localizationService,
            IProductService productService,
            IManufacturerService manufacturerService)
        {
            this._storeContext = storeContext;
            this._permissionService = permissionService;
            this._pictureService = pictureService;
            this._settingService = settingService;
            this._localizationService = localizationService;
            this._productService = productService;
            this._manufacturerService = manufacturerService;
        }

        private ConfigurationModel TrimTextValues(ConfigurationModel model)
        {
            if(!string.IsNullOrEmpty(model.Text1))
                model.Text1.Trim();
            if (!string.IsNullOrEmpty(model.Text2))
                model.Text2.Trim();
            if (!string.IsNullOrEmpty(model.Text3))
                model.Text3.Trim();
            if (!string.IsNullOrEmpty(model.Text4))
                model.Text4.Trim();
            if (!string.IsNullOrEmpty(model.Text5))
                model.Text5.Trim();

            return model;

        }

        public IActionResult Configure()
        {
            _settingService.ClearCache();
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            //load settings for a chosen store scope
            var storeScope = _storeContext.ActiveStoreScopeConfiguration;
            var nivoSliderSettings = _settingService.LoadSetting<NivoSliderSettings>(storeScope);
            var model = new ConfigurationModel
            {
                Picture1Id = nivoSliderSettings.Picture1Id,
                Text1 = nivoSliderSettings.Text1,
                Link1 = nivoSliderSettings.Link1,
                AltText1 = nivoSliderSettings.AltText1,
                Picture2Id = nivoSliderSettings.Picture2Id,
                Text2 = nivoSliderSettings.Text2,
                Link2 = nivoSliderSettings.Link2,
                AltText2 = nivoSliderSettings.AltText2,
                Picture3Id = nivoSliderSettings.Picture3Id,
                Text3 = nivoSliderSettings.Text3,
                Link3 = nivoSliderSettings.Link3,
                AltText3 = nivoSliderSettings.AltText3,
                Picture4Id = nivoSliderSettings.Picture4Id,
                Text4 = nivoSliderSettings.Text4,
                Link4 = nivoSliderSettings.Link4,
                AltText4 = nivoSliderSettings.AltText4,
                Picture5Id = nivoSliderSettings.Picture5Id,
                Text5 = nivoSliderSettings.Text5,
                Link5 = nivoSliderSettings.Link5,
                AltText5 = nivoSliderSettings.AltText5,
                ActiveStoreScopeConfiguration = storeScope
            };

            if (storeScope > 0)
            {
                model.Picture1Id_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Picture1Id, storeScope);
                model.Text1_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Text1, storeScope);
                model.Link1_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Link1, storeScope);
                model.AltText1_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.AltText1, storeScope);
                model.Picture2Id_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Picture2Id, storeScope);
                model.Text2_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Text2, storeScope);
                model.Link2_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Link2, storeScope);
                model.AltText2_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.AltText2, storeScope);
                model.Picture3Id_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Picture3Id, storeScope);
                model.Text3_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Text3, storeScope);
                model.Link3_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Link3, storeScope);
                model.AltText3_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.AltText3, storeScope);
                model.Picture4Id_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Picture4Id, storeScope);
                model.Text4_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Text4, storeScope);
                model.Link4_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Link4, storeScope);
                model.AltText4_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.AltText4, storeScope);
                model.Picture5Id_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Picture5Id, storeScope);
                model.Text5_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Text5, storeScope);
                model.Link5_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.Link5, storeScope);
                model.AltText5_OverrideForStore = _settingService.SettingExists(nivoSliderSettings, x => x.AltText5, storeScope);
            }

            return View("~/Plugins/Widgets.NivoSlider/Views/Configure.cshtml", model);
        }

        [HttpPost]
        public IActionResult Configure(ConfigurationModel model)
        {
            _settingService.ClearCache();
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            StringBuilder InvalidProductIds = new StringBuilder(string.Empty);
            StringBuilder InvalidBrandIds = new StringBuilder(string.Empty);

            var p1 = _productService.GetProductById(Convert.ToInt32(model.Text1));
            var AllManufacturersList = _manufacturerService.GetAllManufacturers();

            //Trimming all the text values
            model = TrimTextValues(model);
           
            #region Banner #1
            if (Convert.ToInt32(model.Link1) == Convert.ToInt32(PromotionalListEnum.Product))
            {
                if (!string.IsNullOrEmpty(model.Text1))
                    if ((p1 = _productService.GetProductById(Convert.ToInt32(model.Text1))) == null)
                        InvalidProductIds.Append(model.Text1 + " ");
            }
            else if (Convert.ToInt32(model.Link1) == Convert.ToInt32(PromotionalListEnum.Brand))
            {
                if (!string.IsNullOrEmpty(model.Text1))
                    if (!AllManufacturersList.Any(Manufacturer => Manufacturer.Id == Convert.ToInt32(model.Text1)))
                        InvalidBrandIds.Append(model.Text1 + " ");
            }
            #endregion

            #region Banner #2
            if (Convert.ToInt32(model.Link2) == Convert.ToInt32(PromotionalListEnum.Product))
            {
                if (!string.IsNullOrEmpty(model.Text2))
                    if ((p1 = _productService.GetProductById(Convert.ToInt32(model.Text2))) == null)
                        InvalidProductIds.Append(model.Text2 + " ");
            }
            else if (Convert.ToInt32(model.Link2) == Convert.ToInt32(PromotionalListEnum.Brand))
            {
                if (!string.IsNullOrEmpty(model.Text2))
                    if (!AllManufacturersList.Any(Manufacturer => Manufacturer.Id == Convert.ToInt32(model.Text2)))
                        InvalidBrandIds.Append(model.Text2 + " ");
            }
            #endregion

            #region Banner #3
            if (Convert.ToInt32(model.Link3) == Convert.ToInt32(PromotionalListEnum.Product))
            {
                if (!string.IsNullOrEmpty(model.Text3))
                    if ((p1 = _productService.GetProductById(Convert.ToInt32(model.Text3))) == null)
                        InvalidProductIds.Append(model.Text3 + " ");
            }
            else if (Convert.ToInt32(model.Link3) == Convert.ToInt32(PromotionalListEnum.Brand))
            {
                if (!string.IsNullOrEmpty(model.Text3))
                    if (!AllManufacturersList.Any(Manufacturer => Manufacturer.Id == Convert.ToInt32(model.Text3)))
                        InvalidBrandIds.Append(model.Text3 + " ");
            }
            #endregion

            #region Banner #4
            if (Convert.ToInt32(model.Link4) == Convert.ToInt32(PromotionalListEnum.Product))
            {
                if (!string.IsNullOrEmpty(model.Text4))
                    if ((p1 = _productService.GetProductById(Convert.ToInt32(model.Text4))) == null)
                        InvalidProductIds.Append(model.Text4 + " ");
            }
            else if (Convert.ToInt32(model.Link4) == Convert.ToInt32(PromotionalListEnum.Brand))
            {
                if (!string.IsNullOrEmpty(model.Text4))
                    if (!AllManufacturersList.Any(Manufacturer => Manufacturer.Id == Convert.ToInt32(model.Text4)))
                        InvalidBrandIds.Append(model.Text4 + " ");
            }
            #endregion

            #region Banner #5
            if (Convert.ToInt32(model.Link5) == Convert.ToInt32(PromotionalListEnum.Product))
            {
                if (!string.IsNullOrEmpty(model.Text5))
                    if ((p1 = _productService.GetProductById(Convert.ToInt32(model.Text5))) == null)
                        InvalidProductIds.Append(model.Text5 + " ");
            }
            else if (Convert.ToInt32(model.Link5) == Convert.ToInt32(PromotionalListEnum.Brand))
            {
                if (!string.IsNullOrEmpty(model.Text5))
                    if (!AllManufacturersList.Any(Manufacturer => Manufacturer.Id == Convert.ToInt32(model.Text5)))
                        InvalidBrandIds.Append(model.Text5 + " ");
            }
            #endregion


            if (!string.IsNullOrEmpty(InvalidProductIds.ToString()) && !string.IsNullOrEmpty(InvalidBrandIds.ToString()))
            {
                ErrorNotification(string.Format(_localizationService.GetResource("Admin.Promotions.Banners.InvalidProductIds"), InvalidProductIds) + "\n" + string.Format(_localizationService.GetResource("Admin.Promotions.Banners.InvalidBrandIds"), InvalidBrandIds), false);
                return Configure();
            }
            else if (!string.IsNullOrEmpty(InvalidProductIds.ToString()))
            {
                ErrorNotification(string.Format(_localizationService.GetResource("Admin.Promotions.Banners.InvalidProductIds"), InvalidProductIds), false);
                return Configure();
            }
            else if (!string.IsNullOrEmpty(InvalidBrandIds.ToString()))
            {
                ErrorNotification(string.Format(_localizationService.GetResource("Admin.Promotions.Banners.InvalidBrandIds"), InvalidBrandIds), false);
                return Configure();
            }

            //load settings for a chosen store scope
            var storeScope = _storeContext.ActiveStoreScopeConfiguration;
            var nivoSliderSettings = _settingService.LoadSetting<NivoSliderSettings>(storeScope);

            //get previous picture identifiers
            var previousPictureIds = new[]
            {
                nivoSliderSettings.Picture1Id,
                nivoSliderSettings.Picture2Id,
                nivoSliderSettings.Picture3Id,
                nivoSliderSettings.Picture4Id,
                nivoSliderSettings.Picture5Id
            };

            nivoSliderSettings.Picture1Id = model.Picture1Id;
            nivoSliderSettings.Text1 = model.Text1;
            nivoSliderSettings.Link1 = model.Link1;
            nivoSliderSettings.AltText1 = model.AltText1;
            nivoSliderSettings.Picture2Id = model.Picture2Id;
            nivoSliderSettings.Text2 = model.Text2;
            nivoSliderSettings.Link2 = model.Link2;
            nivoSliderSettings.AltText2 = model.AltText2;
            nivoSliderSettings.Picture3Id = model.Picture3Id;
            nivoSliderSettings.Text3 = model.Text3;
            nivoSliderSettings.Link3 = model.Link3;
            nivoSliderSettings.AltText3 = model.AltText3;
            nivoSliderSettings.Picture4Id = model.Picture4Id;
            nivoSliderSettings.Text4 = model.Text4;
            nivoSliderSettings.Link4 = model.Link4;
            nivoSliderSettings.AltText4 = model.AltText4;
            nivoSliderSettings.Picture5Id = model.Picture5Id;
            nivoSliderSettings.Text5 = model.Text5;
            nivoSliderSettings.Link5 = model.Link5;
            nivoSliderSettings.AltText5 = model.AltText5;

            /* We do not clear cache after each setting update.
             * This behavior can increase performance because cached settings will not be cleared 
             * and loaded from database after each update */
            _settingService.SaveSettingOverridablePerStore(nivoSliderSettings, x => x.Picture1Id, model.Picture1Id_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(nivoSliderSettings, x => x.Text1, model.Text1_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(nivoSliderSettings, x => x.Link1, model.Link1_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(nivoSliderSettings, x => x.AltText1, model.AltText1_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(nivoSliderSettings, x => x.Picture2Id, model.Picture2Id_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(nivoSliderSettings, x => x.Text2, model.Text2_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(nivoSliderSettings, x => x.Link2, model.Link2_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(nivoSliderSettings, x => x.AltText2, model.AltText2_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(nivoSliderSettings, x => x.Picture3Id, model.Picture3Id_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(nivoSliderSettings, x => x.Text3, model.Text3_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(nivoSliderSettings, x => x.Link3, model.Link3_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(nivoSliderSettings, x => x.AltText3, model.AltText3_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(nivoSliderSettings, x => x.Picture4Id, model.Picture4Id_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(nivoSliderSettings, x => x.Text4, model.Text4_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(nivoSliderSettings, x => x.Link4, model.Link4_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(nivoSliderSettings, x => x.AltText4, model.AltText4_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(nivoSliderSettings, x => x.Picture5Id, model.Picture5Id_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(nivoSliderSettings, x => x.Text5, model.Text5_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(nivoSliderSettings, x => x.Link5, model.Link5_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(nivoSliderSettings, x => x.AltText5, model.AltText5_OverrideForStore, storeScope, false);

            //now clear settings cache
            _settingService.ClearCache();

            //get current picture identifiers
            var currentPictureIds = new[]
            {
                nivoSliderSettings.Picture1Id,
                nivoSliderSettings.Picture2Id,
                nivoSliderSettings.Picture3Id,
                nivoSliderSettings.Picture4Id,
                nivoSliderSettings.Picture5Id
            };

            //delete an old picture (if deleted or updated)
            foreach (var pictureId in previousPictureIds.Except(currentPictureIds))
            {
                var previousPicture = _pictureService.GetPictureById(pictureId);
                if (previousPicture != null)
                    _pictureService.DeletePicture(previousPicture);
            }

            SuccessNotification("The banners has been updated successfully.");// (_localizationService.GetResource("Admin.Plugins.Saved"));
            return Configure();
        }

        #region Dynamic Stuff

        //[HttpPost]
        //public IActionResult ProductPictureAdd(ConfigurationListModel model)
        //{
        //    if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
        //        return AccessDeniedView();

        //    //load settings for a chosen store scope
        //    var storeScope = _storeContext.ActiveStoreScopeConfiguration;

        //    var nivoSliderSettingsList = new NivoSliderSettingsList();

        //    nivoSliderSettingsList.PictureId = model.PictureId;
        //    nivoSliderSettingsList.Text = model.Text;
        //    nivoSliderSettingsList.AltText = model.AltText;
        //    nivoSliderSettingsList.Link = model.Link;

        //    /* We do not clear cache after each setting update.
        //     * This behavior can increase performance because cached settings will not be cleared 
        //     * and loaded from database after each update */
        //    _settingService.SaveSettingOverridablePerStore(nivoSliderSettingsList, x => x.PictureId, model.PictureId_OverrideForStore, storeScope, false);
        //    _settingService.SaveSettingOverridablePerStore(nivoSliderSettingsList, x => x.Text, model.Text_OverrideForStore, storeScope, false);
        //    _settingService.SaveSettingOverridablePerStore(nivoSliderSettingsList, x => x.Link, model.Link_OverrideForStore, storeScope, false);
        //    _settingService.SaveSettingOverridablePerStore(nivoSliderSettingsList, x => x.AltText, model.AltText_OverrideForStore, storeScope, false);

        //    //now clear settings cache
        //    _settingService.ClearCache();

        //    SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));
        //    return BannerPicture();
        //}

        //public IActionResult BannerPicture()
        //{

        //    if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
        //        return AccessDeniedView();

        //    //load settings for a chosen store scope
        //    var storeScope = _storeContext.ActiveStoreScopeConfiguration;

        //    var nivoSliderSettings = _settingService.LoadSettingList<NivoSliderSettingsList>(storeScope);

        //    //var nivoSliderSettings1 = _settingService.LoadSettingList(typeof(ISettings),storeScope);

        //    var model = new ConfigurationListModel();

        //    return View("~/Plugins/Widgets.NivoSlider/Views/BannerPictureList.cshtml", model);
        //}

        //public IActionResult BannerPictureList()
        //{

        //    if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
        //        return AccessDeniedView();

        //    //load settings for a chosen store scope
        //    var storeScope = _storeContext.ActiveStoreScopeConfiguration;

        //    var nivoSliderSettings = _settingService.LoadSettingList<NivoSliderSettingsList>(storeScope);

        //    return View("~/Plugins/Widgets.NivoSlider/Views/BannerPictureList.cshtml", nivoSliderSettings);
        //}

        //[HttpPost]
        //public virtual IActionResult ProductPictureUpdate(ConfigurationListModel model)
        //{

        //    if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
        //        return AccessDeniedView();

        //    //load settings for a chosen store scope
        //    var storeScope = _storeContext.ActiveStoreScopeConfiguration;

        //    var nivoSliderSettingsList = new NivoSliderSettingsList();

        //    nivoSliderSettingsList.PictureId = model.PictureId;
        //    nivoSliderSettingsList.Text = model.Text;
        //    nivoSliderSettingsList.AltText = model.AltText;
        //    nivoSliderSettingsList.Link = model.Link;

        //    /* We do not clear cache after each setting update.
        //     * This behavior can increase performance because cached settings will not be cleared 
        //     * and loaded from database after each update */
        //    _settingService.SaveSettingOverridablePerStore(nivoSliderSettingsList, x => x.PictureId, model.PictureId_OverrideForStore, storeScope, false);
        //    _settingService.SaveSettingOverridablePerStore(nivoSliderSettingsList, x => x.Text, model.Text_OverrideForStore, storeScope, false);
        //    _settingService.SaveSettingOverridablePerStore(nivoSliderSettingsList, x => x.Link, model.Link_OverrideForStore, storeScope, false);
        //    _settingService.SaveSettingOverridablePerStore(nivoSliderSettingsList, x => x.AltText, model.AltText_OverrideForStore, storeScope, false);

        //    //now clear settings cache
        //    _settingService.ClearCache();

        //    SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));
        //    return BannerPictureList();
        //}

        //[HttpPost]
        //public virtual IActionResult ProductPictureDelete(int id)
        //{
        //    if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
        //        return AccessDeniedView();

        //    //try to get a picture with the specified id
        //    var picture = _pictureService.GetPictureById(id)
        //        ?? throw new ArgumentException("No picture found with the specified id");

        //    _pictureService.DeletePicture(picture);

        //    return new NullJsonResult();
        //}

        #endregion
    }
}