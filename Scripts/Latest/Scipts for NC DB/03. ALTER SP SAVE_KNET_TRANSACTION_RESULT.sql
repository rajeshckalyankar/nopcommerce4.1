USE [NopCommerceDB]
GO
/****** Object:  StoredProcedure [dbo].[SAVE_KNET_TRANSACTION_RESULT]    Script Date: 9/9/2019 6:50:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Shekhar
-- Create date: 2019/06/26
-- Description:	This procedure will save the results after KNET transaction and also will update the payment status of order.
-- =============================================
ALTER PROCEDURE [dbo].[SAVE_KNET_TRANSACTION_RESULT] 
	-- Add the parameters for the stored procedure here
	@pPaymentID VARCHAR(100) = NULL, 
	@pTrackID VARCHAR(100),
	@pTranID VARCHAR(100) = NULL,
	@pAuth VARCHAR(100) = NULL,
	@pRef VARCHAR(100) = NULL,
	@pResult VARCHAR(100) = NULL,
	@pUDF1 VARCHAR(100) = NULL,
	@pUDF2 VARCHAR(100) = NULL,
	@pUDF3 VARCHAR(100) = NULL,
	@pUDF4 VARCHAR(100) = NULL,
	@pUDF5 VARCHAR(100) = NULL,
	@pAmt DECIMAL(18,4) = NULL,
	@pPostDate VARCHAR(30) = NULL,
	@pAvr VARCHAR(100) = NULL,
	@pAuthRespCode VARCHAR(100) = NULL,
	@pErrorCode VARCHAR(100) = NULL,
	@pErrorText VARCHAR(1000) = NULL,
	@pPaymentStatus INT,
	@pOrderStatus INT,
	@pPaymentTransactionStatus INT,
	@pSPResult INT OUTPUT
AS
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BEGIN TRAN

		-- Insert statements for procedure here
		DECLARE @CurrentUtcDateTime DATETIME2(7) = GETUTCDATE()

		INSERT INTO KNETPaymentTransaction
		(
			[PaymentID]
			,[TrackID]
			,[TranID]
			,[Auth]
			,[Ref]
			,[Result]
			,[UDF1]
			,[UDF2]
			,[UDF3]
			,[UDF4]
			,[UDF5]
			,[Amt]
			,[PostDate]
			,[Avr]
			,[AuthRespCode]
			,[ErrorCode]
			,[ErrorText]
			,[CreatedOnUtc]
		)
		VALUES
		(
			@pPaymentID
			,@pTrackID
			,@pTranID
			,@pAuth
			,@pRef
			,@pResult
			,@pUDF1
			,@pUDF2
			,@pUDF3
			,@pUDF4
			,@pUDF5
			,@pAmt
			,@pPostDate
			,@pAvr
			,@pAuthRespCode
			,@pErrorCode
			,@pErrorText
			,@CurrentUtcDateTime
		)

		UPDATE [dbo].[PaymentTransaction]
		SET [TransactionStatus] = @pPaymentTransactionStatus
		WHERE [TrackID] = @pTrackID

		UPDATE O
		SET CaptureTransactionId = @pTranID,
		CaptureTransactionResult = @pResult,
		PaymentId = @pPaymentID,
		OrderStatusId = @pOrderStatus
		,PaymentStatusId = @pPaymentStatus
		,UpdatedOnUtc = @CurrentUtcDateTime
		FROM dbo.[Order] O
		JOIN [dbo].[PaymentTransaction] PT
			ON O.Id = PT.OrderID
		WHERE PT.TrackID = @pTrackID
		
		SET @pSPResult = 0 --SUCCESS

	COMMIT TRAN

END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK TRAN
	END

	DECLARE @ErrorNumber VARCHAR(50)
	DECLARE @ErrorMessage VARCHAR(MAX)
	DECLARE @ErrorProcedure VARCHAR(100)
	DECLARE @ErrorState INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorLine INT

	SELECT @ErrorNumber = ERROR_NUMBER()
			,@ErrorMessage = ERROR_MESSAGE()
			,@ErrorProcedure = ERROR_PROCEDURE()
			,@ErrorState = ERROR_STATE()
			,@ErrorSeverity = ERROR_SEVERITY()
			,@ErrorLine = ERROR_LINE()

	DECLARE @FullLogMessage VARCHAR(MAX)
	SET @FullLogMessage = 'ErrorNumber: ' + @ErrorNumber + ' | ' + CHAR(13) + CHAR(10) +
							'ErrorMessage: ' + @ErrorMessage + ' | ' + CHAR(13) + CHAR(10) +
							'ErrorProcedure: ' + @ErrorProcedure + ' | ' + CHAR(13) + CHAR(10) +
							'ErrorState: ' + CAST(@ErrorState AS VARCHAR) + ' | ' + CHAR(13) + CHAR(10) +
							'ErrorSeverity: ' + CAST(@ErrorSeverity AS VARCHAR) + ' | ' + CHAR(13) + CHAR(10) +
							'ErrorLine: ' + CAST(@ErrorLine AS VARCHAR)

	INSERT INTO dbo.[Log]
	(
		LogLevelId,
		ShortMessage,
		FullMessage,
		CreatedOnUtc
	)
	VALUES
	(
		40,
		@ErrorMessage,
		@FullLogMessage,
		GETUTCDATE()
	)

	SET @pSPResult = -1 --EXCEPTION IN SP

END CATCH


