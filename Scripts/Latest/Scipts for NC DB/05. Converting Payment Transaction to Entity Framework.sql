EXEC sp_rename 'dbo.KNETPaymentTransaction', 'KNETPaymentTransaction_Backup';  
GO

ALTER TABLE [dbo].[KNETPaymentTransaction_Backup] DROP CONSTRAINT [FK_KNETPaymentTransaction_PaymentTransaction]
GO

EXEC sp_rename 'dbo.PaymentTransaction', 'PaymentTransaction_Backup';  
GO

ALTER TABLE [dbo].[KNETPaymentTransaction_Backup]  WITH CHECK ADD  CONSTRAINT [FK_KNETPaymentTransaction_Backup_PaymentTransaction_Backup] FOREIGN KEY([TrackID])
REFERENCES [dbo].[PaymentTransaction_Backup] ([TrackID])
GO

ALTER TABLE [dbo].[KNETPaymentTransaction_Backup] CHECK CONSTRAINT [FK_KNETPaymentTransaction_Backup_PaymentTransaction_Backup]
GO

ALTER TABLE [dbo].[PaymentTransaction_Backup] DROP CONSTRAINT [FK_PaymentTransaction_Order]
GO

ALTER TABLE [dbo].[PaymentTransaction_Backup]  WITH CHECK ADD  CONSTRAINT [FK_PaymentTransaction_Backup_Order] FOREIGN KEY([OrderID])
REFERENCES [dbo].[Order] ([Id])
GO

ALTER TABLE [dbo].[PaymentTransaction_Backup] CHECK CONSTRAINT [FK_PaymentTransaction_Backup_Order]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[KNETPaymentTransaction](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PaymentId] [varchar](100) NULL,
	[PaymentTransactionId] [int] NOT NULL,
	[TranId] [varchar](100) NULL,
	[Auth] [varchar](100) NULL,
	[Ref] [varchar](100) NULL,
	[Result] [varchar](100) NULL,
	[UDF1] [varchar](100) NULL,
	[UDF2] [varchar](100) NULL,
	[UDF3] [varchar](100) NULL,
	[UDF4] [varchar](100) NULL,
	[UDF5] [varchar](100) NULL,
	[PostDate] [varchar](30) NULL,
	[Avr] [varchar](100) NULL,
	[AuthRespCode] [varchar](100) NULL,
	[ErrorCode] [varchar](100) NULL,
	[ErrorText] [varchar](1000) NULL,
	[CreatedOnUtc] [datetime2](7) NULL,
	[Amt] [decimal](18, 4) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[PaymentTransaction](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TrackId] [varchar](100) NOT NULL,
	[OrderId] [int] NOT NULL,
	[TransactionStatus] [int] NOT NULL,
	[CreatedOnUtc] [datetime2](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[PaymentTransaction]  WITH CHECK ADD  CONSTRAINT [FK_PaymentTransaction_Order] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Order] ([Id])
GO

ALTER TABLE [dbo].[PaymentTransaction] CHECK CONSTRAINT [FK_PaymentTransaction_Order]
GO

ALTER TABLE [dbo].[KNETPaymentTransaction]  WITH CHECK ADD  CONSTRAINT [FK_KNETPaymentTransaction_PaymentTransaction] FOREIGN KEY([PaymentTransactionId])
REFERENCES [dbo].[PaymentTransaction] ([Id])
GO

ALTER TABLE [dbo].[KNETPaymentTransaction] CHECK CONSTRAINT [FK_KNETPaymentTransaction_PaymentTransaction]
GO

ALTER TABLE [dbo].[PaymentTransaction] ADD  CONSTRAINT [UK_TrackId_PaymentTransaction] UNIQUE NONCLUSTERED 
(
	[TrackId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

INSERT INTO [PaymentTransaction]
           ([TrackId]
           ,[OrderId]
           ,[TransactionStatus]
           ,[CreatedOnUtc])
 SELECT [TrackId]
		,[OrderId]
		,[TransactionStatus]
		,[CreatedOnUtc]
FROM PaymentTransaction_Backup
GO

INSERT INTO [dbo].[KNETPaymentTransaction]
           ([PaymentID]
           ,[PaymentTransactionId]
           ,[TranID]
           ,[Auth]
           ,[Ref]
           ,[Result]
           ,[UDF1]
           ,[UDF2]
           ,[UDF3]
           ,[UDF4]
           ,[UDF5]
           ,[PostDate]
           ,[Avr]
           ,[AuthRespCode]
           ,[ErrorCode]
           ,[ErrorText]
           ,[CreatedOnUtc]
           ,[Amt])
SELECT KNETPTB.[PaymentID]
           ,PT.[Id]
           ,KNETPTB.[TranID]
           ,KNETPTB.[Auth]
           ,KNETPTB.[Ref]
           ,KNETPTB.[Result]
           ,KNETPTB.[UDF1]
           ,KNETPTB.[UDF2]
           ,KNETPTB.[UDF3]
           ,KNETPTB.[UDF4]
           ,KNETPTB.[UDF5]
           ,KNETPTB.[PostDate]
           ,KNETPTB.[Avr]
           ,KNETPTB.[AuthRespCode]
           ,KNETPTB.[ErrorCode]
           ,KNETPTB.[ErrorText]
           ,KNETPTB.[CreatedOnUtc]
           ,KNETPTB.[Amt]
FROM [KNETPaymentTransaction_Backup] KNETPTB
JOIN [PaymentTransaction] PT
	ON KNETPTB.TrackID = PT.TrackId
GO