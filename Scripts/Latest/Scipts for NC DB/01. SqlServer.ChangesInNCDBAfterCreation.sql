ALTER TABLE [dbo].[Order] DROP CONSTRAINT [FK_Order_Address_BillingAddressId]
GO

ALTER TABLE [dbo].[Order] DROP CONSTRAINT [FK_Order_Address_PickupAddressId]
GO

ALTER TABLE [dbo].[Order] DROP CONSTRAINT [FK_Order_Address_ShippingAddressId]
GO

