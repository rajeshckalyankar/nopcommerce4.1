ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_Address_PickupAddressId] FOREIGN KEY([PickupAddressId])
REFERENCES [dbo].[Address] ([Id])
GO

ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_Address_PickupAddressId]
GO


