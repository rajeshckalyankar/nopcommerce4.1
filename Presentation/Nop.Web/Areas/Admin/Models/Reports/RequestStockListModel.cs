﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Reports
{
    /// <summary>
    /// Represents a customer back in stock subscriptions list model
    /// </summary>
    public partial class RequestStockListModel : BasePagedListModel<RequestStockModel>
    {
    }
}