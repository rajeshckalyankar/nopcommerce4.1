﻿using FluentValidation.Attributes;
using Nop.Web.Areas.Admin.Models.Common;
using Nop.Web.Areas.Admin.Validators.Shipping;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Models.Shipping
{
    /// <summary>
    /// Represents a warehouse model
    /// </summary>
    [Validator(typeof(WarehouseValidator))]
    public partial class WarehouseModel : BaseNopEntityModel, ILocalizedModel<WarehouseLocalizedModel>
    {
        #region Ctor

        public WarehouseModel()
        {
            this.Address = new AddressModel();
            Locales = new List<WarehouseLocalizedModel>();
        }

        #endregion

        #region Properties

        [NopResourceDisplayName("Admin.Configuration.Shipping.Warehouses.Fields.Name")]
        public string Name { get; set; }

        // For Almailem: Adding 6 new columns:- StoreCode, ShopTimings, Features, Latitude, Longitude, Governorate.

        [NopResourceDisplayName("Admin.Configuration.Shipping.Warehouses.Fields.StoreCode")]
        public string StoreCode { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Shipping.Warehouses.Fields.StoreTimings")]        
        public string StoreTimings { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Shipping.Warehouses.Fields.Features")]
        public string Features { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Shipping.Warehouses.Fields.Latitude")]
        public string Latitude { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Shipping.Warehouses.Fields.Longitude")]
        public string Longitude { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Shipping.Warehouses.Fields.Governorate")]
        public string Governorate { get; set; }


        [NopResourceDisplayName("Admin.Configuration.Shipping.Warehouses.Fields.AdminComment")]
        public string AdminComment { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Shipping.Warehouses.Fields.Address")]
        public AddressModel Address { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Shipping.Warehouses.Fields.IsWarehouse")]
        public bool IsWarehouse { get; set; }

        public IList<WarehouseLocalizedModel> Locales { get; set; }

        #endregion
    }
    public partial class WarehouseLocalizedModel : ILocalizedLocaleModel
    {
        public WarehouseLocalizedModel()
        {
            this.Address = new AddressModel();
        }
        public int LanguageId { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Shipping.Warehouses.Fields.Name")]
        public string Name { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Shipping.Warehouses.Fields.Address")]
        public AddressModel Address { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Shipping.Warehouses.Fields.Governorate")]
        public string Governorate { get; set; }
    }
}