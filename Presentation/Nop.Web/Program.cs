﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Nop.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = WebHost.CreateDefaultBuilder(args)
                .UseKestrel(options => options.AddServerHeader = false)
                .UseStartup<Startup>()
                .Build();
            //Add StandardLanguage configuration from appsettings.json
            var nopConfig = Core.Infrastructure.EngineContext.Current.Resolve<Core.Configuration.NopConfig>();
            string standardLanguage = nopConfig.StandardLanguage;
            if (standardLanguage == null)
                standardLanguage = string.Empty;
            System.AppDomain.CurrentDomain.SetData("StandardLanguage", standardLanguage);
            host.Run();
        }
    }
}
